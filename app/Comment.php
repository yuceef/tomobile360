<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function video()
    {
        return $this->belongsTo('App\Video');
    }
    public function article()
    {
        return $this->belongsTo('App\Article');
    }
    public function reactions()
    {
        return $this->hasMany('App\Reaction');
    }
}
