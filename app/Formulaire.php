<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formulaire extends Model
{
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
}
