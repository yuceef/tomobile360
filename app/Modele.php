<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modele extends Model
{
    protected $table = 'models';
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
    public function vehicles() 
    {
        return $this->hasMany('App\Vehicle','model_id')->where('status',1);
    }  
    public function historics()
    {
        return $this->hasMany('App\Historic','model_id');
    }
    public function minPrix($neuf=1) {
        return $this->hasMany('App\Vehicle', 'model_id')->where('status', 1)->where('isNeuf',$neuf)->orderBy('price','ASC');
    }   
    public function maxPrix($neuf=1) {
        return $this->hasMany('App\Vehicle', 'model_id')->where('status', 1)->where('isNeuf',$neuf)->orderBy('price','DESC');
    }   
    public function mediasList(){
        $vehicles = $this->vehicles()->get();
        $medias = array();
        foreach ($vehicles as $key => $value) {
            array_push($medias, $value->medias()->get()  );
        }
        return $medias;
    }
}
