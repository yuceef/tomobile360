<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordToken;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'is_active', 'type', 'first_name', 'last_name', 'phone_number', 'civility', 'birthdate', 'adresse', 'city', 'country', 'garage', 'postal', 'fixe', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role(){
        return ($this->type == 0)?'user':'admin';
    }

    public function hasRole($role){
        return ($this->Role() === $role)?true:false;
    }
    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }
    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
