<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category_articles', 'category_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function media()
    {
        return $this->hasOne('App\Media', 'id');
    }
    public function model()
    {
        return $this->belongsTo('App\Modele', 'model_id');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }
    
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}
