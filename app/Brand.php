<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function modeles()
    {
        return $this->hasMany('App\Modele');
    }
    
    public function videos()
    {
        return $this->hasMany('App\Video');
    }
    
    public function historics()
    {
        return $this->hasMany('App\Historic');
    }
    
    public function vehiclesList()
    {
        $modeles = $this->modeles();
        $vehicles = array();
        foreach ($modeles as $key => $value) {
            array_push($vehicles,$value->vehicles);
        }
        return $vehicles;
    }
    public function articles() 
    {
        return $this->hasMany('App\Article');
    }
}
