<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function promotions()
    {
        return $this->hasMany('App\Promotion', 'vehicle_id');
    }

    public function medias()
    {
        return $this->hasMany('App\Media', 'vehicle_id')->where('type','image');
    }

    public function mediasList()
    {
        return $this->hasMany('App\Media', 'vehicle_id')->where('type', 'image')->get();
    }

    public function promotionsList()
    {
        return $this->promotions()->get();
    }

    public function promotionLast()
    {
        return $this->promotions()
                    ->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                    ->where('end_at', '>=', gmdate("Y-m-d H:s:i"))
                    ->orderBy('end_at', 'ASC');
    }

    public function energie()
    {
        return $this->belongsTo('App\Energie');
    }

    public function modele()
    {
        return $this->belongsTo('App\Modele', 'model_id');
    }
}
