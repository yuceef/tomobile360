<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Modele;
use App\Promotion;
use App\Energie;
use App\Vehicle;
use App\Media;
use App\Article;
use App\City;
use App\Historic;
use App\Categorie;
class searchController extends Controller
{
    public function searchNeuf(Request $request){
        $input = $request->all();
        session()->flush();
        if (isset($input['energy']) and $input['energy'] != 0){
            if(is_array($input['energy'])){
                foreach ($input['energy'] as $key => $value) {
                    $input['energieName'][$key] = Energie::find((int)$value)->name;
                }
            }
            else $input['energieName'] = Energie::find((int)$input['energy'])->name;
        }
        if (isset($input['category']) and $input['category'] != 0)$input['categoryName'] = Categorie::find((int)$input['category'])->name;
        
        session()->put($input);
        $historic = new Historic;
        if($input['brand'] == 0) {
            $promotions = Vehicle::where('isNeuf', 1)
                                    ->whereHas('promotions', function ($query) {
                                        $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                            ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                    })
                                    ->with(['modele.brand', 'promotionLast'])
                                    ->limit(10)
                                    ->get();


            $brands = Brand::whereHas('modeles', function ($query) use ($input) {

                            $query->where('type', $input['type'])
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                                    })
                                    ->whereHas('vehicles', function ($query) use ($input) {
                                        $query->where('isNeuf', 1)
                                                ->where('status', 1)
                                                ->where(function ($query) use ($input) {
                                                    if ($input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['trans'])) {
                                                        if (is_array($input['trans'])) $query->whereIn('transmission', $input['trans']);
                                                        elseif ($input['trans'] != 0) $query->where('transmission', $input['trans']);
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['energy'])) {
                                                        if (is_array($input['energy'])) $query->whereIn('energie_id', $input['energy']);
                                                        elseif ($input['energy'] != 0) $query->where('energie_id', $input['energy']);
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['interiorDesign'])) {
                                                        $query->whereIn('interiorDesign', $input['interiorDesign'])
                                                            ->orWhere(function ($query) use ($input) {
                                                                if (in_array('0', $input['interiorDesign'])) {
                                                                    $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                                                }
                                                            });
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['interiorColor'])) {
                                                        $query->whereIn('interiorColor', $input['interiorColor'])
                                                            ->orWhere(function ($query) use ($input) {
                                                                if (in_array('0', $input['interiorColor'])) {
                                                                    $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                                                }
                                                            });
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['exteriorColor'])) {
                                                        $query->whereIn('exteriorColor', $input['exteriorColor'])
                                                            ->orWhere(function ($query) use ($input) {
                                                                if (in_array('0', $input['exteriorColor'])) {
                                                                    $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                                                }
                                                            });
                                                    }
                                                });
                                    });
                        });
            $brands = $brands->withCount(['modeles' => function ($query) use ($input) {
                $query->where('type', $input['type'])
                    ->where(function ($query) use ($input) {
                        if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                    })
                    ->whereHas('vehicles', function ($query) use ($input) {
                        $query->where('isNeuf', 1)
                            ->where('status', 1)
                            ->where(function ($query) use ($input) {
                                if ($input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['trans'])) {
                                    if (is_array($input['trans'])) $query->whereIn('transmission', $input['trans']);
                                    elseif ($input['trans'] != 0) $query->where('transmission', $input['trans']);
                                }
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['energy'])) {
                                    if (is_array($input['energy'])) $query->whereIn('energie_id', $input['energy']);
                                    elseif ($input['energy'] != 0) $query->where('energie_id', $input['energy']);
                                }
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['interiorDesign'])) {
                                    $query->whereIn('interiorDesign', $input['interiorDesign'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorDesign'])) {
                                                $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                            }
                                        });
                                }
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['interiorColor'])) {
                                    $query->whereIn('interiorColor', $input['interiorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorColor'])) {
                                                $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                            }
                                        });
                                }
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['exteriorColor'])) {
                                    $query->whereIn('exteriorColor', $input['exteriorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['exteriorColor'])) {
                                                $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                            }
                                        });
                                }
                            });
                    });
            }])
                ->get();               
            $articles = Article::whereIn('brand_id',$brands->pluck('id'))->limit(10)->get();
            return view('neuf.byBrand')->withInput($input)
                                        ->withBrands($brands)
                                        ->withArticles($articles)
                                        ->withPromotions($promotions);

        } 
        elseif (!isset($input['model']) or $input['model'] == 0) {
            $historic->brand_id = (int)$input['brand'];
            $historic->save();

            $brand = Brand::find((int)$input['brand']);
            $modeles = $brand->modeles()
                            ->where(function ($query) use ($input) {
                                if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                            })
                            ->where('type', $input['type'])
                            ->whereHas('vehicles', function ($query) {
                                $query->where('isNeuf', 1)
                                        ->where('status', 1);
                            });
            $modeles = $modeles->withCount(['vehicles' => function ($query) use ($input) {
                                    $query->where(function ($query) use ($input) {
                                                if ($input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                                            })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['trans'])) {
                                                        if (is_array($input['trans'])) $query->whereIn('transmission', $input['trans']);
                                                        elseif ($input['trans'] != 0) $query->where('transmission', $input['trans']);
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['energy'])) {
                                                        if (is_array($input['energy'])) $query->whereIn('energie_id', $input['energy']);
                                                        elseif ($input['energy'] != 0) $query->where('energie_id', $input['energy']);
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['interiorDesign'])) {
                                                        $query->whereIn('interiorDesign', $input['interiorDesign'])
                                                            ->orWhere(function ($query) use ($input) {
                                                                if (in_array('0', $input['interiorDesign'])) {
                                                                    $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                                                }
                                                            });
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['interiorColor'])) {
                                                        $query->whereIn('interiorColor', $input['interiorColor'])
                                                            ->orWhere(function ($query) use ($input) {
                                                                if (in_array('0', $input['interiorColor'])) {
                                                                    $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                                                }
                                                            });
                                                    }
                                                })
                                                ->where(function ($query) use ($input) {
                                                    if (isset($input['exteriorColor'])) {
                                                        $query->whereIn('exteriorColor', $input['exteriorColor'])
                                                            ->orWhere(function ($query) use ($input) {
                                                                if (in_array('0', $input['exteriorColor'])) {
                                                                    $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                                                }
                                                            });
                                                    }
                                                })
                                                ->where('isNeuf', 1)
                                                ->where('status', 1);
                                }])
                                ->with('minPrix')
                                ->get();
            $promotions = Vehicle::where('isNeuf', 1)
                                    ->whereIn('model_id', $modeles->pluck('id'))
                                    ->whereHas('promotions', function ($query) {
                                        $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                            ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                    })
                                    ->where('status', 1)
                                    ->with(['modele.brand', 'promotionLast'])
                                    ->limit(10)
                                    ->get();
            $articles = Article::whereIn('model_id',$modeles->pluck('id'))->limit(10)->get();
            return view('neuf.byModel')->withInput($input)
                ->withArticles($articles)
                ->withModeles($modeles)
                ->withBrand($brand)
                ->withPromotions($promotions);
        } 
        // By modele
        else {
            $historic->brand_id = (int)$input['brand'];
            $historic->model_id = (int)$input['model'];
            $historic->save();
            $energies = Energie::all();
            $brand = Brand::find((int)$input['brand']);
            $model = Modele::with('minPrix')->find((int)$input['model']);
            $vehicles = $model->vehicles()
                                ->where(function ($query) use ($input) {
                                    if ($input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                                })
                                ->where(function ($query) use ($input) {
                                    if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                                })
                                ->where(function ($query) use ($input) {
                                    if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                                })
                                ->where(function ($query) use ($input) {
                                    if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                                })
                                ->where(function ($query) use ($input) {
                                    if (isset($input['interiorDesign'])) {
                                        $query->whereIn('interiorDesign', $input['interiorDesign'])
                                            ->orWhere(function ($query) use ($input) {
                                                if (in_array('0', $input['interiorDesign'])) {
                                                    $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                                }
                                            });
                                    }
                                })
                                ->where(function ($query) use ($input) {
                                    if (isset($input['interiorColor'])) {
                                        $query->whereIn('interiorColor', $input['interiorColor'])
                                            ->orWhere(function ($query) use ($input) {
                                                if (in_array('0', $input['interiorColor'])) {
                                                    $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                                }
                                            });
                                    }
                                })
                                ->where(function ($query) use ($input) {
                                    if (isset($input['exteriorColor'])) {
                                        $query->whereIn('exteriorColor', $input['exteriorColor'])
                                            ->orWhere(function ($query) use ($input) {
                                                if (in_array('0', $input['exteriorColor'])) {
                                                    $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                                }
                                            });
                                    }
                                })                             
                                ->where('isNeuf', 1)
                                ->where('status', 1)
                                ->with('medias')
                                ->get();

            $promotions = Vehicle::where('isNeuf', 1)
                                    ->where('model_id', $model->id)
                                    ->whereHas('promotions', function ($query) {
                                        $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                            ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                    })
                                    ->where('status', 1)
                                    ->with(['modele.brand', 'promotionLast'])
                                    ->limit(10)
                                    ->get();

            //return $input['energy'];
            return view('neuf.byVersion')->withInput($input)
                ->withBrand($brand)
                ->withVehicles($vehicles)
                ->withEnergies($energies)
                ->withModel($model)
                ->withPromotions($promotions);
        }
    }
    public function searchNeufBrand($brand){
        $brand = Brand::where('slug', $brand)->first();
        $historic = new Historic;
        $historic->brand_id = $brand->id;
        $historic->save();
        $input = session()->all();
        $input['brand'] = $brand->id;
        session()->flush();
        session()->put($input);
        $modeles = $brand->modeles()
                            ->where(function ($query) use ($input) {
                                if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                            })
                            ->where('type', $input['type'])
                            ->whereHas('vehicles', function ($query) {
                                $query->where('isNeuf', 1)
                                    ->where('status', 1);
                            });
        $modeles = $modeles->withCount(['vehicles' => function ($query) use ($input) {
                                $query->where(function ($query) use ($input) {
                                    if ($input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                                })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['trans'])) {
                                            if (is_array($input['trans'])) $query->whereIn('transmission', $input['trans']);
                                            elseif ($input['trans'] != 0) $query->where('transmission', $input['trans']);
                                        }
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['energy'])) {
                                            if (is_array($input['energy'])) $query->whereIn('energie_id', $input['energy']);
                                            elseif ($input['energy'] != 0) $query->where('energie_id', $input['energy']);
                                        }
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['interiorDesign'])) {
                                            $query->whereIn('interiorDesign', $input['interiorDesign'])
                                                ->orWhere(function ($query) use ($input) {
                                                    if (in_array('0', $input['interiorDesign'])) {
                                                        $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                                    }
                                                });
                                        }
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['interiorColor'])) {
                                            $query->whereIn('interiorColor', $input['interiorColor'])
                                                ->orWhere(function ($query) use ($input) {
                                                    if (in_array('0', $input['interiorColor'])) {
                                                        $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                                    }
                                                });
                                        }
                                    })
                                    ->where(function ($query) use ($input) {
                                        if (isset($input['exteriorColor'])) {
                                            $query->whereIn('exteriorColor', $input['exteriorColor'])
                                                ->orWhere(function ($query) use ($input) {
                                                    if (in_array('0', $input['exteriorColor'])) {
                                                        $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                                    }
                                                });
                                        }
                                    })
                                    ->where('isNeuf', 1)
                                    ->where('status', 1);
                            }])
            ->with('minPrix')
            ->get();
        $promotions = Vehicle::where('isNeuf', 1)
                                ->whereIn('model_id', $modeles->pluck('id'))
                                ->whereHas('promotions', function ($query) {
                                    $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                        ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                })
                                ->where('status', 1)
                                ->with(['modele.brand', 'promotionLast'])
                                ->limit(10)
                                ->get();

        $articles = Article::whereIn('model_id',$modeles->pluck('id'))->limit(10)->get();
        return view('neuf.byModel')->withInput($input)
            ->withModeles($modeles)
            ->withArticles($articles)
            ->withBrand($brand)
            ->withPromotions($promotions);
    }
    public function searchNeufModel($brand, $model){
        $model = Modele::where('slug', $model)->with('minPrix')->first();
        $historic = new Historic;
        $historic->model_id = $model->id;
        $historic->save();
        $input = session()->all();
        $input['model'] = $model->id;
        session()->flush();
        session()->put($input);
        $energies = Energie::all();
        $brand = Brand::find((int)$input['brand']);
        $vehicles = $model->vehicles()
                            ->where(function ($query) use ($input) {
                                if ($input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['interiorDesign'])) {
                                    $query->whereIn('interiorDesign', $input['interiorDesign'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorDesign'])) {
                                                $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                            }
                                        });
                                }
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['interiorColor'])) {
                                    $query->whereIn('interiorColor', $input['interiorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorColor'])) {
                                                $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                            }
                                        });
                                }
                            })
                            ->where(function ($query) use ($input) {
                                if (isset($input['exteriorColor'])) {
                                    $query->whereIn('exteriorColor', $input['exteriorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['exteriorColor'])) {
                                                $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                            }
                                        });
                                }
                            })
                            ->where('isNeuf', 1)
                            ->where('status', 1)
                            ->with('medias')
                            ->get();
        $promotions = Vehicle::where('isNeuf', 1)
                                ->where('model_id',$model->id)
                                ->whereHas('promotions', function ($query) {
                                    $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                    ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                })
                                ->where('status', 1)
                                ->with(['modele.brand', 'promotionLast'])
                                ->limit(10)
                                ->get();
        return view('neuf.byVersion')->withInput($input)
            ->withBrand($brand)
            ->withVehicles($vehicles)
            ->withEnergies($energies)
            ->withModel($model)
            ->withPromotions($promotions);        
    }
    public function searchDetailed(Request $request){
        $isneuf = 1;
        $brands = Brand::all();
        $cats = Categorie::all();
        $energies = Energie::all();
        $cities = City::all();
        $modeles = Modele::all();
        return view('neuf.detailed')->withIsneuf($isneuf)
            ->withModeles($modeles)
            ->withCats($cats)
            ->withEnergies($energies)
            ->withCities($cities)
            ->withBrands($brands);
    }
    public function searchDetailedOccasion(Request $request){
        $isneuf = 0;
        $brands = Brand::all();
        $cats = Categorie::all();
        $energies = Energie::all();
        $cities = City::all();
        $modeles = Modele::all();
        return view('neuf.detailed')->withIsneuf($isneuf)
            ->withModeles($modeles)
            ->withCats($cats)
            ->withEnergies($energies)
            ->withCities($cities)
            ->withBrands($brands);
    }
    public function searchOccasion(Request $request){
        $input = $request->all();

        if(!isset($input['_token'])) $input = session()->all();
        else{
            if (isset($input['brand']) and $input['brand'] != 0) $input['brandName'] = Brand::find((int)$input['brand'])->name;
            if (isset($input['model']) and $input['model'] != 0) $input['modelName'] = Modele::find((int)$input['model'])->name;
            if (isset($input['energy']) and $input['energy'] != 0) {
                if (is_array($input['energy'])) {
                    foreach ($input['energy'] as $key => $value) {
                        $input['energieName'][$key] = Energie::find((int)$value)->name;
                    }
                } else $input['energieName'] = Energie::find((int)$input['energy'])->name;
            }
            if (isset($input['category']) and $input['category'] != 0) $input['categoryName'] = Categorie::find((int)$input['category'])->name;
            session()->flush();
            session()->put($input);
        }

        $brands = Brand::all();
        $cats = Categorie::all();
        $energies = Energie::all();
        $cities = City::all();
        $modeles = Modele::all();

        // a Refaire
        $tabModele = [];
        if (!isset($input['model']) or (isset($input['model']) and $input['model'] == 0)) {
            if ($input['brand'] == 0){
                $models = Modele::where('type', $input['type'])
                                ->where(function ($query) use ($input) {
                                    if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                                })
                                ->get();
                foreach ($models as $key => $value) {
                    $tabModele[] = $value->id;
                } 
            }
            else {
                $models = Brand::find($input['brand'])->modeles()
                                ->where('type', $input['type'])
                                ->where(function ($query) use ($input) {
                                    if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                                })
                                ->get();
                foreach ($models as $key => $value) {
                    $tabModele[] = $value->id;
                }
            }
        }

        $vehicles = Vehicle::where('isNeuf', 0)
                        ->where('status', 1)
                        ->where(function ($query) use ($input) {
                            if ($input['isneuf'] == 0 and $input['city'] != 0) $query->where('city_id', $input['city']);
                        })
                        ->where(function ($query) use ($input) {
                            if ($input['isneuf'] == 0 and isset($input['year']) and $input['year'] != 0) $query->where('year', $input['year']);
                        })
                        ->where(function ($query) use ($input, $tabModele) {
                            if (!isset($input['model']) or (isset($input['model']) and $input['model'] == 0)) $query->whereIn('model_id', $tabModele);
                            else $query->where('model_id', $input['model']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['GmaxPrice']) and $input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['trans'])) {
                                if (is_array($input['trans'])) $query->whereIn('transmission', $input['trans']);
                                elseif ($input['trans'] != 0) $query->where('transmission', $input['trans']);
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['energy'])){
                                if(is_array($input['energy'])) $query->whereIn('energie_id', $input['energy']);
                                elseif($input['energy'] != 0) $query->where('energie_id', $input['energy']);
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['interiorDesign'])){
                                $query->whereIn('interiorDesign', $input['interiorDesign'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorDesign'])) {
                                                $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                            }
                                });     
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['interiorColor'])){
                                $query->whereIn('interiorColor', $input['interiorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorColor'])) {
                                                $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                            }
                                });     
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['exteriorColor'])){
                                $query->whereIn('exteriorColor', $input['exteriorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['exteriorColor'])) {
                                                $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                            }
                                });     
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['maxMileage']) and $input['maxMileage'] != 200000) $query->where('mileage', '<=', (int)$input['maxMileage']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['minMileage']) and $input['minMileage'] != 0) $query->where('mileage', '>=', (int)$input['minMileage']);
                        })                        
                        ->where(function ($query) use ($input) {
                            if (isset($input['origine'])) $query->where('origine', (int)$input['origine']);
                        })                        
                        ->where(function ($query) use ($input) {
                            if (isset($input['nbrprop'])) $query->where('nbrprop', (int)$input['nbrprop']);
                        })                        
                        ->paginate(18);
        return view('occasion.search')->withModeles($modeles)
            ->withCats($cats)
            ->withEnergies($energies)
            ->withCities($cities)
            ->withBrands($brands)
            ->withVehicles($vehicles);
    }
}
