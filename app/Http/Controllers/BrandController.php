<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();
        return view('administration.brand.create')->withBrands($brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'logo'         => 'mimes:jpg,png,jpeg',
            'slug'          => 'required|alpha_dash|min:3|max:30|unique:brands,slug',
            'name'   => 'required|max:255',
            'description'   => 'required',
            'logo'         => 'mimes:jpg,png,jpeg'
        ));
        $brand = new Brand;
        $brand->slug = $request->slug;
        $brand->name = $request->name;
        $brand->description = $request->description;
        $brand->url = $request->url;
        if(isset($request->logo) && !empty($request->logo)){
        $fileName = $request->get('slug').'-'.date('YY_mm_dd_H_i_s').'.'.$request->logo->getClientOriginalExtension();
        $request->file('logo')->storeAs(
            'public/brands/', $fileName
        );
        $brand->logo = '/storage/articles/'.$fileName;
        }
        $brand->save();
        return redirect()->route('brand.create')->with('success', 'La marque est bien ajoutée');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }
}
