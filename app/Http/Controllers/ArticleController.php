<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Brand;
use App\Category_articles;
use App\Vehicle;
use App\Modele;
use App\Media;
use App\Comment;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;

class ArticleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allArticles()
    {
        $articles = Article::orderBy('id', 'desc')->with('category')->limit(3)->paginate(9);
        $categories = Category_articles::all();
        return view('articles.index')->withCategories($categories)
                                    ->withArticles($articles);
    }
    public function articlesByCat($id)
    {
        $categorie = Category_articles::find($id);
        $articles = Article::where('category_id', $id)->with(['category','user'])->orderBy('id', 'desc')->paginate(9);
        $categories = Category_articles::all();
        return view('articles.index')->withCategories($categories)
                                    ->withCategorie($categorie)
                                    ->withArticles($articles);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showArticle($slug)
    {
        $slug = explode('-', $slug);
        $id = array_pop($slug);
        $slug = implode("-", $slug);
        $article = Article::where('slug',$slug)->where('id', $id)->first();
        $commentaires = $article->comments()->where('status',1)->orderBy('id','desc')->with('user')->get();
        return view('articles.show')->withArticle($article)
                                   ->withCommentaires($commentaires);    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('id', 'desc')->paginate(10);
        return view('administration.article.index')->withArticles($articles);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $articles = Article::all();
        $brands = Brand::all();
        $categories = Category_articles::all();
        //$medias = Media::all();
        $models = Modele::all();
        return view('administration.article.create')->withBrands($brands)
                                     ->withCategories($categories)
                                     ->withModels($models)
                                     //->withMedias($medias)
                                     ->withArticles($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title'         => 'required|max:255',
            'slug'          => 'required|alpha_dash|min:3|max:30|unique:articles,slug',
            'category_id'   => 'required|integer',
            'brand_id'   => 'required|integer',
            'creation_date'   => 'required',
            'model_id'      => 'integer',
            'body'          => 'required',
            'image'         => 'required|mimes:jpg,png,jpeg'
        ));
        $fileName = $request->get('slug').'-'.date('YY_mm_dd_H_i_s').'.'.$request->image->getClientOriginalExtension();
        $request->file('image')->storeAs(
            'public/articles/', $fileName
        );
        $article = new Article;
        $article->user_id = Auth::user()->id;
        $article->title = $request->title;
        $article->slug = $request->slug;
        $article->category_id = $request->category_id;
        $article->creation_date = $request->creation_date;
        $article->brand_id = $request->brand_id;
        $article->model_id = $request->model_id;
        $article->body = $request->body;
        $article->cover = '/storage/articles/'.$fileName;
        $article->save();
        /*$media = new Media;
        $media->path = '/storage/articles/'.$fileName;
        $media->type = "image";
        $media->article_id = $article->id;
        $media->save();*/
        return redirect()->route('article.show', $article->id)->with('success', 'Article added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $article = Article::find($id);
        //$media = Media::where('article_id', $id)->first();
        return view('administration.article.show')->withArticle($article);
                                   //->withMedia($media);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // find the article in the database and save as a var
        $article = Article::find($id);
        $brands = Brand::all();
        $categories = Category_articles::all();
        //$medias = Media::all();
        $models = Modele::all();
         // return the view and pass in the var we previously created
        return view('administration.article.edit')->withBrands($brands)
                                     ->withCategories($categories)
                                     ->withModels($models)
                                     //->withMedias($medias)
                                     ->withArticle($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the data
        $article = Article::find($id);
        if ($request->input('slug') == $article->slug) {
            $this->validate($request, array(
                'title' => 'required|max:255',
                'category_id' => 'required|integer',
                'model_id'      => 'required|integer',
                'creation_date'   => 'required',
                'brand_id'      => 'required|integer',
                'image' => 'mimes:jpg,png,jpeg',
                'body'  => 'required'
            ));
        } else {
        $this->validate($request, array(
                'title' => 'required|max:255',
                'slug'  => 'required|alpha_dash|min:5|max:255|unique:articles,slug',
                'category_id' => 'required|integer',
                'creation_date'   => 'required',
                'model_id'      => 'required|integer',
                'brand_id'      => 'required|integer',
                'image' => 'mimes:jpg,png,jpeg',
                'body'  => 'required'
            ));
        }
        // Save the data to the database
        $article = Article::find($id);
        //$media = Media::where('article_id', $id)->first();
        $article->title = $request->input('title');
        $article->slug = $request->input('slug');
        $article->category_id = $request->input('category_id');
        $article->creation_date = $request->input('creation_date');
        $article->model_id = $request->input('model_id');
        $article->brand_id = $request->input('brand_id');
        $article->body = $request->input('body');
        
        //$media->path = null;

        if(isset($article->image) && !empty($article->image)){
        Storage::delete(''.str_replace("storage","public",$article->cover));
        $article->cover = NULL;
        $fileName = $article->slug.'-'.date('YY_mm_dd_H_i_s').'.'.$request->image->getClientOriginalExtension();
        $request->file('image')->storeAs(
            'public/articles/', $fileName
        ); 
        //$media->path = '/storage/articles/'.$fileName;
        $article->cover = '/storage/articles/'.$fileName; 
        }

        $article->save();
        //$media->save();
        // redirect with flash data to administration.article.show
        return redirect()->route('article.show', $article->id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        //$media = Media::where('article_id', $id)->first();
        Storage::delete(''.str_replace("storage","public",$article->cover));
        $article->category_id = null;
        /*$media->article_id = null;
        $media->save();*/
        $article->delete();
        //$media->delete();
        return redirect()->route('article.index');
    }
}
