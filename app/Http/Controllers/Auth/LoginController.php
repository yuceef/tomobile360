<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->is_active) {
            auth()->logout();
            if($user->verifyUser->is_used){
                return redirect('/login')->with('status', "Votre compte a été desactivé :(.");            
            }
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        if($user->type == 1) $this->redirectTo = '/administration';
        else $redirectTo = url()->previous();
        return redirect()->intended($this->redirectPath());
    }
    
}
