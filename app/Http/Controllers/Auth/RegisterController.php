<?php

namespace App\Http\Controllers\Auth;

use App\Mail\VerifyMail;
use App\User;
use App\Http\Controllers\Controller;
use App\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['user_type'] == "particulier")
        {
        return Validator::make($data, [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'first_name' => 'required|string|min:3|max:255',
            'last_name' => 'required|string|min:3|max:255',
            'birthdate' => 'required|date',
            'phone_number' => 'required|string|min:10|max:25',
            'adresse' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'user_type' => 'required|string',
        ]);
        }
        else
        {
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
                'first_name' => 'required|string|min:3|max:255',
                'last_name' => 'required|string|min:3|max:255',
                'phone_number' => 'required|string|min:10|max:25',
                'adresse' => 'required|string',
                'city' => 'required|string',
                'country' => 'required|string',
                'garage' => 'string|max:255|unique:users',
                'user_type' => 'required|string',
            ]); 
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if ($data['user_type'] == "particulier") {
        $user = User::create([
            'username' => $data['username'], //par
            'user_type' => $data['user_type'], //par
            //'garage' => $data['garage'], //pro
            'email' => $data['email'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone_number' => $data['phone_number'],
            //'postal' => $data['postal'], //pro
            //'mobile' => $data['mobile'], //pro
            'civility' => $data['civility'],
            'birthdate' => $data['birthdate'], //par
            'adresse' => $data['adresse'],
            'city' => $data['city'],
            'country' => $data['country'],
            'password' => Hash::make($data['password']),
        ]);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
        Mail::to($user->email)->send(new VerifyMail($user));
        return $user;
        }
        else 
        {
            $user = User::create([
                'user_type' => $data['user_type'], 
                'garage' => $data['garage'], //pro
                'email' => $data['email'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'phone_number' => $data['phone_number'],
                'postal' => $data['postal'], //pro
                'fixe' => $data['fixe'], //pro
                'civility' => $data['civility'],
                'adresse' => $data['adresse'],
                'city' => $data['city'],
                'country' => $data['country'],
                'password' => Hash::make($data['password']),
            ]);
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);
            Mail::to($user->email)->send(new VerifyMail($user));
            return $user;
        }
        
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser)){
            $user = $verifyUser->user;
            if(!$verifyUser->is_used){
                $verifyUser->user->is_active = true;
                $verifyUser->user->save();
                $verifyUser->is_used = true;
                $verifyUser->save();
                $user->is_active = true;
                $user->save();
                $status = "Your e-mail is verified. You can now login.";
            }
            elseif(! $user->is_active){
                $status = "Votre compte a été desactiver.";
            }
            else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/register')->with('warning', "Sorry your email cannot be identified.");
        }
 
        return redirect('/login')->with('status', $status);
    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }
}
