<?php

namespace App\Http\Controllers;

use App\Modele;
use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModeleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $models = Modele::all();
        $brands = Brand::all();
        return view('administration.model.create')->withBrands($brands)
                                                  ->withModels($models);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'type'         => 'required',
            'slug'          => 'required|alpha_dash|min:3|max:30|unique:models,slug',
            'brand_id'   => 'required|integer',
            'name'   => 'required|max:255',
            'image'         => 'mimes:jpg,png,jpeg'
        ));
        
        $model = new Modele;
        $model->type = $request->type;
        $model->slug = $request->slug;
        $model->brand_id = $request->brand_id;
        $model->name = $request->name;
        $model->description = $request->description;
        if(isset($request->image) && !empty($request->image)){
        $fileName = $request->get('slug').'-'.date('YY_mm_dd_H_i_s').'.'.$request->image->getClientOriginalExtension();
        $request->file('image')->storeAs(
            'public/models/', $fileName
        );
        $model->image = '/storage/models/'.$fileName;
        }
        $model->save();
        return redirect()->route('model.create')->with('success', 'Le modèle est bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function show(Modele $modele)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function edit(Modele $modele)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modele $modele)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modele $medele)
    {
        //
    }
}
