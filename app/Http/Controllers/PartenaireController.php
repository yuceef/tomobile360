<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partenaire;
class PartenaireController extends Controller
{
    public function index(){
        $partenaires = Partenaire::where('status',2)->get();
        return view('partenaires.index')->withPartenaires($partenaires);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partenaires.devenir');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'nom' => 'required|min:6',
            'prenom' => 'required|min:6',
            'societe' => 'required',
            'phone' => 'required|min:10',
            'email' => 'email',
            'city' => 'required',
        ));
        $partenaire = new Partenaire;
        $partenaire->nom = $request->nom;
        $partenaire->prenom = $request->prenom;
        $partenaire->societe = $request->societe;
        $partenaire->phone = $request->phone;
        $partenaire->city = $request->city;
        if(isset($request->link)) $partenaire->link = $request->link;
        if(isset($request->email)) $partenaire->email = $request->email;
        $partenaire->save();
        session()->put('m', 'Votre demande a été envoyée.');
        return view('partenaires.devenir');
    }
}
