<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category_articles;

class CategorieArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = Category_articles::all();
        return view('administration.categorie.index')->withCats($cats);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.categorie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'color'          => 'required',
            'name'   => 'required|max:255',
            'description'   => 'required',
        ));
        $cat = new Category_articles;
        $cat->name = $request->name;
        $cat->color = $request->color;
        $cat->description = $request->description;
        $cat->save();
        session()->put('m', 'Catégorie a été ajoutée.');
        return redirect('/administration/categorie/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category_articles::find($id);
        return view('administration.categorie.edit')->withCat($cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'color'          => 'required',
            'name'   => 'required|max:255',
            'description'   => 'required',
        ));
        $cat = Category_articles::find($id);
        $cat->name = $request->name;
        $cat->color = $request->color;
        $cat->description = $request->description;
        $cat->save();
        session()->put('m', 'Catégorie a été modifiée.');
        return redirect('/administration/categorie/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category_articles::find($id);
        $cat->delete();
        return "true";
    }
}
