<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promotion;
use App\Brand;
use App\Energie;
use App\Modele;
use App\Vehicle;
use App\Video;
use App\Article;

class NeufController extends Controller
{
    public function index(){
        session()->flush();
        $promotions = Vehicle::where('isNeuf', 1)
                                ->whereHas('promotions', function ($query) {
                                    $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                        ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                })
                                ->with(['modele.brand', 'promotionLast'])
                                ->limit(10)
                                ->get();
        $brands = Brand::whereHas('modeles', function ($query) {
                            $query->whereHas('vehicles', function ($query) {
                                $query->where('isNeuf', 1)
                                        ->where('status', 1);
                            });
                        });
        $brands = $brands->withCount(['modeles' => function ($query) {
                                $query->whereHas('vehicles', function ($query) {
                                    $query->where('isNeuf', 1)
                                        ->where('status', 1);
                                });
                            }])
                            ->get();
        $articles = Article::whereIn('brand_id',$brands->pluck('id'))->limit(10)->get();
        return view('neuf.byBrand')
            ->withBrands($brands)
            ->withArticles($articles)
            ->withPromotions($promotions);    
    }
    
    public function brand($slugBrand){
        session()->flush();
        $brand = Brand::where("slug", $slugBrand)->first();
        $modeles = $brand->modeles()
                            ->whereHas('vehicles', function ($query) {
                                $query->where('isNeuf', 1)
                                        ->where('status', 1);
                            });
        $modeles = $modeles->withCount(['vehicles' => function ($query) {
                                $query->where('isNeuf', 1)->where('status', 1);
                            }])
                            ->with('minPrix')
                            ->get();
        $promotions = Vehicle::where('isNeuf', 1)
                                ->whereIn('model_id',$modeles->pluck('id'))
                                ->whereHas('promotions', function ($query) {
                                    $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                    ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                })
                                ->where('status', 1)
                                ->with(['modele.brand', 'promotionLast'])
                                ->limit(10)
                                ->get();
        $articles = Article::whereIn('model_id',$modeles->pluck('id'))->limit(10)->get();

        return view('neuf.byModel')
            ->withBrand($brand)
            ->withArticles($articles)
            ->withModeles($modeles)
            ->withPromotions($promotions);  
    }

    public function model($slugBrand, $slugModel){
        session()->flush();
        $input['energy'] = 0;
        $energies = Energie::all();
        $brand = Brand::where("slug", $slugBrand)->first();
        $model = Modele::where("slug", $slugModel)->with('minPrix')->first();
        $vehicles = $model->vehicles()
                            ->where('isNeuf', 1)
                            ->where('status', 1)
                            ->with('medias')
                            ->get();
        $articles = Article::where('model_id',$model->id)->limit(10)->get();
        $promotions = Vehicle::where('isNeuf', 1)
                                ->where('model_id',$model->id)
                                ->whereHas('promotions', function ($query) {
                                    $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                                    ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                                })
                                ->where('status', 1)
                                ->with(['modele.brand', 'promotionLast'])
                                ->limit(10)
                                ->get();
        return view('neuf.byVersion')->withInput($input)
            ->withBrand($brand)
            ->withArticles($articles)
            ->withVehicles($vehicles)
            ->withEnergies($energies)
            ->withModel($model)
            ->withPromotions($promotions);
    }

    public function version($slugBrand, $slugModel, $slugVersion){
        $brand = Brand::where("slug", $slugBrand)->first();
        $model = Modele::where("slug", $slugModel)->first();
        $version = Vehicle::where("slug", $slugVersion)->with('energie')->first();
        $promotions = Vehicle::where('isNeuf', 1)
                        ->where('model_id',$model->id)
                        ->whereHas('promotions', function ($query) {
                            $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                            ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                        })
                        ->where('status', 1)
                        ->with(['modele.brand', 'promotionLast'])
                        ->limit(10)
                        ->get();
        $rcPricemin = $version->price * 0.75;
        $rcPricemax = $version->price * 1.25;
        $recommendations = Vehicle::whereBetween('price',[$rcPricemin, $rcPricemax])
                                    ->where('isNeuf', 1)
                                    ->where('status', 1)
                                    ->with('modele.brand')
                                    ->get();
        // refaire
        $videos = $brand->videos()->get();
        $articles = Article::where('model_id',$model->id)->limit(10)->get();        
        return view('neuf.version')->withBrand($brand)
                                    ->withModel($model)
                                    ->withPromotions($promotions)
                                    ->withRecommendations($recommendations)
                                    ->withVersion($version)
                                    ->withArticles($articles)
                                    ->withVideos($videos)
                                    ;
    }

    public function comparateur(Request $request){
        $version1 = Vehicle::find($request->version1);
        $version2 = Vehicle::find($request->version2);
        return view('neuf.comparateur')->withVersion1($version1)->withVersion2($version2);
    }
}
