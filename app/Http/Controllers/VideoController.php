<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use App\Brand;
use App\Categorie;
class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allVideos()
    {
        $videos = Video::paginate(30);
        $brands = Brand::all();
        return view('videos.index')
        ->withVideos($videos)
        ->withBrands($brands);
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function videosBySelect($brandId)
    {
        $brandVideo = Brand::find($brandId);
        if($brandId == "all") return redirect('/videos');
        if(isset($brandVideo)){
            $videos = $brandVideo->videos()
                                ->paginate(30);
        }
        $brands = Brand::all();
        return view('videos.index')
                ->withBrandVideo($brandVideo)
                ->withVideos($videos)
                ->withBrands($brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);
        $videos = Video::limit(3)->inRandomOrder()->get();
        $commentaires = $video->comments()->where('status', 1)->orderBy('id', 'desc')->with('user')->get();
        return view('videos.show')->withVideo($video)->withVideos($videos)->withCommentaires($commentaires);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
    }
}
