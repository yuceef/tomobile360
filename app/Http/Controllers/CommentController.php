<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Reaction;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'exprension', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('index', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::where('status',0)->with(['user','article','video'])->paginate(20);
        //return $comments;
        return view('administration.comment.index')->withComments($comments);
    }
    public function cmntSupp()
    {
        $comments = Comment::onlyTrashed()->where('status',0)->with(['user','article','video'])->paginate(20);
        //return $comments;
        return view('administration.comment.index')->withComments($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'comment' => 'required|min:10',
        ));
        $cmnt = new Comment;
        $cmnt->status = 0;
        $cmnt->body = $request->comment;
        $cmnt->user_id = $request->user()->id;
        if(isset($request->articleId)) $cmnt->article_id = $request->articleId;
        if (isset($request->videoId)) $cmnt->video_id = $request->videoId;
        $cmnt->save();
        return $cmnt;
    }
    public function exprension($exp,$id, Request $request)
    {
        $cmnt = Comment::find($id);
        $reaction = $cmnt->reactions->where('user_id', $request->user()->id)->first();
        if($reaction == null){
            $reaction = new Reaction;
            $reaction->user_id = $request->user()->id;
            $reaction->type = $exp;
            $reaction->comment_id = $id;
            if ($exp == 'like')
                $cmnt->like += 1;
            else $cmnt->dislike += 1;
        }
        else {
            if($reaction->type == $exp){
                if ($exp == 'like')
                    $cmnt->like -= 1;
                else $cmnt->dislike -= 1;
                $reaction->delete();
                $cmnt->save();
                return $cmnt;
            }
            elseif($exp == 'like'){
                $cmnt->like += 1;
                $cmnt->dislike -= 1;
            }
            elseif($exp == 'dislike'){
                $cmnt->like -= 1;
                $cmnt->dislike += 1;
            }
            $reaction->type = $exp;
        }
        $reaction->save();
        $cmnt->save();
        return $cmnt;
    }

    public function accepter($id)
    {
        $article = Comment::find($id);
        $article->status = 1;
        $article->save();
        return $article;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Comment::find($id);
        $article->delete();
        return "true";
    }
}
