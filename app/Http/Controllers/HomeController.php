<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use App\Modele;
use App\Brand;
use App\Categorie;
use App\Historic;
use App\Promotion;
use App\Energie;
use App\City;
use App\Video;
use App\Article;
use App\Partenaire;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        $cats = Categorie::all();
        $brands = Brand::all();
        $models = Modele::where('type', 'voiture')
                        ->whereHas('vehicles')
                        ->get();
        $videos = Video::orderBy('id', 'DESC')
                        ->limit(10)
                        ->get();
        $promotions = Vehicle::where('isNeuf', 1)
                        ->whereHas('promotions', function ($query) {
                            $query->where('start_at', '<=', gmdate("Y-m-d H:s:i"))
                            ->where('end_at', '>=', gmdate("Y-m-d H:s:i"));
                        })
                        ->with(['modele.brand', 'promotionLast'])
                        ->limit(10)
                        ->get();
        $historicsbrd = Brand::withCount('historics')->orderBy('historics_count','desc')->limit(5)->get();
        $historicsmdl = Modele::withCount('historics')->with('brand')->orderBy('historics_count','desc')->limit(5)->get();
        $partenaires = Partenaire::where('status', 2)->get();
        $energies = Energie::all();
        /*foreach ($promotions as $key => $value) {
            $promotions[$key]->vehicle = $value->vehicle;
        }*/
        $articles = Article::orderBy('id','desc')->with(['category', 'user'])->limit(3)->get();
        return view('welcome')
            ->withModels($models)
            ->withCats($cats)
            ->withVideos($videos)
            ->withEnergies($energies)
            ->withBrands($brands)
            ->withCities($cities)
            ->withArticles($articles)
            ->withHistoricsbrd($historicsbrd)
            ->withHistoricsmdl($historicsmdl)
            ->withPartenaires($partenaires)
            ->withPromotions($promotions);
    }
}
