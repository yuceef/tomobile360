<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('admin')->only('index', 'create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('administration.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email'     =>  'required',
            'password'     =>  'required',
            'first_name'     =>  'required',
            'last_name'     =>  'required',
            'phone_number'     =>  'required',
            'type'     =>  'required'
        ]);
        $user = new User([
            'username'    =>  $request->get('username'),
            'email'     =>  $request->get('email'),
            'password'     =>  $request->get('password'),
            'first_name'     =>  $request->get('first_name'),
            'last_name'     =>  $request->get('last_name'),
            'phone_number'     =>  $request->get('phone_number'),
            'garage'     =>  $request->get('garage'),
            'postal'     =>  $request->get('postal'),
            'mobile'     =>  $request->get('mobile'),
            'par_pro'     =>  $request->get('par_pro'),
            'type'     =>  $request->get('type')
        ]);
        $user->save();
        return redirect()->route('administration.create')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('administration.edit', compact('user', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->user_type == "particulier")
        {
        $this->validate($request, [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'first_name' => 'required|string|min:3|max:255',
            'last_name' => 'required|string|min:3|max:255',
            'birthdate' => 'required|date',
            'phone_number' => 'required|string|min:10|max:25',
            'adresse' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'user_type' => 'required|string',
            'rating'    =>  'required',
            'type'    =>  'required',
        ]);
        }
        else 
        {
            $this->validate($request, [
                'email' => 'required|string|email|max:255|unique:users',
                'first_name' => 'required|string|min:3|max:255',
                'last_name' => 'required|string|min:3|max:255',
                'phone_number' => 'required|string|min:10|max:25',
                'adresse' => 'required|string',
                'city' => 'required|string',
                'country' => 'required|string',
                'postal' => 'string',
                'garage' => 'string|max:255|unique:users',
                'fixe' => 'string|min:10|max:25',
                'user_type' => 'string',
                'rating'    =>  'required',
                'type'    =>  'required',
            ]);
        }
        $user = User::find($id);
        $user->user_type = $request->get('user_type');
        $user->username = $request->get('username');
        $user->garage = $request->get('garage');
        $user->email = $request->get('email');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->birthdate = $request->get('birthdate');
        $user->phone_number = $request->get('phone_number');
        $user->city = $request->get('city');
        $user->country = $request->get('country');
        $user->postal = $request->get('postal');
        $user->fixe = $request->get('fixe');
        $user->rating = $request->get('rating');
        $user->type = $request->get('type');
        $user->save();
        return redirect()->route('administration.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('administration.index')->with('success', 'Data Deleted');
    }

    public function active($id)
    {
        $user = User::find($id);
        if($user->is_active==0)
        {
        $user->update(['is_active'=>1]);
        }
        else
        {
            $user->update(['is_active'=>0]);
        }
        return redirect()->route('administration.index');
    }
}

