<?php

namespace App\Http\Controllers;

use App\Mail\EtreConnecteMail;
use Illuminate\Http\Request;
use App\Brand;
use App\Vehicle;
use App\Modele;
use App\Formulaire;
use Mail;

class ApiController extends Controller
{
    public function getNbOfVehicles(Request $request){
        $input = $request->all();
        $nb = 0;
        $tabModele = [];
        if (!isset($input['model']) or (isset($input['model']) and $input['model'] == 0)) {
            if ($input['brand'] == 0){
                $models = Modele::where('type', $input['type'])
                                ->where(function ($query) use ($input) {
                                    if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                                })
                                ->get();
                foreach ($models as $key => $value) {
                    $tabModele[] = $value->id;
                } 
            }
            else {
                $models = Brand::find($input['brand'])->modeles()
                                ->where('type', $input['type'])
                                ->where(function ($query) use ($input) {
                                    if (isset($input['category']) and $input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                                })
                                ->get();
                foreach ($models as $key => $value) {
                    $tabModele[] = $value->id;
                }
            }
        }
        $nb = Vehicle::where('isNeuf', $input['isneuf'])
                        ->where('status', 1)
                        ->where(function ($query) use ($input) {
                            if ($input['isneuf'] == 0 and $input['city'] != 0) $query->where('city_id', $input['city']);
                        })
                        ->where(function ($query) use ($input) {
                            if ($input['isneuf'] == 0 and isset($input['year']) and $input['year'] != 0) $query->where('year', $input['year']);
                        })
                        ->where(function ($query) use ($input, $tabModele) {
                            if (!isset($input['model']) or (isset($input['model']) and $input['model'] == 0)) $query->whereIn('model_id', $tabModele);
                            else $query->where('model_id', $input['model']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['GmaxPrice']) and $input['GmaxPrice'] != 9000000) $query->where('price', '<=', (int)$input['GmaxPrice']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['GminPrice']) and $input['GminPrice'] != 0) $query->where('price', '>=', (int)$input['GminPrice']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['trans'])) {
                                if (is_array($input['trans'])) $query->whereIn('transmission', $input['trans']);
                                elseif($input['trans'] != '0') $query->where('transmission', $input['trans']);
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['energy'])){
                                if(is_array($input['energy'])) $query->whereIn('energie_id', $input['energy']);
                                elseif($input['energy'] != 0) $query->where('energie_id', $input['energy']);
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['maxPuissance']) and $input['maxPuissance'] != 20) $query->where('fiscalPower', '<=', (int)$input['maxPuissance']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['minPuissance']) and $input['minPuissance'] != 0) $query->where('fiscalPower', '>=', (int)$input['minPuissance']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['interiorDesign'])){
                                $query->whereIn('interiorDesign', $input['interiorDesign'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorDesign'])) {
                                                $query->whereNotIn('interiorDesign', ['Cuir', 'Tissu', 'Velours']);
                                            }
                                });     
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['interiorColor'])){
                                $query->whereIn('interiorColor', $input['interiorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['interiorColor'])) {
                                                $query->whereNotIn('interiorColor', ['Noir', 'Gris', 'Beige']);
                                            }
                                });     
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['exteriorColor'])){
                                $query->whereIn('exteriorColor', $input['exteriorColor'])
                                        ->orWhere(function ($query) use ($input) {
                                            if (in_array('0', $input['exteriorColor'])) {
                                                $query->whereNotIn('exteriorColor', ['Noir', 'Gris', 'Bleu']);
                                            }
                                });     
                            }
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['maxMileage']) and $input['maxMileage'] != 200000) $query->where('mileage', '<=', (int)$input['maxMileage']);
                        })
                        ->where(function ($query) use ($input) {
                            if (isset($input['minMileage']) and $input['minMileage'] != 0) $query->where('mileage', '>=', (int)$input['minMileage']);
                        })                        
                        ->where(function ($query) use ($input) {
                            if (isset($input['origine'])) $query->where('origine', (int)$input['origine']);
                        })                        
                        ->where(function ($query) use ($input) {
                            if (isset($input['nbrprop'])) $query->where('nbrprop', (int)$input['nbrprop']);
                        })                        
                        ->count();
       return $nb;
    }
    public function estimVehicles(Request $request){
        $input = $request->all();
        $moy = Vehicle::where('model_id',$input['model'])->where('isNeuf',0)->where('year',$input['year'])->avg('price');
        return $moy;
    }
    public function getcomparateurVehicles($model,Request $request){
        $input = $request->all();
        $vehicles = Vehicle::where('model_id',$model)->where('isNeuf',1)->where('status',1)->get();
        return $vehicles;
    }
    public function brandByType($type,$neuf){
        $brands = [];
        $models = Modele::where('type', $type)
                        ->whereHas('vehicles', function ($query) use($neuf) {
                            $query->where('isNeuf', $neuf);
                        })
                        ->get();
        $brands = Brand::whereHas('modeles', function ($query) use($type, $neuf) {
                                $query->where('type', $type)
                                        ->whereHas('vehicles', function ($query) use($neuf) {
                                            $query->where('isNeuf', $neuf);
                                        });
                            })->get();
        $response = [
            'brands' => $brands,
            'models' => $models,
        ];
        return response()->json($response, 200);
    }
    public function formulaires(Request $request){
        $input = $request->all();
        $etrecnct = new Formulaire;
        $etrecnct->vehicle_id = $input['vehicleId'];
        $etrecnct->firstname = $input['prenom'];
        $etrecnct->lastname = $input['nom'];
        $etrecnct->phone = $input['mobile'];
        $etrecnct->email = $input['email'];
        $etrecnct->type = $input['type'];
        $etrecnct->save();
        $input['vehicle'] = vVehicle::find((int)$input['vehicleId'])->name;
        Mail::to($input['email'])->send(new EtreConnecteMail($input));
        return true;
    }
/*
    public function countGuide(Request $request){
        $input = $request->all();
        $nb = 0;
        $tabModele = [];
        $models = null;
        if ($input['brand'] == 0){
            $models = Modele::where('type', "voiture")
                ->where(function ($query) use ($input) {
                    if ($input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                })
                ->get();
        }
        else{
            $brand = Brand::find($input['brand']);
            $models = $brand->modeles()->where('type', "voiture")
                                    ->where(function ($query) use ($input) {
                                        if ($input['category'] != 0) $query->where('categorie_id', (int)$input['category']);
                                    })
                                    ->get();
        }
        foreach ($models as $key => $value) {
            $tabModele[] = $value->id;
        }
        $nb = Vehicle::where('isNeuf', 1)
            ->where(function ($query) use ($tabModele) {
                $query->whereIn('model_id', $tabModele);
            })
            ->where('price', '<=', (int)$input['GmaxPrice'])
            ->where('price', '>=', (int)$input['GminPrice'])
            ->where(function ($query) use ($input) {
                if ($input['energy'] != 'all') $query->where('energie_id', $input['energy']);
            })
            ->where(function ($query) use ($input) {
                if ($input['trans'] != 'all') $query->where('transmission', $input['trans']);
            })
            ->where('status',1)
            ->count();
            return $nb;
    }
*/
}
