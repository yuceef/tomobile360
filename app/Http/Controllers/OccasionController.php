<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;
use App\City;
use App\Brand;
use App\Energie;
use App\Modele;
use App\Vehicle;
use App\Video;
use App\Article;

class OccasionController extends Controller
{
    public function index(Request $request){
        session()->flush();

        $brands = Brand::all();
        $cats = Categorie::all();
        $energies = Energie::all();
        $cities = City::all();
        $modeles = Modele::all();

        $vehicles = Vehicle::where('isNeuf', 0)
                        ->where('status', 1)
                        ->paginate(18);

        return view('occasion.search')->withModeles($modeles)
            ->withCats($cats)
            ->withEnergies($energies)
            ->withCities($cities)
            ->withBrands($brands)
            ->withVehicles($vehicles);

    }
    public function show($id,Request $request){
        
    }
}
