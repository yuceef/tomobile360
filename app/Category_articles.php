<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_articles extends Model
{
    public function articles() 
    {
        return $this->hasMany('App\Article','id');
    }
}
