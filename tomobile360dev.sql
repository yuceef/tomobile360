/*
 Navicat Premium Data Transfer

 Source Server         : tomobile360dev
 Source Server Type    : MySQL
 Source Server Version : 100212
 Source Host           : evead.clcem4lldyri.us-east-1.rds.amazonaws.com:3306
 Source Schema         : tomobile360dev

 Target Server Type    : MySQL
 Target Server Version : 100212
 File Encoding         : 65001

 Date: 06/08/2018 10:15:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation_date` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES (1, 'Jusqu\'à 600 euros pour abandonner sa voiture de société, vraiment intéressant ?', '<figure class=\"image\"><img src=\"http://www.levif.be/medias/3619/1852931.jpg\" alt=\"Jusqu\'&agrave; 600 euros pour abandonner sa voiture de soci&eacute;t&eacute;, vraiment int&eacute;ressant ?\" width=\"620\" height=\"413\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n<div class=\"row\">\r\n<div class=\"article-body span8\" data-gtm-vis-recent-on-screen-6039843_102=\"1887\" data-gtm-vis-first-on-screen-6039843_102=\"1887\" data-gtm-vis-total-visible-time-6039843_102=\"100\" data-gtm-vis-has-fired-6039843_102=\"1\">\r\n<div id=\"article-body\">\r\n<p>Dans les propositions de mobilit&eacute; &eacute;mises par Charles Michel, on trouve non seulement un abonnement aux transports en commun ou un v&eacute;lo d\'entreprise en remplacement de la voiture de soci&eacute;t&eacute;, mais aussi un salaire net plus &eacute;lev&eacute; si l\'employ&eacute; d&eacute;cide de se passer de son v&eacute;hicule. Le montant allou&eacute; d&eacute;pendra du mod&egrave;le du v&eacute;hicule et des conditions de leasing. Quant aux travailleurs qui n\'ont pas de voiture de soci&eacute;t&eacute;, ils sont les perdants de cette nouvelle politique mise sur la table car ils ne recevront pas un centime de plus ni d\'autres avantages.</p>\r\n<p>Les travailleurs qui disposent d\'une voiture de soci&eacute;t&eacute; en leasing - ils sont 400 000 en Belgique - seront mis face &agrave; un choix en concertation avec leur employeur : la garder ou recevoir de 300 &agrave; 600 euros nets suppl&eacute;mentaires (selon le mod&egrave;le) sur leur salaire mensuel selon des chiffres avanc&eacute;s par&nbsp;<em><a href=\"http://www.standaard.be/cnt/dmf20161023_02535372\" target=\"_blank\" rel=\"noopener\">De Standaard</a></em>. Ce montant qui varie du simple au double reste encore assez flou. La r&egrave;gle: plus la voiture co&ucirc;te cher, plus le suppl&eacute;ment net par mois sera &eacute;lev&eacute;.</p>\r\n<div id=\"INTERSCROLLER\" class=\"Ad INTERSCROLLER\">&nbsp;</div>\r\n<p>Le vice-premier ministre Didier Reynders a &eacute;t&eacute; le premier - et le seul - &agrave; parler d\'un montant de 450 euros. Or, le budget moyen de leasing auto pour les voitures de soci&eacute;t&eacute; en Belgique tourne autour de 560 euros HTVA, selon des chiffres fournis par Renta, la F&eacute;d&eacute;ration belge des loueurs de voitures.&nbsp;<em>De Standaard&nbsp;</em>avance, de son c&ocirc;t&eacute;, le montant de 300 euros nets pour une Renault M&eacute;gane et de presque le double pour une Audi A6.Ce montant devrait &ecirc;tre moins impos&eacute;, car il ne fera pas partie du salaire brut. L\'employ&eacute; devrait donc r&eacute;cup&eacute;rer les co&ucirc;ts de leasing amput&eacute; de l\'avantage fiscal.</p>\r\n<h2>Budg&eacute;tairement neutre</h2>\r\n<p>Dans cette proposition, il est important que le budget mobilit&eacute; soit une alternative budg&eacute;tairement neutre aux voitures de soci&eacute;t&eacute;. Et c\'est l&agrave; que le b&acirc;t blesse, communique la soci&eacute;t&eacute; europ&eacute;enne de services de ressources humaines SD Worx dans les quotidiens flamands&nbsp;<em>De Standaard</em>&nbsp;et&nbsp;<em>De Tijd.</em></p>\r\n<p>\"<em>Une telle augmentation de salaire n\'est possible que si l\'employ&eacute; ne doit pas payer de cotisations sociales ou d\'imp&ocirc;ts sur le salaire suppl&eacute;mentaire</em>\", ressort-il chez SD Worx<em>. \"Cela semble peu vraisemblable, car cela co&ucirc;terait &eacute;norm&eacute;ment d\'argent au fisc et &agrave; la s&eacute;curit&eacute; sociale.\"</em></p>\r\n<p>Si le salaire suppl&eacute;mentaire est impos&eacute; aux taux normaux, l\'employ&eacute; conservera beaucoup moins que les 450 euros avanc&eacute;s par Reynders. Le calcul effectu&eacute; par la soci&eacute;t&eacute; de consultance en ressources humaines avance qu\'une Golf VW co&ucirc;te mensuellement 373 euros (taxes et imp&ocirc;ts inclus) &agrave; une soci&eacute;t&eacute;. Quand ces frais sont convertis en salaire brut, l\'employ&eacute; ne re&ccedil;oit, au final, que 165 euros suppl&eacute;mentaires en net par mois. Selon SD Worx, c\'est beaucoup trop peu pour persuader un employ&eacute; de ne pas opter pour une voiture de soci&eacute;t&eacute;.</p>\r\n<p>L\'Open Vld avait d&eacute;j&agrave; mis au point une proposition en mars concernant la mani&egrave;re dont le salaire suppl&eacute;mentaire devrait &ecirc;tre impos&eacute; pour &eacute;viter que l\'avantage ne soit r&eacute;duit &agrave; peau de chagrin. Selon le parlementaire Egbert Lachaert, ce salaire suppl&eacute;mentaire devrait figurer sous un code sp&eacute;cifique sur la fiche salariale et &ecirc;tre impos&eacute; de la m&ecirc;me mani&egrave;re que le montant du leasing de la voiture de soci&eacute;t&eacute;, donc via un avantage de toute nature. \"<em>Ainsi, cela ne co&ucirc;tera pas un euro de plus &agrave; l\'employeur et l\'employ&eacute; ne recevra pas un euro de moins. Car en travaillant avec un code de salaire net s&eacute;par&eacute;, vous ne risquerez pas de tomber dans une tranche d\'imposition plus &eacute;lev&eacute;e\",</em>&nbsp;ressort-il. Dans&nbsp;<em>De Tijd</em>, Lachaert fait savoir que sa proposition sert de base pour le d&eacute;bat concernant le budget mobilit&eacute;.</p>\r\n<h2>D&eacute;savantages</h2>\r\n<p>Ce syst&egrave;me pr&eacute;sente aussi plusieurs d&eacute;savantages, comme l\'explique&nbsp;<em><a href=\"http://www.demorgen.be/economie/bedrijfswagen-inruilen-tegen-cash-tot-600-euro-extra-nettoloon-b8dab672/\">De Morgen</a>.</em>&nbsp;Les adversaires de cette mesure craignent qu\'elle ne modifie pas les habitudes des navetteurs belges. La FEBIAC, la F&eacute;d&eacute;ration Belge de l\'Automobile, craint que les travailleurs n\'utilisent ce budget de mobilit&eacute; en remplacement de leur voiture de soci&eacute;t&eacute; pour acqu&eacute;rir un mod&egrave;le moins cher d\'occasion, mais aussi plus polluant.</p>\r\n<p>Autre facteur d&eacute;terminant dans ce choix : le facteur \"carburant\". La plupart du temps, les employ&eacute;s qui disposent d\'une voiture jouissent aussi d\'une carte essence, parfois sans restriction selon leur situation (ind&eacute;pendant, commercial,...). Un avantage financi&egrave;rement int&eacute;ressant pour les trajets quotidiens domicile-bureau, mais aussi pour les escapades de week-end et les grandes vacances.</p>\r\n<p>Le kilom&eacute;trage moyen annuel pour un v&eacute;hicule de soci&eacute;t&eacute; est de 28.500 kilom&egrave;tres, selon la FEBIAC qui d&eacute;taille, sur le site de l\'<em>Echo</em>, qu\'en se basant sur une consommation moyenne de 7 litres/100 km, on arrive &agrave; une consommation d\'environ 2.000 litres par voiture et par an. &Agrave; 1,2 euro le litre, cela repr&eacute;sente une d&eacute;pense mensuelle d\'environ 200 euros, un montant non n&eacute;gligeable &agrave; prendre en compte.</p>\r\n<p>En outre, pour une voiture priv&eacute;e, il faut aussi faire peser dans la balance les entretiens, les r&eacute;parations, les pneus hiver ou encore, les co&ucirc;ts de r&eacute;paration en cas d\'accident...des sommes qui peuvent vite grimper.</p>\r\n<p>Un autre b&eacute;mol point&eacute; du doigt est le fait que les employ&eacute;s qui ne disposent pas de voitures de soci&eacute;t&eacute; ne profiteront pas de cette nouvelle politique de mobilit&eacute;. Ils ne pourront jouir ni d\'un abonnement avantageux aux transports en commun ni d\'un salaire plus &eacute;lev&eacute; pour modifier leur habitudes de mobilit&eacute;.<em>\"Il faut plus de temps n&eacute;cessaire pour offrir &agrave; tous un budget mobilit&eacute;\"</em>&nbsp;est d\'avis le parlementaire Lachaert.</p>\r\n<p>Selon L\'<em>Echo</em>, les services publics concern&eacute;s doivent se mettre d\'accord sur les modalit&eacute;s de la mesure pour avril 2017, elle ne devrait pas &ecirc;tre mise en oeuvre avant l\'automne.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'Jusqua-600-euros-pour-abandonner', '/storage/articles/Jusqua-600-euros-pour-abandonner-20182018_0808_0303_09_41_54.jpg', '27-07-2018', '2018-08-03 09:41:54', '2018-08-03 09:41:54', 1, 7, 1);
INSERT INTO `articles` VALUES (2, 'Jusqu\'à 600 euros pour abandonner', '<figure class=\"image\"><img src=\"http://www.levif.be/medias/3619/1852931.jpg\" alt=\"Jusqu\'&agrave; 600 euros pour abandonner sa voiture de soci&eacute;t&eacute;, vraiment int&eacute;ressant ?\" width=\"620\" height=\"413\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n<div class=\"row\">\r\n<div class=\"article-body span8\" data-gtm-vis-recent-on-screen-6039843_102=\"1887\" data-gtm-vis-first-on-screen-6039843_102=\"1887\" data-gtm-vis-total-visible-time-6039843_102=\"100\" data-gtm-vis-has-fired-6039843_102=\"1\">\r\n<div id=\"article-body\">\r\n<p>Dans les propositions de mobilit&eacute; &eacute;mises par Charles Michel, on trouve non seulement un abonnement aux transports en commun ou un v&eacute;lo d\'entreprise en remplacement de la voiture de soci&eacute;t&eacute;, mais aussi un salaire net plus &eacute;lev&eacute; si l\'employ&eacute; d&eacute;cide de se passer de son v&eacute;hicule. Le montant allou&eacute; d&eacute;pendra du mod&egrave;le du v&eacute;hicule et des conditions de leasing. Quant aux travailleurs qui n\'ont pas de voiture de soci&eacute;t&eacute;, ils sont les perdants de cette nouvelle politique mise sur la table car ils ne recevront pas un centime de plus ni d\'autres avantages.</p>\r\n<p>Les travailleurs qui disposent d\'une voiture de soci&eacute;t&eacute; en leasing - ils sont 400 000 en Belgique - seront mis face &agrave; un choix en concertation avec leur employeur : la garder ou recevoir de 300 &agrave; 600 euros nets suppl&eacute;mentaires (selon le mod&egrave;le) sur leur salaire mensuel selon des chiffres avanc&eacute;s par&nbsp;<em><a href=\"http://www.standaard.be/cnt/dmf20161023_02535372\" target=\"_blank\" rel=\"noopener\">De Standaard</a></em>. Ce montant qui varie du simple au double reste encore assez flou. La r&egrave;gle: plus la voiture co&ucirc;te cher, plus le suppl&eacute;ment net par mois sera &eacute;lev&eacute;.</p>\r\n<div id=\"INTERSCROLLER\" class=\"Ad INTERSCROLLER\">&nbsp;</div>\r\n<p>Le vice-premier ministre Didier Reynders a &eacute;t&eacute; le premier - et le seul - &agrave; parler d\'un montant de 450 euros. Or, le budget moyen de leasing auto pour les voitures de soci&eacute;t&eacute; en Belgique tourne autour de 560 euros HTVA, selon des chiffres fournis par Renta, la F&eacute;d&eacute;ration belge des loueurs de voitures.&nbsp;<em>De Standaard&nbsp;</em>avance, de son c&ocirc;t&eacute;, le montant de 300 euros nets pour une Renault M&eacute;gane et de presque le double pour une Audi A6.Ce montant devrait &ecirc;tre moins impos&eacute;, car il ne fera pas partie du salaire brut. L\'employ&eacute; devrait donc r&eacute;cup&eacute;rer les co&ucirc;ts de leasing amput&eacute; de l\'avantage fiscal.</p>\r\n<h2>Budg&eacute;tairement neutre</h2>\r\n<p>Dans cette proposition, il est important que le budget mobilit&eacute; soit une alternative budg&eacute;tairement neutre aux voitures de soci&eacute;t&eacute;. Et c\'est l&agrave; que le b&acirc;t blesse, communique la soci&eacute;t&eacute; europ&eacute;enne de services de ressources humaines SD Worx dans les quotidiens flamands&nbsp;<em>De Standaard</em>&nbsp;et&nbsp;<em>De Tijd.</em></p>\r\n<p>\"<em>Une telle augmentation de salaire n\'est possible que si l\'employ&eacute; ne doit pas payer de cotisations sociales ou d\'imp&ocirc;ts sur le salaire suppl&eacute;mentaire</em>\", ressort-il chez SD Worx<em>. \"Cela semble peu vraisemblable, car cela co&ucirc;terait &eacute;norm&eacute;ment d\'argent au fisc et &agrave; la s&eacute;curit&eacute; sociale.\"</em></p>\r\n<p>Si le salaire suppl&eacute;mentaire est impos&eacute; aux taux normaux, l\'employ&eacute; conservera beaucoup moins que les 450 euros avanc&eacute;s par Reynders. Le calcul effectu&eacute; par la soci&eacute;t&eacute; de consultance en ressources humaines avance qu\'une Golf VW co&ucirc;te mensuellement 373 euros (taxes et imp&ocirc;ts inclus) &agrave; une soci&eacute;t&eacute;. Quand ces frais sont convertis en salaire brut, l\'employ&eacute; ne re&ccedil;oit, au final, que 165 euros suppl&eacute;mentaires en net par mois. Selon SD Worx, c\'est beaucoup trop peu pour persuader un employ&eacute; de ne pas opter pour une voiture de soci&eacute;t&eacute;.</p>\r\n<p>L\'Open Vld avait d&eacute;j&agrave; mis au point une proposition en mars concernant la mani&egrave;re dont le salaire suppl&eacute;mentaire devrait &ecirc;tre impos&eacute; pour &eacute;viter que l\'avantage ne soit r&eacute;duit &agrave; peau de chagrin. Selon le parlementaire Egbert Lachaert, ce salaire suppl&eacute;mentaire devrait figurer sous un code sp&eacute;cifique sur la fiche salariale et &ecirc;tre impos&eacute; de la m&ecirc;me mani&egrave;re que le montant du leasing de la voiture de soci&eacute;t&eacute;, donc via un avantage de toute nature. \"<em>Ainsi, cela ne co&ucirc;tera pas un euro de plus &agrave; l\'employeur et l\'employ&eacute; ne recevra pas un euro de moins. Car en travaillant avec un code de salaire net s&eacute;par&eacute;, vous ne risquerez pas de tomber dans une tranche d\'imposition plus &eacute;lev&eacute;e\",</em>&nbsp;ressort-il. Dans&nbsp;<em>De Tijd</em>, Lachaert fait savoir que sa proposition sert de base pour le d&eacute;bat concernant le budget mobilit&eacute;.</p>\r\n<h2>D&eacute;savantages</h2>\r\n<p>Ce syst&egrave;me pr&eacute;sente aussi plusieurs d&eacute;savantages, comme l\'explique&nbsp;<em><a href=\"http://www.demorgen.be/economie/bedrijfswagen-inruilen-tegen-cash-tot-600-euro-extra-nettoloon-b8dab672/\">De Morgen</a>.</em>&nbsp;Les adversaires de cette mesure craignent qu\'elle ne modifie pas les habitudes des navetteurs belges. La FEBIAC, la F&eacute;d&eacute;ration Belge de l\'Automobile, craint que les travailleurs n\'utilisent ce budget de mobilit&eacute; en remplacement de leur voiture de soci&eacute;t&eacute; pour acqu&eacute;rir un mod&egrave;le moins cher d\'occasion, mais aussi plus polluant.</p>\r\n<p>Autre facteur d&eacute;terminant dans ce choix : le facteur \"carburant\". La plupart du temps, les employ&eacute;s qui disposent d\'une voiture jouissent aussi d\'une carte essence, parfois sans restriction selon leur situation (ind&eacute;pendant, commercial,...). Un avantage financi&egrave;rement int&eacute;ressant pour les trajets quotidiens domicile-bureau, mais aussi pour les escapades de week-end et les grandes vacances.</p>\r\n<p>Le kilom&eacute;trage moyen annuel pour un v&eacute;hicule de soci&eacute;t&eacute; est de 28.500 kilom&egrave;tres, selon la FEBIAC qui d&eacute;taille, sur le site de l\'<em>Echo</em>, qu\'en se basant sur une consommation moyenne de 7 litres/100 km, on arrive &agrave; une consommation d\'environ 2.000 litres par voiture et par an. &Agrave; 1,2 euro le litre, cela repr&eacute;sente une d&eacute;pense mensuelle d\'environ 200 euros, un montant non n&eacute;gligeable &agrave; prendre en compte.</p>\r\n<p>En outre, pour une voiture priv&eacute;e, il faut aussi faire peser dans la balance les entretiens, les r&eacute;parations, les pneus hiver ou encore, les co&ucirc;ts de r&eacute;paration en cas d\'accident...des sommes qui peuvent vite grimper.</p>\r\n<p>Un autre b&eacute;mol point&eacute; du doigt est le fait que les employ&eacute;s qui ne disposent pas de voitures de soci&eacute;t&eacute; ne profiteront pas de cette nouvelle politique de mobilit&eacute;. Ils ne pourront jouir ni d\'un abonnement avantageux aux transports en commun ni d\'un salaire plus &eacute;lev&eacute; pour modifier leur habitudes de mobilit&eacute;.<em>\"Il faut plus de temps n&eacute;cessaire pour offrir &agrave; tous un budget mobilit&eacute;\"</em>&nbsp;est d\'avis le parlementaire Lachaert.</p>\r\n<p>Selon L\'<em>Echo</em>, les services publics concern&eacute;s doivent se mettre d\'accord sur les modalit&eacute;s de la mesure pour avril 2017, elle ne devrait pas &ecirc;tre mise en oeuvre avant l\'automne.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'Jusqua-600-euros-abandonner', '/storage/articles/Jusqua-600-euros-pour-abandonner-20182018_0808_0303_09_41_54.jpg', '27-07-2018', '2018-08-03 09:41:54', '2018-08-03 09:41:54', 2, 8, 1);
INSERT INTO `articles` VALUES (3, 'Jusqu\'à 600 euros pour abandonner sa voiture de société, vraiment intéressant ?', '<figure class=\"image\"><img src=\"http://www.levif.be/medias/3619/1852931.jpg\" alt=\"Jusqu\'&agrave; 600 euros pour abandonner sa voiture de soci&eacute;t&eacute;, vraiment int&eacute;ressant ?\" width=\"620\" height=\"413\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n<div class=\"row\">\r\n<div class=\"article-body span8\" data-gtm-vis-recent-on-screen-6039843_102=\"1887\" data-gtm-vis-first-on-screen-6039843_102=\"1887\" data-gtm-vis-total-visible-time-6039843_102=\"100\" data-gtm-vis-has-fired-6039843_102=\"1\">\r\n<div id=\"article-body\">\r\n<p>Dans les propositions de mobilit&eacute; &eacute;mises par Charles Michel, on trouve non seulement un abonnement aux transports en commun ou un v&eacute;lo d\'entreprise en remplacement de la voiture de soci&eacute;t&eacute;, mais aussi un salaire net plus &eacute;lev&eacute; si l\'employ&eacute; d&eacute;cide de se passer de son v&eacute;hicule. Le montant allou&eacute; d&eacute;pendra du mod&egrave;le du v&eacute;hicule et des conditions de leasing. Quant aux travailleurs qui n\'ont pas de voiture de soci&eacute;t&eacute;, ils sont les perdants de cette nouvelle politique mise sur la table car ils ne recevront pas un centime de plus ni d\'autres avantages.</p>\r\n<p>Les travailleurs qui disposent d\'une voiture de soci&eacute;t&eacute; en leasing - ils sont 400 000 en Belgique - seront mis face &agrave; un choix en concertation avec leur employeur : la garder ou recevoir de 300 &agrave; 600 euros nets suppl&eacute;mentaires (selon le mod&egrave;le) sur leur salaire mensuel selon des chiffres avanc&eacute;s par&nbsp;<em><a href=\"http://www.standaard.be/cnt/dmf20161023_02535372\" target=\"_blank\" rel=\"noopener\">De Standaard</a></em>. Ce montant qui varie du simple au double reste encore assez flou. La r&egrave;gle: plus la voiture co&ucirc;te cher, plus le suppl&eacute;ment net par mois sera &eacute;lev&eacute;.</p>\r\n<div id=\"INTERSCROLLER\" class=\"Ad INTERSCROLLER\">&nbsp;</div>\r\n<p>Le vice-premier ministre Didier Reynders a &eacute;t&eacute; le premier - et le seul - &agrave; parler d\'un montant de 450 euros. Or, le budget moyen de leasing auto pour les voitures de soci&eacute;t&eacute; en Belgique tourne autour de 560 euros HTVA, selon des chiffres fournis par Renta, la F&eacute;d&eacute;ration belge des loueurs de voitures.&nbsp;<em>De Standaard&nbsp;</em>avance, de son c&ocirc;t&eacute;, le montant de 300 euros nets pour une Renault M&eacute;gane et de presque le double pour une Audi A6.Ce montant devrait &ecirc;tre moins impos&eacute;, car il ne fera pas partie du salaire brut. L\'employ&eacute; devrait donc r&eacute;cup&eacute;rer les co&ucirc;ts de leasing amput&eacute; de l\'avantage fiscal.</p>\r\n<h2>Budg&eacute;tairement neutre</h2>\r\n<p>Dans cette proposition, il est important que le budget mobilit&eacute; soit une alternative budg&eacute;tairement neutre aux voitures de soci&eacute;t&eacute;. Et c\'est l&agrave; que le b&acirc;t blesse, communique la soci&eacute;t&eacute; europ&eacute;enne de services de ressources humaines SD Worx dans les quotidiens flamands&nbsp;<em>De Standaard</em>&nbsp;et&nbsp;<em>De Tijd.</em></p>\r\n<p>\"<em>Une telle augmentation de salaire n\'est possible que si l\'employ&eacute; ne doit pas payer de cotisations sociales ou d\'imp&ocirc;ts sur le salaire suppl&eacute;mentaire</em>\", ressort-il chez SD Worx<em>. \"Cela semble peu vraisemblable, car cela co&ucirc;terait &eacute;norm&eacute;ment d\'argent au fisc et &agrave; la s&eacute;curit&eacute; sociale.\"</em></p>\r\n<p>Si le salaire suppl&eacute;mentaire est impos&eacute; aux taux normaux, l\'employ&eacute; conservera beaucoup moins que les 450 euros avanc&eacute;s par Reynders. Le calcul effectu&eacute; par la soci&eacute;t&eacute; de consultance en ressources humaines avance qu\'une Golf VW co&ucirc;te mensuellement 373 euros (taxes et imp&ocirc;ts inclus) &agrave; une soci&eacute;t&eacute;. Quand ces frais sont convertis en salaire brut, l\'employ&eacute; ne re&ccedil;oit, au final, que 165 euros suppl&eacute;mentaires en net par mois. Selon SD Worx, c\'est beaucoup trop peu pour persuader un employ&eacute; de ne pas opter pour une voiture de soci&eacute;t&eacute;.</p>\r\n<p>L\'Open Vld avait d&eacute;j&agrave; mis au point une proposition en mars concernant la mani&egrave;re dont le salaire suppl&eacute;mentaire devrait &ecirc;tre impos&eacute; pour &eacute;viter que l\'avantage ne soit r&eacute;duit &agrave; peau de chagrin. Selon le parlementaire Egbert Lachaert, ce salaire suppl&eacute;mentaire devrait figurer sous un code sp&eacute;cifique sur la fiche salariale et &ecirc;tre impos&eacute; de la m&ecirc;me mani&egrave;re que le montant du leasing de la voiture de soci&eacute;t&eacute;, donc via un avantage de toute nature. \"<em>Ainsi, cela ne co&ucirc;tera pas un euro de plus &agrave; l\'employeur et l\'employ&eacute; ne recevra pas un euro de moins. Car en travaillant avec un code de salaire net s&eacute;par&eacute;, vous ne risquerez pas de tomber dans une tranche d\'imposition plus &eacute;lev&eacute;e\",</em>&nbsp;ressort-il. Dans&nbsp;<em>De Tijd</em>, Lachaert fait savoir que sa proposition sert de base pour le d&eacute;bat concernant le budget mobilit&eacute;.</p>\r\n<h2>D&eacute;savantages</h2>\r\n<p>Ce syst&egrave;me pr&eacute;sente aussi plusieurs d&eacute;savantages, comme l\'explique&nbsp;<em><a href=\"http://www.demorgen.be/economie/bedrijfswagen-inruilen-tegen-cash-tot-600-euro-extra-nettoloon-b8dab672/\">De Morgen</a>.</em>&nbsp;Les adversaires de cette mesure craignent qu\'elle ne modifie pas les habitudes des navetteurs belges. La FEBIAC, la F&eacute;d&eacute;ration Belge de l\'Automobile, craint que les travailleurs n\'utilisent ce budget de mobilit&eacute; en remplacement de leur voiture de soci&eacute;t&eacute; pour acqu&eacute;rir un mod&egrave;le moins cher d\'occasion, mais aussi plus polluant.</p>\r\n<p>Autre facteur d&eacute;terminant dans ce choix : le facteur \"carburant\". La plupart du temps, les employ&eacute;s qui disposent d\'une voiture jouissent aussi d\'une carte essence, parfois sans restriction selon leur situation (ind&eacute;pendant, commercial,...). Un avantage financi&egrave;rement int&eacute;ressant pour les trajets quotidiens domicile-bureau, mais aussi pour les escapades de week-end et les grandes vacances.</p>\r\n<p>Le kilom&eacute;trage moyen annuel pour un v&eacute;hicule de soci&eacute;t&eacute; est de 28.500 kilom&egrave;tres, selon la FEBIAC qui d&eacute;taille, sur le site de l\'<em>Echo</em>, qu\'en se basant sur une consommation moyenne de 7 litres/100 km, on arrive &agrave; une consommation d\'environ 2.000 litres par voiture et par an. &Agrave; 1,2 euro le litre, cela repr&eacute;sente une d&eacute;pense mensuelle d\'environ 200 euros, un montant non n&eacute;gligeable &agrave; prendre en compte.</p>\r\n<p>En outre, pour une voiture priv&eacute;e, il faut aussi faire peser dans la balance les entretiens, les r&eacute;parations, les pneus hiver ou encore, les co&ucirc;ts de r&eacute;paration en cas d\'accident...des sommes qui peuvent vite grimper.</p>\r\n<p>Un autre b&eacute;mol point&eacute; du doigt est le fait que les employ&eacute;s qui ne disposent pas de voitures de soci&eacute;t&eacute; ne profiteront pas de cette nouvelle politique de mobilit&eacute;. Ils ne pourront jouir ni d\'un abonnement avantageux aux transports en commun ni d\'un salaire plus &eacute;lev&eacute; pour modifier leur habitudes de mobilit&eacute;.<em>\"Il faut plus de temps n&eacute;cessaire pour offrir &agrave; tous un budget mobilit&eacute;\"</em>&nbsp;est d\'avis le parlementaire Lachaert.</p>\r\n<p>Selon L\'<em>Echo</em>, les services publics concern&eacute;s doivent se mettre d\'accord sur les modalit&eacute;s de la mesure pour avril 2017, elle ne devrait pas &ecirc;tre mise en oeuvre avant l\'automne.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'Jusqua-600-euros-pour-abandonner-3', '/storage/articles/Jusqua-600-euros-pour-abandonner-20182018_0808_0303_09_41_54.jpg', '27-07-2018', '2018-08-03 09:41:54', '2018-08-03 09:41:54', 3, 9, 1);
INSERT INTO `articles` VALUES (4, 'Jusqu\'à 600 euros pour abandonner sa voiture de société, vraiment intéressant ?', '<figure class=\"image\"><img src=\"http://www.levif.be/medias/3619/1852931.jpg\" alt=\"Jusqu\'&agrave; 600 euros pour abandonner sa voiture de soci&eacute;t&eacute;, vraiment int&eacute;ressant ?\" width=\"620\" height=\"413\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n<div class=\"row\">\r\n<div class=\"article-body span8\" data-gtm-vis-recent-on-screen-6039843_102=\"1887\" data-gtm-vis-first-on-screen-6039843_102=\"1887\" data-gtm-vis-total-visible-time-6039843_102=\"100\" data-gtm-vis-has-fired-6039843_102=\"1\">\r\n<div id=\"article-body\">\r\n<p>Dans les propositions de mobilit&eacute; &eacute;mises par Charles Michel, on trouve non seulement un abonnement aux transports en commun ou un v&eacute;lo d\'entreprise en remplacement de la voiture de soci&eacute;t&eacute;, mais aussi un salaire net plus &eacute;lev&eacute; si l\'employ&eacute; d&eacute;cide de se passer de son v&eacute;hicule. Le montant allou&eacute; d&eacute;pendra du mod&egrave;le du v&eacute;hicule et des conditions de leasing. Quant aux travailleurs qui n\'ont pas de voiture de soci&eacute;t&eacute;, ils sont les perdants de cette nouvelle politique mise sur la table car ils ne recevront pas un centime de plus ni d\'autres avantages.</p>\r\n<p>Les travailleurs qui disposent d\'une voiture de soci&eacute;t&eacute; en leasing - ils sont 400 000 en Belgique - seront mis face &agrave; un choix en concertation avec leur employeur : la garder ou recevoir de 300 &agrave; 600 euros nets suppl&eacute;mentaires (selon le mod&egrave;le) sur leur salaire mensuel selon des chiffres avanc&eacute;s par&nbsp;<em><a href=\"http://www.standaard.be/cnt/dmf20161023_02535372\" target=\"_blank\" rel=\"noopener\">De Standaard</a></em>. Ce montant qui varie du simple au double reste encore assez flou. La r&egrave;gle: plus la voiture co&ucirc;te cher, plus le suppl&eacute;ment net par mois sera &eacute;lev&eacute;.</p>\r\n<div id=\"INTERSCROLLER\" class=\"Ad INTERSCROLLER\">&nbsp;</div>\r\n<p>Le vice-premier ministre Didier Reynders a &eacute;t&eacute; le premier - et le seul - &agrave; parler d\'un montant de 450 euros. Or, le budget moyen de leasing auto pour les voitures de soci&eacute;t&eacute; en Belgique tourne autour de 560 euros HTVA, selon des chiffres fournis par Renta, la F&eacute;d&eacute;ration belge des loueurs de voitures.&nbsp;<em>De Standaard&nbsp;</em>avance, de son c&ocirc;t&eacute;, le montant de 300 euros nets pour une Renault M&eacute;gane et de presque le double pour une Audi A6.Ce montant devrait &ecirc;tre moins impos&eacute;, car il ne fera pas partie du salaire brut. L\'employ&eacute; devrait donc r&eacute;cup&eacute;rer les co&ucirc;ts de leasing amput&eacute; de l\'avantage fiscal.</p>\r\n<h2>Budg&eacute;tairement neutre</h2>\r\n<p>Dans cette proposition, il est important que le budget mobilit&eacute; soit une alternative budg&eacute;tairement neutre aux voitures de soci&eacute;t&eacute;. Et c\'est l&agrave; que le b&acirc;t blesse, communique la soci&eacute;t&eacute; europ&eacute;enne de services de ressources humaines SD Worx dans les quotidiens flamands&nbsp;<em>De Standaard</em>&nbsp;et&nbsp;<em>De Tijd.</em></p>\r\n<p>\"<em>Une telle augmentation de salaire n\'est possible que si l\'employ&eacute; ne doit pas payer de cotisations sociales ou d\'imp&ocirc;ts sur le salaire suppl&eacute;mentaire</em>\", ressort-il chez SD Worx<em>. \"Cela semble peu vraisemblable, car cela co&ucirc;terait &eacute;norm&eacute;ment d\'argent au fisc et &agrave; la s&eacute;curit&eacute; sociale.\"</em></p>\r\n<p>Si le salaire suppl&eacute;mentaire est impos&eacute; aux taux normaux, l\'employ&eacute; conservera beaucoup moins que les 450 euros avanc&eacute;s par Reynders. Le calcul effectu&eacute; par la soci&eacute;t&eacute; de consultance en ressources humaines avance qu\'une Golf VW co&ucirc;te mensuellement 373 euros (taxes et imp&ocirc;ts inclus) &agrave; une soci&eacute;t&eacute;. Quand ces frais sont convertis en salaire brut, l\'employ&eacute; ne re&ccedil;oit, au final, que 165 euros suppl&eacute;mentaires en net par mois. Selon SD Worx, c\'est beaucoup trop peu pour persuader un employ&eacute; de ne pas opter pour une voiture de soci&eacute;t&eacute;.</p>\r\n<p>L\'Open Vld avait d&eacute;j&agrave; mis au point une proposition en mars concernant la mani&egrave;re dont le salaire suppl&eacute;mentaire devrait &ecirc;tre impos&eacute; pour &eacute;viter que l\'avantage ne soit r&eacute;duit &agrave; peau de chagrin. Selon le parlementaire Egbert Lachaert, ce salaire suppl&eacute;mentaire devrait figurer sous un code sp&eacute;cifique sur la fiche salariale et &ecirc;tre impos&eacute; de la m&ecirc;me mani&egrave;re que le montant du leasing de la voiture de soci&eacute;t&eacute;, donc via un avantage de toute nature. \"<em>Ainsi, cela ne co&ucirc;tera pas un euro de plus &agrave; l\'employeur et l\'employ&eacute; ne recevra pas un euro de moins. Car en travaillant avec un code de salaire net s&eacute;par&eacute;, vous ne risquerez pas de tomber dans une tranche d\'imposition plus &eacute;lev&eacute;e\",</em>&nbsp;ressort-il. Dans&nbsp;<em>De Tijd</em>, Lachaert fait savoir que sa proposition sert de base pour le d&eacute;bat concernant le budget mobilit&eacute;.</p>\r\n<h2>D&eacute;savantages</h2>\r\n<p>Ce syst&egrave;me pr&eacute;sente aussi plusieurs d&eacute;savantages, comme l\'explique&nbsp;<em><a href=\"http://www.demorgen.be/economie/bedrijfswagen-inruilen-tegen-cash-tot-600-euro-extra-nettoloon-b8dab672/\">De Morgen</a>.</em>&nbsp;Les adversaires de cette mesure craignent qu\'elle ne modifie pas les habitudes des navetteurs belges. La FEBIAC, la F&eacute;d&eacute;ration Belge de l\'Automobile, craint que les travailleurs n\'utilisent ce budget de mobilit&eacute; en remplacement de leur voiture de soci&eacute;t&eacute; pour acqu&eacute;rir un mod&egrave;le moins cher d\'occasion, mais aussi plus polluant.</p>\r\n<p>Autre facteur d&eacute;terminant dans ce choix : le facteur \"carburant\". La plupart du temps, les employ&eacute;s qui disposent d\'une voiture jouissent aussi d\'une carte essence, parfois sans restriction selon leur situation (ind&eacute;pendant, commercial,...). Un avantage financi&egrave;rement int&eacute;ressant pour les trajets quotidiens domicile-bureau, mais aussi pour les escapades de week-end et les grandes vacances.</p>\r\n<p>Le kilom&eacute;trage moyen annuel pour un v&eacute;hicule de soci&eacute;t&eacute; est de 28.500 kilom&egrave;tres, selon la FEBIAC qui d&eacute;taille, sur le site de l\'<em>Echo</em>, qu\'en se basant sur une consommation moyenne de 7 litres/100 km, on arrive &agrave; une consommation d\'environ 2.000 litres par voiture et par an. &Agrave; 1,2 euro le litre, cela repr&eacute;sente une d&eacute;pense mensuelle d\'environ 200 euros, un montant non n&eacute;gligeable &agrave; prendre en compte.</p>\r\n<p>En outre, pour une voiture priv&eacute;e, il faut aussi faire peser dans la balance les entretiens, les r&eacute;parations, les pneus hiver ou encore, les co&ucirc;ts de r&eacute;paration en cas d\'accident...des sommes qui peuvent vite grimper.</p>\r\n<p>Un autre b&eacute;mol point&eacute; du doigt est le fait que les employ&eacute;s qui ne disposent pas de voitures de soci&eacute;t&eacute; ne profiteront pas de cette nouvelle politique de mobilit&eacute;. Ils ne pourront jouir ni d\'un abonnement avantageux aux transports en commun ni d\'un salaire plus &eacute;lev&eacute; pour modifier leur habitudes de mobilit&eacute;.<em>\"Il faut plus de temps n&eacute;cessaire pour offrir &agrave; tous un budget mobilit&eacute;\"</em>&nbsp;est d\'avis le parlementaire Lachaert.</p>\r\n<p>Selon L\'<em>Echo</em>, les services publics concern&eacute;s doivent se mettre d\'accord sur les modalit&eacute;s de la mesure pour avril 2017, elle ne devrait pas &ecirc;tre mise en oeuvre avant l\'automne.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'Jusqua-600-euros-pour-abandonner-4', '/storage/articles/Jusqua-600-euros-pour-abandonner-20182018_0808_0303_09_41_54.jpg', '27-07-2018', '2018-08-03 09:41:54', '2018-08-03 09:41:54', 3, 7, 1);
INSERT INTO `articles` VALUES (5, 'Jusqu\'à 600 euros pour abandonner', '<figure class=\"image\"><img src=\"http://www.levif.be/medias/3619/1852931.jpg\" alt=\"Jusqu\'&agrave; 600 euros pour abandonner sa voiture de soci&eacute;t&eacute;, vraiment int&eacute;ressant ?\" width=\"620\" height=\"413\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n<div class=\"row\">\r\n<div class=\"article-body span8\" data-gtm-vis-recent-on-screen-6039843_102=\"1887\" data-gtm-vis-first-on-screen-6039843_102=\"1887\" data-gtm-vis-total-visible-time-6039843_102=\"100\" data-gtm-vis-has-fired-6039843_102=\"1\">\r\n<div id=\"article-body\">\r\n<p>Dans les propositions de mobilit&eacute; &eacute;mises par Charles Michel, on trouve non seulement un abonnement aux transports en commun ou un v&eacute;lo d\'entreprise en remplacement de la voiture de soci&eacute;t&eacute;, mais aussi un salaire net plus &eacute;lev&eacute; si l\'employ&eacute; d&eacute;cide de se passer de son v&eacute;hicule. Le montant allou&eacute; d&eacute;pendra du mod&egrave;le du v&eacute;hicule et des conditions de leasing. Quant aux travailleurs qui n\'ont pas de voiture de soci&eacute;t&eacute;, ils sont les perdants de cette nouvelle politique mise sur la table car ils ne recevront pas un centime de plus ni d\'autres avantages.</p>\r\n<p>Les travailleurs qui disposent d\'une voiture de soci&eacute;t&eacute; en leasing - ils sont 400 000 en Belgique - seront mis face &agrave; un choix en concertation avec leur employeur : la garder ou recevoir de 300 &agrave; 600 euros nets suppl&eacute;mentaires (selon le mod&egrave;le) sur leur salaire mensuel selon des chiffres avanc&eacute;s par&nbsp;<em><a href=\"http://www.standaard.be/cnt/dmf20161023_02535372\" target=\"_blank\" rel=\"noopener\">De Standaard</a></em>. Ce montant qui varie du simple au double reste encore assez flou. La r&egrave;gle: plus la voiture co&ucirc;te cher, plus le suppl&eacute;ment net par mois sera &eacute;lev&eacute;.</p>\r\n<div id=\"INTERSCROLLER\" class=\"Ad INTERSCROLLER\">&nbsp;</div>\r\n<p>Le vice-premier ministre Didier Reynders a &eacute;t&eacute; le premier - et le seul - &agrave; parler d\'un montant de 450 euros. Or, le budget moyen de leasing auto pour les voitures de soci&eacute;t&eacute; en Belgique tourne autour de 560 euros HTVA, selon des chiffres fournis par Renta, la F&eacute;d&eacute;ration belge des loueurs de voitures.&nbsp;<em>De Standaard&nbsp;</em>avance, de son c&ocirc;t&eacute;, le montant de 300 euros nets pour une Renault M&eacute;gane et de presque le double pour une Audi A6.Ce montant devrait &ecirc;tre moins impos&eacute;, car il ne fera pas partie du salaire brut. L\'employ&eacute; devrait donc r&eacute;cup&eacute;rer les co&ucirc;ts de leasing amput&eacute; de l\'avantage fiscal.</p>\r\n<h2>Budg&eacute;tairement neutre</h2>\r\n<p>Dans cette proposition, il est important que le budget mobilit&eacute; soit une alternative budg&eacute;tairement neutre aux voitures de soci&eacute;t&eacute;. Et c\'est l&agrave; que le b&acirc;t blesse, communique la soci&eacute;t&eacute; europ&eacute;enne de services de ressources humaines SD Worx dans les quotidiens flamands&nbsp;<em>De Standaard</em>&nbsp;et&nbsp;<em>De Tijd.</em></p>\r\n<p>\"<em>Une telle augmentation de salaire n\'est possible que si l\'employ&eacute; ne doit pas payer de cotisations sociales ou d\'imp&ocirc;ts sur le salaire suppl&eacute;mentaire</em>\", ressort-il chez SD Worx<em>. \"Cela semble peu vraisemblable, car cela co&ucirc;terait &eacute;norm&eacute;ment d\'argent au fisc et &agrave; la s&eacute;curit&eacute; sociale.\"</em></p>\r\n<p>Si le salaire suppl&eacute;mentaire est impos&eacute; aux taux normaux, l\'employ&eacute; conservera beaucoup moins que les 450 euros avanc&eacute;s par Reynders. Le calcul effectu&eacute; par la soci&eacute;t&eacute; de consultance en ressources humaines avance qu\'une Golf VW co&ucirc;te mensuellement 373 euros (taxes et imp&ocirc;ts inclus) &agrave; une soci&eacute;t&eacute;. Quand ces frais sont convertis en salaire brut, l\'employ&eacute; ne re&ccedil;oit, au final, que 165 euros suppl&eacute;mentaires en net par mois. Selon SD Worx, c\'est beaucoup trop peu pour persuader un employ&eacute; de ne pas opter pour une voiture de soci&eacute;t&eacute;.</p>\r\n<p>L\'Open Vld avait d&eacute;j&agrave; mis au point une proposition en mars concernant la mani&egrave;re dont le salaire suppl&eacute;mentaire devrait &ecirc;tre impos&eacute; pour &eacute;viter que l\'avantage ne soit r&eacute;duit &agrave; peau de chagrin. Selon le parlementaire Egbert Lachaert, ce salaire suppl&eacute;mentaire devrait figurer sous un code sp&eacute;cifique sur la fiche salariale et &ecirc;tre impos&eacute; de la m&ecirc;me mani&egrave;re que le montant du leasing de la voiture de soci&eacute;t&eacute;, donc via un avantage de toute nature. \"<em>Ainsi, cela ne co&ucirc;tera pas un euro de plus &agrave; l\'employeur et l\'employ&eacute; ne recevra pas un euro de moins. Car en travaillant avec un code de salaire net s&eacute;par&eacute;, vous ne risquerez pas de tomber dans une tranche d\'imposition plus &eacute;lev&eacute;e\",</em>&nbsp;ressort-il. Dans&nbsp;<em>De Tijd</em>, Lachaert fait savoir que sa proposition sert de base pour le d&eacute;bat concernant le budget mobilit&eacute;.</p>\r\n<h2>D&eacute;savantages</h2>\r\n<p>Ce syst&egrave;me pr&eacute;sente aussi plusieurs d&eacute;savantages, comme l\'explique&nbsp;<em><a href=\"http://www.demorgen.be/economie/bedrijfswagen-inruilen-tegen-cash-tot-600-euro-extra-nettoloon-b8dab672/\">De Morgen</a>.</em>&nbsp;Les adversaires de cette mesure craignent qu\'elle ne modifie pas les habitudes des navetteurs belges. La FEBIAC, la F&eacute;d&eacute;ration Belge de l\'Automobile, craint que les travailleurs n\'utilisent ce budget de mobilit&eacute; en remplacement de leur voiture de soci&eacute;t&eacute; pour acqu&eacute;rir un mod&egrave;le moins cher d\'occasion, mais aussi plus polluant.</p>\r\n<p>Autre facteur d&eacute;terminant dans ce choix : le facteur \"carburant\". La plupart du temps, les employ&eacute;s qui disposent d\'une voiture jouissent aussi d\'une carte essence, parfois sans restriction selon leur situation (ind&eacute;pendant, commercial,...). Un avantage financi&egrave;rement int&eacute;ressant pour les trajets quotidiens domicile-bureau, mais aussi pour les escapades de week-end et les grandes vacances.</p>\r\n<p>Le kilom&eacute;trage moyen annuel pour un v&eacute;hicule de soci&eacute;t&eacute; est de 28.500 kilom&egrave;tres, selon la FEBIAC qui d&eacute;taille, sur le site de l\'<em>Echo</em>, qu\'en se basant sur une consommation moyenne de 7 litres/100 km, on arrive &agrave; une consommation d\'environ 2.000 litres par voiture et par an. &Agrave; 1,2 euro le litre, cela repr&eacute;sente une d&eacute;pense mensuelle d\'environ 200 euros, un montant non n&eacute;gligeable &agrave; prendre en compte.</p>\r\n<p>En outre, pour une voiture priv&eacute;e, il faut aussi faire peser dans la balance les entretiens, les r&eacute;parations, les pneus hiver ou encore, les co&ucirc;ts de r&eacute;paration en cas d\'accident...des sommes qui peuvent vite grimper.</p>\r\n<p>Un autre b&eacute;mol point&eacute; du doigt est le fait que les employ&eacute;s qui ne disposent pas de voitures de soci&eacute;t&eacute; ne profiteront pas de cette nouvelle politique de mobilit&eacute;. Ils ne pourront jouir ni d\'un abonnement avantageux aux transports en commun ni d\'un salaire plus &eacute;lev&eacute; pour modifier leur habitudes de mobilit&eacute;.<em>\"Il faut plus de temps n&eacute;cessaire pour offrir &agrave; tous un budget mobilit&eacute;\"</em>&nbsp;est d\'avis le parlementaire Lachaert.</p>\r\n<p>Selon L\'<em>Echo</em>, les services publics concern&eacute;s doivent se mettre d\'accord sur les modalit&eacute;s de la mesure pour avril 2017, elle ne devrait pas &ecirc;tre mise en oeuvre avant l\'automne.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'Jusqua-600-euros-abandonner-5', '/storage/articles/Jusqua-600-euros-pour-abandonner-20182018_0808_0303_09_41_54.jpg', '27-07-2018', '2018-08-03 09:41:54', '2018-08-03 09:41:54', 2, 8, 1);
INSERT INTO `articles` VALUES (6, 'Jusqu\'à 600 euros pour abandonner sa voiture de société, vraiment intéressant ?', '<figure class=\"image\"><img src=\"http://www.levif.be/medias/3619/1852931.jpg\" alt=\"Jusqu\'&agrave; 600 euros pour abandonner sa voiture de soci&eacute;t&eacute;, vraiment int&eacute;ressant ?\" width=\"620\" height=\"413\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n<div class=\"row\">\r\n<div class=\"article-body span8\" data-gtm-vis-recent-on-screen-6039843_102=\"1887\" data-gtm-vis-first-on-screen-6039843_102=\"1887\" data-gtm-vis-total-visible-time-6039843_102=\"100\" data-gtm-vis-has-fired-6039843_102=\"1\">\r\n<div id=\"article-body\">\r\n<p>Dans les propositions de mobilit&eacute; &eacute;mises par Charles Michel, on trouve non seulement un abonnement aux transports en commun ou un v&eacute;lo d\'entreprise en remplacement de la voiture de soci&eacute;t&eacute;, mais aussi un salaire net plus &eacute;lev&eacute; si l\'employ&eacute; d&eacute;cide de se passer de son v&eacute;hicule. Le montant allou&eacute; d&eacute;pendra du mod&egrave;le du v&eacute;hicule et des conditions de leasing. Quant aux travailleurs qui n\'ont pas de voiture de soci&eacute;t&eacute;, ils sont les perdants de cette nouvelle politique mise sur la table car ils ne recevront pas un centime de plus ni d\'autres avantages.</p>\r\n<p>Les travailleurs qui disposent d\'une voiture de soci&eacute;t&eacute; en leasing - ils sont 400 000 en Belgique - seront mis face &agrave; un choix en concertation avec leur employeur : la garder ou recevoir de 300 &agrave; 600 euros nets suppl&eacute;mentaires (selon le mod&egrave;le) sur leur salaire mensuel selon des chiffres avanc&eacute;s par&nbsp;<em><a href=\"http://www.standaard.be/cnt/dmf20161023_02535372\" target=\"_blank\" rel=\"noopener\">De Standaard</a></em>. Ce montant qui varie du simple au double reste encore assez flou. La r&egrave;gle: plus la voiture co&ucirc;te cher, plus le suppl&eacute;ment net par mois sera &eacute;lev&eacute;.</p>\r\n<div id=\"INTERSCROLLER\" class=\"Ad INTERSCROLLER\">&nbsp;</div>\r\n<p>Le vice-premier ministre Didier Reynders a &eacute;t&eacute; le premier - et le seul - &agrave; parler d\'un montant de 450 euros. Or, le budget moyen de leasing auto pour les voitures de soci&eacute;t&eacute; en Belgique tourne autour de 560 euros HTVA, selon des chiffres fournis par Renta, la F&eacute;d&eacute;ration belge des loueurs de voitures.&nbsp;<em>De Standaard&nbsp;</em>avance, de son c&ocirc;t&eacute;, le montant de 300 euros nets pour une Renault M&eacute;gane et de presque le double pour une Audi A6.Ce montant devrait &ecirc;tre moins impos&eacute;, car il ne fera pas partie du salaire brut. L\'employ&eacute; devrait donc r&eacute;cup&eacute;rer les co&ucirc;ts de leasing amput&eacute; de l\'avantage fiscal.</p>\r\n<h2>Budg&eacute;tairement neutre</h2>\r\n<p>Dans cette proposition, il est important que le budget mobilit&eacute; soit une alternative budg&eacute;tairement neutre aux voitures de soci&eacute;t&eacute;. Et c\'est l&agrave; que le b&acirc;t blesse, communique la soci&eacute;t&eacute; europ&eacute;enne de services de ressources humaines SD Worx dans les quotidiens flamands&nbsp;<em>De Standaard</em>&nbsp;et&nbsp;<em>De Tijd.</em></p>\r\n<p>\"<em>Une telle augmentation de salaire n\'est possible que si l\'employ&eacute; ne doit pas payer de cotisations sociales ou d\'imp&ocirc;ts sur le salaire suppl&eacute;mentaire</em>\", ressort-il chez SD Worx<em>. \"Cela semble peu vraisemblable, car cela co&ucirc;terait &eacute;norm&eacute;ment d\'argent au fisc et &agrave; la s&eacute;curit&eacute; sociale.\"</em></p>\r\n<p>Si le salaire suppl&eacute;mentaire est impos&eacute; aux taux normaux, l\'employ&eacute; conservera beaucoup moins que les 450 euros avanc&eacute;s par Reynders. Le calcul effectu&eacute; par la soci&eacute;t&eacute; de consultance en ressources humaines avance qu\'une Golf VW co&ucirc;te mensuellement 373 euros (taxes et imp&ocirc;ts inclus) &agrave; une soci&eacute;t&eacute;. Quand ces frais sont convertis en salaire brut, l\'employ&eacute; ne re&ccedil;oit, au final, que 165 euros suppl&eacute;mentaires en net par mois. Selon SD Worx, c\'est beaucoup trop peu pour persuader un employ&eacute; de ne pas opter pour une voiture de soci&eacute;t&eacute;.</p>\r\n<p>L\'Open Vld avait d&eacute;j&agrave; mis au point une proposition en mars concernant la mani&egrave;re dont le salaire suppl&eacute;mentaire devrait &ecirc;tre impos&eacute; pour &eacute;viter que l\'avantage ne soit r&eacute;duit &agrave; peau de chagrin. Selon le parlementaire Egbert Lachaert, ce salaire suppl&eacute;mentaire devrait figurer sous un code sp&eacute;cifique sur la fiche salariale et &ecirc;tre impos&eacute; de la m&ecirc;me mani&egrave;re que le montant du leasing de la voiture de soci&eacute;t&eacute;, donc via un avantage de toute nature. \"<em>Ainsi, cela ne co&ucirc;tera pas un euro de plus &agrave; l\'employeur et l\'employ&eacute; ne recevra pas un euro de moins. Car en travaillant avec un code de salaire net s&eacute;par&eacute;, vous ne risquerez pas de tomber dans une tranche d\'imposition plus &eacute;lev&eacute;e\",</em>&nbsp;ressort-il. Dans&nbsp;<em>De Tijd</em>, Lachaert fait savoir que sa proposition sert de base pour le d&eacute;bat concernant le budget mobilit&eacute;.</p>\r\n<h2>D&eacute;savantages</h2>\r\n<p>Ce syst&egrave;me pr&eacute;sente aussi plusieurs d&eacute;savantages, comme l\'explique&nbsp;<em><a href=\"http://www.demorgen.be/economie/bedrijfswagen-inruilen-tegen-cash-tot-600-euro-extra-nettoloon-b8dab672/\">De Morgen</a>.</em>&nbsp;Les adversaires de cette mesure craignent qu\'elle ne modifie pas les habitudes des navetteurs belges. La FEBIAC, la F&eacute;d&eacute;ration Belge de l\'Automobile, craint que les travailleurs n\'utilisent ce budget de mobilit&eacute; en remplacement de leur voiture de soci&eacute;t&eacute; pour acqu&eacute;rir un mod&egrave;le moins cher d\'occasion, mais aussi plus polluant.</p>\r\n<p>Autre facteur d&eacute;terminant dans ce choix : le facteur \"carburant\". La plupart du temps, les employ&eacute;s qui disposent d\'une voiture jouissent aussi d\'une carte essence, parfois sans restriction selon leur situation (ind&eacute;pendant, commercial,...). Un avantage financi&egrave;rement int&eacute;ressant pour les trajets quotidiens domicile-bureau, mais aussi pour les escapades de week-end et les grandes vacances.</p>\r\n<p>Le kilom&eacute;trage moyen annuel pour un v&eacute;hicule de soci&eacute;t&eacute; est de 28.500 kilom&egrave;tres, selon la FEBIAC qui d&eacute;taille, sur le site de l\'<em>Echo</em>, qu\'en se basant sur une consommation moyenne de 7 litres/100 km, on arrive &agrave; une consommation d\'environ 2.000 litres par voiture et par an. &Agrave; 1,2 euro le litre, cela repr&eacute;sente une d&eacute;pense mensuelle d\'environ 200 euros, un montant non n&eacute;gligeable &agrave; prendre en compte.</p>\r\n<p>En outre, pour une voiture priv&eacute;e, il faut aussi faire peser dans la balance les entretiens, les r&eacute;parations, les pneus hiver ou encore, les co&ucirc;ts de r&eacute;paration en cas d\'accident...des sommes qui peuvent vite grimper.</p>\r\n<p>Un autre b&eacute;mol point&eacute; du doigt est le fait que les employ&eacute;s qui ne disposent pas de voitures de soci&eacute;t&eacute; ne profiteront pas de cette nouvelle politique de mobilit&eacute;. Ils ne pourront jouir ni d\'un abonnement avantageux aux transports en commun ni d\'un salaire plus &eacute;lev&eacute; pour modifier leur habitudes de mobilit&eacute;.<em>\"Il faut plus de temps n&eacute;cessaire pour offrir &agrave; tous un budget mobilit&eacute;\"</em>&nbsp;est d\'avis le parlementaire Lachaert.</p>\r\n<p>Selon L\'<em>Echo</em>, les services publics concern&eacute;s doivent se mettre d\'accord sur les modalit&eacute;s de la mesure pour avril 2017, elle ne devrait pas &ecirc;tre mise en oeuvre avant l\'automne.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'Jusqua-600-euros-pour-abandonner-6', '/storage/articles/Jusqua-600-euros-pour-abandonner-20182018_0808_0303_09_41_54.jpg', '27-07-2018', '2018-08-03 09:41:54', '2018-08-03 09:41:54', 3, 9, 1);
INSERT INTO `articles` VALUES (7, 'azezrzefgergfdgdfgdf grfgdfgfdg dgdfgdffsdf', '<p>fsdf sdfsdsdfdfgsdg rtf zetfzergergdf erqgv dfhgdfb sgdfgdfghhutsg dfhqwhr sgsdfgdfh gdfghdf hgdf ghdfghdfsg dsghhsdf</p>', 1, 'azezrzefgergfdgdfgdf-grfgdfgfdg-dgdfgdffsdf-r', '/storage/articles/azezrzefgergfdgdfgdf-grfgdfgfdg-dgdfgdffsdf-r-20182018_0808_0303_15_55_28.jpg', '27-07-2018', '2018-08-03 15:55:29', '2018-08-03 15:55:29', 1, 7, 1);

-- ----------------------------
-- Table structure for brands
-- ----------------------------
DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `logo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 50 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of brands
-- ----------------------------
INSERT INTO `brands` VALUES (1, '/upload/brands/1.png', 'BMW', 'bmw', 'BMW', 'bmw', NULL, NULL);
INSERT INTO `brands` VALUES (2, '/upload/brands/2.png', 'Abarth', 'abarth', 'Abarth', 'abarth', NULL, NULL);
INSERT INTO `brands` VALUES (3, '/upload/brands/3.png', 'Dacia', 'dacia', 'Dacia', 'dacia', NULL, NULL);
INSERT INTO `brands` VALUES (4, '/upload/brands/4.png', 'Mercedes', 'mercedes-benz', 'Mercedes', 'mercedes-benz', NULL, NULL);
INSERT INTO `brands` VALUES (5, '/upload/brands/5.png', 'Alfa Romeo', 'alfa-romeo', 'Alfa Romeo', 'alfa-romeo', NULL, NULL);
INSERT INTO `brands` VALUES (6, '/upload/brands/6.png', 'Audi', 'audi', 'Audi', 'audi', NULL, NULL);
INSERT INTO `brands` VALUES (7, '/upload/brands/7.png', 'Citroen', 'citroen', 'Citroen', 'citroen', NULL, NULL);
INSERT INTO `brands` VALUES (8, '/upload/brands/8.png', 'Volvo', 'volvo', 'Volvo', 'volvo', NULL, NULL);
INSERT INTO `brands` VALUES (9, '/upload/brands/9.png', 'Saab', 'saab', 'Saab', 'saab', NULL, NULL);
INSERT INTO `brands` VALUES (10, '/upload/brands/10.png', 'Toyota', 'toyota', 'Toyota', 'toyota', NULL, NULL);
INSERT INTO `brands` VALUES (11, '/upload/brands/11.png', 'Honda', 'honda', 'Honda', 'honda', NULL, NULL);
INSERT INTO `brands` VALUES (12, '/upload/brands/12.png', 'Opel', 'opel', 'Opel', 'opel', NULL, NULL);
INSERT INTO `brands` VALUES (13, '/upload/brands/13.png', 'Acura', 'acura', 'Acura', 'acura', NULL, NULL);
INSERT INTO `brands` VALUES (14, '/upload/brands/14.png', 'Amg', 'amg', 'Amg', 'amg', NULL, NULL);
INSERT INTO `brands` VALUES (15, '/upload/brands/15.png', 'Aston Martin', 'aston-martin', 'Aston Martin', 'aston-martin', NULL, NULL);
INSERT INTO `brands` VALUES (16, '/upload/brands/16.png', 'Bently', 'bently', 'Bently', 'bently', NULL, NULL);
INSERT INTO `brands` VALUES (17, '/upload/brands/17.png', 'Brilliance', 'brilliance', 'Brilliance', 'brilliance', NULL, NULL);
INSERT INTO `brands` VALUES (18, '/upload/brands/18.png', 'Bugatti', 'bugatti', 'Bugatti', 'bugatti', NULL, NULL);
INSERT INTO `brands` VALUES (19, '/upload/brands/19.png', 'Buik', 'buik', 'Buik', 'buik', NULL, NULL);
INSERT INTO `brands` VALUES (20, '/upload/brands/20.png', 'Byd', 'byd', 'Byd', 'byd', NULL, NULL);
INSERT INTO `brands` VALUES (21, '/upload/brands/21.png', 'Cadillac', 'cadillac', 'Cadillac', 'cadillac', NULL, NULL);
INSERT INTO `brands` VALUES (22, '/upload/brands/22.png', 'Changan', 'changan', 'Changan', 'changan', NULL, NULL);
INSERT INTO `brands` VALUES (23, '/upload/brands/23.png', 'Chevrolet', 'chevrolet', 'Chevrolet', 'chevrolet', NULL, NULL);
INSERT INTO `brands` VALUES (24, '/upload/brands/24.png', 'Chrysler', 'chrysler', 'Chrysler', 'chrysler', NULL, NULL);
INSERT INTO `brands` VALUES (26, '/upload/brands/26.png', 'Faw', 'faw', 'Faw', 'faw', NULL, NULL);
INSERT INTO `brands` VALUES (27, '/upload/brands/27.png', 'Ferrari', 'ferrari', 'Ferrari', 'ferrari', NULL, NULL);
INSERT INTO `brands` VALUES (28, '/upload/brands/28.png', 'Fiat', 'fiat', 'Fiat', 'fiat', NULL, NULL);
INSERT INTO `brands` VALUES (29, '/upload/brands/29.png', 'Ford', 'ford', 'Ford', 'ford', NULL, NULL);
INSERT INTO `brands` VALUES (30, '/upload/brands/30.png', 'Gac', 'gac', 'Gac', 'gac', NULL, NULL);
INSERT INTO `brands` VALUES (31, '/upload/brands/31.png', 'Hyundai', 'hyundai', 'Hyundai', 'hyundai', NULL, NULL);
INSERT INTO `brands` VALUES (32, '/upload/brands/32.png', 'Infiniti', 'infiniti', 'Infiniti', 'infiniti', NULL, NULL);
INSERT INTO `brands` VALUES (33, '/upload/brands/33.png', 'Iveco', 'iveco', 'Iveco', 'iveco', NULL, NULL);
INSERT INTO `brands` VALUES (34, '/upload/brands/34.png', 'Jaquar', 'jaquar', 'Jaquar', 'jaquar', NULL, NULL);
INSERT INTO `brands` VALUES (35, '/upload/brands/35.png', 'Jeep', 'jeep', 'Jeep', 'jeep', NULL, NULL);
INSERT INTO `brands` VALUES (36, '/upload/brands/36.png', 'Lamborghini', 'lamborghini', 'Lamborghini', 'lamborghini', NULL, NULL);
INSERT INTO `brands` VALUES (37, '/upload/brands/37.png', 'Lancia', 'lancia', 'Lancia', 'lancia', NULL, NULL);
INSERT INTO `brands` VALUES (38, '/upload/brands/38.png', 'Land Rover', 'land-rover', 'Land Rover', 'land-rover', NULL, NULL);
INSERT INTO `brands` VALUES (39, '/upload/brands/39.png', 'Lexus', 'lexus', 'Lexus', 'lexus', NULL, NULL);
INSERT INTO `brands` VALUES (40, '/upload/brands/40.png', 'Maserati', 'maserati', 'Maserati', 'maserati', NULL, NULL);
INSERT INTO `brands` VALUES (41, '/upload/brands/41.png', 'Maybach', 'maybach', 'Maybach', 'maybach', NULL, NULL);
INSERT INTO `brands` VALUES (42, '/upload/brands/42.png', 'Mazda', 'mazda', 'Mazda', 'mazda', NULL, NULL);
INSERT INTO `brands` VALUES (43, '/upload/brands/43.png', 'Mini', 'mini', 'Mini', 'mini', NULL, NULL);
INSERT INTO `brands` VALUES (44, '/upload/brands/44.png', 'Mitsubishi', 'mitsubishi', 'Mitsubishi', 'mitsubishi', NULL, NULL);
INSERT INTO `brands` VALUES (45, '/upload/brands/45.png', 'Nissan', 'nissan', 'Nissan', 'nissan', NULL, NULL);
INSERT INTO `brands` VALUES (46, '/upload/brands/46.png', 'Porsche', 'porsche', 'Porsche', 'porsche', NULL, NULL);
INSERT INTO `brands` VALUES (47, '/upload/brands/47.png', 'Suzuki', 'suzuki', 'Suzuki', 'suzuki', NULL, NULL);
INSERT INTO `brands` VALUES (48, '/upload/brands/48.png', 'Tesla', 'tesla', 'Tesla', 'tesla', NULL, NULL);
INSERT INTO `brands` VALUES (49, '/upload/brands/49.png', 'Volkswagen', 'volkswagen', 'Volkswagen', 'volkswagen', NULL, NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_class` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Citadine', 'tmb-31', NULL, NULL);
INSERT INTO `categories` VALUES (2, 'Compacte', 'tmb-20', NULL, NULL);
INSERT INTO `categories` VALUES (3, 'Berline', 'tmb-21', NULL, NULL);
INSERT INTO `categories` VALUES (4, 'Monospace', 'tmb-22', NULL, NULL);
INSERT INTO `categories` VALUES (5, 'Utilitaire léger', 'tmb-19', NULL, NULL);
INSERT INTO `categories` VALUES (6, '4x4 SUV', 'tmb-23', NULL, NULL);
INSERT INTO `categories` VALUES (7, 'Sportive Cabriolet', 'tmb-24', NULL, NULL);

-- ----------------------------
-- Table structure for category_articles
-- ----------------------------
DROP TABLE IF EXISTS `category_articles`;
CREATE TABLE `category_articles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category_articles
-- ----------------------------
INSERT INTO `category_articles` VALUES (1, 'salon', 'salon', '2018-07-26 10:58:53', '2018-07-26 10:58:56');
INSERT INTO `category_articles` VALUES (2, 'actu', 'actu', '2018-07-26 10:59:53', '2018-07-26 10:59:55');
INSERT INTO `category_articles` VALUES (3, 'interview', 'interview', '2018-07-26 11:00:20', '2018-07-26 11:00:23');

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contry_id` int(11) NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double(10, 7) NULL DEFAULT NULL,
  `latitude` double(10, 7) NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 405 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES (1, 1, 'Aïn Harrouda', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (2, 1, 'Ben Yakhlef', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (3, 1, 'Bouskoura', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (4, 1, 'Casablanca', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (5, 1, 'Médiouna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (6, 1, 'Mohammédia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (7, 1, 'Tit Mellil', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (8, 1, 'Ben Yakhlef', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (9, 1, 'Bejaâd', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (10, 1, 'Ben Ahmed', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (11, 1, 'Benslimane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (12, 1, 'Berrechid', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (13, 1, 'Boujniba', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (14, 1, 'Boulanouare', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (15, 1, 'Bouznika', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (16, 1, 'Deroua', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (17, 1, 'El Borouj', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (18, 1, 'El Gara', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (19, 1, 'Guisser', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (20, 1, 'Hattane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (21, 1, 'Khouribga', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (22, 1, 'Loulad', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (23, 1, 'Oued Zem', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (24, 1, 'Oulad Abbou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (25, 1, 'Oulad H\'Riz Sahel', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (26, 1, 'Oulad M\'rah', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (27, 1, 'Oulad Saïd', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (28, 1, 'Oulad Sidi Ben Daoud', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (29, 1, 'Ras El Aïn', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (30, 1, 'Settat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (31, 1, 'Sidi Rahhal Chataï', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (32, 1, 'Soualem', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (33, 1, 'Azemmour', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (34, 1, 'Bir Jdid', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (35, 1, 'Bouguedra', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (36, 1, 'Echemmaia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (37, 1, 'El Jadida', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (38, 1, 'Hrara', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (39, 1, 'Ighoud', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (40, 1, 'Jamâat Shaim', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (41, 1, 'Jorf Lasfar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (42, 1, 'Khemis Zemamra', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (43, 1, 'Laaounate', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (44, 1, 'Moulay Abdallah', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (45, 1, 'Oualidia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (46, 1, 'Oulad Amrane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (47, 1, 'Oulad Frej', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (48, 1, 'Oulad Ghadbane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (49, 1, 'Safi', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (50, 1, 'Sebt El Maârif', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (51, 1, 'Sebt Gzoula', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (52, 1, 'Sidi Ahmed', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (53, 1, 'Sidi Ali Ban Hamdouche', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (54, 1, 'Sidi Bennour', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (55, 1, 'Sidi Bouzid', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (56, 1, 'Sidi Smaïl', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (57, 1, 'Youssoufia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (58, 1, 'Fès', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (59, 1, 'Aïn Cheggag', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (60, 1, 'Bhalil', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (61, 1, 'Boulemane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (62, 1, 'El Menzel', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (63, 1, 'Guigou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (64, 1, 'Imouzzer Kandar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (65, 1, 'Imouzzer Marmoucha', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (66, 1, 'Missour', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (67, 1, 'Moulay Yaâcoub', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (68, 1, 'Ouled Tayeb', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (69, 1, 'Outat El Haj', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (70, 1, 'Ribate El Kheir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (71, 1, 'Séfrou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (72, 1, 'Skhinate', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (73, 1, 'Tafajight', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (74, 1, 'Arbaoua', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (75, 1, 'Aïn Dorij', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (76, 1, 'Dar Gueddari', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (77, 1, 'Had Kourt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (78, 1, 'Jorf El Melha', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (79, 1, 'Kénitra', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (80, 1, 'Khenichet', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (81, 1, 'Lalla Mimouna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (82, 1, 'Mechra Bel Ksiri', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (83, 1, 'Mehdia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (84, 1, 'Moulay Bousselham', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (85, 1, 'Sidi Allal Tazi', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (86, 1, 'Sidi Kacem', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (87, 1, 'Sidi Slimane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (88, 1, 'Sidi Taibi', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (89, 1, 'Sidi Yahya El Gharb', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (90, 1, 'Souk El Arbaa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (91, 1, 'Akka', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (92, 1, 'Assa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (93, 1, 'Bouizakarne', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (94, 1, 'El Ouatia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (95, 1, 'Es-Semara', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (96, 1, 'Fam El Hisn', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (97, 1, 'Foum Zguid', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (98, 1, 'Guelmim', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (99, 1, 'Taghjijt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (100, 1, 'Tan-Tan', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (101, 1, 'Tata', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (102, 1, 'Zag', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (103, 1, 'Marrakech', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (104, 1, 'Ait Daoud', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (115, 1, 'Amizmiz', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (116, 1, 'Assahrij', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (117, 1, 'Aït Ourir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (118, 1, 'Ben Guerir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (119, 1, 'Chichaoua', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (120, 1, 'El Hanchane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (121, 1, 'El Kelaâ des Sraghna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (122, 1, 'Essaouira', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (123, 1, 'Fraïta', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (124, 1, 'Ghmate', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (125, 1, 'Ighounane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (126, 1, 'Imintanoute', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (127, 1, 'Kattara', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (128, 1, 'Lalla Takerkoust', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (129, 1, 'Loudaya', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (130, 1, 'Lâattaouia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (131, 1, 'Moulay Brahim', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (132, 1, 'Mzouda', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (133, 1, 'Ounagha', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (134, 1, 'Sid L\'Mokhtar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (135, 1, 'Sid Zouin', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (136, 1, 'Sidi Abdallah Ghiat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (137, 1, 'Sidi Bou Othmane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (138, 1, 'Sidi Rahhal', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (139, 1, 'Skhour Rehamna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (140, 1, 'Smimou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (141, 1, 'Tafetachte', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (142, 1, 'Tahannaout', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (143, 1, 'Talmest', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (144, 1, 'Tamallalt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (145, 1, 'Tamanar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (146, 1, 'Tamansourt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (147, 1, 'Tameslouht', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (148, 1, 'Tanalt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (149, 1, 'Zeubelemok', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (150, 1, 'Meknès‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (151, 1, 'Khénifra', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (152, 1, 'Agourai', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (153, 1, 'Ain Taoujdate', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (154, 1, 'MyAliCherif', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (155, 1, 'Rissani', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (156, 1, 'Amalou Ighriben', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (157, 1, 'Aoufous', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (158, 1, 'Arfoud', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (159, 1, 'Azrou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (160, 1, 'Aïn Jemaa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (161, 1, 'Aïn Karma', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (162, 1, 'Aïn Leuh', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (163, 1, 'Aït Boubidmane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (164, 1, 'Aït Ishaq', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (165, 1, 'Boudnib', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (166, 1, 'Boufakrane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (167, 1, 'Boumia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (168, 1, 'El Hajeb', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (169, 1, 'Elkbab', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (170, 1, 'Er-Rich', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (171, 1, 'Errachidia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (172, 1, 'Gardmit', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (173, 1, 'Goulmima', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (174, 1, 'Gourrama', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (175, 1, 'Had Bouhssoussen', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (176, 1, 'Haj Kaddour', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (177, 1, 'Ifrane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (178, 1, 'Itzer', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (179, 1, 'Jorf', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (180, 1, 'Kehf Nsour', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (181, 1, 'Kerrouchen', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (182, 1, 'M\'haya', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (183, 1, 'M\'rirt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (184, 1, 'Midelt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (185, 1, 'Moulay Ali Cherif', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (186, 1, 'Moulay Bouazza', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (187, 1, 'Moulay Idriss Zerhoun', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (188, 1, 'Moussaoua', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (189, 1, 'N\'Zalat Bni Amar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (190, 1, 'Ouaoumana', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (191, 1, 'Oued Ifrane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (192, 1, 'Sabaa Aiyoun', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (193, 1, 'Sebt Jahjouh', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (194, 1, 'Sidi Addi', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (195, 1, 'Tichoute', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (196, 1, 'Tighassaline', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (197, 1, 'Tighza', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (198, 1, 'Timahdite', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (199, 1, 'Tinejdad', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (200, 1, 'Tizguite', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (201, 1, 'Toulal', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (202, 1, 'Tounfite', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (203, 1, 'Zaouia d\'Ifrane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (204, 1, 'Zaïda', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (205, 1, 'Ahfir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (206, 1, 'Aklim', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (207, 1, 'Al Aroui', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (208, 1, 'Aïn Bni Mathar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (209, 1, 'Aïn Erreggada', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (210, 1, 'Ben Taïeb', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (211, 1, 'Berkane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (212, 1, 'Bni Ansar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (213, 1, 'Bni Chiker', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (214, 1, 'Bni Drar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (215, 1, 'Bni Tadjite', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (216, 1, 'Bouanane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (217, 1, 'Bouarfa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (218, 1, 'Bouhdila', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (219, 1, 'Dar El Kebdani', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (220, 1, 'Debdou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (221, 1, 'Douar Kannine', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (222, 1, 'Driouch', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (223, 1, 'El Aïoun Sidi Mellouk', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (224, 1, 'Farkhana', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (225, 1, 'Figuig', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (226, 1, 'Ihddaden', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (227, 1, 'Jaâdar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (228, 1, 'Jerada', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (229, 1, 'Kariat Arekmane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (230, 1, 'Kassita', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (231, 1, 'Kerouna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (232, 1, 'Laâtamna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (233, 1, 'Madagh', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (234, 1, 'Midar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (235, 1, 'Nador', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (236, 1, 'Naima', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (237, 1, 'Oued Heimer', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (238, 1, 'Oujda', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (239, 1, 'Ras El Ma', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (240, 1, 'Saïdia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (241, 1, 'Selouane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (242, 1, 'Sidi Boubker', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (243, 1, 'Sidi Slimane Echcharaa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (244, 1, 'Talsint', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (245, 1, 'Taourirt', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (246, 1, 'Tendrara', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (247, 1, 'Tiztoutine', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (248, 1, 'Touima', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (249, 1, 'Touissit', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (250, 1, 'Zaïo', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (251, 1, 'Zeghanghane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (252, 1, 'Rabat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (253, 1, 'Salé', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (254, 1, 'Ain El Aouda', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (255, 1, 'Harhoura', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (256, 1, 'Khémisset', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (257, 1, 'Oulmès', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (258, 1, 'Rommani', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (259, 1, 'Sidi Allal El Bahraoui', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (260, 1, 'Sidi Bouknadel', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (261, 1, 'Skhirat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (262, 1, 'Tamesna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (263, 1, 'Témara', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (264, 1, 'Tiddas', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (265, 1, 'Tiflet', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (266, 1, 'Touarga', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (267, 1, 'Agadir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (268, 1, 'Agdz', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (269, 1, 'Agni Izimmer', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (270, 1, 'Aït Melloul', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (271, 1, 'Alnif', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (272, 1, 'Anzi', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (273, 1, 'Aoulouz', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (274, 1, 'Aourir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (275, 1, 'Arazane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (276, 1, 'Aït Baha', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (277, 1, 'Aït Iaâza', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (278, 1, 'Aït Yalla', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (279, 1, 'Ben Sergao', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (280, 1, 'Biougra', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (281, 1, 'Boualne-Dadès', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (282, 1, 'Dcheira El Jihadia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (283, 1, 'Drargua', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (284, 1, 'El Guerdane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (285, 1, 'Harte Lyamine', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (286, 1, 'Ida Ougnidif', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (287, 1, 'Ifri', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (288, 1, 'Igdamen', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (289, 1, 'Ighil n\'Oumgoun', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (290, 1, 'Imassine', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (291, 1, 'Inezgane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (292, 1, 'Irherm', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (293, 1, 'Kelaat-M\'Gouna', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (294, 1, 'Lakhsas', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (295, 1, 'Lakhsass', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (296, 1, 'Lqliâa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (297, 1, 'M\'semrir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (298, 1, 'Massa (Maroc)', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (299, 1, 'Megousse', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (300, 1, 'Ouarzazate', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (301, 1, 'Oulad Berhil', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (302, 1, 'Oulad Teïma', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (303, 1, 'Sarghine', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (304, 1, 'Sidi Ifni', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (305, 1, 'Skoura', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (306, 1, 'Tabounte', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (307, 1, 'Tafraout', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (308, 1, 'Taghzout', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (309, 1, 'Tagzen', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (310, 1, 'Taliouine', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (311, 1, 'Tamegroute', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (312, 1, 'Tamraght', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (314, 1, 'Taourirt ait zaghar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (315, 1, 'Taroudant', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (316, 1, 'Temsia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (317, 1, 'Tifnit', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (318, 1, 'Tisgdal', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (319, 1, 'Tiznit', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (320, 1, 'Toundoute', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (321, 1, 'Zagora', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (322, 1, 'Afourar', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (323, 1, 'Aghbala', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (324, 1, 'Azilal', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (325, 1, 'Aït Majden', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (326, 1, 'Beni Ayat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (327, 1, 'Béni Mellal', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (328, 1, 'Bin elouidane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (329, 1, 'Bradia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (330, 1, 'Bzou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (331, 1, 'Dar Oulad Zidouh', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (332, 1, 'Demnate', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (333, 1, 'Dra\'a', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (334, 1, 'El Ksiba', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (335, 1, 'Foum Jamaa', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (336, 1, 'Fquih Ben Salah', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (337, 1, 'Kasba Tadla', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (338, 1, 'Ouaouizeght', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (339, 1, 'Oulad Ayad', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (340, 1, 'Oulad M\'Barek', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (341, 1, 'Oulad Yaich', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (342, 1, 'Sidi Jaber', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (344, 1, 'Zaouïat Cheikh', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (345, 1, 'Tanger‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (346, 1, 'Tétouan‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (347, 1, 'Akchour', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (348, 1, 'Assilah', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (349, 1, 'Bab Berred', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (350, 1, 'Bab Taza', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (351, 1, 'Brikcha', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (352, 1, 'Chefchaouen', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (353, 1, 'Dar Bni Karrich', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (354, 1, 'Dar Chaoui', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (355, 1, 'Fnideq', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (356, 1, 'Gueznaia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (357, 1, 'Jebha', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (358, 1, 'Karia', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (359, 1, 'Khémis Sahel', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (360, 1, 'Ksar El Kébir', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (361, 1, 'Larache', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (362, 1, 'M\'diq', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (363, 1, 'Martil', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (364, 1, 'Moqrisset', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (365, 1, 'Oued Laou', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (366, 1, 'Oued Rmel', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (367, 1, 'Ouezzane', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (368, 1, 'Point Cires', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (369, 1, 'Sidi Lyamani', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (371, 1, 'Zinat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (372, 1, 'Ajdir‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (373, 1, 'Aknoul‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (374, 1, 'Al Hoceïma‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (375, 1, 'Aït Hichem‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (376, 1, 'Bni Bouayach‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (377, 1, 'Bni Hadifa‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (378, 1, 'Ghafsai‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (379, 1, 'Guercif‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (380, 1, 'Imzouren‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (381, 1, 'Inahnahen‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (382, 1, 'Issaguen (Ketama)‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (383, 1, 'Karia (El Jadida)‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (384, 1, 'Karia Ba Mohamed‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (385, 1, 'Oued Amlil‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (386, 1, 'Oulad Zbair‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (387, 1, 'Tahla‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (388, 1, 'Tala Tazegwaght‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (389, 1, 'Tamassint‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (390, 1, 'Taounate‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (391, 1, 'Targuist‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (392, 1, 'Taza‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (393, 1, 'Taïnaste‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (394, 1, 'Thar Es-Souk‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (395, 1, 'Tissa‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (396, 1, 'Tizi Ouasli‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (397, 1, 'Laayoune‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (398, 1, 'El Marsa‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (399, 1, 'Tarfaya‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (400, 1, 'Boujdour‎', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (401, 1, 'Awsard', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (402, 1, 'Oued-Eddahab ', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (403, 1, 'Stehat', NULL, NULL, 0, NULL, NULL);
INSERT INTO `cities` VALUES (404, 1, 'Aït Attab', NULL, NULL, 0, NULL, NULL);

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `body_pos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `body_neg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` int(11) NOT NULL,
  `article_id` int(11) NULL DEFAULT NULL,
  `video_id` int(11) NULL DEFAULT NULL,
  `comment_id` int(11) NULL DEFAULT NULL,
  `like` int(11) NOT NULL DEFAULT 0,
  `dislike` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES (1, 1, NULL, NULL, '22', 1, 22, NULL, NULL, 2, 2, '2018-08-01 10:04:07', '2018-08-02 10:42:53');
INSERT INTO `comments` VALUES (2, 1, NULL, NULL, '22', 1, 22, NULL, NULL, 1, 1, '2018-08-01 10:10:10', '2018-08-01 11:11:16');
INSERT INTO `comments` VALUES (3, 1, NULL, NULL, 'ohn Smith\r\nPublié le 20 juin 2018 à 6h57', 1, 22, NULL, NULL, 1, 1, '2018-08-01 10:12:03', '2018-08-01 11:11:21');
INSERT INTO `comments` VALUES (4, 1, NULL, NULL, 'hkhiik uhjkh', 1, 22, NULL, NULL, 1, 1, '2018-08-01 10:12:28', '2018-08-02 10:43:31');
INSERT INTO `comments` VALUES (5, 1, NULL, NULL, 'gfdgfdgdfgdfgh dfg dfgdf', 1, 22, NULL, NULL, 0, 0, '2018-08-01 10:14:12', '2018-08-01 10:14:12');
INSERT INTO `comments` VALUES (6, 1, NULL, NULL, 'fsdfsd gsd dsf sgsdgd', 1, 22, NULL, NULL, 1, 1, '2018-08-01 10:14:34', '2018-08-02 10:44:46');
INSERT INTO `comments` VALUES (7, 1, NULL, NULL, 'fsdfs sdfsdfsdgs', 1, 22, NULL, NULL, 0, 1, '2018-08-01 10:15:16', '2018-08-01 11:10:13');
INSERT INTO `comments` VALUES (8, 1, NULL, NULL, 'gf fgfghgfhfgdh hgfhgfhf', 1, 22, NULL, NULL, 0, 0, '2018-08-01 10:16:53', '2018-08-01 10:16:53');
INSERT INTO `comments` VALUES (9, 1, NULL, NULL, 'gdfg dfgdfg ddh hdfs', 1, 22, NULL, NULL, 0, 0, '2018-08-01 10:19:07', '2018-08-01 10:19:07');
INSERT INTO `comments` VALUES (10, 1, NULL, NULL, 'testttttttttttttttttttttt', 1, 22, NULL, NULL, 1, 1, '2018-08-01 10:19:16', '2018-08-01 11:15:20');
INSERT INTO `comments` VALUES (11, 1, NULL, NULL, 'frt reg dfg fhbsdfhb sdfg sfhdqh bdhbs', 1, 22, NULL, NULL, 1, 1, '2018-08-01 11:15:37', '2018-08-01 11:16:30');
INSERT INTO `comments` VALUES (12, 1, NULL, NULL, 'fdgfd sdgdfg sdg', 1, NULL, 7, NULL, 1, 1, '2018-08-01 11:29:51', '2018-08-01 11:30:37');
INSERT INTO `comments` VALUES (13, 1, NULL, NULL, 'gdgfds sdfgfsdgfsd', 1, 22, NULL, NULL, 0, 0, '2018-08-01 11:39:24', '2018-08-01 11:39:24');
INSERT INTO `comments` VALUES (14, 1, NULL, NULL, 'fdsf sdfsd fsdgh qer sd gqwdf', 1, 22, NULL, NULL, 0, 0, '2018-08-02 10:55:12', '2018-08-02 10:55:12');
INSERT INTO `comments` VALUES (15, 1, NULL, NULL, 'fsdf sdf sdgsfdg sdf', 1, 22, NULL, NULL, 0, 3, '2018-08-02 10:55:32', '2018-08-02 16:22:33');
INSERT INTO `comments` VALUES (16, 1, NULL, NULL, 'gujh gjgjhg jhgkgbh jhb', 1, 23, NULL, NULL, 1, 0, '2018-08-02 11:13:58', '2018-08-02 11:14:44');
INSERT INTO `comments` VALUES (17, 1, NULL, NULL, 'dgdfgdfg dgd', 1, 23, NULL, NULL, 0, 0, '2018-08-02 11:14:48', '2018-08-02 11:14:48');
INSERT INTO `comments` VALUES (18, 1, NULL, NULL, 'gfdgdf gdfg d', 1, 23, NULL, NULL, 0, 1, '2018-08-02 11:16:15', '2018-08-02 11:19:30');
INSERT INTO `comments` VALUES (19, 1, NULL, NULL, 'fdfds gfsdg sd', 1, 23, NULL, NULL, 1, 1, '2018-08-02 11:19:18', '2018-08-02 11:19:26');
INSERT INTO `comments` VALUES (20, 1, NULL, NULL, 'test zefsd fsdf dgsq', 1, 22, NULL, NULL, 1, 1, '2018-08-02 16:05:40', '2018-08-02 16:22:30');
INSERT INTO `comments` VALUES (21, 1, NULL, NULL, 'tydrytry drtyrtdu jdtr', 1, 22, NULL, NULL, 5, 1, '2018-08-02 16:15:28', '2018-08-02 16:31:05');
INSERT INTO `comments` VALUES (22, 1, NULL, NULL, '33333333333333333333', 1, 22, NULL, NULL, 3, 1, '2018-08-02 16:21:39', '2018-08-02 16:31:12');
INSERT INTO `comments` VALUES (23, 1, NULL, NULL, '33333333333333333333', 1, 22, NULL, NULL, 10, 0, '2018-08-02 16:21:41', '2018-08-02 16:32:05');
INSERT INTO `comments` VALUES (24, 1, NULL, NULL, '33333333333333333333', 1, 22, NULL, NULL, 1, 0, '2018-08-02 16:21:43', '2018-08-02 16:22:10');
INSERT INTO `comments` VALUES (25, 1, NULL, NULL, '33333333333333333333', 1, 22, NULL, NULL, 0, 0, '2018-08-02 16:21:45', '2018-08-02 16:21:45');
INSERT INTO `comments` VALUES (26, 1, NULL, NULL, 'fdgfhy fgh fgd jfg jdj', 2, 1, NULL, NULL, 0, 1, '2018-08-03 10:35:47', '2018-08-03 10:36:39');
INSERT INTO `comments` VALUES (27, 1, NULL, NULL, 'fdsf sdfgsd gfbh sdhndfg sdfhnf d', 2, 1, NULL, NULL, 1, 0, '2018-08-03 10:36:21', '2018-08-03 10:36:33');
INSERT INTO `comments` VALUES (28, 1, NULL, NULL, 'tgers tsert sertg', 2, 4, NULL, NULL, 0, 0, '2018-08-03 15:00:47', '2018-08-03 15:00:47');
INSERT INTO `comments` VALUES (29, 1, NULL, NULL, 'treter ertgrfghs rtsgh f sghsdf', 2, 4, NULL, NULL, 0, 0, '2018-08-03 15:00:52', '2018-08-03 15:00:52');
INSERT INTO `comments` VALUES (30, 1, NULL, NULL, 'g gfdhgfh srfgdf', 2, 4, NULL, NULL, 0, 0, '2018-08-03 15:00:58', '2018-08-03 15:00:58');
INSERT INTO `comments` VALUES (31, 1, NULL, NULL, 'fsdgfdgsdf', 2, NULL, 2, NULL, 0, 0, '2018-08-03 15:01:45', '2018-08-03 15:01:45');
INSERT INTO `comments` VALUES (32, 1, NULL, NULL, 'rt rtfdgrt hrtfhgdrt', 2, NULL, 2, NULL, 0, 0, '2018-08-03 15:01:51', '2018-08-03 15:01:51');

-- ----------------------------
-- Table structure for energies
-- ----------------------------
DROP TABLE IF EXISTS `energies`;
CREATE TABLE `energies`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_class` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of energies
-- ----------------------------
INSERT INTO `energies` VALUES (1, 'Essence ', 'tmb-26', NULL, NULL);
INSERT INTO `energies` VALUES (2, 'Diesel ', 'tmb-26', NULL, NULL);
INSERT INTO `energies` VALUES (3, 'Hybride', 'tmb-28', NULL, NULL);
INSERT INTO `energies` VALUES (4, ' Electrique ', 'tmb-27', NULL, NULL);

-- ----------------------------
-- Table structure for formulaires
-- ----------------------------
DROP TABLE IF EXISTS `formulaires`;
CREATE TABLE `formulaires`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vehicle_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('etreconnecte','alerte','test') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of formulaires
-- ----------------------------
INSERT INTO `formulaires` VALUES (1, '47', 'youssef7', 'HAMLILI7', '0658626243', 'yuceef.hamlili@gmail.com', 'etreconnecte', 1, '2018-07-31 13:41:52', '2018-07-31 13:41:52');
INSERT INTO `formulaires` VALUES (2, '47', 'youssef7', 'HAMLILI7', '0658626243', 'yuceef.hamlili@gmail.com', 'alerte', 1, '2018-07-31 13:50:55', '2018-07-31 13:50:55');

-- ----------------------------
-- Table structure for historics
-- ----------------------------
DROP TABLE IF EXISTS `historics`;
CREATE TABLE `historics`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NULL DEFAULT NULL,
  `model_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1001 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of historics
-- ----------------------------
INSERT INTO `historics` VALUES (1, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (2, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (3, 4, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (4, NULL, 8, NULL, NULL);
INSERT INTO `historics` VALUES (5, NULL, 3, NULL, NULL);
INSERT INTO `historics` VALUES (6, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (7, 4, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (8, 4, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (9, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (10, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (11, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (12, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (13, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (14, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (15, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (16, 4, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (17, 4, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (18, NULL, 6, NULL, NULL);
INSERT INTO `historics` VALUES (19, NULL, 6, NULL, NULL);
INSERT INTO `historics` VALUES (20, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (21, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (22, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (23, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (24, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (25, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (26, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (27, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (28, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (29, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (30, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (31, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (32, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (33, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (34, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (35, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (36, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (37, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (38, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (39, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (40, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (41, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (42, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (43, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (44, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (45, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (46, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (47, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (48, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (49, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (50, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (51, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (52, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (53, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (54, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (55, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (56, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (57, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (58, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (59, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (60, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (61, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (62, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (63, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (64, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (65, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (66, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (67, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (68, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (69, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (70, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (71, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (72, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (73, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (74, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (75, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (76, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (77, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (78, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (79, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (80, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (81, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (82, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (83, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (84, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (85, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (86, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (87, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (88, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (89, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (90, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (91, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (92, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (93, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (94, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (95, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (96, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (97, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (98, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (99, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (100, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (101, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (102, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (103, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (104, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (105, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (106, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (107, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (108, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (109, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (110, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (111, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (112, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (113, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (114, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (115, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (116, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (117, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (118, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (119, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (120, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (121, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (122, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (123, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (124, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (125, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (126, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (127, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (128, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (129, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (130, NULL, 3, NULL, NULL);
INSERT INTO `historics` VALUES (131, NULL, 3, NULL, NULL);
INSERT INTO `historics` VALUES (132, NULL, 3, NULL, NULL);
INSERT INTO `historics` VALUES (133, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (134, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (135, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (136, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (137, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (138, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (139, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (140, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (141, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (142, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (143, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (144, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (145, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (146, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (147, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (148, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (149, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (150, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (151, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (152, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (153, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (154, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (155, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (156, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (157, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (158, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (159, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (160, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (161, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (162, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (163, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (164, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (165, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (166, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (167, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (168, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (169, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (170, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (171, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (172, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (173, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (174, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (175, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (176, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (177, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (178, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (179, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (180, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (181, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (182, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (183, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (184, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (185, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (186, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (187, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (188, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (189, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (190, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (191, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (192, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (193, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (194, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (195, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (196, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (197, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (198, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (199, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (200, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (201, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (202, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (203, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (204, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (205, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (206, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (207, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (208, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (209, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (210, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (211, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (212, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (213, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (214, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (215, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (216, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (217, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (218, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (219, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (220, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (221, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (222, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (223, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (224, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (225, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (226, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (227, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (228, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (229, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (230, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (231, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (232, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (233, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (234, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (235, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (236, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (237, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (238, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (239, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (240, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (241, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (242, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (243, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (244, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (245, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (246, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (247, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (248, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (249, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (250, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (251, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (252, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (253, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (254, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (255, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (256, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (257, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (258, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (259, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (260, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (261, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (262, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (263, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (264, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (265, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (266, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (267, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (268, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (269, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (270, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (271, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (272, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (273, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (274, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (275, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (276, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (277, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (278, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (279, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (280, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (281, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (282, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (283, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (284, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (285, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (286, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (287, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (288, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (289, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (290, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (291, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (292, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (293, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (294, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (295, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (296, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (297, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (298, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (299, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (300, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (301, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (302, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (303, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (304, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (305, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (306, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (307, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (308, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (309, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (310, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (311, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (312, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (313, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (314, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (315, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (316, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (317, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (318, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (319, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (320, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (321, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (322, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (323, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (324, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (325, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (326, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (327, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (328, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (329, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (330, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (331, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (332, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (333, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (334, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (335, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (336, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (337, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (338, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (339, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (340, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (341, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (342, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (343, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (344, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (345, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (346, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (347, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (348, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (349, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (350, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (351, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (352, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (353, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (354, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (355, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (356, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (357, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (358, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (359, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (360, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (361, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (362, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (363, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (364, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (365, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (366, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (367, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (368, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (369, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (370, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (371, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (372, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (373, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (374, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (375, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (376, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (377, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (378, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (379, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (380, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (381, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (382, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (383, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (384, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (385, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (386, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (387, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (388, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (389, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (390, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (391, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (392, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (393, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (394, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (395, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (396, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (397, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (398, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (399, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (400, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (401, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (402, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (403, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (404, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (405, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (406, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (407, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (408, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (409, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (410, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (411, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (412, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (413, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (414, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (415, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (416, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (417, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (418, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (419, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (420, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (421, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (422, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (423, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (424, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (425, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (426, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (427, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (428, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (429, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (430, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (431, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (432, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (433, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (434, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (435, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (436, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (437, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (438, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (439, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (440, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (441, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (442, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (443, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (444, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (445, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (446, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (447, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (448, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (449, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (450, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (451, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (452, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (453, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (454, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (455, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (456, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (457, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (458, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (459, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (460, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (461, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (462, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (463, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (464, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (465, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (466, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (467, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (468, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (469, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (470, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (471, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (472, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (473, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (474, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (475, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (476, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (477, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (478, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (479, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (480, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (481, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (482, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (483, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (484, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (485, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (486, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (487, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (488, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (489, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (490, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (491, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (492, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (493, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (494, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (495, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (496, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (497, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (498, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (499, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (500, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (501, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (502, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (503, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (504, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (505, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (506, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (507, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (508, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (509, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (510, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (511, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (512, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (513, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (514, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (515, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (516, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (517, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (518, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (519, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (520, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (521, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (522, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (523, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (524, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (525, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (526, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (527, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (528, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (529, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (530, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (531, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (532, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (533, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (534, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (535, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (536, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (537, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (538, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (539, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (540, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (541, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (542, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (543, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (544, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (545, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (546, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (547, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (548, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (549, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (550, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (551, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (552, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (553, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (554, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (555, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (556, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (557, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (558, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (559, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (560, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (561, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (562, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (563, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (564, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (565, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (566, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (567, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (568, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (569, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (570, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (571, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (572, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (573, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (574, 3, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (575, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (576, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (577, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (578, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (579, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (580, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (581, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (582, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (583, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (584, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (585, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (586, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (587, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (588, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (589, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (590, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (591, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (592, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (593, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (594, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (595, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (596, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (597, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (598, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (599, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (600, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (601, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (602, NULL, 4, NULL, NULL);
INSERT INTO `historics` VALUES (603, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (604, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (605, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (606, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (607, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (608, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (609, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (610, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (611, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (612, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (613, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (614, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (615, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (616, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (617, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (618, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (619, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (620, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (621, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (622, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (623, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (624, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (625, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (626, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (627, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (628, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (629, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (630, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (631, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (632, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (633, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (634, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (635, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (636, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (637, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (638, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (639, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (640, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (641, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (642, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (643, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (644, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (645, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (646, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (647, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (648, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (649, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (650, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (651, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (652, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (653, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (654, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (655, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (656, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (657, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (658, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (659, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (660, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (661, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (662, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (663, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (664, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (665, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (666, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (667, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (668, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (669, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (670, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (671, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (672, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (673, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (674, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (675, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (676, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (677, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (678, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (679, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (680, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (681, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (682, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (683, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (684, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (685, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (686, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (687, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (688, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (689, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (690, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (691, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (692, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (693, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (694, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (695, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (696, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (697, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (698, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (699, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (700, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (701, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (702, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (703, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (704, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (705, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (706, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (707, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (708, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (709, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (710, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (711, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (712, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (713, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (714, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (715, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (716, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (717, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (718, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (719, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (720, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (721, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (722, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (723, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (724, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (725, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (726, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (727, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (728, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (729, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (730, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (731, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (732, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (733, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (734, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (735, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (736, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (737, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (738, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (739, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (740, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (741, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (742, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (743, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (744, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (745, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (746, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (747, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (748, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (749, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (750, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (751, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (752, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (753, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (754, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (755, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (756, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (757, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (758, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (759, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (760, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (761, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (762, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (763, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (764, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (765, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (766, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (767, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (768, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (769, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (770, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (771, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (772, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (773, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (774, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (775, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (776, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (777, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (778, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (779, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (780, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (781, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (782, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (783, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (784, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (785, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (786, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (787, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (788, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (789, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (790, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (791, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (792, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (793, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (794, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (795, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (796, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (797, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (798, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (799, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (800, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (801, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (802, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (803, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (804, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (805, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (806, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (807, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (808, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (809, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (810, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (811, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (812, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (813, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (814, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (815, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (816, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (817, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (818, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (819, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (820, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (821, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (822, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (823, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (824, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (825, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (826, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (827, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (828, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (829, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (830, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (831, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (832, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (833, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (834, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (835, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (836, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (837, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (838, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (839, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (840, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (841, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (842, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (843, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (844, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (845, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (846, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (847, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (848, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (849, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (850, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (851, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (852, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (853, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (854, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (855, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (856, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (857, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (858, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (859, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (860, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (861, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (862, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (863, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (864, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (865, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (866, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (867, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (868, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (869, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (870, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (871, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (872, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (873, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (874, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (875, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (876, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (877, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (878, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (879, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (880, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (881, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (882, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (883, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (884, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (885, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (886, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (887, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (888, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (889, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (890, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (891, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (892, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (893, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (894, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (895, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (896, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (897, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (898, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (899, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (900, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (901, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (902, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (903, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (904, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (905, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (906, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (907, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (908, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (909, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (910, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (911, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (912, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (913, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (914, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (915, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (916, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (917, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (918, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (919, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (920, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (921, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (922, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (923, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (924, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (925, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (926, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (927, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (928, 1, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (929, NULL, 1, NULL, NULL);
INSERT INTO `historics` VALUES (930, 4, NULL, NULL, NULL);
INSERT INTO `historics` VALUES (931, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (932, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (933, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (934, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (935, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (936, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (937, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (938, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (939, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (940, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (941, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (942, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (943, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (944, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (945, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (946, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (947, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (948, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (949, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (950, NULL, 7, NULL, NULL);
INSERT INTO `historics` VALUES (951, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (952, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (953, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (954, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (955, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (956, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (957, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (958, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (959, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (960, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (961, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (962, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (963, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (964, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (965, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (966, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (967, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (968, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (969, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (970, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (971, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (972, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (973, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (974, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (975, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (976, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (977, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (978, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (979, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (980, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (981, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (982, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (983, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (984, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (985, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (986, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (987, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (988, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (989, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (990, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (991, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (992, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (993, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (994, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (995, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (996, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (997, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (998, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (999, NULL, 2, NULL, NULL);
INSERT INTO `historics` VALUES (1000, NULL, 2, NULL, NULL);

-- ----------------------------
-- Table structure for media
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NULL DEFAULT NULL,
  `vehicle_id` int(11) NULL DEFAULT NULL,
  `article_id` int(11) NULL DEFAULT NULL,
  `path` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'image',
  `height` int(11) NULL DEFAULT NULL,
  `width` int(11) NULL DEFAULT NULL,
  `thumbnail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 602 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media
-- ----------------------------
INSERT INTO `media` VALUES (1, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (2, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (3, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (4, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (5, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (6, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (7, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (8, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (9, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (10, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (11, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (12, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (13, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (14, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (15, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (16, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (17, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (18, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (19, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (20, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (21, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (22, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (23, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (24, NULL, 35, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (25, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (26, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (27, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (28, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (29, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (30, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (31, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (32, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (33, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (34, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (35, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (36, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (37, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (38, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (39, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (40, NULL, 42, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (41, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (42, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (43, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (44, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (45, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (46, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (47, NULL, 35, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (48, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (49, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (50, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (51, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (52, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (53, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (54, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (55, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (56, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (57, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (58, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (59, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (60, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (61, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (62, NULL, 51, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (63, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (64, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (65, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (66, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (67, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (68, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (69, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (70, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (71, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (72, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (73, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (74, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (75, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (76, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (77, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (78, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (79, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (80, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (81, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (82, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (83, NULL, 59, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (84, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (85, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (86, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (87, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (88, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (89, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (90, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (91, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (92, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (93, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (94, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (95, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (96, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (97, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (98, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (99, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (100, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (101, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (102, NULL, 59, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (103, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (104, NULL, 60, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (105, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (106, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (107, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (108, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (109, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (110, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (111, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (112, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (113, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (114, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (115, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (116, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (117, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (118, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (119, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (120, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (121, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (122, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (123, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (124, NULL, 60, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (125, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (126, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (127, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (128, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (129, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (130, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (131, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (132, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (133, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (134, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (135, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (136, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (137, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (138, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (139, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (140, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (141, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (142, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (143, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (144, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (145, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (146, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (147, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (148, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (149, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (150, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (151, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (152, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (153, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (154, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (155, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (156, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (157, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (158, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (159, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (160, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (161, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (162, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (163, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (164, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (165, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (166, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (167, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (168, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (169, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (170, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (171, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (172, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (173, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (174, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (175, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (176, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (177, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (178, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (179, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (180, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (181, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (182, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (183, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (184, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (185, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (186, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (187, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (188, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (189, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (190, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (191, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (192, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (193, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (194, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (195, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (196, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (197, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (198, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (199, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (200, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (201, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (202, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (203, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (204, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (205, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (206, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (207, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (208, NULL, 59, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (209, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (210, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (211, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (212, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (213, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (214, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (215, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (216, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (217, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (218, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (219, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (220, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (221, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (222, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (223, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (224, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (225, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (226, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (227, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (228, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (229, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (230, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (231, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (232, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (233, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (234, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (235, NULL, 35, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (236, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (237, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (238, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (239, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (240, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (241, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (242, NULL, 60, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (243, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (244, NULL, 42, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (245, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (246, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (247, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (248, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (249, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (250, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (251, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (252, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (253, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (254, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (255, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (256, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (257, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (258, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (259, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (260, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (261, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (262, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (263, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (264, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (265, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (266, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (267, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (268, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (269, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (270, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (271, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (272, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (273, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (274, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (275, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (276, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (277, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (278, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (279, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (280, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (281, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (282, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (283, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (284, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (285, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (286, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (287, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (288, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (289, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (290, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (291, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (292, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (293, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (294, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (295, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (296, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (297, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (298, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (299, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (300, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (301, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (302, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (303, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (304, NULL, 42, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (305, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (306, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (307, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (308, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (309, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (310, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (311, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (312, NULL, 35, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (313, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (314, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (315, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (316, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (317, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (318, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (319, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (320, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (321, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (322, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (323, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (324, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (325, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (326, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (327, NULL, 8, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (328, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (329, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (330, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (331, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (332, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (333, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (334, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (335, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (336, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (337, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (338, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (339, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (340, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (341, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (342, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (343, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (344, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (345, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (346, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (347, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (348, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (349, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (350, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (351, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (352, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (353, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (354, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (355, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (356, NULL, 42, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (357, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (358, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (359, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (360, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (361, NULL, 59, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (362, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (363, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `media` VALUES (364, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (365, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (366, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (367, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (368, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (369, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (370, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (371, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (372, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (373, NULL, 60, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (374, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (375, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (376, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (377, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (378, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (379, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (380, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (381, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (382, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (383, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (384, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (385, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (386, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (387, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (388, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (389, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (390, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (391, NULL, 51, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (392, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (393, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (394, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (395, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (396, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (397, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (398, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (399, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (400, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (401, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (402, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (403, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (404, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (405, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (406, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (407, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (408, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (409, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (410, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (411, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (412, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (413, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (414, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (415, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (416, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (417, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (418, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (419, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (420, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (421, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (422, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (423, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (424, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (425, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (426, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (427, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (428, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (429, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (430, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (431, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (432, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (433, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (434, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (435, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (436, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (437, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (438, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (439, NULL, 26, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (440, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (441, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (442, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (443, NULL, 8, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (444, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (445, NULL, 23, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (446, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (447, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (448, NULL, 34, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (449, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (450, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (451, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (452, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (453, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (454, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (455, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (456, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (457, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (458, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (459, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (460, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (461, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (462, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (463, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (464, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (465, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (466, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (467, NULL, 51, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (468, NULL, 21, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (469, NULL, 59, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (470, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (471, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (472, NULL, 29, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (473, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (474, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (475, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (476, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (477, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (478, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (479, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (480, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (481, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (482, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (483, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (484, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (485, NULL, 39, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (486, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (487, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (488, NULL, 52, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (489, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (490, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (491, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (492, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (493, NULL, 5, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (494, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (495, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (496, NULL, 2, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (497, NULL, 51, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (498, NULL, 20, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (499, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (500, NULL, 41, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (501, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (502, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (503, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (504, NULL, 22, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (505, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (506, NULL, 37, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (507, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (508, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (509, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (510, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (511, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (512, NULL, 43, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (513, NULL, 38, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (514, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (515, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (516, NULL, 56, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (517, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (518, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (519, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (520, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (521, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (522, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (523, NULL, 45, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (524, NULL, 57, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (525, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (526, NULL, 35, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (527, NULL, 42, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (528, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (529, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (530, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (531, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (532, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (533, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (534, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (535, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (536, NULL, 18, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (537, NULL, 47, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (538, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (539, NULL, 25, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (540, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (541, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (542, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (543, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (544, NULL, 49, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (545, NULL, 50, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (546, NULL, 59, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (547, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (548, NULL, 28, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (549, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (550, NULL, 17, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (551, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (552, NULL, 11, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (553, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (554, NULL, 9, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (555, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (556, NULL, 48, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (557, NULL, 19, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (558, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (559, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (560, NULL, 7, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (561, NULL, 42, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (562, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (563, NULL, 32, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (564, NULL, 58, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (565, NULL, 13, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (566, NULL, 6, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (567, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (568, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (569, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (570, NULL, 1, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (571, NULL, 36, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (572, NULL, 40, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (573, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (574, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (575, NULL, 46, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (576, NULL, 60, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (577, NULL, 54, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (578, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (579, NULL, 3, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (580, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (581, NULL, 4, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (582, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (583, NULL, 30, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (584, NULL, 24, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (585, NULL, 8, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (586, NULL, 12, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (587, NULL, 53, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (588, NULL, 8, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (589, NULL, 60, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (590, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (591, NULL, 31, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (592, NULL, 44, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (593, NULL, 27, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (594, NULL, 55, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (595, NULL, 14, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (596, NULL, 51, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (597, NULL, 10, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (598, NULL, 16, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (599, NULL, 33, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (600, NULL, 15, NULL, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-18 09:40:11', '2018-07-18 09:40:11');
INSERT INTO `media` VALUES (601, NULL, NULL, 1, 'https://loremflickr.com/640/480/voiture', 'image', NULL, NULL, NULL, '2018-07-26 10:00:57', '2018-07-26 10:00:57');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_07_10_163926_create_vehicles_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_07_10_165115_create_promotions_table', 1);
INSERT INTO `migrations` VALUES (5, '2018_07_11_100835_create_models_table', 1);
INSERT INTO `migrations` VALUES (6, '2018_07_11_101421_create_brands_table', 1);
INSERT INTO `migrations` VALUES (7, '2018_07_11_101903_create_historics_table', 1);
INSERT INTO `migrations` VALUES (8, '2018_07_11_102217_create_media_table', 1);
INSERT INTO `migrations` VALUES (9, '2018_07_12_090239_create_verify_users_table', 1);
INSERT INTO `migrations` VALUES (10, '2018_07_12_155326_create_categories_table', 1);
INSERT INTO `migrations` VALUES (11, '2018_07_16_090711_create_energies_table', 1);
INSERT INTO `migrations` VALUES (12, '2018_07_17_100050_create_cities_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_07_16_141336_create_videos_table', 3);
INSERT INTO `migrations` VALUES (14, '2018_07_11_103434_create_category_articles_table', 2);
INSERT INTO `migrations` VALUES (28, '2014_10_12_000000_create_users_table', 8);
INSERT INTO `migrations` VALUES (29, '2018_07_20_113807_create_articles_table', 8);
INSERT INTO `migrations` VALUES (22, '2018_07_31_115150_create_etreconnectes_table', 5);
INSERT INTO `migrations` VALUES (23, '2018_07_31_115150_create_formulaires_table', 6);
INSERT INTO `migrations` VALUES (26, '2018_08_01_091918_create_comments_table', 7);

-- ----------------------------
-- Table structure for models
-- ----------------------------
DROP TABLE IF EXISTS `models`;
CREATE TABLE `models`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('voiture','moto','camion','equipement') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `categorie_id` int(11) NULL DEFAULT NULL,
  `brand_id` int(11) NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 50 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of models
-- ----------------------------
INSERT INTO `models` VALUES (49, 'voiture', 3, 27, '/uploadmodels/UPERFAST', 'UPERFAST', 'UPERFAST', 'UPERFAST', NULL, NULL);
INSERT INTO `models` VALUES (48, 'voiture', 6, 31, '/uploadmodels/Grand Santa Fe', 'Grand Santa Fe', 'Grand Santa Fe', 'Grand Santa Fe', NULL, NULL);
INSERT INTO `models` VALUES (47, 'voiture', 5, 31, '/uploadmodels/H1', 'H1', 'H1', 'H1', NULL, NULL);
INSERT INTO `models` VALUES (45, 'voiture', 3, 6, '/uploadmodels/A4', 'A4', 'A4', 'A4', NULL, NULL);
INSERT INTO `models` VALUES (46, 'voiture', 3, 28, '/uploadmodels/Tipo', 'Tipo', 'Tipo', 'Tipo', NULL, NULL);
INSERT INTO `models` VALUES (44, 'voiture', 3, 6, '/uploadmodels/A3', 'A3', 'A3', 'A3', NULL, NULL);
INSERT INTO `models` VALUES (19, 'voiture', 6, 4, '/uploadmodels/GLE', 'GLE', 'GLE', 'GLE', NULL, NULL);
INSERT INTO `models` VALUES (43, 'voiture', 7, 6, '/uploadmodels/S1', 'S1', 'S1', 'S1', NULL, NULL);
INSERT INTO `models` VALUES (18, 'voiture', 7, 1, '/uploadmodels/X6', 'X6', 'X6', 'X6', NULL, NULL);
INSERT INTO `models` VALUES (17, 'voiture', 7, 1, '/uploadmodels/X5', 'X5', 'X5', 'X5', NULL, NULL);
INSERT INTO `models` VALUES (16, 'voiture', 7, 1, '/uploadmodels/X2', 'X2', 'X2', 'X2', NULL, NULL);
INSERT INTO `models` VALUES (15, 'voiture', 2, 1, '/uploadmodels/Série 1', 'Série 1', 'Série 1', 'Série 1', NULL, NULL);
INSERT INTO `models` VALUES (14, 'voiture', 4, 1, '/upload/models/ 225xe iPerformance .jpg', ' 225xe iPerformance ', ' 225xe iPerformance ', ' 225xe iPerformance ', NULL, NULL);
INSERT INTO `models` VALUES (13, 'voiture', 4, 1, '/upload/models/Série 2.jpg', 'Série 2', 'Série 2', 'Série 2', NULL, NULL);
INSERT INTO `models` VALUES (11, 'voiture', 6, 1, '/upload/models/X3.jpg', 'X3', 'X3', 'X3', NULL, NULL);
INSERT INTO `models` VALUES (12, 'voiture', 6, 1, '/upload/models/X5.jpg', 'X5', 'X5', 'X5', NULL, NULL);
INSERT INTO `models` VALUES (10, 'voiture', 6, 1, '/upload/models/X1.jpg', 'X1', 'X1', 'X1', NULL, NULL);
INSERT INTO `models` VALUES (9, 'voiture', 3, 1, '/upload/models/M5.jpg', 'M5', 'M5', 'M5', NULL, NULL);
INSERT INTO `models` VALUES (8, 'voiture', 3, 1, '/upload/models/M3.jpg', 'M3', 'M3', 'M3', NULL, NULL);
INSERT INTO `models` VALUES (7, 'voiture', 3, 1, '/upload/models/Série 3.jpg', 'Série 3', 'Série 3', 'Série 3', NULL, NULL);
INSERT INTO `models` VALUES (6, 'voiture', 4, 4, '/upload/models/Classe V.jpg', 'Classe V', 'Classe V\r\nClasse V\r\nClasse V', 'Classe V', NULL, NULL);
INSERT INTO `models` VALUES (5, 'voiture', 3, 4, '/upload/models/Classe A.jpg', 'Classe A', 'Classe A', 'Classe A', NULL, NULL);
INSERT INTO `models` VALUES (4, 'voiture', 1, 3, '/upload/models/Sandero.jpg', 'Sandero', 'Sandero', 'Sandero', NULL, NULL);
INSERT INTO `models` VALUES (2, 'voiture', 6, 3, '/upload/models/Duster.jpg', 'Duster', 'Duster', 'Duster', NULL, NULL);
INSERT INTO `models` VALUES (3, 'voiture', 4, 3, '/upload/models/Lodgy.jpg', 'Lodgy', 'Lodgy', 'Lodgy', NULL, NULL);
INSERT INTO `models` VALUES (1, 'voiture', 4, 3, '/upload/models/Dokker.jpg', 'Dokker', 'Dokker', 'Dokker', NULL, NULL);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('yuceef.hamlili@gmail.comm', '$2y$10$u9krMfGsZ.MEgxy/jaKCTegFNuowfgyLw/xgr2j/bthcQglPTWcka', '2018-08-02 17:16:06');

-- ----------------------------
-- Table structure for promotions
-- ----------------------------
DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `percentage` double(4, 2) NOT NULL,
  `start_at` datetime(0) NOT NULL,
  `end_at` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of promotions
-- ----------------------------
INSERT INTO `promotions` VALUES (1, 3, 'libero', 10.70, '2018-06-28 06:57:19', '2018-09-07 11:53:12', '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `promotions` VALUES (4, 1, 'Edebitis.', 34.80, '2018-07-17 04:00:32', '2018-10-09 08:16:44', '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `promotions` VALUES (5, 5, 'Neque cumque dolore autem libero ', 15.90, '2018-07-19 07:18:01', '2018-08-21 15:39:20', '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `promotions` VALUES (8, 6, 'Ut nisiipsam.', 9.70, '2018-07-01 15:19:44', '2018-10-05 21:06:38', '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `promotions` VALUES (13, 19, 'Sit aut qui at et nam necessitatibus.', 27.00, '2018-07-06 16:08:59', '2018-08-26 00:33:21', '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `promotions` VALUES (14, 11, 'Autem consequatur expedita eos sunt.', 39.20, '2018-06-18 16:37:53', '2018-08-23 05:58:35', '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `promotions` VALUES (19, 19, 'Qui eos aliquid nihil recusandae.', 21.70, '2018-06-25 21:54:16', '2018-09-30 07:23:53', '2018-07-18 09:40:10', '2018-07-18 09:40:10');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `garage` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `civility` enum('Mr','Mlle','Madame') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NULL DEFAULT NULL,
  `postal` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `fixe` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_type` enum('particulier','professionnel') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `rating` int(11) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_garage_unique`(`garage`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (2, 'youssef', NULL, 'yuceef.hamlili@gmail.com', '$2y$10$7nVmF/NmklvqaiF6Wh4QO.62qRPa/z.bfVJtyxpevfqmOwzfnhXsu', 'Mr', 'yuceef', 'hamlili', '2018-08-03', NULL, NULL, 'particulier', '0658626243', 'fsdf dfsdf sdqg sdg s', 'gsdfgsdgfsd', 'gdsgsd', 0, 0, 1, '2018-08-03 13:22:07', 'a5mpmlEJn5bTfSFba2CECmnYRtLUvg14KOePHOw4VR8pBZ7twLht4KgKa8IF', '2018-08-03 10:26:26', '2018-08-03 13:22:07', NULL);
INSERT INTO `users` VALUES (3, 'nabil', NULL, 'nabil.mabrouk3@gmail.com', '$2y$10$9x05yIP1Kvs1XPfaKc55deQ3un9GLmi.uWghdv9Yb8GXFP2Cv/tzG', 'Mr', 'nabil', 'mabrouk', '1992-03-22', NULL, NULL, 'particulier', '0623389941', 'N70 BD IBN AADADA HAY RAKBOUTE SIDI MOUMEN', 'BENSLIMANE', 'maroc', 0, 0, 1, '2018-08-03 15:49:33', 'zNzpWHkPLSjCm2cdRqdTvnLorUBq3j9q26sFYizwa812p3uayQpGHJxQjID2', '2018-08-03 14:27:56', '2018-08-03 15:49:33', NULL);

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `model_id` int(11) NULL DEFAULT NULL,
  `energie_id` int(11) NULL DEFAULT NULL,
  `price` float NOT NULL,
  `transmission` enum('automatique','manuelle') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `isNeuf` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `immatriculation` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `rating` int(11) NULL DEFAULT NULL,
  `ad_id` int(11) NULL DEFAULT NULL,
  `city_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  `nbrprop` int(11) NULL DEFAULT NULL,
  `length` int(11) NULL DEFAULT NULL,
  `width` int(11) NULL DEFAULT NULL,
  `height` int(11) NULL DEFAULT NULL,
  `wheelbase` int(11) NULL DEFAULT NULL,
  `numberOfDoors` int(11) NULL DEFAULT NULL,
  `numberOfPlaces` int(11) NULL DEFAULT NULL,
  `trunkVolume` int(11) NULL DEFAULT NULL,
  `unloadedWeight` int(11) NULL DEFAULT NULL,
  `loadedWeight` int(11) NULL DEFAULT NULL,
  `frontTireWidth` int(11) NULL DEFAULT NULL,
  `frontTire` int(11) NULL DEFAULT NULL,
  `frontTiresManufacturingCode` int(11) NULL DEFAULT NULL,
  `diameterFrontWheels` int(11) NULL DEFAULT NULL,
  `maximumSpeedAllowedFrontTires` int(11) NULL DEFAULT NULL,
  `rearTire` int(11) NULL DEFAULT NULL,
  `rearTireWidthHeightRatio` int(11) NULL DEFAULT NULL,
  `rearTireManufacturingCode` int(11) NULL DEFAULT NULL,
  `diameterRearWheels` int(11) NULL DEFAULT NULL,
  `maximumSpeedAllowedRearTires` int(11) NULL DEFAULT NULL,
  `engine` int(11) NULL DEFAULT NULL,
  `cylinder` int(11) NULL DEFAULT NULL,
  `reports` int(11) NULL DEFAULT NULL,
  `frame` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `fiscalPower` int(11) NULL DEFAULT NULL,
  `realPower` int(11) NULL DEFAULT NULL,
  `gearbox` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `numberOfSpeeds` int(11) NULL DEFAULT NULL,
  `origine` int(11) NULL DEFAULT NULL,
  `numberOfOwners` int(11) NULL DEFAULT NULL,
  `aerodynamics` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `valves` int(11) NULL DEFAULT NULL,
  `engineCouple` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `traction` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `consumptionRoad` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `consumptionCity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `mixedConsumption` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `distanceWidthFull` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `maximumSpeed` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `acceleration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `color` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `waranty` int(11) NULL DEFAULT NULL,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `mileage` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `turningDiameter` int(11) NULL DEFAULT NULL,
  `kilometerRepeated` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `emissionCarbonDioxide` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `emissionHydrocarbonParticles` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `particleEmission` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `normeAntiPollution` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `placeManufacture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `interiorDesign` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `interiorColor` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `exteriorColor` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cylinderNumber` int(11) NULL DEFAULT NULL,
  `reservoirCapacity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `recommandation` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `vehicles_slug_unique`(`slug`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES (59, 'schneider.cyril', 'https://loremflickr.com/640/480/voiture', 'quaerat-sint-laborum-nulla-sapiente-quas-laudantium', 'Deserunt soluta ut voluptas ut molestiae. Sit eveniet suscipit rerum sint perspiciatis asperiores eaque maxime. Minima natus unde consequatur neque quia. Perferendis veritatis et velit dolore suscipit cumque nesciunt est.', 6, 2, 981.12, 'automatique', 0, 1, '274-760-8989 x603', 6, NULL, 6, NULL, 2019, 6, 7, 8290, 876038, 6, 1, 7, 2, 1, 8, 1, 8, 7, 6, 8, 7, 3, 0, 9, 2, 9, 4, NULL, '49', 7, 63, '1', 4, 0, 5, '9', 6, '3', '3', '4', '8', '5', '9', '9', '5', 'Rouge', 1, '4', 1, 8, 1, '8', '2', '3', '6', '1', '1', 'Cuir', 'Bleu', 'Orange', 1, '4', 1, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (58, 'janet18', 'https://loremflickr.com/640/480/voiture', 'occaecati-eligendi-reiciendis-totam-et-doloremque', 'Qui eaque et est vitae qui aliquam tenetur. Quaerat atque suscipit sed velit enim. Aut tempora id hic. Deserunt optio debitis voluptatem architecto quis. Facere iusto quia numquam ipsa.', 3, 3, 8.42, 'manuelle', 0, 1, '(837) 923-6580', 7, NULL, 7, NULL, 2019, 7, 676, 9682, 672, 0, 9, 1, 7, 1, 5, 5, 7, 3, 9, 4, 5, 4, 7, 3, 1, 8, 4, NULL, '429540886', 1, 283189733, '6', 8, 0, 9, '2', 2, '8', '1', '9', '2', '0', '3', '6', '2', 'Bleu', 7, '1', 6, 6, 1, '9', '2', '4', '3', '8', '0', 'Velours', 'Orange', 'Vert', 8, '4', 2, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (57, 'madie88', 'https://loremflickr.com/640/480/voiture', 'voluptatem-blanditiis-fuga-qui-saepe', 'Ut ipsa eum vel quidem. Qui molestiae velit id sit. Numquam officiis placeat vitae.', 13, 1, 451391, 'manuelle', 0, 1, '809.580.4872 x6416', 8, NULL, 8, NULL, 2019, 8, 1275431, 375635, 66, 5, 1, 8, 0, 7, 1, 2, 6, 5, 6, 0, 3, 5, 0, 6, 6, 0, 3, NULL, '60133', 8, 88419782, '0', 9, 0, 6, '6', 5, '1', '1', '5', '4', '0', '2', '3', '6', 'Gris', 8, '8', 8, 3, 8, '3', '3', '5', '5', '8', '4', 'Velours', 'Beige', 'Noir', 5, '1', 5, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (56, 'esther31', 'https://loremflickr.com/640/480/voiture', 'quasi-eum-nisi-recusandae-dolores-sint-voluptas', 'Delectus ut eveniet id quae est. Explicabo iusto architecto non doloremque quisquam blanditiis eum. Qui facere ut reiciendis voluptas at dolorum qui. Dolores saepe nemo maxime et voluptatem ut et.', 9, 3, 5861.04, 'manuelle', 1, 1, '1-591-530-0897', 7, NULL, 7, NULL, 2018, 7, 88, 60, 42, 3, 2, 4, 7, 1, 0, 5, 5, 0, 8, 0, 9, 0, 1, 7, 6, 5, 8, NULL, '4511', 3, 6759710, '1', 4, 0, 6, '2', 6, '2', '1', '3', '6', '8', '7', '1', '6', 'Orange', 2, '2', 1, 4, 0, '9', '0', '2', '3', '9', '5', 'Cuir', 'Noir', 'Rouge', 6, '0', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (55, 'janice31', 'https://loremflickr.com/640/480/voiture', 'nisi-maxime-sit-eaque-sunt-ut-ut-occaecati', 'Assumenda aut quod sunt eos natus molestias molestias. Ut voluptate ex quae commodi velit omnis eum. Asperiores cum blanditiis molestias autem omnis eius. Maiores cupiditate sunt facere enim cum et animi. Quos quo nisi nobis eos dolorem iusto.', 2, 3, 78009, 'manuelle', 1, 1, '640.525.2125', 4, NULL, 4, NULL, 2018, 4, 27632, 807105, 4966361, 3, 4, 7, 9, 4, 5, 9, 6, 4, 2, 1, 8, 5, 1, 4, 4, 2, 4, NULL, '756', 2, 345037009, '6', 4, 0, 1, '0', 3, '5', '7', '1', '1', '7', '9', '1', '9', 'Bleu', 0, '1', 1, 2, 4, '1', '3', '5', '7', '7', '4', 'Velours', 'Gris', 'Rouge', 4, '6', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (54, 'kbernier', 'https://loremflickr.com/640/480/voiture', 'repellendus-ipsam-officiis-ducimus', 'Consequuntur vel dolorem dicta pariatur dolor. Nostrum et placeat harum aspernatur eos sit dolor.', 6, 2, 561189, 'manuelle', 0, 1, '905-986-6177', 8, NULL, 8, NULL, 2019, 8, 2643390, 9256, 17378, 7, 7, 0, 3, 7, 5, 7, 1, 0, 8, 9, 6, 2, 2, 4, 3, 3, 0, NULL, '5959672', 9, 9550651, '4', 7, 0, 8, '6', 3, '1', '0', '2', '4', '8', '2', '4', '3', 'Noir', 5, '7', 8, 6, 3, '0', '6', '1', '8', '7', '4', 'Velours', 'Noir', 'Gris', 5, '1', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (53, 'waldo26', 'https://loremflickr.com/640/480/voiture', 'aut-ratione-ea-recusandae-perspiciatis-sint', 'Est cupiditate amet quia facilis nostrum. Accusamus esse veniam excepturi est quisquam et. Voluptas autem distinctio sed eos fuga tempore.', 17, 4, 1.01, 'manuelle', 1, 1, '930-865-2435 x45495', 1, NULL, 1, NULL, 2018, 1, 60, 51050460, 9000327, 4, 3, 7, 1, 0, 3, 4, 7, 5, 8, 0, 6, 7, 5, 7, 0, 5, 5, NULL, '4887505', 0, 732846837, '6', 4, 0, 9, '6', 8, '6', '0', '1', '2', '6', '7', '7', '9', 'Beige', 9, '7', 8, 6, 8, '1', '4', '7', '1', '7', '2', 'Velours', 'Bleu', 'Vert', 6, '3', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (52, 'harris.heloise', 'https://loremflickr.com/640/480/voiture', 'impedit-voluptatem-a-reprehenderit-ratione', 'Natus officia fugit quae. Aut corporis molestiae sed. Quisquam nulla tenetur et cupiditate rerum blanditiis nemo.', 19, 3, 229645, 'manuelle', 1, 1, '770.289.3792', 7, NULL, 7, NULL, 2018, 7, 745, 6, 397839, 5, 3, 0, 5, 3, 0, 1, 6, 9, 4, 2, 8, 7, 4, 8, 6, 8, 9, NULL, '421646139', 5, 824662080, '6', 2, 1, 3, '0', 8, '3', '1', '4', '2', '9', '1', '8', '9', 'Orange', 9, '6', 1, 3, 4, '1', '6', '7', '4', '7', '1', 'Velours', 'Noir', 'Noir', 8, '4', 8, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (51, 'orn.cristopher', 'https://loremflickr.com/640/480/voiture', 'sapiente-tenetur-aut-cum-ea-totam', 'Omnis ut est sit. Et iusto sapiente magnam perferendis. Rerum dignissimos eum itaque dolor iusto. Dolores tenetur ipsam enim odio et. Alias non quis error.', 3, 4, 76785.1, 'automatique', 0, 1, '571.580.9567 x7916', 6, NULL, 6, NULL, 2019, 6, 88, 43687589, 289928433, 6, 1, 8, 7, 5, 6, 8, 9, 6, 1, 8, 9, 8, 9, 1, 6, 0, 4, NULL, '9', 2, 17518659, '6', 7, 0, 5, '1', 8, '9', '1', '4', '0', '3', '5', '9', '3', 'Noir', 7, '2', 8, 6, 2, '7', '8', '9', '5', '3', '7', 'Tissu', 'Gris', 'Bleu', 8, '4', 3, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (50, 'emilie.dare', 'https://loremflickr.com/640/480/voiture', 'voluptatibus-eos-pariatur-ut', 'Mollitia doloribus aut reiciendis aut. Qui in consequatur sed qui sed id facere. Omnis commodi inventore at enim magnam consectetur voluptate id. Et est aut autem incidunt blanditiis.', 4, 2, 330.9, 'automatique', 0, 1, '269.533.2306', 9, NULL, 9, NULL, 2019, 9, 8, 5, 31350187, 3, 1, 6, 3, 4, 9, 6, 8, 0, 6, 0, 1, 4, 2, 8, 4, 1, 6, NULL, '2', 3, 156510, '0', 5, 1, 5, '3', 7, '4', '6', '9', '4', '3', '9', '4', '4', 'Gris', 2, '2', 0, 6, 9, '7', '8', '6', '2', '9', '6', 'Cuir', 'Noir', 'Gris', 7, '2', 0, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (49, 'hortense.stehr', 'https://loremflickr.com/640/480/voiture', 'reiciendis-veniam-odio-ad-et-provident-tempore-id', 'Laborum praesentium culpa unde officia dolorem quos consequatur. Debitis sint ipsam eum in est consequuntur quaerat et.', 18, 2, 3662.19, 'manuelle', 0, 1, '1-854-468-8213 x7443', 1, NULL, 1, NULL, 2019, 1, 4965, 138, 249364833, 1, 5, 2, 6, 0, 9, 5, 7, 2, 0, 5, 1, 2, 6, 0, 1, 4, 5, NULL, '7137', 8, 4, '6', 3, 0, 6, '2', 9, '5', '5', '3', '3', '5', '6', '3', '1', 'Orange', 6, '3', 8, 0, 6, '0', '3', '5', '6', '6', '9', 'Tissu', 'Vert', 'Vert', 3, '7', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (48, 'qwuckert', 'https://loremflickr.com/640/480/voiture', 'ullam-magni-sequi-magnam-sunt-corrupti-totam', 'Qui corrupti nobis accusantium mollitia et. Minima consequatur laboriosam dolorem ut. Possimus architecto labore dolore sint qui aliquid.', 14, 4, 4877.59, 'automatique', 0, 1, '+1 (461) 651-5288', 5, NULL, 5, NULL, 2019, 5, 204149139, 62, 3773796, 5, 5, 6, 4, 9, 6, 0, 7, 5, 4, 1, 6, 5, 5, 3, 8, 7, 1, NULL, '518013', 7, 52214016, '3', 4, 0, 0, '3', 8, '3', '2', '0', '5', '5', '3', '7', '6', 'Vert', 8, '4', 4, 5, 5, '1', '0', '0', '3', '1', '3', 'Velours', 'Noir', 'Orange', 7, '2', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (47, 'gerry.konopelski', 'https://loremflickr.com/640/480/voiture', 'vero-hic-placeat-odio-dolorem', 'Recusandae ea rerum mollitia quam minus quia rerum. Excepturi vel dignissimos minima labore illo aut id.', 18, 4, 5861.04, 'manuelle', 1, 1, '+14064456191', 5, NULL, 5, NULL, 2018, 5, 1, 82501, 705, 5, 9, 0, 5, 5, 2, 3, 1, 4, 7, 8, 2, 3, 4, 9, 1, 9, 5, NULL, '2468', 6, 43229, '4', 4, 1, 6, '4', 4, '4', '3', '1', '8', '2', '9', '1', '7', 'Vert', 8, '2', 8, 1, 7, '7', '8', '9', '2', '8', '1', 'Cuir', 'Gris', 'Orange', 3, '4', 8, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (46, 'kameron.brown', 'https://loremflickr.com/640/480/voiture', 'sapiente-adipisci-vel-perspiciatis-sed-non-nulla-consequatur-illo', 'Voluptas excepturi quod perspiciatis voluptates aliquid accusantium. Beatae officia accusamus ipsum vitae quisquam laudantium soluta. Itaque est consequatur accusantium quibusdam dolorem.', 13, 3, 507.78, 'manuelle', 0, 1, '+1-737-427-3837', 2, NULL, 2, NULL, 2019, 2, 770151724, 982913745, 927463, 8, 2, 0, 7, 0, 3, 6, 7, 9, 1, 4, 6, 2, 2, 0, 9, 8, 1, NULL, '690220', 9, 905, '5', 9, 0, 5, '4', 1, '4', '4', '7', '2', '8', '2', '8', '7', 'Orange', 6, '1', 4, 1, 3, '0', '3', '9', '5', '2', '9', 'Cuir', 'Bleu', 'Bleu', 6, '5', 3, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (45, 'upton.bill', 'https://loremflickr.com/640/480/voiture', 'exercitationem-sed-libero-molestias-cupiditate-aut-ad', 'Voluptates in aut esse tenetur quidem nobis qui porro. Ut harum quas eum ex eum consequatur. Necessitatibus est libero alias dicta tempore culpa. Amet hic inventore nostrum aut.', 14, 3, 182935, 'manuelle', 1, 1, '(494) 748-8808 x51144', 2, NULL, 2, NULL, 2017, 2, 6860, 621463930, 28190, 6, 9, 9, 6, 4, 6, 3, 8, 5, 9, 1, 3, 1, 3, 2, 3, 4, 3, NULL, '2970', 0, 435, '9', 1, 0, 9, '4', 9, '0', '4', '0', '6', '7', '6', '8', '6', 'Bleu', 8, '6', 7, 6, 7, '1', '1', '0', '4', '5', '5', 'Tissu', 'Orange', 'Gris', 3, '0', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (44, 'erna46', 'https://loremflickr.com/640/480/voiture', 'impedit-voluptas-architecto-vel-rerum-doloremque-doloremque-quos-aut', 'Fuga quo est rerum nesciunt. Ipsa illum sunt ducimus nostrum odit inventore fugit. Repellat voluptas rerum quia aperiam fuga. Voluptates laboriosam autem vitae assumenda et.', 16, 4, 61560.2, 'automatique', 0, 1, '1-372-527-8611', 8, NULL, 8, NULL, 2019, 8, 67014946, 1, 864, 9, 7, 1, 2, 5, 7, 2, 8, 0, 6, 4, 5, 9, 6, 5, 1, 9, 8, NULL, '86484', 7, 864053, '9', 8, 1, 5, '1', 8, '6', '4', '8', '3', '0', '0', '7', '6', 'Rouge', 9, '9', 0, 8, 1, '7', '7', '8', '6', '2', '2', 'Tissu', 'Vert', 'Vert', 6, '9', 2, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (43, 'tamia.lubowitz', 'https://loremflickr.com/640/480/voiture', 'nisi-sunt-reiciendis-quae-laboriosam-officia', 'Aliquam ducimus nobis repudiandae dolorem eaque. Deserunt deleniti sint consequatur qui. Rerum quas tenetur ducimus ipsa ipsam.', 10, 1, 7096.9, 'automatique', 1, 1, '1-406-393-7949 x13641', 8, NULL, 8, NULL, 2017, 8, 5697971, 808, 371, 7, 3, 3, 1, 2, 7, 1, 6, 8, 5, 7, 0, 2, 3, 4, 6, 0, 8, NULL, '558534974', 4, 9775, '1', 3, 1, 6, '2', 7, '8', '3', '1', '8', '0', '7', '0', '8', 'Bleu', 6, '7', 6, 0, 0, '7', '9', '3', '2', '2', '3', 'Cuir', 'Beige', 'Noir', 8, '0', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (42, 'helmer.rohan', 'https://loremflickr.com/640/480/voiture', 'tempora-sequi-aut-saepe-eveniet-error', 'Ut consequatur maiores provident porro perferendis sed eum. Veritatis numquam itaque officia cumque fugit illum velit. Ut et sit iure qui expedita.', 4, 2, 1612430, 'manuelle', 0, 1, '804-812-7165 x65496', 1, NULL, 1, NULL, 2019, 1, 16224, 59, 2611895, 5, 6, 7, 6, 4, 4, 9, 3, 1, 8, 9, 2, 4, 8, 1, 9, 2, 8, NULL, '682621', 5, 997988, '3', 1, 1, 9, '9', 2, '7', '1', '4', '1', '2', '3', '2', '8', 'Rouge', 0, '8', 9, 6, 6, '8', '4', '7', '3', '9', '1', 'Cuir', 'Noir', 'Noir', 4, '2', 3, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (41, 'stracke.janae', 'https://loremflickr.com/640/480/voiture', 'et-provident-voluptatum-eveniet', 'Rem suscipit totam veritatis sit facilis. Omnis enim doloremque exercitationem. Vero quos officiis corrupti et. Labore ex quas ducimus saepe. Dolor temporibus molestias esse nemo.', 7, 3, 3777.92, 'automatique', 1, 1, '1-550-304-0896', 9, NULL, 9, NULL, 2017, 9, 40959, 864, 53736889, 8, 7, 0, 7, 0, 9, 9, 3, 8, 1, 7, 1, 8, 3, 3, 7, 1, 3, NULL, '779325811', 7, 9, '4', 8, 1, 1, '0', 3, '7', '7', '5', '0', '9', '8', '2', '2', 'Orange', 4, '6', 4, 5, 3, '2', '3', '2', '0', '2', '8', 'Velours', 'Bleu', 'Noir', 9, '7', 7, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (40, 'kuhn.kadin', 'https://loremflickr.com/640/480/voiture', 'est-non-ipsum-dolores-non-accusamus-id-ea', 'Nihil ea facilis mollitia sed reprehenderit sed. Laudantium eveniet aut ipsam illo in. Dolorem dolorem qui dolore explicabo est repudiandae. Cupiditate autem distinctio iusto occaecati doloremque cumque ut.', 13, 2, 484.77, 'automatique', 0, 1, '939-427-6559 x79270', 1, NULL, 1, NULL, 2019, 1, 47588, 5422599, 356, 6, 5, 8, 7, 1, 9, 1, 6, 6, 3, 2, 5, 8, 2, 4, 3, 9, 0, NULL, '75', 3, 94573841, '5', 7, 0, 5, '9', 6, '0', '4', '7', '5', '8', '2', '1', '7', 'Beige', 7, '1', 0, 5, 9, '6', '2', '3', '2', '8', '9', 'Tissu', 'Vert', 'Vert', 9, '6', 1, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (39, 'alanis28', 'https://loremflickr.com/640/480/voiture', 'nulla-repellat-voluptas-veritatis-aut-non-aut', 'Maiores aut ut tempore corporis. Hic velit corrupti aspernatur consequatur distinctio. Deserunt quisquam consequatur aut qui inventore cum sit et. Quisquam repellendus earum debitis porro et est. Illo quis et autem eligendi harum voluptas et aperiam.', 15, 3, 7158310, 'automatique', 0, 1, '360-585-3663 x76617', 5, NULL, 5, NULL, 2019, 5, 7973122, 6801, 201416, 9, 1, 6, 0, 1, 9, 7, 1, 0, 8, 0, 6, 7, 3, 4, 0, 7, 9, NULL, '506905998', 9, 7250093, '6', 6, 1, 0, '0', 1, '6', '5', '9', '4', '1', '3', '3', '6', 'Orange', 7, '4', 7, 6, 2, '2', '1', '8', '5', '6', '7', 'Cuir', 'Vert', 'Bleu', 9, '5', 1, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (38, 'sanford.nannie', 'https://loremflickr.com/640/480/voiture', 'occaecati-mollitia-quod-perferendis-dolores', 'Vel nihil eum provident doloribus ut voluptas ducimus. Similique pariatur illum voluptatem non. Qui ut sed quisquam aut fuga consequatur.', 9, 4, 3136030, 'manuelle', 1, 1, '+1-742-414-3350', 9, NULL, 9, NULL, 2017, 9, 797, 2664, 8, 4, 7, 3, 3, 4, 6, 9, 5, 8, 2, 1, 7, 1, 0, 4, 1, 2, 5, NULL, '20', 7, 6381372, '0', 3, 0, 0, '6', 5, '7', '8', '2', '4', '6', '4', '4', '6', 'Bleu', 3, '6', 6, 0, 2, '8', '9', '0', '7', '0', '5', 'Cuir', 'Noir', 'Beige', 1, '5', 0, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (37, 'treutel.yolanda', 'https://loremflickr.com/640/480/voiture', 'ullam-dicta-error-earum-praesentium-velit', 'Voluptatem reiciendis quia aliquid nam nihil. Qui mollitia provident tempora quis quod excepturi iste. Soluta voluptatem dolorem voluptate quos. Ut adipisci quia deleniti in ea nihil.', 4, 2, 14857, 'automatique', 1, 1, '406.637.1065 x779', 8, NULL, 8, NULL, 2017, 8, 407211956, 57, 8, 2, 2, 4, 6, 9, 9, 6, 5, 0, 7, 7, 2, 5, 9, 6, 2, 1, 5, NULL, '686197', 7, 496, '7', 3, 0, 0, '7', 9, '7', '5', '4', '3', '5', '4', '2', '8', 'Vert', 8, '8', 7, 0, 6, '9', '4', '6', '6', '8', '4', 'Velours', 'Noir', 'Beige', 7, '1', 1, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (35, 'arnoldo.yundt', 'https://loremflickr.com/640/480/voiture', 'accusantium-ut-occaecati-ea-nobis-quidem-natus-nihil', 'Eum soluta doloremque impedit et quibusdam dolores beatae. Est earum quo autem excepturi laboriosam dolorem quidem. Facilis quam est sed veniam deleniti aperiam. Quae recusandae natus fuga facere ea voluptas.', 13, 3, 65.31, 'manuelle', 1, 1, '1-447-251-7017', 3, NULL, 3, NULL, 2017, 3, 346, 89421236, 743118, 5, 9, 0, 4, 5, 1, 8, 7, 1, 2, 9, 3, 4, 3, 3, 6, 7, 0, NULL, '65063621', 7, 37027, '1', 8, 0, 4, '1', 9, '6', '8', '3', '2', '2', '9', '7', '5', 'Beige', 7, '5', 5, 7, 1, '2', '3', '7', '0', '5', '0', 'Tissu', 'Orange', 'Bleu', 8, '5', 4, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (34, 'dbrakus', 'https://loremflickr.com/640/480/voiture', 'incidunt-quidem-facilis-fugiat-eius-illum-et', 'Doloremque facilis provident voluptate dicta. Voluptatibus laborum voluptas aut eaque voluptatibus ipsam adipisci. Impedit ab veniam cum saepe consectetur omnis dolores quibusdam. Incidunt aut consequatur consequatur occaecati labore et.', 9, 3, 48742.5, 'automatique', 1, 1, '(448) 384-9427', 0, NULL, 0, NULL, 2017, 0, 174459, 39896, 8689733, 3, 2, 6, 9, 1, 2, 8, 7, 9, 6, 0, 2, 6, 5, 6, 2, 6, 6, NULL, '481499874', 2, 44, '1', 8, 1, 2, '2', 8, '6', '7', '4', '8', '6', '0', '7', '7', 'Orange', 7, '0', 8, 1, 5, '1', '1', '4', '2', '9', '3', 'Tissu', 'Rouge', 'Orange', 3, '4', 2, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (33, 'kbaumbach', 'https://loremflickr.com/640/480/voiture', 'eveniet-ea-aut-ut-nihil-omnis-quo', 'Id accusamus veniam reiciendis libero ducimus delectus a dolor. Ut alias aut tenetur qui quod placeat. Aperiam velit ut perspiciatis reiciendis deserunt inventore iusto. Occaecati corporis illum consequatur.', 12, 4, 170.93, 'automatique', 0, 1, '806.488.2061', 9, NULL, 9, NULL, 2019, 9, 10, 497310, 60330745, 4, 4, 7, 5, 3, 0, 3, 6, 9, 4, 4, 2, 4, 6, 8, 2, 0, 6, NULL, '184337013', 2, 77524, '7', 5, 0, 9, '0', 0, '0', '0', '8', '2', '5', '1', '8', '0', 'Orange', 7, '4', 1, 2, 2, '0', '2', '8', '5', '4', '4', 'Tissu', 'Beige', 'Beige', 8, '7', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (32, 'ywill', 'https://loremflickr.com/640/480/voiture', 'sed-velit-reiciendis-tempora-autem', 'Soluta perspiciatis enim maiores voluptas dolor. Similique quibusdam adipisci dolorem et sit. Minus modi necessitatibus odio accusamus illo.', 16, 3, 30.6, 'manuelle', 1, 1, '(395) 433-2936', 0, NULL, 0, NULL, 2017, 0, 52622195, 143216533, 9, 7, 1, 4, 1, 3, 8, 0, 2, 1, 3, 4, 7, 3, 8, 3, 4, 1, 2, NULL, '4861', 9, 67, '4', 9, 0, 3, '8', 8, '5', '6', '8', '8', '9', '8', '1', '8', 'Beige', 0, '3', 0, 2, 5, '2', '5', '5', '3', '6', '6', 'Velours', 'Vert', 'Noir', 5, '6', 7, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (31, 'bins.sister', 'https://loremflickr.com/640/480/voiture', 'debitis-ea-repudiandae-repudiandae-assumenda', 'Possimus iure commodi quasi aliquam dolores ipsa error. Mollitia nisi nihil voluptatibus molestias veniam. Voluptas fugiat et perferendis.', 11, 4, 4007.05, 'manuelle', 1, 1, '1-524-810-4662 x067', 8, NULL, 8, NULL, 2017, 8, 4631, 95075390, 5, 5, 5, 7, 5, 7, 3, 1, 7, 5, 9, 0, 3, 7, 7, 0, 3, 4, 9, NULL, '33164293', 1, 683783, '4', 3, 1, 2, '0', 8, '5', '1', '0', '9', '7', '6', '2', '4', 'Rouge', 7, '1', 3, 9, 5, '9', '7', '9', '7', '1', '9', 'Velours', 'Orange', 'Beige', 0, '0', 1, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (30, 'nannie88', 'https://loremflickr.com/640/480/voiture', 'asperiores-sed-quos-velit', 'Deleniti error fugiat dolorem impedit quaerat harum nam. Numquam dolor quidem non rerum est libero quo sint. Est quia in impedit aut corrupti velit praesentium.', 4, 3, 5526.32, 'automatique', 1, 1, '873-342-9466 x74283', 6, NULL, 6, NULL, 2017, 6, 271345, 80667, 806432768, 6, 6, 0, 5, 3, 5, 9, 0, 7, 7, 0, 8, 5, 3, 4, 7, 1, 8, NULL, '707202723', 9, 3954, '2', 4, 0, 3, '6', 5, '5', '9', '6', '7', '7', '7', '6', '2', 'Beige', 0, '4', 9, 7, 5, '8', '4', '1', '1', '2', '7', 'Velours', 'Noir', 'Orange', 1, '8', 4, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (29, 'oswaniawski', 'https://loremflickr.com/640/480/voiture', 'culpa-ullam-est-veniam-perspiciatis-occaecati-quo', 'Cum similique voluptas maiores dolorem culpa. Perferendis laudantium placeat deleniti sunt accusamus et aperiam recusandae. Fuga aut facere qui nulla repudiandae.', 13, 4, 13064, 'manuelle', 0, 1, '(821) 956-6316', 1, NULL, 1, NULL, 2019, 1, 264304, 17712, 704980995, 4, 3, 2, 7, 4, 2, 1, 9, 6, 7, 3, 5, 2, 6, 1, 2, 5, 1, NULL, '762', 9, 458541, '1', 9, 1, 4, '4', 4, '2', '5', '4', '9', '9', '2', '4', '7', 'Vert', 1, '2', 4, 1, 6, '3', '8', '3', '6', '8', '6', 'Tissu', 'Vert', 'Vert', 3, '7', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (28, 'clotilde.zulauf', 'https://loremflickr.com/640/480/voiture', 'voluptatibus-numquam-sed-dolorum', 'Mollitia recusandae rerum ipsa reprehenderit ratione. Et doloremque placeat et cumque aliquid rerum. Dignissimos laudantium unde maiores soluta quaerat esse velit.', 5, 1, 828206, 'manuelle', 0, 1, '(506) 312-2006 x618', 7, NULL, 7, NULL, 2018, 7, 780342, 17, 819679302, 1, 9, 9, 2, 5, 4, 9, 2, 9, 5, 4, 7, 0, 4, 1, 2, 0, 0, NULL, '93879523', 6, 43542990, '1', 4, 1, 8, '2', 6, '2', '8', '6', '9', '6', '3', '4', '3', 'Beige', 2, '9', 4, 5, 6, '6', '5', '9', '9', '8', '9', 'Velours', 'Orange', 'Vert', 8, '1', 8, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (27, 'jaskolski.quinten', 'https://loremflickr.com/640/480/voiture', 'dolor-autem-nam-eligendi-eius-vero-quia', 'Ex officiis voluptas rerum adipisci aut. Voluptas doloremque facilis aliquid qui. Optio officia aut quia omnis cum et aut. Distinctio recusandae ea suscipit quis.', 6, 3, 9643, 'automatique', 0, 1, '1-612-557-4643 x11185', 2, NULL, 2, NULL, 2018, 2, 9343040, 1698, 656340056, 4, 6, 2, 0, 5, 8, 2, 4, 9, 1, 9, 3, 8, 1, 0, 9, 2, 5, NULL, '660981', 3, 67320, '3', 5, 1, 5, '6', 0, '5', '3', '0', '4', '5', '2', '3', '2', 'Rouge', 5, '0', 80000, 5, 9, '0', '4', '0', '0', '2', '9', 'Cuir', 'Rouge', 'Rouge', 3, '8', 5, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (26, 'gisselle64', 'https://loremflickr.com/640/480/voiture', 'aspernatur-accusantium-architecto-consequatur-sit-enim', 'Impedit eveniet praesentium cupiditate. Quis ea inventore et. Doloremque magnam perspiciatis rerum reiciendis quia.', 11, 1, 240331, 'manuelle', 0, 1, '941.662.8502', 6, NULL, 6, NULL, 2018, 6, 460460019, 56783265, 5239, 9, 0, 3, 9, 3, 4, 6, 2, 3, 8, 9, 9, 3, 1, 1, 3, 1, 3, NULL, '129021566', 2, 6797, '7', 8, 1, 5, '8', 9, '8', '9', '0', '2', '9', '3', '2', '2', 'Vert', 1, '1', 194414, 7, 6, '8', '8', '3', '2', '6', '2', 'Velours', 'Gris', 'Rouge', 8, '7', 2, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (25, 'walter.bette', 'https://loremflickr.com/640/480/voiture', 'odio-recusandae-suscipit-voluptatem-nam', 'Voluptate commodi aut aliquid atque sit cum. Omnis fuga adipisci aut dolorum qui ut adipisci eveniet. Eos rerum et impedit sed in. Eligendi consequatur magnam corrupti delectus occaecati consequatur.', 12, 1, 1.42, 'manuelle', 0, 1, '(634) 427-8389', 0, NULL, 0, NULL, 2018, 0, 88802, 602, 99598654, 6, 5, 0, 8, 7, 9, 6, 6, 5, 7, 9, 8, 4, 0, 6, 0, 0, 0, NULL, '757', 4, 971, '3', 3, 0, 8, '7', 3, '6', '2', '7', '1', '5', '3', '9', '0', 'Noir', 9, '1', 400, 7, 0, '3', '2', '1', '9', '5', '2', 'Velours', 'Orange', 'Orange', 6, '6', 7, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (24, 'elfrieda55', 'https://loremflickr.com/640/480/voiture', 'assumenda-voluptate-impedit-temporibus-quidem-ut-nihil-cupiditate', 'Voluptatibus cum dolor sit quia expedita consequatur non. Totam saepe hic voluptatem reprehenderit. Omnis commodi est voluptatibus ut et.', 6, 4, 59.54, 'manuelle', 1, 1, '1-330-896-7963 x429', 2, NULL, 2, NULL, 2017, 2, 649928808, 123489, 39681, 1, 1, 5, 2, 8, 2, 0, 5, 5, 1, 6, 5, 4, 3, 5, 9, 4, 9, NULL, '1', 7, 1, '1', 4, 1, 3, '5', 6, '5', '4', '0', '7', '0', '3', '5', '3', 'Rouge', 8, '2', 3775, 4, 1, '4', '0', '8', '3', '0', '2', 'Tissu', 'Noir', 'Vert', 3, '2', 3, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (23, 'ykutch', 'https://loremflickr.com/640/480/voiture', 'corporis-qui-praesentium-facilis-et-sit', 'Totam vero aliquid sunt dolorum. Commodi iusto nisi modi delectus at quod. Consectetur architecto perferendis eligendi aspernatur omnis occaecati facere.', 18, 3, 41586.1, 'automatique', 0, 1, '1-805-533-7918 x5561', 6, NULL, 6, NULL, 2018, 6, 135089630, 14132, 452, 1, 6, 0, 4, 2, 8, 7, 8, 6, 5, 4, 7, 9, 4, 6, 4, 9, 5, NULL, '4632', 6, 61226286, '7', 2, 0, 9, '3', 8, '9', '7', '0', '6', '8', '1', '7', '9', 'Beige', 9, '1', 67, 5, 1, '9', '3', '3', '5', '4', '4', 'Velours', 'Bleu', 'Gris', 7, '1', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (22, 'ona.lakin', 'https://loremflickr.com/640/480/voiture', 'quod-saepe-ducimus-fuga-sapiente-corrupti-voluptas', 'Et quos optio hic nam architecto quia. Dolor voluptas vel exercitationem ipsa nisi et ratione. Iste est quo aut quibusdam qui.', 13, 3, 1023.58, 'manuelle', 0, 1, '570.375.6292', 5, NULL, 5, NULL, 2018, 5, 1474946, 37992267, 50, 6, 7, 6, 8, 2, 8, 9, 3, 7, 1, 2, 2, 0, 7, 2, 3, 6, 1, NULL, '1', 6, 78801325, '8', 1, 1, 7, '3', 4, '6', '1', '2', '1', '8', '1', '3', '6', 'Vert', 4, '3', 496, 0, 2, '8', '8', '4', '8', '3', '9', 'Cuir', 'Bleu', 'Orange', 5, '7', 4, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (21, 'ebony71', 'https://loremflickr.com/640/480/voiture', 'quasi-doloremque-nemo-est-corporis-dolorem-et-saepe', 'Voluptas quo porro maxime dolorem sapiente. Non ut reprehenderit voluptatum ea harum et. Accusamus nihil beatae culpa nihil beatae.', 14, 3, 385445, 'manuelle', 0, 1, '(379) 557-7692 x0701', 1, NULL, 1, NULL, 2018, 1, 4410939, 547, 39, 2, 3, 9, 2, 9, 7, 6, 6, 4, 1, 7, 7, 7, 7, 8, 9, 4, 5, NULL, '5313', 7, 851, '4', 9, 1, 2, '2', 3, '1', '5', '6', '6', '9', '1', '9', '8', 'Bleu', 2, '6', 9, 2, 2, '8', '5', '1', '4', '5', '3', 'Tissu', 'Beige', 'Rouge', 4, '4', 5, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (20, 'nhermann', 'https://loremflickr.com/640/480/voiture', 'quo-aut-id-atque', 'Cumque suscipit doloribus quo ut impedit. Molestiae aliquid itaque excepturi eos voluptates corrupti. Mollitia autem asperiores ea commodi animi rem. Aspernatur accusantium enim voluptatem sed culpa fuga et.', 7, 3, 645.96, 'automatique', 1, 1, '(597) 736-9242 x45809', 8, NULL, 8, NULL, 2017, 8, 839, 8, 7681, 5, 7, 9, 0, 9, 0, 7, 3, 1, 3, 1, 9, 3, 1, 7, 3, 2, 7, NULL, '21601222', 4, 845, '8', 5, 0, 2, '1', 9, '3', '7', '3', '7', '8', '9', '0', '6', 'Gris', 5, '7', 80000, 8, 8, '4', '7', '6', '4', '2', '4', 'Tissu', 'Vert', 'Rouge', 5, '9', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (19, 'ulises42', 'https://loremflickr.com/640/480/voiture', 'perspiciatis-libero-est-fugiat-explicabo-enim', 'Ut nulla laborum inventore blanditiis excepturi est. Et quia sint dicta quidem accusamus tempora. Voluptatem enim deserunt molestias aut vel est. Voluptatem dolore quaerat consequatur cumque praesentium quibusdam reprehenderit.', 9, 1, 628.1, 'automatique', 1, 1, '879-581-0723', 7, NULL, 7, NULL, 2017, 7, 81981, 81, 207, 0, 4, 3, 3, 7, 1, 3, 2, 4, 7, 6, 9, 9, 2, 4, 6, 1, 7, NULL, '494751382', 5, 960153, '4', 5, 1, 2, '5', 4, '8', '1', '0', '2', '6', '5', '0', '0', 'Orange', 8, '5', 194414, 6, 9, '5', '5', '7', '5', '5', '6', 'Cuir', 'Gris', 'Rouge', 5, '9', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (18, 'vicky.russel', 'https://loremflickr.com/640/480/voiture', 'voluptatem-nulla-sint-vel-est', 'Iure est est dolor eos. Voluptatem numquam aut voluptas ipsa qui placeat. Sint perspiciatis temporibus ratione provident corrupti.', 19, 1, 491380, 'manuelle', 0, 1, '(548) 984-6921 x320', 2, NULL, 2, NULL, 2018, 2, 4447, 700810, 7, 4, 5, 7, 9, 0, 6, 9, 4, 6, 7, 4, 8, 6, 5, 4, 4, 9, 2, NULL, '127022844', 1, 3184, '8', 2, 1, 0, '5', 5, '7', '3', '9', '3', '9', '4', '7', '5', 'Gris', 1, '0', 400, 6, 3, '0', '0', '7', '8', '9', '9', 'Velours', 'Rouge', 'Noir', 5, '0', 8, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (17, 'rgreenholt', 'https://loremflickr.com/640/480/voiture', 'suscipit-tempore-voluptatem-fugit-cum', 'Illum cumque est voluptates vel et laborum facere. Voluptatem voluptates cumque distinctio debitis odio quisquam. Exercitationem culpa ea quisquam optio sint vel. Cum facere veritatis nam aut velit.', 4, 1, 403903, 'automatique', 1, 1, '(790) 380-3501', 8, NULL, 8, NULL, 2017, 8, 1176, 659, 3008, 7, 2, 0, 9, 7, 5, 3, 5, 3, 7, 2, 9, 7, 2, 0, 2, 0, 5, NULL, '33', 5, 255504223, '5', 7, 0, 4, '9', 2, '0', '5', '6', '2', '9', '2', '4', '4', 'Vert', 7, '0', 3775, 4, 6, '8', '4', '3', '3', '7', '5', 'Velours', 'Orange', 'Vert', 7, '8', 5, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (16, 'wilson72', 'https://loremflickr.com/640/480/voiture', 'quod-consequatur-minima-amet', 'Ducimus iusto quis sunt et quia iure delectus. Sunt et ut vel quod. Accusantium ea fuga vitae et debitis. Ex voluptatibus minus quidem sint. Quo id vel excepturi consequatur.', 2, 4, 18080.2, 'automatique', 0, 1, '(590) 230-5713', 0, NULL, 0, NULL, 2018, 0, 592579695, 849555, 51339, 9, 4, 8, 4, 5, 2, 1, 4, 6, 7, 3, 0, 1, 0, 4, 5, 8, 2, NULL, '82', 8, 215968, '1', 6, 0, 3, '3', 2, '1', '0', '4', '4', '8', '0', '7', '1', 'Orange', 3, '0', 67, 7, 1, '9', '8', '6', '8', '8', '7', 'Cuir', 'Orange', 'Gris', 3, '4', 5, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (15, 'jimmy.wuckert', 'https://loremflickr.com/640/480/voiture', 'quia-consequatur-hic-qui', 'Sit eos omnis consequuntur fugiat. Odio corrupti aut rerum quos aut voluptatem voluptas architecto. At maxime fuga exercitationem sed qui et. Veritatis et sint enim cupiditate quis.', 4, 2, 19.93, 'manuelle', 1, 1, '667.328.2917 x451', 2, NULL, 2, NULL, 2017, 2, 91422, 695223, 478, 5, 9, 5, 0, 2, 3, 1, 1, 1, 3, 3, 0, 9, 9, 9, 1, 0, 1, NULL, '91206', 1, 56, '1', 9, 0, 3, '9', 0, '5', '7', '9', '5', '1', '4', '1', '6', 'Orange', 8, '0', 496, 6, 8, '7', '4', '8', '2', '4', '6', 'Velours', 'Bleu', 'Noir', 7, '1', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (14, 'phomenick', 'https://loremflickr.com/640/480/voiture', 'quaerat-veniam-temporibus-autem-et', 'Non optio qui blanditiis non eum rem quia. Numquam voluptate cumque aut unde dolorem amet minus voluptatem. Quisquam veniam vel facere rerum.', 2, 1, 28082.8, 'manuelle', 1, 1, '1-328-350-2410 x89123', 8, NULL, 8, NULL, 2017, 8, 87, 28662545, 699195, 8, 6, 1, 2, 4, 2, 2, 8, 0, 8, 9, 7, 9, 0, 6, 8, 5, 8, NULL, '68330069', 2, 88, '9', 0, 1, 4, '0', 1, '5', '2', '4', '9', '2', '8', '7', '3', 'Orange', 0, '3', 5, 3, 6, '2', '8', '4', '5', '4', '0', 'Tissu', 'Vert', 'Beige', 7, '1', 3, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (12, 'runolfsson.dax', 'https://loremflickr.com/640/480/voiture', 'quisquam-rerum-praesentium-omnis', 'Distinctio sed a voluptas fugiat. Quia repellat et optio nihil quo libero nisi. Asperiores id quo ea quidem atque est soluta omnis. Assumenda doloremque voluptatibus ut mollitia corporis dignissimos.', 11, 2, 13739, 'manuelle', 1, 1, '389.321.1287', 9, NULL, 9, NULL, 2017, 9, 5591242, 94775, 9, 7, 5, 3, 3, 1, 1, 2, 9, 1, 0, 3, 4, 0, 0, 3, 0, 5, 2, NULL, '491787', 6, 5331733, '3', 4, 1, 9, '7', 2, '2', '4', '8', '6', '3', '2', '3', '8', 'Orange', 0, '4', 194414, 6, 5, '5', '8', '6', '3', '7', '8', 'Velours', 'Rouge', 'Orange', 3, '3', 4, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (11, 'bhudson', 'https://loremflickr.com/640/480/voiture', 'provident-mollitia-quia-dignissimos-cupiditate', 'Enim qui velit incidunt delectus fugiat. Nam iste voluptate in consequatur. Qui ut facere assumenda vitae vitae quam libero voluptatem.', 19, 4, 70.27, 'manuelle', 0, 1, '1-621-638-7399 x61727', 4, NULL, 4, NULL, 2018, 4, 1537565, 8, 5732, 6, 4, 6, 5, 1, 1, 4, 8, 6, 0, 0, 9, 9, 6, 5, 4, 6, 2, NULL, '3', 6, 964586, '3', 7, 1, 9, '9', 5, '5', '8', '8', '5', '1', '8', '9', '4', 'Vert', 6, '4', 400, 0, 5, '4', '4', '9', '9', '7', '1', 'Velours', 'Orange', 'Orange', 4, '1', 8, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (10, 'johnston.bradly', 'https://loremflickr.com/640/480/voiture', 'tempora-in-est-sint-dignissimos-similique-adipisci', 'Laudantium in saepe repellendus excepturi deleniti optio similique. Architecto sit aperiam est dignissimos voluptate. Est et maxime est sint inventore. Accusantium reprehenderit debitis qui aut et.', 10, 4, 89592.5, 'automatique', 0, 1, '370.808.9679', 0, NULL, 0, NULL, 2018, 0, 440448, 521248, 872, 0, 8, 8, 2, 0, 6, 0, 1, 1, 5, 1, 1, 5, 3, 4, 7, 2, 4, NULL, '3865', 9, 137, '8', 8, 1, 8, '2', 8, '1', '7', '6', '2', '9', '5', '1', '3', 'Beige', 1, '2', 3775, 2, 7, '3', '1', '3', '8', '4', '2', 'Velours', 'Gris', 'Beige', 5, '5', 2, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (8, 'mckenna.waters', 'https://loremflickr.com/640/480/voiture', 'perferendis-hic-dolor-fuga-illum-culpa-voluptate-non', 'Possimus qui qui error odio omnis animi voluptatibus et. Dolorum sed ullam quia et ea et et. Cumque dolorum hic ullam et sint et harum. Natus quis iure sit dolores itaque.', 16, 4, 205713, 'manuelle', 1, 1, '526.296.9538 x371', 2, NULL, 2, NULL, 2017, 2, 945, 2663, 49545037, 0, 7, 4, 7, 6, 7, 6, 6, 3, 3, 4, 1, 2, 1, 3, 3, 4, 5, NULL, '1417836', 8, 878, '4', 6, 1, 0, '2', 1, '4', '6', '3', '5', '1', '3', '9', '3', 'Noir', 5, '1', 67, 7, 3, '5', '4', '8', '3', '6', '3', 'Velours', 'Noir', 'Bleu', 2, '8', 6, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (7, 'kmccullough', 'https://loremflickr.com/640/480/voiture', 'sint-sit-magnam-eius-modi-et-et-et-qui', 'Ipsum ratione voluptatem esse in possimus voluptates sequi voluptate. Eum culpa sit totam non deleniti.', 8, 4, 387.15, 'automatique', 1, 1, '1-250-926-6676', 0, NULL, 0, NULL, 2017, 0, 57, 977, 194414, 9, 5, 5, 0, 3, 6, 1, 7, 0, 6, 4, 7, 0, 9, 0, 9, 7, 8, NULL, '9', 5, 34926, '7', 6, 1, 1, '2', 0, '1', '3', '6', '3', '7', '5', '5', '4', 'Rouge', 3, '5', 496, 3, 6, '8', '9', '3', '9', '9', '3', 'Cuir', 'Orange', 'Rouge', 7, '9', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (6, 'pschinner', 'https://loremflickr.com/640/480/voiture', 'omnis-aut-impedit-nobis-sunt-sit', 'Ut illo saepe laborum in sed soluta et odio. Tenetur voluptate excepturi neque minus consequatur voluptatibus et. Aut assumenda adipisci exercitationem sed. Occaecati omnis totam perferendis maiores neque fugit.', 13, 3, 682161, 'manuelle', 1, 1, '242-810-6660', 6, NULL, 6, NULL, 2017, 6, 335358, 540, 4, 8, 1, 2, 3, 6, 5, 5, 6, 5, 5, 6, 1, 9, 6, 8, 6, 6, 1, NULL, '4055228', 5, 111, '8', 0, 0, 6, '8', 6, '2', '3', '1', '6', '7', '9', '2', '0', 'Rouge', 9, '4', 80000, 7, 8, '4', '3', '1', '6', '5', '7', 'Velours', 'Gris', 'Rouge', 8, '8', 5, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (5, 'davis.noemie', 'https://loremflickr.com/640/480/voiture', 'est-et-vero-temporibus-id-maiores', 'Fuga omnis alias nemo dolor. Laudantium non voluptatem occaecati fugit excepturi minima. Qui commodi sed cumque unde labore. Aliquam reiciendis quam iusto aperiam nulla nihil in.', 2, 3, 7.17, 'automatique', 0, 1, '1-523-687-3590 x2765', 7, NULL, 7, NULL, 2018, 7, 15, 1849, 3775, 8, 2, 8, 9, 3, 6, 5, 3, 3, 2, 6, 9, 5, 5, 9, 7, 4, 6, NULL, '401', 7, 998075156, '5', 1, 1, 6, '2', 1, '9', '4', '3', '6', '2', '8', '3', '9', 'Beige', 3, '1', 194414, 0, 9, '0', '1', '4', '0', '8', '9', 'Cuir', 'Gris', 'Bleu', 4, '1', 2, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (4, 'leif.luettgen', 'https://loremflickr.com/640/480/voiture', 'sint-minus-sunt-dolores-ut-corporis-possimus', 'Eum harum optio laudantium quam ratione. Veniam et ipsum dicta aut totam dolorem. Provident sed qui eos et. Omnis aliquid ducimus dolores error voluptate.', 11, 3, 5.88, 'automatique', 1, 1, '+1-258-645-6059', 6, NULL, 6, NULL, 2017, 6, 3477169, 8, 67, 0, 6, 4, 6, 2, 5, 8, 5, 7, 3, 4, 2, 7, 3, 6, 9, 4, 1, NULL, '9', 5, 76, '7', 9, 1, 6, '5', 7, '5', '4', '6', '0', '7', '2', '7', '8', 'Orange', 4, '1', 400, 5, 5, '8', '3', '7', '1', '8', '0', 'Cuir', 'Orange', 'Rouge', 9, '0', 1, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (2, 'doug61', 'https://loremflickr.com/640/480/voiture', 'minima-sunt-asperiores-aliquid-soluta', 'Hic quos velit voluptatem repellat culpa sint. Ea quidem qui alias veniam ea unde. Magnam et fugiat recusandae ut maxime.', 14, 2, 709.92, 'manuelle', 1, 1, '1-290-472-0299 x9301', 2, NULL, 2, NULL, 2017, 2, 6, 64358, 3162697, 4, 8, 6, 7, 8, 1, 2, 7, 2, 9, 5, 0, 8, 3, 3, 7, 0, 1, NULL, '2', 15, 5730, '5', 8, 1, 5, '5', 3, '0', '9', '2', '0', '5', '8', '1', '0', 'Bleu', 6, '3', 67, 0, 4, '0', '6', '4', '9', '2', '1', 'interiorDesign', 'Beige', 'Bleu', 8, '1', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (3, 'susana28', 'https://loremflickr.com/640/480/voiture', 'voluptate-ad-labore-quas-quo', 'Laudantium qui libero non tempora nihil. Iste aut voluptatem voluptatem et quia accusamus consequatur. Fuga aut perspiciatis saepe voluptatibus.', 4, 2, 12.86, 'automatique', 0, 1, '+1.460.905.2548', 8, NULL, 8, NULL, 2018, 8, 974547621, 92641479, 496, 9, 1, 8, 1, 9, 5, 2, 6, 1, 0, 1, 5, 3, 1, 6, 1, 9, 1, NULL, '9', 9, 248489571, '4', 2, 1, 4, '7', 8, '5', '4', '0', '3', '9', '1', '9', '1', 'Noir', 5, '6', 3775, 2, 9, '0', '0', '9', '2', '8', '9', 'Velours', 'Bleu', 'Gris', 9, '7', 4, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (1, 'stephany54', 'https://loremflickr.com/640/480/voiture', 'est-possimus-vel-a-sequi-dolorem-rerum', 'Sint repellendus et ad beatae distinctio non et. Molestias tempore quisquam et quis ducimus exercitationem. Inventore omnis et maxime illum exercitationem nulla. Reiciendis reprehenderit asperiores et nam.', 2, 1, 430, 'manuelle', 1, 1, '+1-768-239-3542', 19, NULL, 3, NULL, 2019, 3, 15514, 771273, 72, 0, 8, 6, 2, 3, 6, 0, 4, 3, 2, 7, 4, 8, 4, 0, 0, 3, 5, 2, '41051995', 3, 9, '3', 0, 1, 2, '9', 8, '6', '6', '3', '7', '8', '5', '3', '9', 'Noir', 0, '2', 496, 1, 2, '8', '9', '8', '0', '2', '8', 'Cuir', 'Noir', 'Beige', 2, '2', 0, '2018-07-18 09:40:10', '2018-07-18 09:40:10');
INSERT INTO `vehicles` VALUES (60, 'jennings.mitchell', 'https://loremflickr.com/640/480/voiture', 'beatae-labore-nisi-exercitationem-quis-quia-et-perspiciatis-molestias', 'Dolorum consequatur minima quibusdam tempora eum omnis accusamus. Laudantium sunt corporis veritatis reprehenderit. Magni est totam impedit.', 5, 2, 40.36, 'automatique', 1, 1, '1-874-629-2140', 8, NULL, 8, NULL, 2018, 8, 684000, 1320175, 645, 4, 4, 5, 2, 1, 0, 6, 9, 3, 1, 5, 7, 1, 3, 5, 5, 1, 0, NULL, '752103', 9, 790, '6', 7, 1, 3, '7', 2, '0', '6', '1', '5', '5', '6', '4', '6', 'Rouge', 0, '9', 4, 3, 5, '6', '3', '4', '6', '2', '6', 'Velours', 'Orange', 'Vert', 0, '0', 9, '2018-07-18 09:40:10', '2018-07-18 09:40:10');

-- ----------------------------
-- Table structure for verify_users
-- ----------------------------
DROP TABLE IF EXISTS `verify_users`;
CREATE TABLE `verify_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of verify_users
-- ----------------------------
INSERT INTO `verify_users` VALUES (5, 3, 1, 'i69QSiS8UOIcXNiXMcAdvP3vg6XBLac3cuIhjrRf', '2018-08-03 14:27:56', '2018-08-03 14:28:32');
INSERT INTO `verify_users` VALUES (4, 2, 1, 'MZWn28JeFOY0bc2YrYjsknBGKjUFrAqhiERDFYBc', '2018-08-03 10:26:27', '2018-08-03 10:26:44');

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NULL DEFAULT NULL,
  `model_id` int(11) NULL DEFAULT NULL,
  `vehicle_id` int(11) NULL DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES (1, 1, NULL, NULL, 'BMW M Experience - Circuit du Laquais\r\n\r\n', '/upload/models/M5.jpg', 'UKc271fj2ok', 'BMW M Experience - Circuit du Laquais\r\n\r\n', '2018-07-19 17:01:52', NULL);
INSERT INTO `videos` VALUES (2, 1, NULL, NULL, 'Mercedes-Benz | Découvrez le GLC 350 e avec Stéphane Rotenberg', 'https://lorempixel.com/640/480/transport/', 'm5A2jkSCDn4', 'Mercedes-Benz | Découvrez le GLC 350 e avec Stéphane Rotenberg', '2018-07-19 17:01:52', NULL);
INSERT INTO `videos` VALUES (3, 2, NULL, NULL, 'stgret sert', 'https://lorempixel.com/640/480/transport/', 'tEuyWsB0n5g', '1etertertet', '2018-07-19 17:01:52', NULL);
INSERT INTO `videos` VALUES (4, 2, NULL, NULL, 'Audi A4\r\n\r\n', 'https://lorempixel.com/640/480/transport/', 'sWLfB37EUMo', 'Audi A4\r\n\r\n', '2018-07-19 17:01:52', NULL);
INSERT INTO `videos` VALUES (5, 3, NULL, NULL, 'Essai - Dacia Logan restylée 2017 : cure de modernisme\r\n\r\n', 'https://lorempixel.com/640/480/transport/', 'S5mAwz03YvE', 'Essai - Dacia Logan restylée 2017 : cure de modernisme\r\n\r\n', '2018-07-19 17:01:52', '2018-07-19 17:01:56');
INSERT INTO `videos` VALUES (6, 3, NULL, NULL, 'L\'Histoire de la BMW Série 3', '/upload/models/Serie_3.jpg', 'IyJOXt-P7Yg', 'L\'Histoire de la BMW Série 3', '2018-07-19 17:01:52', NULL);
INSERT INTO `videos` VALUES (7, 4, NULL, NULL, 'Mercedes-Benz 230 G: A timeless legend in France', 'https://lorempixel.com/640/480/transport/', 'Fi8Md_mr50M', 'Mercedes-Benz 230 G: A timeless legend in France\r\n\r\n', '2018-07-19 17:01:52', NULL);

SET FOREIGN_KEY_CHECKS = 1;
