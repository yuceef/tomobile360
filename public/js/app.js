// Fixed Nav

$(document).ready(function ($) {
  var $header = $('.Elemfixed'),
      posTop = $header.position().top;
  $(window).scroll(function () {
    var y = $(this).scrollTop();
    if (y > posTop) { $header.addClass('fixed'); }
    else { $header.removeClass('fixed'); }
  });
});

// Slider Hero 

$('.Heroslider').owlCarousel({
    loop:true,
    nav:true,
    navText:false,
    dots:false,
    autoplay:true,
    items:1
})

// Auto Logos Carousel 

$('.AutoBrand').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    navText:false,
    dots:false,
    autoplay:true,
    items:6
})

// Offres & Services Carousel 

$('.Of-ico-items').owlCarousel({
    loop:false,
    margin:30,
    nav:true,
    navText:false,
    dots:false,
    autoplay:true,
    items:4
})

// Voitures neuves promo Carousel

$('.CarPromo').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
    autoplay:true,
    items:2
})

// Voitures similaires Carousel

$('.Car-sm').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText:false,
    dots:false,
    autoplay:true,
    items:4
})

// Last Ads Carousel

$('.OwlAds').owlCarousel({
    loop:true,
    margin:15,
    nav:true,
    navText:false,
    dots:false,
    autoplay:true,
    items:6
})

// Video Carousel

$('.OwlVideo').owlCarousel({
    loop:1,
    margin:30,
    dots:true,
    autoplay:true,
    items:4
})

// Partenaires Logos Carousel

$('.OwlPart').owlCarousel({
    loop:true,
    margin:40,
    nav:true,
    navText:false,
    dots:false,
    autoplay:true,
    fluidSpeed: 5000,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    items:5
})

// Carousel Page VO Annonces

$('.HeroChildSlider').owlCarousel({
    loop:false,
    margin:15,
    nav:true,
    navText:false,
    dots:false,
    autoplay:false,
    fluidSpeed: 5000,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    URLhashListener:true,
    startPosition: 'URLHash',
    items:3
})

// Offre speciale Carousel

$('.Offre-spe').owlCarousel({
    loop:1,
    margin:0,
    dots:false,
    nav:true,
    navText:false,
    autoplay:true,
    items:1
})

// Range Sliders

$(function () {

    $("#range").ionRangeSlider({
        hide_min_max: true,
        keyboard: true,
        min: 0,
        max: 5000,
        from: 1000,
        to: 4000,
        type: 'double',
        step: 1,
        prefix: "DH ",
        grid: true
    });

});

$(function () {

    $("#range-km").ionRangeSlider({
        hide_min_max: true,
        keyboard: true,
        min: 0,
        max: 250000,
        from: 1000,
        to: 250000,
        type: 'double',
        step: 1,
        prefix: "KM ",
        grid: true
    });

});

$(function () {

    $("#range-year").ionRangeSlider({
        hide_min_max: true,
        keyboard: true,
        min: 1988,
        max: 2018,
        from: 1988,
        to: 2018,
        type: 'double',
        step: 1,
        grid: true
    });

});

$(function () {

    $("#range-pf").ionRangeSlider({
        hide_min_max: true,
        keyboard: true,
        min: 1,
        max: 20,
        from: 1,
        to: 20,
        type: 'double',
        prefix: "CV ",
        step: 1,
        grid: true
    });

});
function extractContent(s) {
    var span = document.createElement('span'); 
    span.innerHTML = s.replace('><','> <'); 
    $('PostExcept')
    return span.textContent || span.innerText;
}; 