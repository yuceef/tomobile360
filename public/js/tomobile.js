$(document).ready(function () {
    $('select:not(.notSel2)').select2({
        templateResult: formatStat,
        templateSelection: formatStat,
        minimumResultsForSearch: -1
    });
}); 
$(document).ready(function () {
    $('select.isSearch').select2({
        templateResult: formatStat,
        templateSelection: formatStat,
    });
}); 
function formatStat(state) {
    var $state = $(
        /*'<span style="padding-left:7px;">' + state.text + '</span>'*/
        '<span>' + state.text + '</span>'
    );
    return $state;
};