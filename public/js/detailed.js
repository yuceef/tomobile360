function formatState (state) {
    if(state.text == "Toutes") return state.text;
    var Url = $("#BrandId" + state.text.replace(' ','_')).val();
    var $state = $(
        '<span><img src="'+Url+'" class="img-flag" /> ' + state.text + '</span>'
    );
    return $state;
};
var savePrice = function (data) {
    $("#GminPrice").val(data.from).trigger('change');
    $("#GmaxPrice").val(data.to).trigger('change');
};
var saveRangeFiscal = function (data) {
    if(data.from != $("#minPuissance").val()){
        $("#minPuissance").val(data.from).trigger('change');
    }
    if(data.to != $("#maxPuissance").val()){
        $("#maxPuissance").val(data.to).trigger('change');
    }

};
var saveMileage = function (data) {
    if(data.from != $("#minMileage").val()){
        $("#minMileage").val(data.from).trigger('change');
    }
    if(data.to != $("#maxMileage").val()){
        $("#maxMileage").val(data.to).trigger('change');
    }

};
function getResultCount(neuf) {
    console.log("start getResultCount");   
    var idName = (neuf == 1) ? 'nf' : 'occ'
    var btn = $('#afficher2')
    var form = $('#formSearch');
    $.ajax({
        type: "POST",
        url: '/api/vehicles/count',
        data: form.serialize(),
        success: function (result) {
            if (result == 0) {
                $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Malheureusement votre dernière sélection ne donne aucun résultat.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'danger'
                    });
                btn.val(result + ' résultats');
                btn.attr('disabled', true);
            } else {
                btn.attr('disabled', false);
                btn.val(result + ' résultats');
            }

        }
    });
}
getResultCount($('#dataIsneuf').val())
$(document).ready(function() {
    $('#brand').select2({
        templateResult: formatState,
        templateSelection: formatState,
    });
}); 
$(function () {
    $("#mileage").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: 0,
        max: 200000,
        from: $("#minMileage").val(),
        to: $("#maxMileage").val(),
        type: 'double',
        step: 1,
        postfix: " KM",
        max_postfix: " +",
        grid: true,
        onFinish: saveMileage,
    });

});
$(function () {  
                console.log($("#GminPrice").val());
        console.log($("#GmaxPrice").val());
            
    $("#price").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: 0,
        max: 9000000,
        from: $("#GminPrice").val(),
        to: $("#GmaxPrice").val(),
        type: 'double',
        step: 1000,
        postfix: " DH",
        max_postfix: " +",
        grid: true,
        onFinish: savePrice,
    });
});
$(function () {
    $("#rangeFiscal").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: 0,
        max: 20,
        from: $("#minPuissance").val(),
        to: $("#maxPuissance").val(),
        type: 'double',
        step: 1,
        postfix: " CH",
        max_postfix: " +",
        grid: true,
        onFinish: saveRangeFiscal,
    });
});
$('#brand').change(function () {        
    var model = $("#model");
    var models = $("#dataModels").data("models");
    var filtred = Array();
    for (var i = 0; i < models.length; i++) {
        if (models[i].brand_id == $(this).val()) {
            filtred.push(models[i]);
        }
    }
    if (filtred.length > 0) {
        model.empty();
        model.removeAttr('disabled');
        model.append('<option value="0">Tous</option>');
        for (var i = 0; i < filtred.length; i++) {
            model.append('<option value="' + filtred[i]['id'] + '">' + filtred[i]['name'] + '</option>');
        }
    } else {
        model.empty();
        model.attr('disabled', 'disabled');
        model.append('<option value="0">Tous</option>');
    }
    getResultCount($('#dataIsneuf').val()) 
});
$('.category').click(function () {
    $('.listing-car-Gf .category').removeClass('active');
    $('.listing-car-Gf-Rch .category').removeClass('active');
    $(this).addClass('active');
    $('#category2').val($(this).attr('id')).trigger('change');
    $('#category').val($(this).attr('id')).trigger('change');
    //console.log($('#category2').val());
    //console.log($('#category').val());
});
$('.energy').click(function () {
    $('.listing-eng-Gf .energy').removeClass('active');
    $(this).addClass('active');
    $('#energy2').val($(this).attr('id')).trigger('change');
}); 
$('.trans').click(function () {
    $('.listing-eng-Gf .trans').removeClass('active');
    $(this).addClass('active');
    $('#trans2').val($(this).attr('id')).trigger('change');
});
$('.result').change(function (){        
    getResultCount($('#dataIsneuf').val()) 
});    
$('#clearBrand').click(function(){
    $('#brand').val(0).change();
    $('#model').val(0).change();
    $('#version').val(0).change();
})
$('#clearPrix').click(function(){
    instance = $("#price").data("ionRangeSlider");
    $('#GminPrice').val(0).change();
    $('#GmaxPrice').val(9000000).change();
    instance.update({
        from:0,to:9000000
    });
})
$('#clearCat').click(function(){
    $('#category').val(0).change();
    $('.listing-car-Gf-Rch .category').removeClass('active');
})
$('#clearEnergy').click(function(){ 
    $('.moteur_clear').removeAttr('checked').change()
    instance = $("#mileage").data("ionRangeSlider");
    $('#minMileage').val(0).change();
    $('#maxMileage').val(200000).change();
    instance.update({
        from:0,to:200000
    });
    instance2 = $("#rangeFiscal").data("ionRangeSlider");
    $('#minPuissance').val(0).change();
    $('#maxPuissance').val(20).change();
    instance2.update({
        from:0,to:20
    });
})
$('#clearCity').click(function(){
    $('#city').val(0).change();
})
$('#clearEtat').click(function(){
    $('.etat_clear').removeAttr('checked').change()
})
$('#clearEquipements').click(function(){
    $('.equi_clear').removeAttr('checked').change()
})
