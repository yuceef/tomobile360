//Select2
    function formatState (state) {
        if (state.text == "Toutes") 
            var $state = $(
                /*'<span style="padding-left:7px;">' + state.text + '</span>'*/
                '<span>' + state.text + '</span>'
            );
        else{
            var Url = $("#BrandId" + state.text.split(' ').join('_')).val();            
            var $state = $(
                '<span><img src="' + Url + '" class="img-flag" /> ' + state.text + '</span>'
            );
        }
        return $state;
    };

    $(document).ready(function() {
        $('#brandSearch-nf').select2({
            templateResult: formatState,
            templateSelection: formatState,
        });
    }); 
    
    $(document).ready(function() {
        $('#brandSearch-occ').select2({
            templateResult: formatState,
            templateSelection: formatState,
        });
    }); 
    $(document).ready(function() {
        $('.brand4').select2({
            templateResult: formatState,
            templateSelection: formatState,
        });
    }); 

// Function
var ajaxgetResltCount = null;
function getResultCount(neuf) {
    console.log("start getResultCount");   
    var idName = (neuf == 1) ? 'nf' : 'occ'
    var btn = $('#afficher-'+idName)
    var form = $('#formSearch'+idName);
    if (ajaxgetResltCount != null)ajaxgetResltCount.abort()
    ajaxgetResltCount = $.ajax({
        type: "POST",
        url: '/api/vehicles/count',
        data: form.serialize(),
        success: function (result) {
            if (result == 0) {
                $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Malheureusement votre dernière sélection ne donne aucun résultat.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'danger'
                    });
                btn.val(result + ' résultats');
                btn.attr('disabled', true);
            } else {
                btn.attr('disabled', false);
                btn.val(result + ' résultats');
            }

        }
    });
    console.log("end getResultCount");
}

function getBrands(neuf){
    var idName = (neuf == 1) ? 'nf' : 'occ'
    var model = $("#modelSearch-" + idName);
    type = $('#typeSearch-' + idName).val()
    $.ajax({
        url: "/api/brand/byType/" + type + "/" + neuf, 
        success: function (result) {
            var brandc = $("#brandSearch-" + idName);
            brandc.empty();
            brandc.append('<option style="color:black" value="0" selected>Toutes</option>');
            for (var i = 0; i < result.brands.length; i++) {
                brandc.append('<option value="' + result.brands[i]['id'] + '">' + result.brands[i]['name'] + '</option>');
            }
            model.empty();
            model.attr('disabled', 'disabled');
            model.append('<option value="0">Tous</option>');
            $("#dataModels").data("models", result.models)
            getResultCount(neuf)
        }
    }); 
}
function showModel(model, selector) {
    var models = $("#dataModels").data("models");
    var filtred = Array();
    for (var i = 0; i < models.length; i++) {
        if (models[i].brand_id == selector.val()) {
            filtred.push(models[i]);
        }
    }
    if (filtred.length > 0) {
        model.empty();
        model.removeAttr('disabled');
        model.append('<option value="0" selected>Tous</option>');
        for (var i = 0; i < filtred.length; i++) {
            model.append('<option value="' + filtred[i]['id'] + '">' + filtred[i]['name'] + '</option>');
        }
    } else {
        model.empty();
        model.attr('disabled', 'disabled');
        model.append('<option value="0" selected>Tous</option>');
    }
}
// Search Neuf   
getBrands(1)
$('#tab1').click(function () {
    getBrands(1)
});
$('#nvcf-tab').click(function () {
    $('#typeSearch-nf').val('voiture')
    getBrands(1)
});
$('#nvcf-moto').click(function () {
    $('#typeSearch-nf').val('moto')
    getBrands(1)
});
$('#nvcf-bcar').click(function () {
    $('#typeSearch-nf').val('camion')
    getBrands(1)
});
$('#brandSearch-nf').change(function () {
    var model = $("#modelSearch-nf");
    showModel(model, $(this));
});
$('.resultNeuf').change(function () {
    getResultCount(1)
}); 

// Search-occ
$('#tab2').click(function () {
    getBrands(0)
});
$('#brandSearch-occ').change(function () {
    var model = $("#modelSearch-occ");
    showModel(model, $(this));
    getResultCount(0)
});
$('.resultOcc').change(function () {
    getResultCount(0)
});
$('#occcf-tab').click(function () {
    $('#typeSearch-occ').val('voiture')
    getBrands(0)
});
$('#occcf-moto').click(function () {
    $('#typeSearch-occ').val('moto')
    getBrands(0)
});
$('#occcf-bcar').click(function () {
    $('#typeSearch-occ').val('camion')
    getBrands(0)
});
$('#occcf-acc').click(function () {
    $('#typeSearch-occ').val('equipement')
    getBrands(0)
});

// Guide
$('.category').click(function () {
    $('.listing-car-Gf .category').removeClass('active');
    $('.listing-car-Gf-Rch .category').removeClass('active');
    $(this).addClass('active');
    $('#category2').val($(this).attr('id')).trigger('change');
    $('#category').val($(this).attr('id')).trigger('change');
    //console.log($('#category2').val());
    //console.log($('#category').val());
});
$('.energy').click(function () {
    $('.listing-eng-Gf .energy').removeClass('active');
    $(this).addClass('active');
    $('#energy2').val($(this).attr('id')).trigger('change');
}); 
$('.trans').click(function () {
    $('.listing-eng-Gf .trans').removeClass('active');
    $(this).addClass('active');
    $('#trans2').val($(this).attr('id')).trigger('change');
});
var savePrice = function (data) {
    if (data.from != $("#GminPrice").val()) {
        $("#GminPrice").val(data.from).trigger('change');
    }
    if (data.to != $("#GmaxPrice").val()) {
        $("#GmaxPrice").val(data.to).trigger('change');
    }
};
var writeResult = function () {
    var result = "from: " + from + ", to: " + to;
    //console.log("result=>"+result);
};
$(function () {
    $("#price").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: 0,
        max: 9000000,
        type: 'double',
        step: 1000,
        postfix: " DH",
        max_postfix: " +",
        grid: true,
        onFinish: savePrice,
    });
});
$('.result3').change(function () {
    minPrice = $('#GminPrice').val();
    maxPrice = $('#GmaxPrice').val();
    energy = $('#energy2').val();
    category = $('#category2').val();
    trans = $('#trans2').val();
    categoryText = $('li.category.active p').text();
    var btn = $('#afficherGuide')
    var form = $('#formGuide');
    if (ajaxgetResltCount != null) ajaxgetResltCount.abort()
    ajaxgetResltCount = $.ajax({
        type: "POST",
        url: '/api/vehicles/count',
        data: form.serialize(),
        success: function (result) {
            if (result == 0) {
                $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Malheureusement votre dernière sélection ne donne aucun résultat.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'danger'
                    });
                btn.val(result + ' résultats');
                btn.attr('disabled', true);
            } else {
                btn.attr('disabled', false);
                btn.val(result + ' résultats');
            }

        }
    });

});    

//Estimation
$('.result-estim').change(function () {
    if($("#modelSearch-estim").val() != '0' && $("#brandSearch-estim").val() != '0' && $("#yearSearch-estim").val() != '0'){
        $('#valider-estim').removeAttr('disabled')
    }
    else $("#valider-estim").attr('disabled', 'disabled');
})
$('#brandSearch-estim').change(function () {
    var model = $("#modelSearch-estim");
    showModel(model, $(this));
});
$('#valider-estim').click(function () {
    var form = $('#formEstim');
    var prix =  $('#prixEstim');
    prix.empty()
    if (ajaxgetResltCount != null) ajaxgetResltCount.abort()
    ajaxgetResltCount = $.ajax({
        type: "POST",
        url: '/api/vehicles/estim',
        data: form.serialize(),
        success: function (result) {
            if (result == 0) {
                $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Malheureusement votre dernière sélection ne donne aucun résultat.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'danger'
                    });

            } else {
                prix.append('<b>'+result + ' DH</b>');
            }

        }
    });

});

// Comparateur
$('.result-compa').change(function () {
    if ($("#brandComparateur1").val() != '0' && $("#modelComparateur1").val() != '0' && $("#versionComparateur1").val() != '0' && $("#brandComparateur2").val() != '0' && $("#modelComparateur2").val() != '0' && $("#versionComparateur2").val() != '0' ) {
        $('#submitComparateur').removeAttr('disabled')
    }
    else $("#submitComparateur").attr('disabled', 'disabled');
})
$('#brandComparateur1').change(function () {
    var model = $("#modelComparateur1");
    showModel(model, $(this));
});
$('#brandComparateur2').change(function () {
    var model = $("#modelComparateur2");
    showModel(model, $(this));
});
$('#modelComparateur1').change(function () {
    var version = $("#versionComparateur1");
    $.ajax({
        type: "GET",
        url: '/api/vehicles/comparateurVehicles/' + $(this).val(),
        success: function (result) {
            if (result.length > 0) {
                version.empty();
                version.removeAttr('disabled');
                version.append('<option value="0" selected>Toutes</option>');
                for (var i = 0; i < result.length; i++) {
                    version.append('<option value="' + result[i]['id'] + '">' + result[i]['name'] + '</option>');
                }
            } 
            else {
                version.empty();
                version.attr('disabled', 'disabled');
                version.append('<option value="0" selected>Toutes</option>');
            }
        }
    });
});
$('#modelComparateur2').change(function () {
    var version = $("#versionComparateur2");
    $.ajax({
        type: "GET",
        url: '/api/vehicles/comparateurVehicles/' + $(this).val(),
        success: function (result) {
            if (result.length > 0) {
                version.empty();
                version.removeAttr('disabled');
                version.append('<option value="0" selected>Toutes</option>');
                for (var i = 0; i < result.length; i++) {
                    version.append('<option value="' + result[i]['id'] + '">' + result[i]['name'] + '</option>');
                }
            } 
            else {
                version.empty();
                version.attr('disabled', 'disabled');
                version.append('<option value="0" selected>Toutes</option>');
            }
        }
    });
});
