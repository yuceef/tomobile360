<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/', 'HomeController@index')->name('home');

//Api Ajax
Route::post('/api/vehicles/count', 'ApiController@getNbOfVehicles');
Route::post('/api/vehicles/estim', 'ApiController@estimVehicles');
Route::post('/api/vehicles/formulaires', 'ApiController@formulaires');
Route::get('/api/brand/byType/{type}/{neuf}', 'ApiController@brandByType');
Route::post('/api/comments/store', 'CommentController@store');
Route::get('/api/comments/exprension/{exp}/{id}', 'CommentController@exprension');
Route::get('/api/vehicles/comparateurVehicles/{model}', 'ApiController@getcomparateurVehicles');

//Search Routes
Route::post('/search/searchNeuf', "searchController@searchNeuf");
Route::post('/search/guide_search', "searchController@searchNeuf");
Route::post('/search/searchOccasion', "searchController@searchOccasion");
Route::get('/search/searchOccasion', "searchController@searchOccasion"); // pagination
Route::get('/search/neuf/{brand}', "searchController@searchNeufBrand");
Route::get('/search/neuf/{brand}/{model}', "searchController@searchNeufModel");
Route::get('/search/searchDetailed', "searchController@searchDetailed");
Route::get('/search/searchDetailedOccasion', "searchController@searchDetailedOccasion");

//Routes Neuf
Route::get('/neuf', 'NeufController@index');
Route::post('/neuf/comparateur', 'NeufController@comparateur');
Route::get('/neuf/{brand}', 'NeufController@brand');
Route::get('/neuf/{brand}/{model}', 'NeufController@model');
Route::get('/neuf/{brand}/{model}/{version}', 'NeufController@version');

//Route Occasion
Route::get('/occasion', 'OccasionController@index');
Route::get('/occasion/annonce/{{id}}', 'OccasionController@index');

//Route Partenaire
Route::resource('/partenaires', 'PartenaireController');

//Route Video
Route::get('/videos', 'VideoController@allVideos');
Route::get('/videos/brand/{brandname}', 'VideoController@videosBySelect');
Route::get('/videos/{id}', 'ArticleController@show');

//Routes article
Route::get('/articles', 'ArticleController@allArticles');
Route::get('/articles/cat/{catname}', 'ArticleController@articlesByCat');
Route::get('/articles/{id}', 'ArticleController@showArticle');

// Routes administration
Route::resource('/administration/video', 'VideoController');
Route::resource('/administration/article', 'ArticleController');
Route::resource('/administration/brand', 'BrandController');
Route::resource('/administration/model', 'ModeleController');

Route::get('/administration/comment', 'CommentController@index');
//Route::get('/administration/comment/cmntSupp', 'CommentController@cmntSupp');
Route::get('/administration/comment/accepter/{id}', 'CommentController@accepter');
Route::get('/administration/comment/supprimer/{id}', 'CommentController@destroy');
Route::get('/administration/active/{id}/finished', 'UserController@active');

Route::resource('/administration/categorie', 'CategorieArticleController');
Route::resource('/administration', 'UserController');