<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartenairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partenaires', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nom');
            $table->string('prenom');
            $table->string('societe');
            $table->string('logo')->nullable();
            $table->string('link')->nullable();
            $table->string('email')->nullable();
            $table->string('phone');
            $table->string('city');
            $table->unsignedInteger('user_id')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        
        Schema::table('partenaires', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partenaires');
    }
}
