<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('image');
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->unsignedInteger('model_id')->nullable();
            $table->unsignedInteger('energie_id')->nullable();//Carburant
            $table->double('price');
            $table->enum('transmission', ['automatique', 'manuelle'])->nullable();
            $table->boolean('isNeuf')->default(false);
            $table->boolean('status')->default(false);
            $table->string('immatriculation')->nullable();
            $table->integer('rating')->nullable();
            $table->unsignedInteger('ad_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();            
            $table->unsignedInteger('user_id')->nullable();                        
            $table->integer('year')->nullable();   

            $table->integer('nbrprop')->nullable();
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('wheelbase')->nullable();
            $table->integer('numberOfDoors')->nullable();
            $table->integer('numberOfPlaces')->nullable();
            $table->integer('trunkVolume')->nullable();
            $table->integer('unloadedWeight')->nullable();
            $table->integer('loadedWeight')->nullable();
            $table->integer('frontTireWidth')->nullable();
            $table->integer('frontTire')->nullable();
            $table->integer('frontTiresManufacturingCode')->nullable();
            $table->integer('diameterFrontWheels')->nullable();
            $table->integer('maximumSpeedAllowedFrontTires')->nullable();
            $table->integer('rearTire')->nullable();
            $table->integer('rearTireWidthHeightRatio')->nullable();
            $table->integer('rearTireManufacturingCode')->nullable();
            $table->integer('diameterRearWheels')->nullable();
            $table->integer('maximumSpeedAllowedRearTires')->nullable();
            $table->integer('engine')->nullable();
            $table->integer('cylinder')->nullable();
            $table->integer('reports')->nullable();
            $table->string('frame')->nullable();
            $table->integer('fiscalPower')->nullable();
            $table->integer('realPower')->nullable();
            $table->string('gearbox')->nullable();
            $table->integer('numberOfSpeeds')->nullable();
            $table->integer('origine')->nullable();
            $table->integer('numberOfOwners')->nullable();
            $table->string('aerodynamics')->nullable();
            $table->integer('valves')->nullable();
            $table->string('engineCouple')->nullable();
            $table->string('traction')->nullable();
            $table->string('consumptionRoad')->nullable();
            $table->string('consumptionCity')->nullable();
            $table->string('mixedConsumption')->nullable();
            $table->string('distanceWidthFull')->nullable();
            $table->string('maximumSpeed')->nullable();
            $table->string('acceleration')->nullable();
            $table->string('color')->nullable();
            $table->integer('waranty')->nullable();
            $table->string('reference')->nullable();
            $table->integer('mileage')->nullable();
            $table->integer('position')->nullable();
            $table->integer('turningDiameter')->nullable();
            $table->string('kilometerRepeated')->nullable();
            $table->string('emissionCarbonDioxide')->nullable();
            $table->string('emissionHydrocarbonParticles')->nullable();
            $table->string('particleEmission')->nullable();
            $table->string('normeAntiPollution')->nullable();
            $table->string('placeManufacture')->nullable();
            $table->string('interiorDesign')->nullable();
            $table->string('interiorColor')->nullable();
            $table->string('exteriorColor')->nullable();
            $table->integer('cylinderNumber')->nullable();
            $table->string('reservoirCapacity')->nullable();
            $table->integer('recommandation')->nullable();            
            $table->timestamps();
        });
        
        Schema::table('vehicles', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('energie_id')->references('id')->on('energies');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
