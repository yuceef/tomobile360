<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('username')->unique()->nullable();
            $table->string('garage')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('civility', ['Mr', 'Mlle','Madame']);
            $table->string('first_name');
            $table->string('last_name');
            $table->date('birthdate')->nullable();
            $table->string('postal')->nullable();
            $table->string('fixe')->nullable();
            $table->enum('user_type', ['particulier', 'professionnel']);
            $table->string('phone_number');
            $table->string('adresse');
            $table->string('city');
            $table->string('country');
            $table->integer('type')->default(0); //0:user 1:admin 
            $table->integer('rating')->default(0);
            $table->boolean('is_active')->default(false);
            $table->dateTime('last_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
