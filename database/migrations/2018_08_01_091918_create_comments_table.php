<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->boolean('status')->default(1);
            $table->text('body_pos')->nullable();
            $table->text('body_neg')->nullable();
            $table->text('body')->nullable();
            $table->unsignedInteger('video_id')->nullable();
            $table->unsignedInteger('article_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('like')->default(0);
            $table->unsignedInteger('dislike')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('comments', function($table) {
            $table->foreign('video_id')->references('id')->on('videos');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('article_id')->references('id')->on('articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
