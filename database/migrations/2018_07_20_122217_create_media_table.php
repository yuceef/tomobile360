<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('video_id')->nullable();
            $table->unsignedInteger('vehicle_id')->nullable();
            $table->unsignedInteger('article_id')->nullable();
            $table->string('path');
            $table->string('type')->default('image');
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->string('thumbnail')->nullable();
            $table->timestamps();
        });
        
        Schema::table('media', function($table) {
            $table->foreign('video_id')->references('id')->on('videos');
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->foreign('article_id')->references('id')->on('articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
