<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('body');
            $table->unsignedInteger('user_id')->nullable();
            $table->boolean('status')->default('1');
            $table->string('slug');
            $table->string('cover');
            $table->string('creation_date');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('model_id')->nullable();
            $table->unsignedInteger('brand_id');
            $table->timestamps();
        });
        
        Schema::table('articles', function($table) {
            $table->foreign('category_id')->references('id')->on('category_articles');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('model_id')->references('id')->on('models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
