<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('model_id')->nullable();
            $table->unsignedInteger('brand_id')->nullable();
            $table->unsignedInteger('vehicle_id')->nullable();
            $table->string('title');
            $table->string('image')->nullable();
            $table->string('url')->nullable();
            $table->text('description');
            $table->timestamps();
        });
        
        Schema::table('videos', function($table) {
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('model_id')->references('id')->on('models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
