<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->enum('type', ['voiture', 'moto', 'camion', 'equipement']);
            $table->unsignedInteger('categorie_id')->nullable();
            $table->unsignedInteger('brand_id');
            $table->string('image')->default('/img/vehicule.png');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->timestamps();
        });
        Schema::table('models', function($table) {
            $table->foreign('categorie_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models');
    }
}
