@extends('layouts.app')

@section('content')
<section class="PreBd m-t-30">
    <div class="container">
        <div class="section-block section-block-white HasPadding">
            <div class="row">
                <div class="Pagesheader color-theme col-md-12">
					<h2 class="text-uppercase">Devenir partenaire</h2>
				</div>
                <div class="Breadcrumps col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{ url('/') }}">Accueil</a></li>
                        <li><a href="#" class="active">Devenir partenaire</a></li>
                    </ul>
                </div>
            </div>
            <p class="col-md-12 m-b-30 m-t-10">
                Vous souhaitez booster votre activité et capter de nouveaux clients ! Rejoignez <strong class="color-theme">Tomobile 360</strong> .
                <br>
                Nous vous contacterons sous <span class="color-theme">24h</span> pour vous présenter en detail notre offre et mettre en place un partenariat.
            </p>
            <div class="col-md-12 m-b-20 m-t-10">
                <form class="col-md-6 offset-md-3" method="post" action="/partenaires">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                         @if(Session::has('m'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->pull('m') }}
                            </div>
                        @endif
                    @csrf
                    <div class="form-group row m-b-10">
                        <label for="nom" class="col-md-3 col-form-label">Nom<span class="color-theme">*</span> :</label>
                        <div class="col-md-9">
                            <input id="nom" type="text" class="form-control" name="nom" required>
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label for="prenom" class="col-md-3 col-form-label">Prénom<span class="color-theme">*</span> :</label>
                        <div class="col-md-9">
                            <input id="prenom" type="text" class="form-control" name="prenom" required>
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label for="societe" class="col-md-3 col-form-label">Société<span class="color-theme">*</span> :</label>
                        <div class="col-md-9">
                            <input id="societe" type="text" class="form-control" name="societe" required>
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label for="link" class="col-md-3 col-form-label">Site :</label>
                        <div class="col-md-9">
                            <input id="link" type="text" class="form-control" name="link">
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label for="email" class="col-md-3 col-form-label">E-mail :</label>
                        <div class="col-md-9">
                            <input id="email" type="text" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label for="phone" class="col-md-3 col-form-label">Téléphone<span class="color-theme">*</span> :</label>
                        <div class="col-md-9">
                            <input id="phone" type="text" class="form-control" name="phone" required>
                        </div>
                    </div>
                    <div class="form-group row m-b-10">
                        <label for="city" class="col-md-3 col-form-label">Ville<span class="color-theme">*</span> :</label>
                        <div class="col-md-9">
                            <input id="city" type="text" class="form-control" name="city" required>
                        </div>
                    </div>
                    <div class="form-group offset-md-9 m-b-10 m-t-10">
                        <button type="submit" class="btn btn-primary btn-blue btn-block">Envoyer</button>
                    </div>
                </form> 
            </div>
    </div>
</section>
@endsection