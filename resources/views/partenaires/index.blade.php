@extends('layouts.app')

@section('content')
<section class="PreBd m-t-30">
    <div class="container">
        <div class="section-block section-block-white HasPadding">
            <div class="row">
                <div class="Pagesheader color-theme col-md-12">
					<h2 class="text-uppercase">Nos partenaires</h2>
				</div>
                <div class="Breadcrumps col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{ url('/') }}">Accueil</a></li>
                        <li><a href="#" class="active">Tous les partenaires</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="AutoRM-container m-t-15">
                    <div class="row m-0">
                        @foreach ($partenaires as $partenaire)
                            <div class="col-md-3">
                                <article class="AutoRM-Box AutoRM-DIV-Brand">
                                    <div class="AutoRM-Thumb ">
                                        <a class="brResult" href="@if($partenaire->link != null) {{ $partenaire->link }} @else # @endif"><img class="brandWidth" src="{{ $partenaire->logo }}" alt=""></a>
                                    </div>
                                </article>
                            </div>
                        @endforeach   
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center m-b-20 m-t-20">
                <a href="/partenaires/create"><button class="btn btn-primary btn-blue btn-block">Devenir partenaire </button></a>
            </div>
        </div>
    </div>
</section>
@endsection