@extends('layouts.app')

@section('style')
<style>
.checkbox-ui+label:before, .checkbox-ui:not(.filled-in)+label:after{
    left: -25px !important;
    top: 2px !important;
}
.checkbox-ui:checked+label:before{
    left:-30px !important;
    top: -4px !important;
}
.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.InscriptionHeader h2{
    font-size: 30px;
    font-weight: 900; 
    color: #1eb6e7 !important;
    -webkit-margin-before: 0.83em;
    -webkit-margin-after: 0.83em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
}
.InscriptionHeader h4 {
    font-weight: 600;
    font-size: 20px;
}
.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    line-height: 1.5;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    font-size: .9rem;
    color: #555;
}
button, input {
    overflow: visible;
}
input{
    margin: 0;
    font-family: inherit;
    -webkit-rtl-ordering: logical;
    cursor: text;
    text-rendering: auto;
    letter-spacing: normal;
    word-spacing: normal;
    text-transform: none;
    text-indent: 0px;
    text-shadow: none;
    text-align: start;
    font: 400 13.3333px Arial;
    -webkit-writing-mode: horizontal-tb !important;
}
.form-group{
    padding-bottom:8px;
}

</style>
@endsection
@section('content')
<div class="PreBd m-t-30">	
	<div class="container">
	    <div class="section-block HasPadding WhiteSection">		
		    <div class="row">
                    

			        <div class="FormInscription" >
                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" >
                        @csrf


                        <div class="InscriptionHeader" align="center">
                            <h2>Choisir votre profil</h2>
                        </div>
                        <div class="form-group" align="center">
                            <div class="form-check form-check-inline col-md-2">
                                <input class="form-check-input etat_clear radiobox-ui result" type="radio" name="user_type" id="origine1" value="particulier" checked="checked" onchange="showDiv(this)">
                                <label class="form-check-label" for="origine1">Particulier</label>
                            </div>
                            <div class="form-check form-check-inline">
                                    <input class="form-check-input etat_clear radiobox-ui result" type="radio" name="user_type" id="origine2" value="professionnel" onchange="showDiv(this)">
                                    <label class="form-check-label" for="origine2">Professionnel</label>
                            </div>
                        </div>
                    </br>


                    <div id="par">   
                        <div class="col-md-12 InscriptionHeader" align="center">
                            <h2 class="color-theme text-uppercase m-b-10">Inscription</h2>
                            <h4 class="text-uppercase">Création de mon compte client</h4>
                        </div>   
                    <div class="col-md-8 offset-md-2 m-t-60">
                        <div class="form-group row">
                            <label for="username" class="col-sm-4 col-form-label">{{ __('Nom d\'utilisateur') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}">

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label">{{ __('E-mail') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-4 col-form-label">{{ __('Mot de passe') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-sm-4 col-form-label">{{ __('Confirmer le mot de passe') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="civility" class="col-sm-4 col-form-label">{{ __('Civilité') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <select name="civility" class="form-control result{{ $errors->has('civility') ? ' is-invalid' : '' }}" id="civility" required>
                                    <option>Mr</option>
								    <option>Mlle</option>
								    <option>Madame</option>                                           
                                </select>
                                @if ($errors->has('civility'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('civility') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="first_name" class="col-sm-4 col-form-label">{{ __('Prénom') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-sm-4 col-form-label">{{ __('Nom') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birthdate" class="col-sm-4 col-form-label">{{ __('Date de naissance') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="birthdate" type="date" class="form-control{{ $errors->has('birthdate') ? ' is-invalid' : '' }}" name="birthdate" value="{{ old('birthdate') }}" required>

                                @if ($errors->has('birthdate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_number" class="col-sm-4 col-form-label">{{ __('Tél') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" required>

                                @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="adresse" class="col-sm-4 col-form-label">{{ __('Adresse') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="adresse" type="text" class="form-control{{ $errors->has('adresse') ? ' is-invalid' : '' }}" name="adresse" value="{{ old('adresse') }}" required>

                                @if ($errors->has('adresse'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('adresse') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="city" class="col-sm-4 col-form-label">{{ __('Ville') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required>

                                @if ($errors->has('city'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country" class="col-sm-4 col-form-label">{{ __('Pays') }}<span class="color-theme">*</span></label>

                            <div class="col-sm-8">
                                <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}" required>

                                @if ($errors->has('country'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    </div>







                    
                    <div id="pro" style="display:none;">
                        <div class="col-md-12 InscriptionHeader" align="center">
                            <h2 class="color-theme text-uppercase m-b-10">Inscription</h2>
                            <h4 class="text-uppercase">SOUSCRIPTION CLIENT PROFESSIONNEL</h4>
                        </div>   
                        <div class="col-md-8 offset-md-2 m-t-60">
                            <div class="form-group row">
                                <label for="garage" class="col-sm-4 col-form-label">{{ __('Nom du garage / société') }}<span class="color-theme">*</span></label>
        
                                <div class="col-sm-8">
                                    <input id="garage" type="text" class="form-control{{ $errors->has('garage') ? ' is-invalid' : '' }}" name="garage" value="{{ old('garage') }}" required autofocus>
        
                                        @if ($errors->has('garage'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('garage') }}</strong>
                                            </span>
                                        @endif
                                </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="civility" class="col-sm-4 col-form-label">{{ __('Civilité') }}<span class="color-theme">*</span></label>
            
                                        <div class="col-sm-8">
                                            <select name="civility" class="form-control result{{ $errors->has('civility') ? ' is-invalid' : '' }}" id="civility2" required>
                                                <option>Mr</option>
                                                <option>Mlle</option>
                                                <option>Madame</option>                                           
                                            </select>
                                            @if ($errors->has('civility'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('civility') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password" class="col-sm-4 col-form-label">{{ __('Mot de passe') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="password2" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-sm-4 col-form-label">{{ __('Confirmer le mot de passe') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="password-confirm2" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="last_name" class="col-sm-4 col-form-label">{{ __('Nom du contact') }}<span class="color-theme">*</span></label>
            
                                        <div class="col-sm-8">
                                            <input id="last_name2" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>
            
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="first_name" class="col-sm-4 col-form-label">{{ __('Prénom du contact') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="first_name2" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>
        
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label">{{ __('Adresse e-mail') }}<span class="color-theme">*</span></label>
            
                                        <div class="col-sm-8">
                                            <input id="email2" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
            
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="phone_number" class="col-sm-4 col-form-label">{{ __('Tél') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="phone_number2" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" required>
        
                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="fixe" class="col-sm-4 col-form-label">{{ __('Fixe') }}</label>
            
                                        <div class="col-sm-8">
                                            <input id="fixe" type="text" class="form-control{{ $errors->has('fixe') ? ' is-invalid' : '' }}" name="fixe" value="{{ old('fixe') }}">
            
                                            @if ($errors->has('fixe'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('fixe') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="adresse" class="col-sm-4 col-form-label">{{ __('Adresse') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="adresse2" type="text" class="form-control{{ $errors->has('adresse') ? ' is-invalid' : '' }}" name="adresse" value="{{ old('adresse') }}" required>
        
                                        @if ($errors->has('adresse'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('adresse') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="postal" class="col-sm-4 col-form-label">{{ __('Code postale') }}</label>
            
                                        <div class="col-sm-8">
                                            <input id="postal" type="text" class="form-control{{ $errors->has('postal') ? ' is-invalid' : '' }}" name="postal" value="{{ old('postal') }}">
            
                                            @if ($errors->has('postal'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('postal') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="city" class="col-sm-4 col-form-label">{{ __('Ville') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="city2" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required>
        
                                        @if ($errors->has('city'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="country" class="col-sm-4 col-form-label">{{ __('Pays') }}<span class="color-theme">*</span></label>
        
                                    <div class="col-sm-8">
                                        <input id="country2" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}" required>
        
                                        @if ($errors->has('country'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </div>




















                    
                    <div class="col-md-8 offset-md-2 m-t-6">
                        <div class="form-group row">
                                <label for="in-lb-1" class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-8">
                                    <div class="form-check text-justify">
                                        <input class="form-check-input checkbox-ui" type="checkbox" name="cond" id="cond" value="cond" required>
                                        <label class="form-check-label" for="cond">
                                        En vous inscrivant sur Tomobile 360, vous acceptez les <a href="" class="font-600"><strong>Conditions Générales d’Utilisation.</strong></a>
                                        </label>
                                    </div>
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <label for="in-lb-1" class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-8">
                                    <div class="form-check text-justify">
                                        <input class="form-check-input checkbox-ui" type="checkbox" name="info" id="info" value="info" >
                                        <label class="form-check-label" for="info">
                                        Je souhaite recevoir les informations et promotions de Tomobile 360 par mail
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row m-t-15">
                                <label for="in-lb-1" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8" >
                                        <button type="submit" class="btn btn-big btn-block btn-blue">
                                            {{ __('Inscription') }}
                                        </button>
                                    </div>
                            </div>
    
                            <div class="form-group row m-t-15">
                                <label for="in-lb-1" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <div class="text-justify" style="color: #999;font-size: 10px;display: block; margin-bottom:20px;">
                                            Les champs avec (*) nous sont indispensables pour la création de votre compte. Nous en sommes les seuls destinataires, ces informations pouvant cependant être accessibles, pour des raisons techniques, aux prestataires assurant leur traitement, notamment les télé-opérateurs, ainsi que l’hébergement du site. Vous bénéficiez d’un droit d’accès, de rectification et de suppression des informations vous concernant ainsi que d’un droit d’opposition, le cas échéant  pour des motifs légitimes, au traitement de ces données, que vous pouvez exercer à tout moment par le biais de votre compte ou en écrivant à : contact@tomobile360.ma
                                        </div>
                                    </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script> 
        $("#pro *").attr('disabled', true);
        function showDiv(select){
            var checkBox1 = document.getElementById("origine1");
            var checkBox2 = document.getElementById("origine2");
            if(checkBox1.checked == true){
               
             document.getElementById('par').style.display = "block";
             document.getElementById('pro').style.display = "none";
             $("#pro *").attr('disabled', true);
             $("#par *").removeAttr('disabled');
            } else{
                document.getElementById('pro').style.display = "block";
                document.getElementById('par').style.display = "none";
                $("#par *").attr('disabled', true);
                $("#pro *").removeAttr('disabled');
            } 
         } 

    </script>
@endsection