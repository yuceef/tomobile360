@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/login.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="login-form">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('SE CONNECTER') }}</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if (session('warning'))
                                <div class="alert alert-warning">
                                    {{ session('warning') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Adresse e-mail') }}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="email" value="{{ old('email') }}" required autofocus>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback d-flex" role="alert">
                                                <i class="zmdi zmdi-alert-circle" style="font-size:16px ; color:#c80500"> &nbsp;</i>
                                                <strong class="vertical-align-center">{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback d-flex" role="alert">
                                                <i class="zmdi zmdi-alert-circle" style="font-size:16px ; color:#c80500"> &nbsp;</i>
                                                <strong class="vertical-align-center"> {{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div id="remember-me" class="form-check">
                                            <input class="form-check-input checkbox-ui"
                                                   type="checkbox" name="remember"
                                                   id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Se souvenir de moi') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                    <div class="col-md-6 offset-md-4 d-md-flex justify-content-between mb-3">
                                        <div id="reset-password" class="col-md-6  vertical-align-center">
                                            <a href="{{ route('password.request') }}">
                                                 {{ __('Mot de passe oublié ?') }}
                                            </a>
                                        </div>
                                        <div class="justify-content-end align-middle">
                                            <button type="submit" class="btn btn-blue btn-big btn-unrounded btn-block">
                                                {{ __('Se connecter') }}
                                            </button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection