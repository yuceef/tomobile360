@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/login.css')}}">
@endsection
@section('content')
    <div class="login-form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Réinitialiser le mot de passe') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('password.request') }}"
                                  aria-label="{{ __('Reset Password') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Adresse e-mail') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback d-flex" role="alert">
                                                <i class="zmdi zmdi-alert-circle" style="font-size:16px ; color:#c80500"> &nbsp;</i>
                                                <strong class="vertical-align-center">{{ $errors->first('email') }}</strong>

                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password"
                                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback d-flex" role="alert">
                                                <i class="zmdi zmdi-alert-circle" style="font-size:20px ; color:#c80500"> &nbsp;</i>
                                                <strong class="vertical-align-center">{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Confirmer le mot de passe') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div id="btn-reset" class="col-md-10">
                                        <div class="float-md-right">
                                            <button type="submit" class="btn btn-blue btn-big btn-unrounded btn-block">
                                                {{ __('Réinitialiser') }}
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
