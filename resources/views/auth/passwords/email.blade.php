@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/login.css')}}">
@endsection

@section('content')
    <div class="login-form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Récupérer le mot de passe') }}</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            <br/>
                                <div class="align-content-center">
                                    <a href="{{ url('/login') }}">
                                        ‹ {{ __(" Retour à la page d'authentification") }}
                                    </a>
                                </div>
                                @else

                            <form method="POST" action="{{ route('password.email') }}"
                                  aria-label="{{ __('Reset Password') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Adresse e-mail') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback d-flex" role="alert">
                                                <i class="zmdi zmdi-alert-circle" style="font-size:16px ; color:#c80500"> &nbsp;</i>
                                                <strong class="vertical-align-center">{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div id="btn-reset" class="col-md-10">
                                        <div class="float-md-right">
                                            <button type="submit" class="btn btn-blue btn-big btn-unrounded btn-block">
                                                {{ __('Récupérer') }}
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection