@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">
	
	<div class="container">
        <div class="section-block section-block-white HasPadding">
            
            <div class="row">

                <div class="Pagesheader color-theme col-md-12">
                    <h2 class="text-uppercase">Video</h2>
                </div>

                <div class="Breadcrumps col-md-12">
                    <ul>
                        <li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/videos')}}">Vidéos</a></li>
                        <li><a href="">{{$video->title}}</a></li>
                    </ul>
                </div>

            </div>
            
            <div class="row m-t-20">
                <input type="hidden" id="videoUrl" value="{{$video->url}}">
                <div class="Ads-video Ads-hg relative col-md-12 m-b-10" id="videoIf">
                    <div class="cover" style=" background-image: url('https://img.youtube.com/vi/{{$video->url}}/mqdefault.jpg'); "></div>
                    <a href="#videoInfo" class="Ads-video-action transform-both">
                        <i class="zmdi zmdi-play-circle-outline"></i>
                    </a>
                </div>
                
                <div class="Pagesheader col-md-12">
                    <h2 class="text-transform">{{$video->title}}</h2>
                    <div class="lpv-share">
                        <ul>
                            <li><label>Partager :</label></li>
                            <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank" ><i class="zmdi zmdi-facebook-box"></i></a></li>
                        </ul>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-9">
                    <div class="Comment-Listings">
                        <h4 class="Comment-L-title"><span>Commentaires</span></h4>
                        <div class="Comment-L-header overflow">
                            <div class="float-left"><label class="sr-1 m-t-10">{{sizeof($commentaires)}}@if(sizeof($commentaires)>1) commentaires @else commentaire @endif</label></div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="comment-box-container m-t-15">
                            <figure class="Comment-box pb-0" id="comments">
                                @auth
                                <form id="Commentform">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="mb-1 col-md-2 comment-avatar pl-2 pr-1">
                                            <center class="text-capitalize">
                                                <img src="/img/profile.png" class="d-block" />
                                                {{ Auth::user()->username }}
                                            </center>
                                        </div>
                                        <div class="col-md-10 pl-1">
                                            <textarea name="comment" class="form-control mb-2 col-md-12" rows="3"></textarea>
                                            <input type="hidden" name="videoId" value="{{$video->id}}">
                                            <div class="form-group mb-2 offset-md-10 m-t-10">
                                                <button type="button" id="commenter" class="btn btn-blue" style="padding: .375rem 1rem;">Commenter</button>
                                            </div>                                        
                                        </div>
                                    </div>
                                </form>
                                @else
                                    <p>
                                        Vous devez vous connecter pour laisser un commentaire. 
                                        <a class="btn btn-blue" href="{{ url('/login') }}" >Se connecter</a> 
                                        <a class="btn btn-blue" href="{{ url('/register') }}" >S'inscrire</a>
                                    </p>
                                @endauth
                            </figure>
                            @foreach($commentaires as $commentaire)
                            <figure class="Comment-box" id="cmnt{{$commentaire->id}}">
                                <div class="row">
                                    <div class="mb-2 col-md-2 comment-avatar pl-2 pr-1">
                                        <center class="text-capitalize">
                                            <img src="/img/profile.png" class="d-block" />
                                            {{ $commentaire->user->username }}
                                        </center>
                                    </div>
                                    <div class="col-md-8 comment-body pl-1">
                                        <div class="comment-date">Publié le {{(new Date($commentaire->created_at))->format('d F Y à H:i')}} </div>
                                        <div class="comment-content m-t-5">
                                            {{$commentaire->body}}
                                        </div>
                                    </div>
                                    <div class="col-md-2 relative comment-action">
                                        <div class="comment-exprension">
                                            <ul class="reaction">
                                                <li class="thumb-up">
                                                    <a style="cursor:pointer" @auth class="btnLike" @endauth>
                                                        <input type="hidden" value="{{$commentaire->id}}">
                                                        <span class="thumb-counter">{{$commentaire->like}}</span>
                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                    </a>
                                                </li>
                                                <li class="thumb-down">
                                                    <a style="cursor:pointer"  @auth class="btnDislike" @endauth>
                                                        <input type="hidden" value="{{$commentaire->id}}">
                                                        <i class="zmdi zmdi-thumb-down"></i>
                                                        <span class="thumb-counter">{{$commentaire->dislike}}</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            @endforeach
                        </div>

                    </div>
                </div>

                <aside class="col-md-3">
                    <div class="Ads-Box m-t-15">
                        <img src="/img/corbeille/ads-4.jpg">
                    </div>
                </aside>

            </div>

            <div class="row video-listings m-t-40">
                @foreach($videos as $video)
				<div class="col-md-4">
					<article class="video-box-item">
						<div class="Ads-video cover relative" style=" background-image: url('{{$video->image}}'); ">
							<a href="{{url('/videos')}}/{{$video->id}}" class="Ads-video-action transform-both">
								<i class="zmdi zmdi-play-circle-outline"></i>
							</a>
						</div>
						<div class="color-theme text-uppercase">VIDÉO</div>
						<a href="{{url('/videos')}}/{{$video->id}}">
                            <h4 class="video-box-title">{{$video->title}}</h4>
                        </a>
						<span class="block">{{(new Date($video->created_at))->format('d F Y à H:i')}}</h4>
					</article>
				</div>
				@endforeach
            </div>

        </div>
	</div>
	
</section>
@endsection
@section('script')
<script>
var reaction = true;
$('#videoIf a').click(function(){
    $('#videoIf').empty();
    $('#videoIf').append('<iframe width="100%" height="520" src="https://www.youtube.com/embed/'+$('#videoUrl').val()+'?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>')
})
$('.comment-box-container').on('click','.btnLike',function(){    
    if(reaction){
        reaction = false
        idcnt = $('input',this).val();
        myCnt = $(this)
        pp = $('.btnDislike ',myCnt.parents('ul'))       
        $.ajax({
            type: "GET",
            url: '/api/comments/exprension/like/'+idcnt,
            data: idcnt,
            success: function (result) {
                $('.thumb-counter',myCnt).remove()
                myCnt.append('<span class="thumb-counter">'+result.like+'</span>')
                $('.thumb-counter',pp).remove()
                pp.append('<span class="thumb-counter">'+result.dislike+'</span>')
                reaction = true
            }
        });    
    }    
})
$('.comment-box-container').on('click','.btnDislike',function(){    
    if(reaction){
        reaction = false
        idcnt = $('input',this).val();
        myCnt = $(this)
        pp = $('.btnLike ',myCnt.parents('ul'))       
        $.ajax({
            type: "GET",
            url: '/api/comments/exprension/dislike/'+idcnt,
            data: idcnt,
            success: function (result) {
                $('.thumb-counter',myCnt).remove()
                myCnt.append('<span class="thumb-counter">'+result.dislike+'</span>')
                $('.thumb-counter',pp).remove()
                pp.append('<span class="thumb-counter">'+result.like+'</span>')
                reaction = true
            }
        });            
    }    
})
$('#commenter').click(function(){
    $('#commenter').attr('disabled',true)
    $.ajax({
        type: "POST",
        url: '/api/comments/store',
        data: $('#Commentform').serialize(),
        success: function (result) {
            cmntBy = $('#cmntBy').val()
            cnt = '<div class="alert alert-success" role="alert">Merci pour votre commentaire.</div>'
            $('#Commentform textarea').val('');
            $(cnt).insertAfter('#comments')
            $('#commenter').removeAttr('disabled')

        }
    });
})
</script>
@endsection
@section('style')
<style>
    .iconPar{
        font-size: 24px;
        position: relative;
        top: 3px;
    }
    #shareModal li a {
        font-weight: 400;
    }
</style>
@endsection