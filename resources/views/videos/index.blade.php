@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">
	
	<div class="container">
		<div class="section-block section-block-white HasPadding">
			
			<div class="row">

				<div class="Pagesheader color-theme col-md-12">
					<h2 class="text-uppercase">Video</h2>
				</div>

				<div class="Breadcrumps col-md-12">
					<ul>
						<li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/videos')}}">Vidéos</a></li>
					</ul>
				</div>

			</div>
			<div class="row justify-content-end col-12 ClearMarginRt ClearPaddingRt m-t-15">
				<div class="form-group col-md-4 row">
					<label for="catSelect" class="col-md-3 col-form-label">Modéle:</label>
					<div class="col-md-9">
						<select id="catSelect" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" class="form-control">
							<option value="{{url('/videos')}}/brand/all" >Toutes</option>
							@foreach ($brands as $brand)
								<option @if(isset($brandVideo) and $brand->id == $brandVideo->id) selected @endif value="{{url('/videos')}}/brand/{{$brand->id}}"  >{{$brand->name}}</option>
							@endforeach  
						</select>
					</div>
				</div>
			</div>

			<div class="row video-listings m-t-40">
				@foreach($videos as $video)
				<div class="col-md-4">
					<article class="video-box-item">
						<div class="Ads-video cover relative" style=" background-image: url('https://img.youtube.com/vi/{{$video->url}}/mqdefault.jpg'); ">
							<a href="{{url('/videos')}}/{{$video->id}}" class="Ads-video-action transform-both">
								<i class="zmdi zmdi-play-circle-outline"></i>
							</a>
						</div>
						<div class="color-theme text-uppercase">Video</div>
						<a href="{{url('/videos')}}/{{$video->id}}" >
							<h4 class="video-box-title">{{$video->title}}</h4>
						</a>
						<span class="block">{{date_format($video->created_at,'d M Y à H:i')}}</h4>
					</article>
				</div>
				@endforeach
			</div>
			<div class="col-md-12 m-t-25 m-b-25">
					{{ $videos->links() }}
			</div>
		</div>
	</div>
	
</section>
    
@endsection