@extends('layouts.app')
@section('style')
<style>
    td{
        max-width:350px
    }
</style>
@endsection
@section('content')
<div class="col-md-12 m-t-10 m-b-10">
    <!--
    <div class="col-md-3 m-b-10">
        <div class="form-group row m-b-10">
            <label for="type" class="col-md-6 col-form-label">Commentaires :</label>
            <div class="col-md-6">
                <select id="type" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" class="form-control">
                    <option value="{{url('/administration/comment')}}">En attente</option>
                    <option value="{{url('/administration/comment/cmntSupp')}}">Refusés</option>
                </select>
            </div>
        </div>
        
    </div>
    -->
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">Type</th>
            <th scope="col">Titre</th>
            <th scope="col">Commentaire</th>
            <th scope="col">Par</th>
            <th scope="col">Date de création</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody class="bg-white">
            @foreach($comments as $comment)
            <tr id="comnt{{$comment->id}}">
                @if($comment->article)
                    <td class="text-info font-weight-bold">Article</td>
                    <td>{{substr(strip_tags($comment->article->title),0,50) }}</td>
                @endif
                @if($comment->video)
                    <td class="text-danger font-weight-bold">Video</td>
                    <td>{{substr(strip_tags($comment->video->title),0,50) }}</td>
                @endif
                <td class="bodyCom">{{substr(strip_tags($comment->body),0,100) }} @if(strlen(strip_tags($comment->body))>100) ... <span class="text-primary" style="cursor:pointer;">Afficher plus </span>@endif</td>
                <td class="bodyCom" style="display: none;">{{strip_tags($comment->body) }}<span class="text-primary" style="cursor:pointer;"> Afficher moins </span></td>
                <th>{{$comment->user->username}}</th>
                <td>{{$comment->created_at}}</td>
                <td>
                    <button class="btn btn-success acc" onClick="accepter({{$comment->id}})" style="padding: 2px 10px"><i class="zmdi zmdi-check"></i></button> 
                    <button class="btn btn-danger ref" onClick="supprimer({{$comment->id}})" style="padding: 2px 10px"><i class="zmdi zmdi-close"></i></button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-md-12 m-t-25 m-b-25">
            {{ $comments->links() }}
    </div>
</div>
@endsection
@section('script')
<script>
var acctionAja = null
$('.bodyCom span').click(function(){
    pp = $('.bodyCom ',$(this).parents('tr'))  
    pp.show();     
    $(this).parent().hide();
})
var supprimer = function (id){
    if (acctionAja != null)acctionAja.abort()
    acctionAja = $.ajax({
        type: "GET",
        url: '/administration/comment/supprimer/'+id,
        success: function (result) {            
            $('#comnt'+id).remove();     
            $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Le commentaire a été supprimé.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'danger'
                    }
                );
        }
    });
}
var accepter = function (id){
    if (acctionAja != null)acctionAja.abort()
    acctionAja = $.ajax({
        type: "GET",
        url: '/administration/comment/accepter/'+id,
        success: function (result) {            
            $('#comnt'+id).remove();     
            $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Le commentaire a été confirmé.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'success'
                    }
                );
        }
    });
}

</script>
@endsection