@extends('home')

@section('style')
<style>
    th, td
    {
        text-align: center;
    }
</style>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <br />
        
        
        <div class="InscriptionHeader" align="right">
            <a href="{{route('article.create')}}" class="btn btn-primary">Ajouter un article</a>
            <a href="{{route('brand.create')}}" class="btn btn-primary">Ajouter une marque</a>
            <a href="{{route('model.create')}}" class="btn btn-primary">Ajouter un modèle</a>
            <br />
        </div>
        <div class="InscriptionHeader" align="center">
        <h2 class="text-uppercase m-b-10">Users</h2>
        </div>
        <table class="table">
                <tr class="thead-dark">
                    <th>E-mail</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Numéro de téléphone</th>
                    <th>Type</th>
                    <th>Statut</th>
                    <th>Note</th>
                    <th>Dernière connexion</th>
                    <th>Date de création</th>
                    <th>Actions</th>
                </tr>
                @foreach($users as $row)
                <tr style="background-color:#FFFF;">
                    <td>{{ $row['email']}}</td> 
                    <td>{{ $row['first_name']}}</td> 
                    <td>{{ $row['last_name']}}</td> 
                    <td>{{ $row['phone_number']}}</td> 
                    <td>
                        @if($row->type==1)
                        <strong>Admin</strong>
                        @else
                        User
                        @endif
                    </td> 
                    <td>
                        @if($row->is_active==1)
                        <b style="color:green;">Active</b>
                        @else
                        <b style="color:red;">Inactive</b>
                        @endif
                    </td> 
                    <td>{{ $row['rating']}}</td> 
                    <td>{{ $row['last_login']}}</td>
                    <td>{{ $row['created_at']}}</td>
                    <td width="25%">
                            <a style="display:inline;" href="{{action('UserController@edit', $row['id'])}}"><span class="btn btn-outline-success ">&Eacute;diter</span></a>
                            <form style="display:inline;" method="post" class="delete_form" action="{{action('UserController@destroy', $row['id'])}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />
                                    <button type="submit" class="btn btn-outline-danger">Supprimer</button>
                            </form>
                            <a href="/administration/active/{{$row->id}}/finished" class="btn btn-outline-info">Changer le statut</a>
                        </span>
                    </td>
                </tr>
                @endforeach
        </table>
        {{ $users->links() }}
    </div>
</div>
@endsection

@section('script')
<script>
        $(document).ready(function(){
         $('.delete_form').on('submit', function(){
          if(confirm("Are you sure you want to delete it?"))
          {
           return true;
          }
          else
          {
           return false;
          }
         });
        });
</script>
@endsection