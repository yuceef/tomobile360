@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">
	<div class="container">
        <div class="section-block section-block-white HasPadding">
            
            <div class="row">

                <div class="Pagesheader color-theme col-md-12">
                    <h2 class="text-uppercase">Article</h2>
                </div>

            </div>
            
            <div class="row">

                <div class="col-md-9">
                    <div class="Pagesheader m-t-10">
                        <h2 class="text-transform">{{$article->title}}</h2>
                        <h6 class="text-muted m-b-10 font-weight-light" >
                            <i class="zmdi zmdi-edit"></i> 
                            Publié le {{date_format($article->created_at,'d M Y à H:i')}} 
                        </h6>
                        <img src="{{$article->cover }}" width="100%" alt="">
                    </div>
                    <div class="contentarticle col-md-12 m-t-10 m-b-10">
                        {!! $article->body !!}
                    </div>
                    <div align="center" class="col mb-3">
						<a style="display:inline;" href="{{action('ArticleController@index')}}"><span class="btn btn-outline-primary ">Voir tous les articles</span></a>
						<form style="display:inline;" method="post" class="delete_form" action="{{action('ArticleController@destroy', $article->id)}}">
								{{csrf_field()}}
								<input type="hidden" name="_method" value="DELETE" />
								<button type="submit" class="btn btn-outline-danger">Supprimer</button>
						</form>
						<a href="{{action('ArticleController@edit',$article->id)}}" class="btn btn-outline-success">Editer</a>
                    </div>
                </div>


            </div>
@endsection

@section('script')
<script>
		$(document).ready(function(){
		 $('.delete_form').on('submit', function(){
		  if(confirm("Are you sure you want to delete it?"))
		  {
		   return true;
		  }
		  else
		  {
		   return false;
		  }
		 });
		});
	</script>
@endsection