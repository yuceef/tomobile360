@extends('layouts.app')

@section('style')
<style>
    .glyphicon.glyphicon-plus{
        padding-top:35px;
    }
    .form-group{
        margin-top: 10px;
    }  
    .form-control{
        margin-top: 5px;
    } 
    label{
        margin-bottom: 5px;
    }
</style>
@endsection

@section('content')
<div class="container mt-4 mb-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">Ajouter un article</div>

                <div class="card-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div>
                        @endif
                    <input type="hidden" id="create_dataModels" data-models="{{ $models }}" >  
                    <form method="post" action="{{action('ArticleController@store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                         <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="">Marques</label>
                                <select name="brand_id" class="form-control result" id="create_brand">
                                    <option value="" selected>Choisir une marque</option>
                                        @foreach ($brands as $brand)
                                            <option @if($brand->id == old('brand_id')) selected @endif value="{{$brand->id}}" >{{$brand->name}}</option>
                                        @endforeach                                            
                                </select>
                            </div>
                            <div class="form-group col-md-1">
                                <a style="color:#28a745;" href="{{route('brand.create')}}" class="glyphicon glyphicon-plus" title="Ajouter une marque"></a>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="">Modèles</label>
                                <select name="model_id" disabled="" class="form-control resultNeuf" id="create_model">
                                    <option value="" selected>Vous devez choisir une marque</option>  
                                </select>
                            </div>
                            <div class="form-group col-md-1" >
                                <a style="color:#28a745;" href="{{route('model.create')}}" class="glyphicon glyphicon-plus" title="Ajouter un modèle"></a>
                            </div>
                        </div>
                        <div class="form-group col-md-15">
                            <label for="">Catégories</label>
                            <select name="category_id" class="form-control result" >
                                <option value="" selected>Choisir une catégorie</option>
                                    @foreach ($categories as $categorie)
                                        <option @if($categorie->id == old('category_id')) selected @endif value="{{$categorie->id}}" >{{$categorie->name}}</option>
                                    @endforeach                                            
                            </select>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="">Titre</label>
                                <input type="text" name="title" placeholder="Entrer le titre de l'article" class="form-control" id="title_slug" value="{{ old('title')}}">
                                    @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                    @endif
                            </div>
                                <div class="form-group col-md-6">
                                    <label for="">Image de couverture</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                        </div>
                        <div class="form-group">
                            <label for="">Date de création</label>
                            <input type="string" name="creation_date" placeholder="Jour-Mois-Année" class="form-control" value="{{ old('creation_date')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Article</label>
                            <textarea name="body" class="form-control" id="editor" >{{ old('body')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" name="slug" placeholder="title-article" class="form-control" id="slug_article" value="{{ old('slug')}}">
                        </div>
                        <div class="form-group center">
                            <input type="submit" class="btn btn-info pull-right" value="Submit" />
                        </div>
                    </form>
                    <a href="javascript:history.back()" class="btn btn-primary">
                        <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
    <!-- start filter models-brands -->
    <script src="/js/tomobile_home.js"></script>
    <script>
            function getBrands(neuf){
                var model = $("#create_model");
                var idName = (neuf == 1) ? 'nf' : 'occ'
                type = $('#typeSearch-' + idName).val()
                $.ajax({
                    url: "/api/brand/byType/" + type, success: function (result) {
                        var brandc = $("#create_brand");
                        brandc.empty();
                        brandc.append('<option style="color:black" value="0" selected>Toutes</option>');
                        for (var i = 0; i < result.brands.length; i++) {
                            brandc.append('<option value="' + result.brands[i]['id'] + '">' + result.brands[i]['name'] + '</option>');
                        }
                        model.empty();
                        model.attr('disabled', 'disabled');
                        model.append('<option value="0">Vous devez choisir une marque</option>');
                        $("#create_dataModels").data("models", result.models)
                        getResultCount(neuf)
                    }
                }); 
            }
            $('#create_brand').change(function () {
                var model = $("#create_model");
                var models = $("#create_dataModels").data("models");    
                var filtred = Array();
                for (var i = 0; i < models.length; i++) {
                    if (models[i].brand_id == $(this).val() ) {
                        filtred.push(models[i]);
                    }
                }
                if (filtred.length > 0) {
                    model.empty();
                    model.removeAttr('disabled');
                    model.append('<option value="0">Choisir un modèle</option>');
                    for (var i = 0; i < filtred.length; i++) {
                        model.append('<option value="' + filtred[i]['id'] + '">' + filtred[i]['name'] + '</option>');
                    }
                } else {
                    model.empty();
                    model.attr('disabled', 'disabled');
                    model.append('<option value="0">Vous devez choisir une marque</option>');
                }
                getResultCount(1)
            })
                    
    </script>
                                    <!-- end filter models-brands -->
    <script>
            $("#title_slug").change(function(){
                var Text = $("#title_slug").val();

                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i=0, l=from.length ; i<l ; i++) {
                    Text = Text.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                Text = Text.split(' ').join('-');
                Text = Text.split("'").join('-');
                Text = Text.substr(0, 29);
                Text = Text.replace(/[^\w\-]+/g, '');
                Text = Text.replace(/\-\-+/g, '-');
                Text = Text.replace(/^-+/, '');
                Text = Text.replace(/-+$/, '');
                Text = Text.replace(";", '-');
                $("#slug_article").val(Text.toLowerCase());        
            });
            $(document).ready(function() {
                $('#create_brand').select2({
                });
            });
            $(document).ready(function() {
                $('#create_model').select2({
                });
            });
    </script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
        tinymce.init({
        selector: 'textarea',
        height: 500,
        setup: function (editor) {
            editor.on('init change', function () {
                editor.save();
            });
        },
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools"
        ],
        toolbar: "fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        fontsize_formats: "2pt 4pt 6pt 8pt 10pt 12pt 14pt 18pt 20pt 24pt 26pt 28pt 30pt 32pt 34pt 36pt",
        branding: false,
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // add custom filepicker only to Image dialog
        file_picker_types: 'image',
        file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        input.onchange = function() {
        var file = this.files[0];
        var reader = new FileReader();
      
        reader.onload = function () {
        var id = 'blobid' + (new Date()).getTime();
        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        // call the callback and populate the Title field with the file name
        cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
        };
    
        input.click();
        },
    });
    </script>
        
@endsection
