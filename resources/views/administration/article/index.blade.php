@extends('home')

@section('content')

	<div class="row">
		<div class="col-md-10" align="center">
			<h1>All articles</h1>
		</div>

		<div class="col-md-2">
			<a href="{{ route('article.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create new article</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>image</th>
					<th>Title</th>
					<th>Category</th>
					<th>Created at</th>
					<th>updated at</th>
				</thead>

				<tbody>
					
					@foreach ($articles as $article)
						
						<tr>
							<td><img src="{{ asset(''.$article->cover)}}" alt="{{ $article->title }}" height="200" width="200"></td>
							<td>{{ $article->title }}</td>
							<td>{{ $article->category->name }}</td>
							<td>{{ date('j M Y  H:i:s', strtotime($article->created_at)) }}</td>
							<td>{{ date('j M Y  H:i:s', strtotime($article->updated_at)) }}</td>
							<td><a href="{{ route('article.show', $article->id) }}" class="btn btn-default btn-sm">View</a></td>
						</tr>
							
					@endforeach

				</tbody>
			</table>
			<div class="text-center">
					{!! $articles->links(); !!}
			</div>
		</div>
	</div>

@endsection