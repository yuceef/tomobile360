@extends('home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">Edit</div>
                    <div class="card-body">

                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                                @endif
    <input type="hidden" id="edit_dataModels" data-models="{{ $models }}" >  
    <form method="post" action="{{action('ArticleController@update', $article->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH" />
                <div class="form-group">
                    <label for="">Brands</label>
                        <select name="brand_id" class="form-control result" id="edit_brand">
                                @foreach($brands as $brand)
                                    @if(old('brand_id', $article->brand_id) == $brand->id )
                                        <option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
                                    @else
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endif
                                @endforeach                                           
                        </select>
                </div> 
                <div class="form-group">
                    <label for="">Models</label>
                        <select name="model_id" class="form-control resultNeuf" id="edit_model">
                            <option value="0" selected></option>  
                            @foreach($models as $model)
                                    @if(old('model_id', $article->model_id) == $model->id )
                                        <option value="{{ $model->id }}" selected>{{ $model->name }}</option>
                                    @else
                                        <option value="{{ $model->id }}">{{ $model->name }}</option>
                                    @endif
                            @endforeach 
                        </select>
                </div> 
                <div class="form-group">
                    <label for="">Categories</label>
                        <select name="category_id" class="form-control result" >
                            @foreach($categories as $categorie)
                                    @if(old('category_id', $article->category_id) == $categorie->id )
                                        <option value="{{ $categorie->id }}" selected>{{ $categorie->name }}</option>
                                    @else
                                        <option value="{{ $categorie->id }}">{{ $categorie->name }}</option>
                                    @endif
                            @endforeach                                              
                        </select>
                </div>
                <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" name="title" placeholder="Enter the title" class="form-control" id="title_slug" value="{{ old('title', $article->title)}}">
                </div>
                <div class="form-group">
                        <label for="">Creation date</label>
                        <input type="string" name="creation_date" placeholder="Jour-Mois-Année" class="form-control" value="27-07-2018" value="{{ old('creation_date', $article->creation_date)}}">
                </div>
                <div class="form-group">
                        <label for="">Image</label>
                        <input type="file" name="image" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="">Article</label>
                    <textarea name="body" class="form-control">{{ old('body', $article->body) }}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Slug</label>
                    <input type="text" name="slug" placeholder="title-article" class="form-control" id="slug_article" value="{{ old('body', $article->slug)}}">
                </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Edit" />
        </div>
        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
                                    <!-- start filter models-brands -->
                                    <script src="/js/tomobile_home.js"></script>
    <script>
            function getBrands(neuf){
                var model = $("#edit_model");
                var idName = (neuf == 1) ? 'nf' : 'occ'
                type = $('#typeSearch-' + idName).val()
                $.ajax({
                    url: "/api/brand/byType/" + type, success: function (result) {
                        var brandc = $("#edit_brand");
                        brandc.empty();
                        brandc.append('<option style="color:black" value="0" selected>Toutes</option>');
                        for (var i = 0; i < result.brands.length; i++) {
                            brandc.append('<option value="' + result.brands[i]['id'] + '">' + result.brands[i]['name'] + '</option>');
                        }
                        model.empty();
                        model.attr('disabled', 'disabled');
                        model.append('<option value="0">You should choose a brand</option>');
                        $("#edit_dataModels").data("models", result.models)
                        getResultCount(neuf)
                    }
                }); 
            }
            $('#edit_brand').change(function () {
                var model = $("#edit_model");
                var models = $("#edit_dataModels").data("models");    
                var filtred = Array();
                for (var i = 0; i < models.length; i++) {
                    if (models[i].brand_id == $(this).val() ) {
                        filtred.push(models[i]);
                    }
                }
                if (filtred.length > 0) {
                    model.empty();
                    model.removeAttr('disabled');
                    model.append('<option value="0">choose a model</option>');
                    for (var i = 0; i < filtred.length; i++) {
                        model.append('<option value="' + filtred[i]['id'] + '">' + filtred[i]['name'] + '</option>');
                    }
                } else {
                    model.empty();
                    model.attr('disabled', 'disabled');
                    model.append('<option value="0">You should choose a brand</option>');
                }
                getResultCount(1)
            });
                                       
    </script>
                                    <!-- end filter models-brands -->
    <script>
            $("#title_slug").change(function(){
                var Text = $("#title_slug").val();

                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i=0, l=from.length ; i<l ; i++) {
                    Text = Text.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                Text = Text.split(' ').join('-');
                Text = Text.split("'").join('-');
                Text = Text.substr(0, 29);
                Text = Text.replace(/[^\w\-]+/g, '');
                Text = Text.replace(/\-\-+/g, '-');
                Text = Text.replace(/^-+/, '');
                Text = Text.replace(/-+$/, '');
                Text = Text.replace(";", '-');
                $("#slug_article").val(Text.toLowerCase());        
            });
            $(document).ready(function() {
                $('#create_brand').select2({
                });
            });
            $(document).ready(function() {
                $('#create_model').select2({
                });
            });
    </script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ypkb4walqyu214gr36hckm2a7eys1x3047ep0frg6y0txg47"></script>
    <script>
            tinymce.init({
            selector: 'textarea',
            height: 500,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            fontsize_formats: "2pt 4pt 6pt 8pt 10pt 12pt 14pt 18pt 20pt 24pt 26pt 28pt 30pt 32pt 34pt 36pt",
            branding: false,
            image_title: true, 
            // enable automatic uploads of images represented by blob or data URIs
            automatic_uploads: true,
            // add custom filepicker only to Image dialog
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
    
            input.onchange = function() {
            var file = this.files[0];
            var reader = new FileReader();
          
            reader.onload = function () {
            var id = 'blobid' + (new Date()).getTime();
            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);
    
            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
            };
        
            input.click();
            },
        });
        </script>

@endsection