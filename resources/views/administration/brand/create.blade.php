@extends('layouts.app')

@section('style')
<style>
    .form-group{
        margin-top: 10px;
    }  
    .form-control{
        margin-top: 5px;
    } 
</style>
@endsection

@section('content')
<div class="container mt-4 mb-4" >
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">
                <div class="card-header" align="center">Ajouter une marque</div>

                <div class="card-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div>
                        @endif
                    <form method="post" action="{{action('BrandController@store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                         <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="">Nom de la marque</label>
                                <input type="text" name="name" placeholder="Entrer le nom de la marque" class="form-control" id="name_slug" value="{{ old('name')}}">
                                    @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                            </div>
                            <div class="form-group col-md-6">
                                    <label for="">Logo</label>
                                    <input type="file" name="logo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Site officiel</label>
                            <input type="string" name="url" placeholder="Entrer le site officiel de la marque" class="form-control" value="{{ old('url')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea name="description" class="form-control">{{ old('description')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" name="slug" placeholder="slug" class="form-control" id="slug_brand" value="{{ old('slug')}}">
                        </div>
                        <div class="form-group center">
                            <input type="submit" class="btn btn-info pull-right" value="Submit" />
                        </div>
                    </form>
                    <a href="javascript:history.back()" class="btn btn-primary">
                        <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
    <script>
            $("#name_slug").change(function(){
                var Text = $("#name_slug").val();

                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i=0, l=from.length ; i<l ; i++) {
                    Text = Text.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                Text = Text.split(' ').join('-');
                Text = Text.split("'").join('-');
                Text = Text.substr(0, 29);
                Text = Text.replace(/[^\w\-]+/g, '');
                Text = Text.replace(/\-\-+/g, '-');
                Text = Text.replace(/^-+/, '');
                Text = Text.replace(/-+$/, '');
                Text = Text.replace(";", '-');
                $("#slug_brand").val(Text.toLowerCase());        
            });
            $(document).ready(function() {
                $('#create_brand').select2({
                });
            });
            $(document).ready(function() {
                $('#create_model').select2({
                });
            });
    </script>
@endsection