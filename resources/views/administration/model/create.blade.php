@extends('layouts.app')

@section('style')
<style>
    .glyphicon.glyphicon-plus{
        padding-top:35px;
    }
    .form-group{
        margin-top: 10px;
    }  
    .form-control{
        margin-top: 5px;
    } 
    .lab{
        margin-bottom: 5px;
    }
</style>
@endsection

@section('content')
<div class="container mt-4 mb-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">Ajouter un modèle</div>

                <div class="card-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div>
                        @endif
                    <form method="post" action="{{action('ModeleController@store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                         <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="" class="lab">Type</label>
                                    <select name="type" class="form-control result">
                                        <option value="voiture">Voiture</option>
                                        <option value="moto">Moto</option>
                                        <option value="camion">Camion</option>  
                                        <option value="equipement">Equipement</option>  
                                    </select>
                            </div>
                            <div class="form-group col-md-5 ml-5">
                                <label for="" class="lab">Marque</label>
                                <select name="brand_id" class="form-control result" id="create_brand">
                                    <option value="" selected>Choisir une marque</option>
                                        @foreach ($brands as $brand)
                                            <option @if($brand->id == old('brand_id')) selected @endif value="{{$brand->id}}" >{{$brand->name}}</option>
                                        @endforeach                                            
                                </select>
                            </div>
                            <div class="form-group col-md-1">
                                <a style="color:#28a745;" href="{{route('brand.create')}}" class="glyphicon glyphicon-plus" title="Ajouter une marque"></a>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="">Nom</label>
                                    <input type="text" name="name" placeholder="Entrer le nom du modèle" class="form-control" id="nam_slug" value="{{ old('name')}}">
                                        @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                        @endif
                            </div>
                            <div class="form-group col-md-6">
                                    <label for="">Image du modèle</label>
                                    <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" class="form-control">{{ old('description')}}</textarea>
                            </div>
                        <div class="form-group">
                                <label for="">Slug</label>
                                <input type="text" name="slug" placeholder="slug" class="form-control" id="slug_model" value="{{ old('slug')}}">
                        </div>
                        <div class="form-group center">
                            <input type="submit" class="btn btn-info pull-right" value="Submit" />
                        </div>
                    </form>
                    <a href="javascript:history.back()" class="btn btn-primary">
                        <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
    <script>
            $("#nam_slug").change(function(){
                var Text = $("#nam_slug").val();

                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i=0, l=from.length ; i<l ; i++) {
                    Text = Text.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                Text = Text.split(' ').join('-');
                Text = Text.split("'").join('-');
                Text = Text.substr(0, 29);
                Text = Text.replace(/[^\w\-]+/g, '');
                Text = Text.replace(/\-\-+/g, '-');
                Text = Text.replace(/^-+/, '');
                Text = Text.replace(/-+$/, '');
                Text = Text.replace(";", '-');
                $("#slug_model").val(Text.toLowerCase());        
            });
            $(document).ready(function() {
                $('#create_brand').select2({
                });
            });
            $(document).ready(function() {
                $('#create_model').select2({
                });
            });
    </script>
@endsection