@extends('layouts.app')
@section('style')
<style>
    .form-group{
        margin-top: 10px;
    }  
    .form-control{
        margin-top: 5px;
    } 
    label{
        margin-bottom: 5px;
    }
</style>
@endsection
@section('content')
<div class="container mt-4 mb-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" align="center">Modifier les informations de l'utilisateur</div>
    
                    <div class="card-body">
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(\Session::has('success'))
                                <div class="alert alert-success">
                                    <p>{{ \Session::get('success') }}</p>
                                </div>
                            @endif
    <form method="post" action="{{action('UserController@update', $id)}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH" />
        <div class="form-group">
            <div><label for="">Type de l'utilisateur</label></div>
                <select name="user_type" class="form-control result">
                    <option value="particulier">Particulier</option>    
                    <option value="professionnel">Professionnel</option>                                        
                </select>
        </div>
        <div class="form-group">
            <label>Username</label><input type="text" name="username" class="form-control" value="{{$user->username}}" />
        </div>
        <div class="form-group">
            <label>Garage</label><input type="text" name="garage" class="form-control" value="{{$user->garage}}" />
        </div>
        <div class="form-group">
            <label>E-mail</label><input type="text" name="email" class="form-control" value="{{$user->email}}" />
        </div>
        <div class="form-group">
            <label>Prénom</label><input type="text" name="first_name" class="form-control" value="{{$user->first_name}}" />
        </div>
        <div class="form-group">
            <label>Nom</label><input type="text" name="last_name" class="form-control" value="{{$user->last_name}}" />
        </div>
        <div class="form-group">
            <label>Adresse</label><input type="text" name="adresse" class="form-control" value="{{$user->adresse}}" />
        </div>
        <div class="form-group">
            <label>Date de naissance</label><input type="text" name="birthdate" class="form-control" value="{{$user->birthdate}}" />
        </div>
        <div class="form-group">
            <label>Numéro de téléphone</label><input type="text" name="phone_number" class="form-control" value="{{$user->phone_number}}" />
        </div>
        <div class="form-group">
            <label>Fixe</label><input type="text" name="fixe" class="form-control" value="{{$user->fixe}}"/>
        </div>
        <div class="form-group">
            <label>Ville</label><input type="text" name="city" class="form-control" value="{{$user->city}}"/>
        </div>
        <div class="form-group">
            <label>Pays</label><input type="text" name="country" class="form-control" value="{{$user->country}}" />
        </div>
        <div class="form-group">
            <label>Code postal</label><input type="text" name="postal" class="form-control" value="{{$user->postal}}"/>
        </div>
        <div class="form-group">
            <label>Rating</label><input type="text" name="rating" class="form-control" value="{{$user->rating}}" />
        </div>
        <div class="form-group">
            <label>Type</label><input type="text" name="type" class="form-control" value="{{$user->type}}" />
        </div>
        <div class="form-group center">
            <input type="submit" class="btn btn-info pull-right" value="Submit" />
        </div>
        </form>
        <a href="javascript:history.back()" class="btn btn-primary">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
            </a>
        </div>
    </div>
</div>
</div>
</div>

@endsection
