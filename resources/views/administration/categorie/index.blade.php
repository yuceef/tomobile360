@extends('layouts.app')
@section('style')
<style>
    td{
        max-width:350px
    }
</style>
@endsection
@section('content')
<div class="col-md-12 m-t-10 m-b-10">
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">Affichage</th>
            <th scope="col">Nom</th>
            <th scope="col">Description</th>
            <th scope="col">Coleur</th>
            <th scope="col">Date de création</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody class="bg-white">
            @foreach($cats as $cat)
            <tr id="cat{{$cat->id}}">
                <th><span class="PostCat cat{{$cat->name}}" style="background:{{$cat->color}};color:white;padding:5px;">{{$cat->name}}</span></th>
                <td>{{$cat->name}}</td>
                <td>{{$cat->description}}</td>
                <td><span style="background:{{$cat->color}};color:white;padding:5px;">{{$cat->color}}</span></td>
                <td>{{$cat->created_at}}</td>
                <td>
                    <a href="/administration/categorie/{{$cat->id}}/edit"><button class="btn btn-success acc" style="padding: 2px 10px"><i class="zmdi zmdi-edit"></i></button> </a>
                    <form class="d-inline" method="post" id="delete_form">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE" />
                        <button type="button" class="btn btn-danger ref" onClick="supprimer({{$cat->id}})" style="padding: 2px 10px"><i class="zmdi zmdi-close"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
var acctionAja = null
$('.bodyCom span').click(function(){
    pp = $('.bodyCom ',$(this).parents('tr'))  
    pp.show();     
    $(this).parent().hide();
})
var supprimer = function (id){
    console.log(2017);
    form = $('#delete_form')
    if (acctionAja != null)acctionAja.abort()
    acctionAja = $.ajax({
        type: "DELETE",
        url: '/administration/categorie/'+id,
        data: form.serialize(),
        success: function (result) {            
            $('#cat'+id).remove();     
            $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'La catégorie a été supprimée.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'danger'
                    }
                );
        }
    });
}
var accepter = function (id){
    if (acctionAja != null)acctionAja.abort()
    acctionAja = $.ajax({
        type: "GET",
        url: '/administration/comment/accepter/'+id,
        success: function (result) {            
            $('#comnt'+id).remove();     
            $.notifyClose('all');
                $.notify({
                    icon: 'fa fa-paw',
                    message: 'Le commentaire a été confirmé.'
                }, {
                        // settings
                        placement: { align: 'center' },
                        type: 'success'
                    }
                );
        }
    });
}

</script>
@endsection