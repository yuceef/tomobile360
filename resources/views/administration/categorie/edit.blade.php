@extends('layouts.app')

@section('style')
<style>
    input[type=color].form-control {
        padding: 0px;
    }
    .PostCat {
        color: white;
        text-transform: uppercase;
        font-family: 'Montserrat', sans-serif;
        padding: 6px 9px;
        font-size: 13px;
        display: inline-block;
        margin-bottom: 10px;
    }
</style>
@endsection

@section('content')
<div class="container mt-4 mb-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">Ajouter une catégorie</div>
                <div class="card-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(Session::has('m'))
                            <div class="alert alert-success">
                                <p>{{ Session::pull('m') }}</p>
                            </div>
                        @endif
                        <form class="col-md-8 offset-md-2" method="post" action="/administration/categorie/{{$cat->id}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="PATCH" />
                            <div class="form-group row m-b-10">
                                <label for="Nom" class="col-md-3 col-form-label">Nom:</label>
                                <div class="col-md-9">
                                    <input id="Nom" type="text" class="form-control resul" name="name" value="{{$cat->name}}" required="">
                                </div>
                            </div>
                            <div class="form-group row m-b-10">
                                <label for="color" class="col-md-3 col-form-label">Coleur:</label>
                                <div class="col-md-9">
                                    <input name="color" class="form-control resul" type="color" value="{{$cat->color}}"  required=""/>
                                </div>
                            </div>
                            <div class="form-group row m-b-10">
                                <label for="description" class="col-md-3 col-form-label">Description:</label>
                                <div class="col-md-9">
                                <textarea name="description" class="form-control"  required="">{{$cat->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row m-b-10">
                                <label for="description" class="col-md-3 col-form-label">Affiche:</label>
                                <div class="col-md-9" id="affEx"></div>
                            </div>
                            <div class="form-group center">
                                <input type="submit" class="btn btn-blue pull-right" value="Submit" />
                            </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#affEx').append('<span class="PostCat catactu" style="background:'+$('input[type=color]').val()+'">'+$('input[type=text]').val()+'</span>')
        $('.resul').change(function () {
            $('#affEx').empty()
            $('#affEx').append('<span class="PostCat catactu" style="background:'+$('input[type=color]').val()+'">'+$('input[type=text]').val()+'</span>')
        })
    </script>      
@endsection
