@extends('home')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" align="center">Create new user</div>
                        <div class="card-body">
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(\Session::has('success'))
                                <div class="alert alert-success">
                                    <p>{{ \Session::get('success') }}</p>
                                </div>
                            @endif

    <form method="post" action="{{action('UserController@store')}}">
    {{ csrf_field() }}
        <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Enter username" />
        </div>
        <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Enter email" />
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Enter password" />
        </div>
        <div class="form-group">
            <input type="text" name="first_name" class="form-control" placeholder="Enter first name" />
        </div>
        <div class="form-group">
            <input type="text" name="last_name" class="form-control" placeholder="Enter last name" />
        </div>
        <div class="form-group">
            <input type="text" name="phone_number" class="form-control" placeholder="Enter phone number" />
        </div>
        <div class="form-group">
            <input type="text" name="type" class="form-control" placeholder="Type" />
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" />
        </div>
    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
