<!DOCTYPE html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tomobile 360') }} | @yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-glyphicons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-select.less')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/vertical.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/ion.rangeSlider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/tomobile-font.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/lightbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/sumoselect.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/tomobile.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/select2.css')}}"> 
    @yield('style')
    <link rel="icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

</head>
<body>
    <header class="header"  id="videoInfo">
        <div class="ContainerHeader Elemfixed">
            <div class="container">
            <div class="row">
                <div class="col-md-3 logo">
                    <a href="{{ url('/') }}" title="Tomobile 360"><img src="{{ asset('/img/brand.png')}}" alt="Tomobile 360"></a>
                </div>
                <div class="col-md-9">
                    <nav class="navbar SiteNav float-right">
                        <ul>
                            <li><a href="{{ url('/neuf') }}" >Neuf</a></li>
                            <li><a href="{{ url('/occasion') }}" >Occasion</a></li>
                            <li><a href="{{ url('/articles') }}" >News</a></li>
                            <li><a href="{{ url('/Services') }}" >Services</a></li>
                            <li><a href="{{ url('/partenaires') }}" >Partenaires</a></li>
                            @guest
                                <li><a href="{{ url('/register') }}" >S'inscrire</a></li>
                                <li><a href="{{ url('/login') }}" >Se connecter</a></li>
                            @else
                                <li class="dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->username }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        @if(Auth::user()->type == 1)
                                        <a class="dropdown-item" href="{{ url('/administration') }}">
                                            Administration
                                        </a>
                                        @endif
                                        <a class="dropdown-item" href="{{ url('/panneau') }}">
                                            Panneau
                                        </a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </nav>
                </div>
            </div>
            </div>
        </div>
    </header>

    <main>
        @yield('content')
    </main> 
    <footer class="footer">
        <div class="container">
            <div class="row">
                
                <div class="col">
                    <img src="/img/brand_white.png" class="m-b-20" alt="Tomobile 360"> 
                    <ul class="footer-nav">
                        <li><a href="">Qui sommes-nous?</a></li>
                        <li><a href="">Nous contacter</a></li>
                        <li><a href="">Aide / FAQ</a></li>
                    </ul>
                </div>	
                
                <div class="col">
                    <ul class="footer-nav">
                        <li><a href="">Neuf</a></li>
                        <li><a href="">Occasion</a></li>
                        <li><a href="">Espace PRO</a></li>
                        <li><a href="">Services premiums</a></li>
                    </ul>
                </div>	
                
                <div class="col-3">
                    <ul class="footer-nav">
                        <li><a href="">Top des marques - neuf</a></li>
                        <li><a href="">Top des marques - Occasion</a></li>
                        <li><a href="">Partenaires</a></li>
                    </ul>
                    <div class="footer-social m-t-20">
                        <h4 class="footer-title">SUIVEZ-NOUS SUR</h4>
                        <ul class="social-footer">
                            <li><a href=""><img src="/img/facebook-icon.png"></a></li>
                            <li><a href=""><img src="/img/twitter-icon.png"></a></li>
                            <li><a href=""><img src="/img/instagram-icon.png"></a></li>
                            <li><a href=""><img src="/img/youtube-icon.png"></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col">
                    <ul class="footer-nav">
                        <li><a href="">Plan du site</a></li>
                        <li><a href="">Conditions générales</a></li>
                        <li><a href="">Mentions légales</a></li>
                    </ul>
                </div>	
                
                <div class="col text-center">
                    <div class="text-uppercase">TELECHARGEZ NOTRE APPLICATION MOBILE</div>
                    <img src="/img/mobile-app.png" class="m-t-15" alt="Tomobile 360 Application Mobile" style="max-width: 90px;">
                </div>	

            </div>
        </div>
    </footer>
    <div class="FxSocial">
        <ul>
            <li class="Fxitem facebook"><a href="facebook"><i class="zmdi zmdi-facebook"></i></a></li>
            <li class="Fxitem youtube"><a href="youtube"><i class="zmdi zmdi-youtube-play"></i></a></li>
            <li class="Fxitem twitter"><a href="twitter"><i class="zmdi zmdi-twitter"></i></a></li>
            <li class="Fxitem instagram"><a href="instagram"><i class="zmdi zmdi-instagram"></i></a></li>
        </ul>
    </div>
        <script src="{{ asset('/js/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.sumoselect.min.js')}}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('/js/demo.js')}}"></script> 
        <script src="{{ asset('/js/select2.js') }}"></script>
        <script src="{{ asset('/js/ion.rangeSlider.js')}}"></script>
        <script src="{{ asset('/js/jquery.multiselect.js')}}"></script>
        <script src="{{ asset('/js/jquery.session.js')}}"></script>
        <script src="{{ asset('/js/bootstrap-notify.js')}}"></script> 
        <script src="{{ asset('/js/lightbox.js')}}"></script> 
        <script src="{{ asset('/js/app.js')}}"></script> 
        <script src="{{ asset('/js/croppie.js')}}"></script> 
        <script src="{{ asset('/js/prism.js')}}"></script> 
        <script src="{{ asset('/js/tomobile.js')}}"></script>
        @yield('script')
</body>
</html>
