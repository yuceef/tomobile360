@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">
	
	<div class="container">
		<div class="section-block section-block-white HasPadding">
			
			<div class="row">

				<div class="Pagesheader color-theme col-md-12">
					<h2 class="text-uppercase">Article</h2>
				</div>

				<div class="Breadcrumps col-md-12">
					<ul>
						<li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/articles')}}">Articles</a></li>
                        @if(isset($categorie))
                             <li><a href="{{url('/articles')}}/cat/{{$categorie->id}}">{{$categorie->name}}</a></li>
                        @endif

					</ul>
				</div>

			</div>

			<div class="row justify-content-end col-12 ClearMarginRt ClearPaddingRt m-t-15">
				<div class="form-group col-md-4 row">
					<label for="catSelect" class="col-md-3 col-form-label">Catégorie:</label>
					<div class="col-md-9">
						<select id="catSelect" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" class="form-control ">
							<option value="{{url('/articles')}}">Toutes</option>
							@foreach($categories as $cat)
								<option @if(isset($categorie) and $cat->id == $categorie->id) selected @endif value="{{url('/articles')}}/cat/{{$cat->id}}">{{$cat->name}}</option>
							@endforeach  
						</select>
					</div>
				</div>
			</div>

			<div class="row video-listings m-t-40">
                @foreach($articles as $article)
                	<article class="PostBox col-md-4">
                        <div class="PostThumbnail">
                            <a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">
                                <div class="cover"
                                    style="background-image: url('{{$article->cover }}')"></div>
                            </a>
                            <div class="PostBody">
                                <a href="{{url('/articles/cat')}}/{{$article->category->id}}"><span class="PostCat cat{{$article->category->name}}" style="background:{{$article->category->color}}">{{$article->category->name}}</span></a>
                                <a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}"><h3 class="PostTitle">{{$article->title}}</h3></a>
                                <h6 class="text-muted m-b-10 font-weight-light" style="font-size: 13px;" >
									<i class="zmdi zmdi-edit"></i> 
									Le {{(new Date($article->created_at))->format('d F Y à H:i')}} 
									par {{$article->user->username}}
								</h6>
								<a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">
                                    <p class="PostExcept text-justify" id="art_{{$article->id}}">
                                        {!! substr(strip_tags($article->body),0,400) !!}
                                    </p>
                                </a>
                            </div>
                        </div>
                    </article>
                @endforeach
			</div>
			<div class="col-md-12 d-flex justify-content-center m-t-25 m-b-25">
					{{ $articles->links() }}
			</div>
		</div>
	</div>
	
</section>
    
@endsection