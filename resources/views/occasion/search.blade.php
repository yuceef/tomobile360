@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">
	<div class="container">
        <div class="section-block section-block-white HasPadding">
            <div class="row">
                <div class="Breadcrumps bdc-no-margin col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{ url('/') }}">Accueil</a></li>
                        <li>Résultat de la recherche</a>
                    </ul>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="PreBd m-t-30 m-b-30">
	<div class="container">	
		<div class="row">	
			<aside class="col-md-3">
			<form id="formOccasion" method="POST">
                <div class="section-block-white">
					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Ville</h4>
						<div class="Vo-W-Body">
							<select class="form-control js-example-basic-single result2" id="city" name="city">
								<option value="0" selected>Tous</option>
                                    @foreach ($cities as $city)
                                        <option value="{{$city->id}}" @if(session()->get('city') == $city->id) selected @endif >{{$city->name}}</option>
                                    @endforeach 
							</select>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Prix</h4>
						<div class="Vo-W-Body">
							<div class="rang1e">
								<input id="GminPrice" class="result" name="GminPrice" type="hidden" @if(session()->has('GminPrice')) value="{{session()->get('GminPrice')}}" @else value="0" @endif>
                                <input id="GmaxPrice" class="result" name="GmaxPrice" type="hidden" @if(session()->has('GmaxPrice')) value="{{session()->get('GmaxPrice')}}" @else value="9000000" @endif>
								<input type="text" class="result" id="price" value="" name="range" />
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Kilométrage</h4>
						<div class="Vo-W-Body">
							<div class="rang1e">
								<input id="minMileage" class="result" name="minMileage" type="hidden" @if(session()->has('minMileage')) value="{{session()->get('minMileage')}}" @else value="0" @endif>
                                <input id="maxMileage" class="result" name="maxMileage" type="hidden" @if(session()->has('maxMileage')) value="{{session()->get('maxMileage')}}" @else value="200000" @endif>
                                <input type="text"  id="mileage" value="" name="range" />
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Année</h4>
						<div class="Vo-W-Body">
							<div class="range">
								<select name="year" class="form-control result" id="year">
                                    <option value="0" selected>Toutes</option>
                                    @for($i=2019; $i >= 2000 ; $i--)
                                    <option value="{{$i}}" @if(session()->get('year') == $i) selected @endif >{{$i}}</option>
                                    @endfor
                                </select>
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Marques</h4>
						<div class="Vo-W-Body">
							<div id="brandList">
							@foreach($brands as $brand)
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui result2" type="checkbox" id="marque{{ $brand->id }}" @if(session()->get('brand') == $brand->id) checked @endif data-name="{{ $brand->name }}" name="brand[]">
									<label class="form-check-label" for="marque{{ $brand->id }}">{{ $brand->name }}</label>
								</div>
                            @endforeach
							<label style="display:none;" id="hideAllBrands"><i class="zmdi zmdi-minus-square"></i> Voir moins de Marques</label>
							</div>
							<div class="Vo-W-more">
								<label id="showAllBrands"><i class="zmdi zmdi-plus-square"></i> Voir plus de Marques</label>
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Modèles</h4>
						<div class="Vo-W-Body">
							<div class="ScrollBody">
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele1" value="modele1">
									<label class="form-check-label" for="modele1">308</label>
									<span class="Vo-W-Counter">23 342</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele2" value="modele2">
									<label class="form-check-label" for="modele2">208</label>
									<span class="Vo-W-Counter">11 342</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele3" value="modele3">
									<label class="form-check-label" for="modele3">2008</label>
									<span class="Vo-W-Counter">5000</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele4" value="modele4">
									<label class="form-check-label" for="modele4">Clio</label>
									<span class="Vo-W-Counter">2100</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele1" value="modele1">
									<label class="form-check-label" for="modele1">308</label>
									<span class="Vo-W-Counter">23 342</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele2" value="modele2">
									<label class="form-check-label" for="modele2">208</label>
									<span class="Vo-W-Counter">11 342</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele3" value="modele3">
									<label class="form-check-label" for="modele3">2008</label>
									<span class="Vo-W-Counter">5000</span>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui" type="checkbox" id="modele4" value="modele4">
									<label class="form-check-label" for="modele4">Clio</label>
									<span class="Vo-W-Counter">2100</span>
								</div>
							</div>
							<div class="Vo-W-more">
								<a href=""><i class="zmdi zmdi-plus-square"></i> Voir plus de modèles</a>
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Enérgie</h4>
						<div class="Vo-W-Body">
							@foreach ($energies as $energie)
								<div class="form-check form-check-inline">
									<input class="form-check-input checkbox-ui moteur_clear result" type="checkbox" name="energy[]" id="{{$energie->name}}" value="{{$energie->id}}" 
									@if( session()->has('energy') and ( $energie->id==session()->get('energy') or ( is_array(session()->get('energy')) and in_array($energie->id,session()->get('energy')) ) ) ) ) checked @endif>
									<label class="form-check-label" for="{{$energie->name}}">{{$energie->name}}</label>
								</div>
							@endforeach 
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Catéogie</h4>
						<div class="Vo-W-Body">
							@foreach($cats as $cat)
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="{{$cat->name}}" value="{{$cat->id}}"
								@if( session()->has('category') and ( $cat->id==session()->get('category') or ( is_array(session()->get('category')) and in_array($cat->id,session()->get('category')) ) ) ) ) checked @endif>
								<label class="form-check-label" for="{{$cat->name}}">{{$cat->name}}</label>
							</div>
							@endforeach
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Transmission</h4>
						<div class="Vo-W-Body">
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui result" type="checkbox" name="trans[]" id="automatique" value="automatique"
									@if( session()->has('trans') and ( session()->get('trans')=="automatique" or ( is_array(session()->get('trans')) and in_array("automatique",session()->get('trans')) ) ) ) ) checked @endif>
								<label class="form-check-label" for="automatique">Automatique</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input moteur_clear checkbox-ui result" type="checkbox" name="trans[]" id="manuelle" value="manuelle"
									@if( session()->has('trans') and ( session()->get('trans')=="manuelle" or ( is_array(session()->get('trans')) and in_array("manuelle",session()->get('trans')) ) ) ) ) checked @endif>
								<label class="form-check-label" for="manuelle">Manuelle</label>
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Nombre de proriétaires</h4>
						<div class="Vo-W-Body">
							@for($i=1; $i <= 5 ; $i++)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input etat_clear radiobox-ui result" type="radio" name="nbrprop" id="pro{{$i}}" value="{{$i}}" @if(session()->get('nbrprop')==$i) checked @endif>
                                    <label class="form-check-label" for="pro{{$i}}">{{$i}}</label>
                                </div>
                            @endfor
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Nombre de portes</h4>
						<div class="Vo-W-Body">
							<div class="form-check form-check-inline">
								<input class="form-check-input radiobox-ui" type="radio" id="nbporte1" value="nbporte1">
								<label class="form-check-label" for="nbporte1">1</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input radiobox-ui" type="radio" id="nbporte2" value="nbporte2">
								<label class="form-check-label" for="nbporte2">2</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input radiobox-ui" type="radio" id="nbporte3" value="nbporte3">
								<label class="form-check-label" for="nbporte3">3</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input radiobox-ui" type="radio" id="nbporte4" value="nbporte4">
								<label class="form-check-label" for="nbporte4">4</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input radiobox-ui" type="radio" id="nbporte5" value="nbporte5">
								<label class="form-check-label" for="nbporte5">5</label>
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Couleurs</h4>
						<div class="Vo-W-Body">
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color1" value="color1">
								<label class="form-check-label" for="color1">Rouge</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color2" value="color2">
								<label class="form-check-label" for="color2">Bleu</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color3" value="color3">
								<label class="form-check-label" for="color3">Vert</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color4" value="color4">
								<label class="form-check-label" for="color4">Jaune</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color5" value="color5">
								<label class="form-check-label" for="color5">Noir</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color6" value="color6">
								<label class="form-check-label" for="color6">Gris</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input checkbox-ui" type="checkbox" id="color7" value="color7">
								<label class="form-check-label" for="color7">Blanc</label>
								<span class="Vo-W-Counter">11 342</span>
							</div>
						</div>
					</div>

					<div class="Vo-Widget">
						<h4 class="Vo-W-title">Puissance fiscale</h4>
						<div class="Vo-W-Body">
							<div class="range">
								<input id="minPuissance" class="result" name="minPuissance" type="hidden" @if(session()->has('minPuissance')) value="{{session()->get('minPuissance')}}" @else value="0" @endif>
                                <input id="maxPuissance" class="result" name="maxPuissance" type="hidden" @if(session()->has('maxPuissance')) value="{{session()->get('maxPuissance')}}" @else value="20" @endif>
                                <input type="text"  id="rangeFiscal" value="" name="rangeFiscal" />
							</div>
						</div>
					</div>

				</div>
            </aside>
			<div class="col-md-9">
				<figure class="section-block HasPadding section-block-white">
					<div class="Sr-results-Vn">
						<h6 class="Title-Lis-bn text-uppercase" ><span id="countResult">{{sizeof($vehicles)}}</span> Résultats pour <span>Occasion</span></h6>
						<div class="Sr-banner-sp">
							<div class="row">
                                <div class="col-md-12" id="criteria">
                                    @if(Session::has('brand') and session()->get('brand') != 0)
                                    <div class="Sr-Banner-child">
                                        <span>{{ session()->get('brandName') }}</span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                    @endif
                                    @if(Session::has('model') and session()->get('model') != 0)
                                    <div class="Sr-Banner-child">
                                        <span>{{ session()->get('modelName') }}</span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                    @endif
                                    @if(Session::has('GmaxPrice')  and session()->get('GmaxPrice') != 9000000)
                                        <div class="Sr-Banner-child">
                                            <span>max: {{ session()->get('GmaxPrice') }} DH </span>
                                            <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>

                                    @endif
                                    @if(Session::has('GminPrice')  and session()->get('GminPrice') != 0)
                                        <div class="Sr-Banner-child">
                                            <span>min: {{ session()->get('GminPrice') }} DH </span>
                                            <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>
                                    @endif
                                    @if(Session::has('maxPuissance')  and session()->get('maxPuissance') != 20)
                                        <div class="Sr-Banner-child">
                                            <span>max: {{ session()->get('maxPuissance') }} CH </span>
                                            <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>
                                    @endif
                                    @if(Session::has('minPuissance')  and session()->get('minPuissance') != 0)
                                        <div class="Sr-Banner-child">
                                            <span>min: {{ session()->get('minPuissance') }} CH </span>
                                            <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>
                                    @endif
                                    @if(Session::has('category')  and session()->get('category') != 0)
                                        <div class="Sr-Banner-child">
                                            <span>{{ session()->get('categoryName') }}</span>
                                            <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>
                                    @endif
                                    @if(Session::has('energy') and session()->get('energy') != 0)
                                        @if(is_array(session()->get('energieName')))
                                            @foreach(session()->get('energieName') as $energie)
                                            <div class="Sr-Banner-child energie_jq">
                                                <span>{{ $energie }} </span>
                                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                            @endforeach
                                        @else
                                            <div class="Sr-Banner-child energie_jq">
                                                <span>{{ session()->get('energieName') }} </span>
                                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                        @endif
                                    @endif
                                    @if(Session::has('trans') and session()->get('trans') != 0)
                                        @if(is_array(session()->get('trans')))
                                            @foreach(session()->get('trans') as $trans)
                                            <div class="Sr-Banner-child trans_jq">
                                                <span>{{ $trans }} </span>
                                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                            @endforeach
                                        @else
                                            <div class="Sr-Banner-child trans_jq">
                                                <span>{{ session()->get('trans') }} </span>
                                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                        @endif
                                    @endif
                                    @if(Session::has('interiorDesign'))
                                        @foreach(session()->get('interiorDesign') as $interiorDesign)
                                            <div class="Sr-Banner-child">
                                                <span>Design intérieur: @if($interiorDesign != '0'){{ $interiorDesign }} @else Autres @endif</span>
                                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if(Session::has('interiorColor'))
                                        @foreach(session()->get('interiorColor') as $interiorColor)
                                            <div class="Sr-Banner-child">
                                                <span>Couleur intérieure: @if($interiorColor != '0'){{ $interiorColor }} @else Autres @endif</span>
                                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if(Session::has('exteriorColor'))
                                        @foreach(session()->get('exteriorColor') as $exteriorColor)
                                            
                                                <div class="Sr-Banner-child">
                                                    <span>Couleur extérieure: @if($exteriorColor != '0'){{ $exteriorColor }} @else Autres @endif</span>
                                                    <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                                </div>
                                            
                                        @endforeach
                                    @endif
                                    <a href="{{ url('/neuf')}}" class="Sr-clear" id="clearAll">(tout effacer)</a>

                                    <div class="col-md-12 float-right text-right">
                                        <a href="{{ url('/search/searchDetailedOccasion')}}" class="btn btn-blue float-right"><i class="zmdi zmdi-edit"></i> Modifier ma recherche</a>
										@guest
											<a href="{{ url('/login') }}" class="btn btn-blue float-right"><i class="zmdi zmdi-save"></i> Sauvegarder ma recherche</a>
										@else
											<a href="#" class="btn btn-blue float-right" data-toggle="modal" data-target="#sauvegarde"><i class="zmdi zmdi-edit"></i> Sauvegarder ma recherche</a>
							   			@endguest
							        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>
                <figure class="m-t-30">
					<div class="row" id="vehicleList">
                    @foreach($vehicles as $vehicle)
						<article class="col-xs-12 col-sm-6 col-md-4">
							<div class="vo-CarBox">
								<div class="vo-CarThumb">
									<a href="{{ url('/') }}"><div class="cover" style="background-image: url('{{ $vehicle->image }}');"></div></a>
								</div>
								<div class="vo-CarPrice text-right">{{$vehicle->price}} DH</div>
								<div class="vo-CarBody">
									<ul>
										<li>
											<span>Année : {{$vehicle->year}}</span>
											<span>{{$vehicle->price}}</span>
										</li>
										<li>
											<span>Propriétaires préc. : {{ $vehicle->numberOfOwners}}</span>
											<span>{{$vehicle->mileage}} KM</span>
										</li>
										<li>
											<span>Boîte <span class="text-capitalize">{{ $vehicle->transmission }}</span></span>
											<span>{{ $vehicle->realPower }} CH</span>
										</li>
									</ul>
								</div>
							</div>
						</article>
					@endforeach
					</div>
					<div class="navigation m-t-30">
      					{{ $vehicles->links() }}
                    </div>
                    
				</figure>

            </div>
        </div>
	</div>
</section>
<div class="modal fade" id="sauvegarde" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sauvegarder ma recherche</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col">
              <input type="text" id="searchName" class="form-control" placeholder="Nom de la recherche">
              <input type="hidden" id="link" value="?type=0">
            </div>
          </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary sauvegarde">Sauvegarder ma recherche</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{asset('/js/detailed.js')}}"></script>
<script>
$(function () {
	$('#hideAllBrands').click();
	//$('#hideAllModels').click();
});
$('#showAllBrands').click(function(){
    $('#brandList').addClass('ScrollBody');
    $('#hideAllBrands').show();
    $('#showAllBrands').hide();
    $('#brandList div').each(function() {
        if($(this).index() > 4 ) $(this).show();
    });

});

$('#hideAllBrands').click(function(){
    $('#brandList').removeClass('ScrollBody');
    $('#showAllBrands').show();
    $('#hideAllBrands').hide();
    $('#brandList div').each(function() {
        if($(this).index() > 4 ) $(this).hide();
    });
});

$('#showAllModels').click(function(){
    $('#modelList').addClass('ScrollBody');
    $('#hideAllModels').show();
    $('#showAllModels').hide();
    $('#modelList div').each(function() {
        if($(this).index() > 4 ) $(this).show();
    });
});

$('#hideAllModels').click(function(){
    $('#modelList').addClass('ScrollBody');
    $('#showAllModels').show();
    $('#hideAllModels').hide();
    $('#modelList div').each(function() {
        if($(this).index() > 4 ) $(this).hide();
    });
});

</script>
@endsection