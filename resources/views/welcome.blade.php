@extends('layouts.app')

@section('title', 'Accueil')

@section('content')
<section class="Herocontent overflow">
    <div class="HeroForm relative">
        <div class="container">
            <div class="row">
                <div class="HeroFormInner ml-3">
                    <ul class="HeroForm-Header nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item Hf-H-Nf showNeuf active"><a class="active show" id="tab1" data-toggle="pill" href="#Hf-Tab-Nf" role="tab" aria-controls="tab1" aria-selected="true">Neuf</a></li>
                        <li class="nav-item Hf-H-Occ showOcc"><a class="" id="tab2" data-toggle="pill" href="#Hf-Tab-Occ" role="tab" aria-controls="tab2" aria-selected="false">Occasions</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <!-- debut Neuf -->
                        <figure id="Hf-Tab-Nf" role="tabpanel" aria-labelledby="tab1" class="Hf-Tab-Nf tab-pane active">
                            <nav class="FormSubHeader">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nvcf-tab" data-toggle="tab" href="#nav-nf-car" role="tab" aria-controls="nav-nf-car" aria-selected="true"><i class="tmb-16"></i></a>
                                    <a class="nav-item nav-link" id="nvcf-moto" data-toggle="tab" href="#nav-nf-moto" role="tab" aria-controls="nav-nf-moto" aria-selected="false"><i class="tmb-32"></i></a>
                                    <a class="nav-item nav-link" id="nvcf-bcar" data-toggle="tab" href="#nav-nf-bcar" role="tab" aria-controls="nav-nf-bcar" aria-selected="false"><i class="tmb-19"></i></a>
                                </div>
                            </nav>

                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active Hf-Child C1" id="nav-nf-car" role="tabpanel" aria-labelledby="occcf-tab">
                                    <input type="hidden" id="dataModels" data-models="{{ $models }}" >                                        
                                    <form action="/search/searchNeuf" method="post" id="formSearchnf">
                                        @csrf
                                        <input id="typeSearch-nf" type="hidden" value="voiture" name="type">
                                        <input type="hidden" value="1" name="isneuf">
                                        <div class="row">                            
                                            <div class="form-group col-sm-6">
                                                <label for="">Marque</label>
                                                <select name="brand" class="form-control brand2 brand resultNeuf" id="brandSearch-nf">
                                                    <option style="color:black" value="0" selected>Toutes</option>
                                                    @foreach ($brands as $brand)
                                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                                    @endforeach          
                                                </select>
                                                @foreach ($brands as $brand)
                                                    <input type="hidden" id="BrandId{{str_replace(' ','_',$brand->name)}}" value="{{$brand->logo}}" />
                                                @endforeach     
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Modèle</label>
                                                <select name="model" disabled="" class="form-control resultNeuf isSearch" id="modelSearch-nf">
                                                    <option value="0" selected>Tous</option>  
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Prix max.</label>
                                                <select name="GmaxPrice" class="form-control resultNeuf" id="prixSearch-nf">
                                                    <option value="9000000" selected="">Tous</option>
                                                    <option value="10000">10000 DH</option>
                                                    <option value="50000">50000 DH</option>
                                                    <option value="120000">120000 DH</option>
                                                    <option value="180000">180000 DH</option>
                                                    <option value="250000">250000 DH</option>
                                                    <option value="400000">400000 DH</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Carburant</label>
                                                <select name="energy" class="form-control resultNeuf" id="energieSearch-nf">
                                                    <option value="0" selected>Tous</option>
                                                    @foreach ($energies as $energie)
                                                        <option value="{{$energie->id}}" >{{$energie->name}}</option>
                                                    @endforeach  
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <a href="{{ url('/search/searchDetailed') }}" class="Pn-more TextBlack">Recherche détaillée <i class="zmdi zmdi-caret-right-circle"></i></a>
                                            </div>        
                                            <div class="form-group col-sm-6">
                                                <input type="submit" class="btn btn-primary btn-blue btn-block" value="0 résultats" name="afficher" id="afficher-nf">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </figure>
                        <!-- Fin Tab Neuf -->

                        <!-- Début Tab Occasion -->
                        <figure id="Hf-Tab-Occ" role="tabpanel" aria-labelledby="tab2" class="Hf-Tab-Occ tab-pane fade">
                            
                            <nav class="FormSubHeader">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="occcf-tab" data-toggle="tab" href="#nav-occ-car" role="tab" aria-controls="nav-occ-car" aria-selected="true"><i class="tmb-16"></i></a>
                                <a class="nav-item nav-link" id="occcf-moto" data-toggle="tab" href="#nav-occ-moto" role="tab" aria-controls="nav-occ-moto" aria-selected="false"><i class="tmb-32"></i></a>
                                <a class="nav-item nav-link" id="occcf-bcar" data-toggle="tab" href="#nav-occ-bcar" role="tab" aria-controls="nav-occ-bcar" aria-selected="false"><i class="tmb-19"></i></a>
                                <a class="nav-item nav-link" id="occcf-acc" data-toggle="tab" href="#nav-occ-acc" role="tab" aria-controls="nav-occ-acc" aria-selected="false"><i class="tmb-33"></i></a>
                            </div>
                            </nav>

                            <div class="tab-content" id="nav-tabContent-2">
                                <div class="tab-pane fade show active Hf-Child C1" id="nav-occ-car" role="tabpanel" aria-labelledby="nav-occ-car">
                                    <form action="/search/searchOccasion" id="formSearchocc" method="POST">
                                        @csrf
                                        <input id="typeSearch-occ" type="hidden" value="voiture" name="type">
                                        <input type="hidden" value="0" name="isneuf">
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="">Marque</label>
                                                <select name="brand" class="form-control brand2 brand resultOcc" id="brandSearch-occ">
                                                    <option style="color:black" value="0" selected>Toutes</option>
                                                    @foreach ($brands as $brand)
                                                        <option value="{{$brand->id}}" >{{$brand->name}}</option>
                                                    @endforeach          
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Modèle</label>
                                                <select name="model" disabled="" class="form-control resultOcc isSearch" id="modelSearch-occ">
                                                    <option value="0" selected>Tous</option>  
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Prix max.</label>
                                                <select name="GmaxPrice" class="form-control resultOcc" id="prixSearch-occ">
                                                    <option value="9000000" selected="">Tous</option>
                                                    <option value="10000">10000 DH</option>
                                                    <option value="50000">50000 DH</option>
                                                    <option value="120000">120000 DH</option>
                                                    <option value="180000">180000 DH</option>
                                                    <option value="250000">250000 DH</option>
                                                    <option value="400000">400000 DH</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Année</label>
                                                <select name="year" class="form-control resultOcc" id="yearSearch-occ">
                                                    <option value="0" selected>Toutes</option>
                                                    @for($i=2019; $i >= 2000 ; $i--)
                                                    <option value="{{$i}}" @if(session()->get('year') == $i) selected @endif >{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Carburant</label>
                                                <select name="energy" class="form-control resultOcc" id="energieSearch-occ">
                                                    <option value="0" selected>Tous</option>
                                                    @foreach ($energies as $energie)
                                                        <option value="{{$energie->id}}" >{{$energie->name}}</option>
                                                    @endforeach  
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="">Ville</label>
                                                <select class="form-control selectpicker resultOcc" data-live-search="true" id="citySearch-occ" name="city">
                                                    <option value="0" selected>Tous</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{$city->id}}" >{{$city->name}}</option>
                                                    @endforeach                                                      
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <a href="{{ url('/search/searchDetailedOccasion') }}" class="Pn-more TextBlack">Recherche détaillée <i class="zmdi zmdi-caret-right-circle"></i></a>
                                            </div>     
                                            <div class="form-group col-sm-6">
                                                <input type="submit" class="btn btn-primary btn-yellow btn-block" value="0 résultats" name="afficher" id="afficher-occ">
                                            </div>
                                        </div>
                                    </form>
		
                                </div>                       
                            </div>

                        </figure>
                        <!-- Fin Tab Occasion -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Heroslider owl-carousel owl-theme">
        <div class="HeroImg cover item" style="background-image: url('{{ asset('/img/hero_bg1.jpg')}}')"></div>
        <div class="HeroImg cover item" style="background-image: url('{{ asset('/img/hero_bg2.jpg')}}')"></div>
        <div class="HeroImg cover item" style="background-image: url('{{ asset('/img/hero_bg3.jpg')}}')"></div>
    </div>
</section>   

<section class="PreBd m-t-30">
    <div class="container">
        <div class="section-block">
            <div class="AtBrandContainer">
                <div class="row">
                    <div class="SectionTitle text-center col-md-12">
                        <h3 class="TheChild">Neuf</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="AutoBrand owl-carousel owl-theme col-md-10 offset-md-1 m-t-10 m-b-20">
                        @foreach ($brands as $brand)
                            <div class="AB-Logo item"><a href="{{ url('/neuf')}}/{{$brand->slug}}"><img src="{{$brand->logo}}" alt="{{$brand->name}}"> <span>{{$brand->name}}</span></a></div>
                        @endforeach  
                    </div>
                </div>
            </div>
            <div class="Banner1">
                <img src="{{ asset('/img/banner1.jpg')}}" alt="">
            </div>
            <div class="WidgetContainer">
                <div class="row">
                    <div class="col-md-7">
                        <aside class="Widget Wd-blue gnf">
                            <div class="WidgetTitle">
                                <h4>Le guide du neuf</h4>
                            </div>
                            <div class="WidgetBody">
                                <p style="font-size: 13px;line-height: 20px;">Vous réfléchissez à l’achat de votre
                                    prochaine voiture neuve ?<br>Tomobile360 vous aide à comparer l’ensemble des
                                    voitures neuves du marché&nbsp;: fiches techniques, photos et comparatifs.
                                </p>
                                <div class="Gf-Form m-t-10">
                                    <form action="{{ url('/search/guide_search') }}" method="post" id="formGuide">
                                        @csrf
                                            <input id="typeSearch-guide" type="hidden" value="voiture" name="type">
                                            <input type="hidden" value="1" name="isneuf">                                        
                                            <div class="row">
                                            <div class="form-group col-md-12">
                                                <label>Catégorie</label>
                                                <ul class="listing-car-Gf m-t-5">
                                                    @foreach($cats as $cat)
                                                    <li class="category" id="{{$cat->id}}"><i
                                                        class="{{$cat->slug_class}}"></i><span>{{$cat->name}}</span>
                                                    </li>
                                                    @endforeach
                                                    <input id="category2" class="result3" name="category" type="hidden" value="0">
                                                </ul>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label clss="d-block">Marque</label>
                                                <select name="brand" class="form-control brand4 br m-t-5 result3">
                                                    <option value="0" selected>Toutes</option>
                                                    @foreach ($brands as $brand)
                                                        <option value="{{$brand->id}}" >{{$brand->name}}</option>
                                                    @endforeach  
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 m-t-10">
                                                <label>&Eacute;nergie</label>
                                                <ul class="listing-eng-Gf m-t-5">
                                                    @foreach ($energies as $energie)
                                                        <li class="energy col-md-6" id="{{$energie->id}}" ><i class="{{$energie->slug_class}}"></i>{{$energie->name}}</option>
                                                    @endforeach  
                                                </ul>
                                                <input id="energy2" class="result3" name="energy" type="hidden" value="0">
                                            </div>
                                            <div class="form-group col-md-6 m-t-10">
                                                <label>Transmission</label>
                                                <ul class="listing-eng-Gf m-t-5">
                                                    <li class="trans col-md-6" id="automatique" style="min-height: 75px;padding-top: 16px; border-right: 1px solid #ced4da54;">
                                                        <i class="tmb-29"></i>
                                                        <div style="padding-top:5px;">Automatique</div>
                                                    </li>
                                                    <li class="trans col-md-6" id="manuelle" style="min-height: 75px;padding-top: 16px;">
                                                        <i class="tmb-30"></i>
                                                        <div style="padding-top:5px;">Manuelle</div>
                                                    </li>
                                                </ul>
                                                <input id="trans2" class="result3" name="trans" type="hidden" value="0">
                                            </div>
                                            <input id="GminPrice" name="GminPrice" class="result3" type="hidden" value="0">
                                            <input id="GmaxPrice" name="GmaxPrice" class="result3" type="hidden" value="9000000">
                                            <div class="form-group col-md-12 m-t-10 relative">
                                                <label style="margin-bottom:8px !important;">Prix</label>
                                                <input type="text" id="price" value="" name="price"/>
                                            </div>
                                            <div class="form-group col-sm-6 offset-sm-6 m-t-20 m-b-0">
                                                <input type="submit"
                                                    class="btn btn-primary btn-yellow btn-block submit-button afficher3"
                                                    value="Afficher" name="" id="afficherGuide">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-5">
                        @if(sizeof($promotions))
                        <aside class="Widget Wd-yellow promotionHome">
                            <div class="WidgetTitle"><h4>Voitures neuves en promotion</h4></div>
                            <div class="WidgetBody">
                                <div class="CarPromo owl-carousel owl-theme">
                                    @foreach ($promotions as $promotion)
                                        <div class="CP-Box text-center" @if(sizeof($promotions)<3) style="padding:15px 0" @endif>
                                            <a href="{{url('/neuf')}}/{{$promotion->modele->brand->slug}}/{{$promotion->modele->slug}}/{{$promotion->slug}}">
                                                <div class="CP-Box-Thumb">
                                                    <img src="{{$promotion->image}}" alt="{{$promotion->name}}">
                                                </div>
                                                <div class="CP-Box-Body">
                                                    <h5 class="CP-Box-Title">{{$promotion->name}}</h5>
                                                    <span class="CP-des Promodes">{{$promotion->promotionLast[0]->description}}</span>
                                                    @if($promotion->promotionLast[0]->percentage>0)
                                                        <span class="CD-Prox-span">Jusqu'à <strong>-{{$promotion->promotionLast[0]->percentage}}%</strong></span>
                                                    @else
                                                        <span class="CD-Prox-span" style="background-color: transparent; height:30px"></span>
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach  
                                </div>
                            </div>
                        </aside>
                        @endif
                        <aside class="Banner3 m-t-20">
                            <div class="WhiteSection">
                                <div class="BlockCr-Box">
                                    <h5 class="BlockCr-title">Comparateur auto</h5>
                                    <p class="BlockCr-title-small text-center">Choisissez un ou plusieurs véhicule(s) à comparer</p>
                                    <div class="BlockCr-Form m-t-10">
                                        <form action="/neuf/comparateur" method="post" id="formComparateur">
                                        @csrf
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <select name="brand1" class="form-control brand4 result-compa" id="brandComparateur1">
                                                        <option style="color:black" value="0" selected>Toutes</option>
                                                        @foreach ($brands as $brand)
                                                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                                                        @endforeach 
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <select name="brand2" class="form-control brand4 result-compa" id="brandComparateur2">
                                                        <option style="color:black" value="0" selected>Toutes</option>
                                                        @foreach ($brands as $brand)
                                                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                                                        @endforeach 
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <select class="form-control result-compa isSearch" name="model1" disabled="" id="modelComparateur1">
                                                        <option value="0">Modèle</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <select class="form-control result-compa isSearch" name="model2" disabled="" id="modelComparateur2">
                                                        <option value="0">Modèle</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <select name="version1" disabled="" class="form-control result-compa" id="versionComparateur1">
                                                        <option value="0" selected>Version</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <select name="version2" disabled="" class="form-control result-compa" id="versionComparateur2">
                                                        <option value="0" selected>Version</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 offset-md-3">
                                                    <input type="submit" value="Comparer" class="btn btn-blue btn-big btn-unrounded btn-block" disabled="" id="submitComparateur">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Ofse">
	
	<div class="container">
        <div class="section-block Ofse-yellow ">
            
            <div class="row">
                <div class="SectionTitle text-center col-md-12">
                    <h3 class="TheChild TextBlack">Offres & Services</h3>
                </div>
            </div>

            <div class="Of-ico-container">
                <div class="row">
                    <div class="Of-ico-items owl-carousel owl-theme col-md-10 offset-md-1">
                        
                        <div class="Of-Child item">
                            <h5>Crédit Auto</h5>
                            <div><i class="tmb-15"></i></div>
                            <span>Faites votre <br />simulation</span>
                        </div>

                        <div class="Of-Child item">
                            <h5>Assurance Auto</h5>
                            <div><i class="tmb-7"></i></div>
                            <span>Demandez un devis <br />meilleures formules</span>
                        </div>

                        <div class="Of-Child item">
                            <h5>Test-drive</h5>
                            <div><i class="tmb-14"></i></div>
                            <span>Réservez <br />le véhicule de votre choix</span>
                        </div>

                        <div class="Of-Child item">
                            <h5>Voiture neuve <small>Au meilleur prix</small></h5>
                            <div><i class="tmb-3"></i></div>
                            <span>Demandez un devis</span>
                        </div>

                        <div class="Of-Child item">
                            <h5>Voiture neuve <small>Au meilleur prix</small></h5>
                            <div><i class="tmb-5"></i></div>
                            <span>Service dépannage</span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
	</div>
	
</section>

<section class="Occ">	
	<div class="container">
        <div class="section-block">
            
            <div class="row">
                <div class="SectionTitle text-center col-md-12">
                    <h3 class="TheChild">Occasion</h3>
                </div>
            </div>

            <div class="WidgetContainer">
                <div class="row">

                    <div class="col-md-4">
                        <aside class="Widget WidgetFlex WhiteSection Wd-blue">
                            <div class="WidgetTitle text-center"><h4>Vendre ma voiture</h4></div>
                            <div class="WidgetBody text-center">
                                <span>Gratuit,<br />rapide et au meilleur prix.</span>
                                <a href="" class="Zoom">
                                    <img src="/img/submit_ads.svg" class="m-t-15 m-b-15 Wd-submit-ads">
                                </a>
                                <h6 class="sb-title-sd">Passer une annonce</h6>
                            </div>
                        </aside>
                    </div>

                    <div class="col-md-4">
                        <aside class="Widget WidgetFlex WhiteSection Wd-yellow">
                            <div class="WidgetTitle text-center"><h4>Estimation de mon véhicule</h4></div>
                            <div class="WidgetBody">
                                <div class="Estv-form">
                                    <form method="post" id="formEstim">
                                        @csrf
                                        <div class="form-group row" style="margin-bottom: 1rem !important;">
                                            <label for="yearSearch-estim" class="col-md-3 col-form-label">Année:</label>
                                            <div class="col-md-9 ">
                                                <select name="year" class="form-control result-estim" id="yearSearch-estim">
                                                    <option value="0" selected>Toutes</option>
                                                    @for($i=date('Y'); $i >= 1900 ; $i--)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-bottom: 1rem !important;">
                                            <label for="brandSearch-estim" class="col-md-3 col-form-label">Marque:</label>
                                            <div class="col-md-9 ">
                                                <select name="brand" class="form-control brand4 brand result-estim" id="brandSearch-estim">
                                                    <option style="color:black" value="0" selected>Toutes</option>
                                                    @foreach ($brands as $brand)
                                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                                    @endforeach          
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-bottom: 1rem !important;">
                                            <label for="modelSearch-estim" class="col-md-3 col-form-label">Modèle:</label>
                                            <div class="col-md-9 ">
                                                <select class="form-control result-estim isSearch" name="model" disabled="" id="modelSearch-estim">
                                                    <option value="0">Tous</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="float-left" id="prixEstim"></div>
                                        <div class="float-right">
                                            <button type="button" disabled class="btn btn-primary btn-blue" id="valider-estim">Valider</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </aside>
                    </div>

                    <div class="col-md-4">
                    <aside class="Widget WidgetFlex WhiteSection Wd-blue">
                        <div class="WidgetTitle text-center"><h4>Les voitures les + recherchées</h4></div>
                        <div class="WidgetBody">
                            <ul class="Tags">
                                @foreach($historicsbrd as $hbdr)
                                <li><a href="{{url('/neuf')}}/{{$hbdr->slug}}">{{$hbdr->name}}</a></li>
                                @endforeach
                                @foreach($historicsmdl as $hmdl)
                                    <li><a href="{{url('/neuf')}}/{{$hmdl->brand->slug}}/{{$hmdl->slug}}">{{$hmdl->name}}</a></li>
                                @endforeach
                                <li><a href="" class="font-italic">Afficher plus ..</a></li>
                            </ul>
                        </div>
                    </aside>
                    </div>

                    <div class="col-md-12 m-t-30">
                    <aside class="Widget Wd-TxBlue">
                        <div class="WidgetTitle"><h4>Les dernières annonces</h4></div>
                        <div class="WidgetBody">
                            <div class="OwlAds owl-carousel owl-theme">
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item1.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item2.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item3.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item4.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item5.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item6.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>
                                
                                <article class="AdsItem item">
                                    <div class="AdsThumbnail">
                                        <a href=""><img src="/img/ads-img/item7.jpg" alt="BMW Serie 1"></a>
                                    </div>
                                    <div class="AdsBody">
                                        <a href=""><h3>BMW Serie 1</h3></a>
                                        <div class="AdsVh-ca">
                                            <span>41 594 KM</span>
                                            <span>Diesel</span>
                                            <span>2016</span>
                                        </div>
                                        <div class="AdsPrice">
                                            23490 <span class="Currency">€</span>
                                        </div>
                                    </div>
                                </article>

                            </div>
                        </div>
                    </aside>
                    </div>

                </div>
            </div>

        </div>
	</div>
	
</section>

<section class="Ofse">
    <div class="container">
        <div class="section-block ClearPadding">
            <div class="Banner2">
                <img src="{{ asset('/img/banner2.jpg')}}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="VideoWrapper">
	
	<div class="container">
	<div class="section-block VideoInner">
		
		<div class="row">
			<div class="SectionTitle TextWhite text-center col-md-12">
				<h3 class="TheChild TextWhite">Vidéo</h3>
			</div>
		</div>

		<div class="row">

			<div class="OwlVideo owl-carousel owl-theme col-md-12 m-t-30">
				@foreach($videos as $video)
				<div class="Vd-Child item">
					<div class="VdThumbnail">
						<a href="{{url('/videos')}}/{{$video->id}}">
							<img src="https://img.youtube.com/vi/{{$video->url}}/mqdefault.jpg">
							<h4>{{$video->title}}</h4>
						</a>
					</div>
				</div>
				@endforeach
			</div>

		</div>
		
		<div class="row">
			<div class="col-md-12">
				<a href="{{url('/videos')}}" class="Pn-more float-right" style="padding-right: 36px;">Consulter toutes les vidéos <i class="zmdi zmdi-caret-right-circle"></i></a>
			</div>
		</div>			

	</div>
	</div>
	
</section>

<section class="PostListing">
    <div class="container">
        <div class="section-block">
            <div class="row">
                <div class="SectionTitle TextWhite text-center col-md-12">
                    <h3 class="TheChild">Actus <span>&</span> Essais</h3>
                </div>
            </div>
            <div class="PostInner" style="margin-top:22px !important;">
                <div class="row">
                    @foreach($articles as $article)
                    <article class="PostBox col-md-4">
                        <div class="PostThumbnail">
                            <a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">
                                <div class="cover"
                                    style="background-image: url('{{$article->cover }}')"></div>
                            </a>
                            <div class="PostBody">
                                <a href="{{url('/articles/cat')}}/{{$article->category->id}}"><span class="PostCat cat{{$article->category->name}}" style="background:{{$article->category->color}}">{{$article->category->name}}</span></a>
                                <a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}"><h3 class="PostTitle">{{$article->title}}</h3></a>
                                <h6 class="text-muted m-b-10 font-weight-light" style="font-size: 13px;">
									<i class="zmdi zmdi-edit"></i> 
									Le {{(new Date($article->created_at))->format('d F Y à H:i')}} 
									par {{$article->user->username}}
								</h6>
                                <a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">
                                    <p class="PostExcept text-justify" id="art_{{$article->id}}">
                                        {!! substr(strip_tags($article->body),0,400) !!}
                                    </p>
                                </a>
                            </div>
                        </div>
                    </article>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 m-t-10">
                    <a href="{{url('/articles')}}" class="Pn-more TextBlack float-right" style="padding-right: 50px;">Consulter tous les articles <i class="zmdi zmdi-caret-right-circle"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Gde">
    <div class="container">
        <div class="section-block Gde-container">
            <div class="row">
                <div class="SectionTitle text-center col-md-12">
                    <h3 class="TheChild">Guide <span>&</span> Conseils utiles</h3>
                </div>
            </div>
            <div class="GdeInner" style="margin-top: 8px !important;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="Gde-block Gde-1">
                            <div class="transform-both">
                                <img src="{{ asset('/img/Gde1.png')}}">
                                <h4 class="Gde-title Gde-t-small m-t-20">Achat d'un Véhicule neuf ou occasion</h4>
                                <h5 class="Gde-subtitle Gde-st-small text-uppercase m-t-10">Ce qu'il faut
                                    savoir
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="Gde-block Gde-2">
                            <div class="transform-both">
                                <img src="{{ asset('/img/Gde2.png')}}">
                                <h4 class="Gde-title Gde-t-small TextWhite m-t-20">Avis & Commentaires des
                                    Automobilistes
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="Gde-block Gde-5">
                            <div class="abs">
                                <h4 class="Gde-title">véhicules<br />hybrides & electriques</h4>
                                <h5 class="Gde-subtitle text-transform">Actus - Guide d’achat - Infos pratiques</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="Gde-block Gde-3">
                            <div class="transform-both">
                                <img src="{{ asset('/img/Gde3.png')}}">
                                <h4 class="Gde-title Gde-t-small TextWhite m-t-20">Astuces pour<br/>Economosier
                                    du<br/>Carburant
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="Gde-block Gde-4">
                            <div class="transform-both">
                                <h4 class="Gde-title Gde-t-small m-b-10">
                                Top vente</h5>
                                <img src="{{ asset('/img/Gde4.png')}}">
                                <h4 class="Gde-title Gde-t-small m-t-10">Les véhicules<br/>les plus vendus<br/>au
                                    maroc
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="Gde-block Gde-6">
                            <div class="abs">
                                <h4 class="Gde-title">Véhicules de collection</h4>
                                <h5 class="Gde-subtitle">Infos pratiques & Petites annonces</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="" class="Pn-more TextBlack float-right" style="padding-right: 50px;">Consulter tous les éléments <i class="zmdi zmdi-caret-right-circle"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Partenaires">
    <div class="container">
        <div class="section-block section-block-white">
            <div class="PartInner">
                <div class="row">
                    <div class="SectionTitle TextWhite text-center col-md-12">
                        <h3 class="TheChild">Partenaires</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="OwlPart owl-carousel owl-theme col-md-10 offset-md-1 m-b-20" style="margin-top: 22px !important;">
                        @foreach ($partenaires as $partenaire)
                            <div class="item"><img src="{{$partenaire->logo}}" alt="{{$partenaire->societe}}"></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
    <script src="/js/tomobile_home.js"></script>
@endsection
@section('style')
<style>
.PostBody {
    padding: 15px 15px 20px 15px !important;
    background-color: white;
}
</style>
@endsection