@extends('layouts.app')
@section('title',$model->name.'- Versions')

@section('content')
<section class="PreBd m-t-30">	
	<div class="container">
        <div class="section-block section-block-white HasPadding">            
            <div class="row">
                <div class="Breadcrumps col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/neuf')}}">Voitures neuves</a></li>
                        <li><a href="{{url('/neuf')}}/{{$brand->slug}}">{{$brand->name}}</a></li>
                        <li><a href="{{url('/neuf')}}/{{$brand->slug}}/{{$model->slug}}">{{$model->name}}</a></li>
                    </ul>
                </div>
            </div>   
            
            <div class="Sr-results-Vn">

                <h6 class="Title-Lis-bn text-uppercase">Mes critères</h6>
                <div class="Sr-banner-sp">

                    <div class="row">
                        <div class="col-md-12" id="criteria">
                            <div class="Sr-Banner-child">
                                <span>{{ $brand->name }}</span>
                                <a href="{{url('/neuf')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                            </div>

                            <div class="Sr-Banner-child">
                                <span>{{ $model->name }}</span>
                                <a href="{{url('/neuf')}}/{{ $brand->slug }}" class="close"><i class="zmdi zmdi-close"></i></a>
                            </div>
                            
                            @if(Session::has('GmaxPrice')  and session()->get('GmaxPrice') != 9000000)
                                <div class="Sr-Banner-child">
                                    <span>max: {{ session()->get('GmaxPrice') }} DH </span>
                                    <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('GminPrice')  and session()->get('GminPrice') != 0)
                                <div class="Sr-Banner-child">
                                    <span>min: {{ session()->get('GminPrice') }} DH </span>
                                    <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('maxPuissance')  and session()->get('maxPuissance') != 20)
                                <div class="Sr-Banner-child">
                                    <span>max: {{ session()->get('maxPuissance') }} CH </span>
                                    <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('minPuissance')  and session()->get('minPuissance') != 0)
                                <div class="Sr-Banner-child">
                                    <span>min: {{ session()->get('minPuissance') }} CH </span>
                                    <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('category')  and session()->get('category') != 0)
                                <div class="Sr-Banner-child">
                                    <span>{{ session()->get('categoryName') }}</span>
                                    <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('energy') and session()->get('energy') != 0)
                                @if(is_array(session()->get('energieName')))
                                    @foreach(session()->get('energieName') as $key=>$energie)
                                    <div class="Sr-Banner-child energie_jq">
                                        <input type="hidden" value="{{ session()->get('energy')[$key] }}" />
                                        <span>{{ $energie }} </span>
                                        <a href="#" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="Sr-Banner-child energie_jq">
                                        <input type="hidden" value="{{ session()->get('energy') }}" />
                                        <span>{{ session()->get('energieName') }} </span>
                                        <a href="#" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endif
                            @endif
                            @if(Session::has('trans') and session()->get('trans') != 0)
                                @if(is_array(session()->get('trans')))
                                    @foreach(session()->get('trans') as $key=>$trans)
                                    <div class="Sr-Banner-child trans_jq">
                                        <input type="hidden" value="{{ session()->get('trans')[$key] }}" />
                                        <span>{{ $trans }} </span>
                                        <a href="#" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="Sr-Banner-child trans_jq">
                                        <input type="hidden" value="{{ session()->get('trans') }}" />
                                        <span>{{ session()->get('trans') }} </span>
                                        <a href="#" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endif
                            @endif
                            @if(Session::has('interiorDesign'))
                                @foreach(session()->get('interiorDesign') as $interiorDesign)
                                    <div class="Sr-Banner-child">
                                        <span>Design intérieur: @if($interiorDesign != '0'){{ $interiorDesign }} @else Autres @endif</span>
                                        <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endforeach
                            @endif
                            @if(Session::has('interiorColor'))
                                @foreach(session()->get('interiorColor') as $interiorColor)
                                    <div class="Sr-Banner-child">
                                        <span>Couleur intérieure: @if($interiorColor != '0'){{ $interiorColor }} @else Autres @endif</span>
                                        <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endforeach
                            @endif
                            @if(Session::has('exteriorColor'))
                                @foreach(session()->get('exteriorColor') as $exteriorColor)
                                    
                                        <div class="Sr-Banner-child">
                                            <span>Couleur extérieure: @if($exteriorColor != '0'){{ $exteriorColor }} @else Autres @endif</span>
                                            <a href="{{ url('/search/searchDetailed')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>
                                    
                                @endforeach
                            @endif
                            <a href="{{ url('/neuf')}}" class="Sr-clear" id="clearAll">(tout effacer)</a>

                            <div class="col-md-12 float-right text-right">
                                <a href="{{ url('/search/searchDetailed')}}" class="btn btn-blue float-right"><i class="zmdi zmdi-edit"></i> Modifier ma recherche</a>
                                @guest
                                    <a href="{{ url('/login') }}" class="btn btn-blue float-right"><i class="zmdi zmdi-save"></i> Sauvegarder ma recherche</a>
                                @else
						            <a href="#" class="btn btn-blue float-right" data-toggle="modal" data-target="#sauvegarde"> <i class="zmdi zmdi-save"></i> Sauvegarder ma recherche</a>
                                @endguest
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="dataVehicles" data-vehicles="{{ $vehicles }}" >                                        
            <input type="hidden" id="dataEnergies" data-energies="{{ $energies }}" >		            
            <div class="row">
                <div class="col-md-9">

                    <h3 class="m-t-20 Title-Lis-bn text-uppercase">Fiche technique <strong>{{ $model->name }}</strong></h3>

                    <div class="AutoLP-container m-t-20">
                        <div class="row">
                            
                            <div class="col-md-6">
                                <div class="LP-AutoBrand">
                                    <!-- LightBox -->
                                    @php $i = 1 @endphp
                                    @foreach($vehicles as $vehicle)
                                        @foreach($vehicle->medias as $media)
                                            @php $i++ @endphp
                                        @endforeach                                    
                                    @endforeach   
                                    <a href="{{ $model->image }}" data-lightbox="autobrand-lb" data-title="{{ $model->name }}">
                                        <img src="{{ $model->image }}" alt="{{ $model->name }}">
                                        <span class="Lp-LightboxAction"><i class="zmdi zmdi-collection-image"></i>{{$i}}</span>
                                    </a>       
                                    @foreach($vehicles as $vehicle)
                                        @foreach($vehicle->medias as $media)
								            <a href="{{ $media->path }}" data-title="{{ $model->name }}" data-lightbox="autobrand-lb"></a>
                                        @endforeach                                    
                                    @endforeach                             
                                </div>
                            </div>
                            
                            <div class="col-md-6 LP-Cm-body">
                                <span class="Title-Lis-bl text-uppercase"><span class="color-theme"><span id="countVehicles" >{{sizeof($vehicles)}}</span> version(s)</span> disponible(s)</span></br>
                                @if(sizeof($vehicles) !=0)
                                    <span class="Title-Lis-bl text-uppercase m-t-5">À PARTIR DE <span id="vrprice">{{$model->minPrix[0]['price']}}</span> DH</span>
                                @endif
                                <div class="Lp-Cm-sl-inner">
                                    <div class="row nopadding">
                                        <div class="col-md-1 nopadding">
                                            <img style="height: 16px;margin-top: 4px;" src="{{asset('/img/system-controller.png') }}">
                                        </div>
                                        <div class="col-md-5 form-group nopadding">
                                            <select class="energySelect form-control notSel2" title="Energie" multiple name="energies[]" id="vrenergy">
                                            @foreach ($energies as $energie)
                                                @if(Session::has('energy') and (session()->get('energy') == $energie->id or (is_array(session()->get('energy')) and in_array($energie->id,session()->get('energy'))))) 
                                                    <option value="{{$energie->id}}" selected >{{$energie->name}}</option>
                                                @else
                                                    <option value="{{$energie->id}}" >{{$energie->name}}</option>
                                                @endif
                                            @endforeach 
                                            </select> 
                                        </div>	
                                        <div class="col-md-6 form-group nopadding">
                                            <select class="transSelect form-control notSel2" title="Transmission" multiple name="trans[]" id="vrtrans">
                                                <option value="automatique" @if(Session::has('trans')  and (session()->get('trans') == "automatique" or (is_array(session()->get('trans')) and in_array("automatique",session()->get('trans')))) ) selected @endif >Automatique</option>
                                                <option value="manuelle" @if(Session::has('trans')  and (session()->get('trans') == "manuelle" or (is_array(session()->get('trans')) and in_array("manuelle",session()->get('trans')))) ) selected @endif >Manuelle</option>
                                            </select>
                                        </div>                                        
                                        <input type="hidden" id='brandSlug' name="brand" value="{{ $brand->slug }}" >
                                        <input type="hidden" id='modelSlug' name="model" value="{{ $model->slug }}" >                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <img src="{{ $model->image }}" alt="{{ $brand->name }}"></div>
                            </div>

                            <div class="col-md-12 m-t-40">
                                <table class="table table-theme">
                                    <thead>
                                        <tr>
                                        <th scope="col">Version</th>
                                        <th scope="col" class="text-right">Prix</th>
                                        </tr>
                                    </thead>
                                    <tbody id="vrlist">
                                        @foreach($vehicles as $vehicle)
                                        <tr onclick="window.location.href ='{{url('/')}}/{{$brand->slug}}/{{$model->slug}}/{{$vehicle->slug}}'">
                                            <th>{{  $vehicle->name }}</th>
                                            <td>{{  $vehicle->price }} DH</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>                            

                        </div>
                    </div>

                </div>

                <div class="col-md-3">
                    <aside class="Sidebar">
                        @if(sizeof($vehicles) !=0)
                        <div class="Widget WidgetInfolettre">
                            <div class="Icon"><i class="zmdi zmdi-notifications"></i></div>
                            <span>Recevez par e-mail toutes les infos de la {{ $brand->name }} {{ $model->name }}</span>
                        </div>
                        @endif
                        <div class="Widget WidgetWhite">
                            <img src="{{asset('/img/corbeille/ads-2.jpg') }}">
                        </div>

                        <div class="Widget WidgetWhite">
                            <img src="{{asset('/img/corbeille/ads-1.jpg') }}">
                        </div>
                        @if(sizeof($promotions))
                            <div class="Widget WidgetWhite">
                                <div class="WidgetBanner">Offres spéciales</div>
                                <div class="Offre-spe owl-carousel owl-theme">
                            
                                    @foreach ($promotions as $promotion)
                                    <div class="CP-Box text-center">
                                        <a href="{{url('/neuf')}}/{{$promotion->modele->brand->slug}}/{{$promotion->modele->slug}}/{{$promotion->slug}}">
                                            <div class="CP-Box-Thumb">
                                                <img src="{{$promotion->image}}" alt="{{$promotion->name}}">
                                            </div>
                                            <div class="CP-Box-Body">
                                                <h5 class="CP-Box-Title">{{$promotion->name}}</h5>
                                                <span class="CP-des">{{$promotion->promotionLast[0]->description}}</span>
                                                <span class="CD-Prox-span">Jusqu'à <strong>-{{$promotion->promotionLast[0]->percentage}}</strong></span>
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach  
                                </div>
                            </div>
                        @endif	
                        </div>
                    </aside>
                </div>

            </div>  
		</div>
	</div>	
</section>   
<div class="modal fade" id="sauvegarde" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content" id="formSauvegarder">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sauvegarder ma recherche</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                    <input type="text" id="searchName" class="form-control" placeholder="Nom de la recherche">
                    </div>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary sauvegarde">Sauvegarder ma recherche</button>
            </div>
        </form>
    </div>
</div>                     
@endsection
@section('script')
<script>
/* By VERSION Page */
$('#vrenergy').SumoSelect({placeholder: 'Carburant'});
$('#vrtrans').SumoSelect({placeholder: 'Transmission'});   
function getVehicles() {
    var vrlist = $("#vrlist");
    var vehicles = $("#dataVehicles").data("vehicles");
    var filtred = Array();
    var prix;
    for (var i = 0; i < vehicles.length; i++) {
        if (($('#vrenergy').val()== null ||jQuery.inArray("" + vehicles[i].energie_id, $('#vrenergy').val()) != -1) && ($('#vrtrans').val()== null || jQuery.inArray("" + vehicles[i].transmission, $('#vrtrans').val()) != -1)  ) {
            filtred.push(vehicles[i]);
        }
    }
    $('#countVehicles').text(filtred.length)
    if (filtred.length > 0) {
        vrlist.empty();
        prix = filtred[0].price;
        for (var i = 0; i < filtred.length; i++) {
            if (prix > filtred[i].price){prix = filtred[i].price}
            vrlist.append('<tr onclick="window.location.href ='+"'"+'/neuf/'+$('#brandSlug').val()+'/'+$('#modelSlug').val()+'/'+filtred[i].slug +"'"+'"><th>' + filtred[i].name + '</th><td> ' + filtred[i].price + 'DH</td></tr>');
        }
        $('#vrprice').text(prix)
    } else {
        vrlist.empty();
        vrlist.html('<div id="msgRes" class="msgRes"><h4 style="text-align: center;font-weight: 600;" > Aucun résultat trouvé.<br /> Retour à la page <a style = "color: #4086c7;" href = "/detailedSearch" > recherche détaillée</a></h4></div>');
    }
}
getVehicles()
$('#vrenergy').change(function () {
    $('.energie_jq').remove()
    console.log($('#vrenergy').val());    
    var energies = $('#dataEnergies').data("energies")
    for (var i = 0; i < energies.length; i++) {
        if(jQuery.inArray("" + energies[i].id, $('#vrenergy').val()) != -1)
            $('<div class="Sr-Banner-child energie_jq"><input type="hidden" value="'+energies[i].id+'" /><span>'+energies[i].name+'</span><a href="#" class="close"><i class="zmdi zmdi-close"></i></a></div>').insertBefore('#clearAll')
    }
    getVehicles()
});
$('#vrtrans').change(function () {
    $('.trans_jq').remove()
    if(jQuery.inArray("automatique", $('#vrtrans').val()) != -1)
            $('<div class="Sr-Banner-child trans_jq"><input type="hidden" value="automatique" /><span>automatique</span><a href="#" class="close"><i class="zmdi zmdi-close"></i></a></div>').insertBefore('#clearAll')
    if(jQuery.inArray("manuelle", $('#vrtrans').val()) != -1)
            $('<div class="Sr-Banner-child trans_jq"><input type="hidden" value="manuelle" /><span>manuelle</span><a href="#" class="close"><i class="zmdi zmdi-close"></i></a></div>').insertBefore('#clearAll')
    getVehicles()
});
$('#criteria').on('click','.trans_jq .close',function(){
    prt = $(this).parent();
    $('select#vrtrans')[0].sumo.unSelectItem(prt.children('input').val())
    prt.remove();    
})
$('#criteria').on('click','.energie_jq .close',function(){

    console.log($.session.get('GminPrice'));
    
    prt = $(this).parent();
    $('select#vrenergy')[0].sumo.unSelectItem(prt.children('input').val())
    prt.remove();    
})
</script>
@endsection