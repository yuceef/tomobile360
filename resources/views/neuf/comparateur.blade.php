@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">
	<div class="container">
        <div class="section-block section-block-white HasPadding">
            <div class="row">
                <div class="Pagesheader color-theme col-md-12">
                    <h2 class="text-uppercase">Comparateur</h2>
                </div>
                <div class="Breadcrumps col-md-12">
                    <ul>
                        <li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/neuf/comparateur')}}">Comparateur</a></li>
                    </ul>
                </div>
            </div>
            <input type="text" id="versionId">
            <button class="btn btn-blue" id="ajouterComp">Ajouter</button>
            <input type="hidden" value="{{$version1}}">
            <input type="hidden" value="{{$version2}}">
            <div class="row col-md-12 m-t-20" id="affComp">
                <div class="col-md-6">{!! str_replace(',"','<br />"',$version1) !!}</div>
                <div class="col-md-6">{!! str_replace(',"','<br />"',$version2) !!}</div>
            </div>
        </div>
	</di
</section>            
@endsection
@section('script')
<script>
    $('#ajouterComp').click(function () {
        console.log($('#affComp col-md-*'));
        
    });
</script>
@endsection