@extends('layouts.app')
@section('title', 'Toutes les marques')
@section('style')
<style>
.brandWidth {
    max-height: 82px;
    width: auto !important;
}
</style>
@endsection
@section('content')
<section class="PreBd m-t-30">
    <div class="container">
        <div class="section-block section-block-white HasPadding">
            <div class="row">
                <div class="Breadcrumps col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{ url('/') }}">Accueil</a></li>
                        <li><a href="{{ url('/neuf') }}" class="text-capitalize">@if(isset($input['type'])){{$input['type']}}s @else véhicules @endif neuves</a></li>
                        <li><a href="#" class="active">Toutes les marques</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-9">
                    <div class="AutoRM-container m-t-15">
                        <div class="row m-0">
                            @foreach ($brands as $brand)
                                @if($brand->modeles_count >0)
                                <div class="col-md-3">
                                    <article class="AutoRM-Box AutoRM-DIV-Brand">
                                        <div class="AutoRM-Thumb ">
                                            @if(Session::has('brand'))
                                                <a class="brResult" href="{{url('/search/neuf')}}/{{$brand->slug}}"><img class="brandWidth" src="{{ $brand->logo }}" alt=""></a>
                                            @else
                                                <a class="brResult" href="{{url('/neuf')}}/{{$brand->slug}}"><img class="brandWidth" src="{{ $brand->logo }}" alt=""></a>
                                            @endif
                                        </div>
                                        <div class="AutoRM-Body">
                                            <h4>{{ $brand->name}}</h4>
                                            <span class="AutoRM-Version">{{ $brand->modeles_count }} @if($brand->modeles_count > 1)modèles disponibles @else modèle disponible @endif  </span>
                                        </div>
                                    </article>
                                </div>
                                @endif  
                            @endforeach   
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <aside class="Sidebar">
                        @if(sizeof($promotions))
                        <div class="Widget WidgetWhite">
                            <div class="WidgetBanner">Offres spéciales</div>
                            <div class="Offre-spe owl-carousel owl-theme">
                        
                                @foreach ($promotions as $promotion)
                                    <div class="CP-Box text-center">
                                        <a href="{{url('/neuf')}}/{{$promotion->modele->brand->slug}}/{{$promotion->modele->slug}}/{{$promotion->slug}}">
                                            <div class="CP-Box-Thumb">
                                                <img src="{{$promotion->image}}" alt="{{$promotion->name}}">
                                            </div>
                                            <div class="CP-Box-Body">
                                                <h5 class="CP-Box-Title">{{$promotion->name}}</h5>
                                                <span class="CP-des">{{$promotion->promotionLast[0]->description}}</span>
                                                <span class="CD-Prox-span">Jusqu'à <strong>-{{$promotion->promotionLast[0]->percentage}}</strong></span>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach  

                            </div>
                        </div>
                        @endif
                        @if(sizeof($articles))
                        <div class="Widget WidgetWhite">
                            <div class="WidgetBanner">Dernières ACTUS des marques</div>
                            <div class="ActuListing">
                                <ul>
                                    @foreach($articles as $article)
                                        <li><a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">{{substr($article->title,0,50)}}...</a></li>
                                    @endforeach
                                </ul>
                                <a href="{{url('/articles')}}" class="text-right actu-more">Voir toutes les actus</a>
                            </div>
                        </div>
                        @endif
                        <div class="Widget WidgetWhite">
                            <img src="{{ asset('/img/corbeille/ads-1.jpg') }}">
                        </div>
                    </aside>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection