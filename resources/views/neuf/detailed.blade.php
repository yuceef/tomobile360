@extends('layouts.app')
@section('content')
    <section class="PreBd m-t-30">
        <div class="container">
            <div class="section-block HasPadding">
                <div class="row">
                    <div class="Breadcrumps col-md-12 m-t-10">
                        <ul>
                            <li><a href="{{ url('/')}}">Accueil</a></li>
                            <li>                        
                                @if($isneuf==1)
                                    <a href="{{ url('/neuf')}}">Voitures neuves</a>
                                 @else    
                                    <a href="{{ url('/occasion')}}">Voitures occasion</a>
                                @endif
                            </li>
                            <li class="active">Recherche détaillée</li>                           
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <input type="hidden" id="dataModels" data-models="{{ $modeles }}" >                                        
                        @if($isneuf==1)
                            <form action="/search/searchNeuf" method="post" id="formSearch" style="width: 100%;">
                        @else
                            <form  action="/search/searchOccasion" method="post" id="formSearch" style="width: 100%;">
                        @endif

                        @csrf   
                        <input id="typeSearch-nf" type="hidden" value="voiture" name="type">
                        <input type="hidden" id="dataIsneuf" value="{{ $isneuf }}" name="isneuf">
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Recherche détaillée</h4>
                                <div class="Sg-del float-right"><label id="clearBrand" class="btn btn-light-blue clearButton"><i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Marque</label>
                                <select name="brand" class="form-control brand2 result brand" id="brand">
                                    <option value="0" selected>Toutes</option>
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}" title="{{ $brand->name }}" @if(session()->get('brand') == $brand->id) selected @endif>{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                                @foreach ($brands as $brand)
                                    <input type="hidden" id="BrandId{{str_replace(" ","_",$brand->name)}}" value="{{$brand->logo}}" />
                               @endforeach     
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="model">Modèle</label>
                                <select name="model" class="form-control result" disabled="" id="model">
                                    <option value="0" selected>Tous</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Version</label>
                                <select name="version" class="form-control result" id="version">
                                    <option value="0" selected>Toutes</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                            @if($isneuf==0)
                                <label for="">Année</label>
                                <select name="year" class="form-control result" id="year">
                                    <option value="0" selected>Toutes</option>
                                    @for($i=2019; $i >= 2000 ; $i--)
                                    <option value="{{$i}}" @if(session()->get('year') == $i) selected @endif >{{$i}}</option>
                                    @endfor
                                </select>
                            @endif
                            </div>
                        </figure>
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Prix</h4>
                                <div class="Sg-del float-right"><label id="clearPrix" class="btn btn-light-blue clearButton">
                                        <i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="form-group col-md-9">
                                <input id="GminPrice" class="result" name="GminPrice" type="hidden" @if(session()->has('GminPrice')) value="{{session()->get('GminPrice')}}" @else value="0" @endif>
                                <input id="GmaxPrice" class="result" name="GmaxPrice" type="hidden" @if(session()->has('GmaxPrice')) value="{{session()->get('GmaxPrice')}}" @else value="9000000" @endif>
                                <input type="text" class="result" id="price" value="" name="price" />
                            </div>
                            <div class="form-group col-md-3"></div>
                        </figure>
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Catégorie</h4>
                                <div class="Sg-del float-right"><label id="clearCat" class="btn btn-light-blue clearButton"><i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="form-group col-md-12">
                                <ul class="listing-car-Gf-Rch">
                                    @foreach($cats as $cat)
                                    <li @if(session()->get('category') == $cat->id) class="category active" @else class="category"  @endif id="{{$cat->id}}"><i class="{{$cat->slug_class}}"></i><p class="text-center">{{$cat->name}}</p></li>
                                    @endforeach
                                    <input id="category" class="result" name="category" type="hidden" @if(session()->has('category')) value="{{session()->get('category')}}" @else value="0" @endif>
                                </ul>
                            </div>
                        </figure>
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Moteur</h4>
                                <div class="Sg-del float-right"><label id="clearEnergy" class="btn btn-light-blue clearButton"><i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="form-group form-group-check col-md-6">
                                <label class="block">Carburant</label>
                                @foreach ($energies as $energie)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input checkbox-ui moteur_clear result" type="checkbox" name="energy[]" id="{{$energie->name}}" value="{{$energie->id}}" 
                                        @if( session()->has('energy') and ( $energie->id==session()->get('energy') or ( is_array(session()->get('energy')) and in_array($energie->id,session()->get('energy')) ) ) ) ) checked @endif>
                                        <label class="form-check-label" for="{{$energie->name}}">{{$energie->name}}</label>
                                    </div>
                                @endforeach 
                            </div>

                            <div class="form-group col-md-6">
                                <label class="block">Transmission</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input moteur_clear checkbox-ui result" type="checkbox" name="trans[]" id="automatique" value="automatique"
                                        @if( session()->has('trans') and ( session()->get('trans')=="automatique" or ( is_array(session()->get('trans')) and in_array("automatique",session()->get('trans')) ) ) ) ) checked @endif>
                                    <label class="form-check-label" for="automatique">Automatique</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input moteur_clear checkbox-ui result" type="checkbox" name="trans[]" id="manuelle" value="manuelle"
                                        @if( session()->has('trans') and ( session()->get('trans')=="manuelle" or ( is_array(session()->get('trans')) and in_array("manuelle",session()->get('trans')) ) ) ) ) checked @endif>
                                    <label class="form-check-label" for="manuelle">Manuelle</label>
                                </div>
                            </div>
                        </figure>
                        <figure class="SearchGroup">                            
                            <div class="form-group col-md-6">
                                <label class="block">Puissance Fiscale (Ch DIN)</label>
                                <input id="minPuissance" class="result" name="minPuissance" type="hidden" @if(session()->has('minPuissance')) value="{{session()->get('minPuissance')}}" @else value="0" @endif>
                                <input id="maxPuissance" class="result" name="maxPuissance" type="hidden" @if(session()->has('maxPuissance')) value="{{session()->get('maxPuissance')}}" @else value="20" @endif>
                                <input type="text"  id="rangeFiscal" value="" name="rangeFiscal" />
                            </div>
                            @if($isneuf==0)
                            <div class="form-group offset-md-1 col-md-5">
                                <label class="block">Kilométrage (Km)</label>
                                <input id="minMileage" class="result" name="minMileage" type="hidden" @if(session()->has('minMileage')) value="{{session()->get('minMileage')}}" @else value="0" @endif>
                                <input id="maxMileage" class="result" name="maxMileage" type="hidden" @if(session()->has('maxMileage')) value="{{session()->get('maxMileage')}}" @else value="200000" @endif>
                                <input type="text"  id="mileage" value="" name="mileage" />
                            </div>
                            @endif
                        </figure>
                        @if($isneuf==0)
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Recherche géographique</h4>
                                <div class="Sg-del float-right"><label id="clearCity" class="btn btn-light-blue clearButton"><i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="form-group col-md-5">
                                <label>Recherche par ville</label>
                                <select class="form-control js-example-basic-single result" id="city" name="city">
                                    <option value="0" selected>Tous</option>
                                    @foreach ($cities as $city)
                                        <option value="{{$city->id}}" @if(session()->get('city') == $city->id) selected @endif >{{$city->name}}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </figure>
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Etat de véhicule</h4>
                                <div class="Sg-del float-right"><label id="clearEtat" class="btn btn-light-blue clearButton"><i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="form-group form-group-check col-md-6">
                                <label class="block">Nombre de propriétaire précédents</label>
                                @for($i=1; $i <= 5 ; $i++)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input etat_clear radiobox-ui result" type="radio" name="nbrprop" id="pro{{$i}}" value="{{$i}}" @if(session()->get('nbrprop')==$i) checked @endif>
                                    <label class="form-check-label" for="pro{{$i}}">{{$i}}</label>
                                </div>
                                @endfor
                            </div>

                            <div class="form-group col-md-6">
                                <label class="block">Origine</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input etat_clear radiobox-ui result" type="radio" name="origine" id="origine1" value="0" @if(session()->get('origine')=='0') checked @endif>
                                    <label class="form-check-label" for="origine1">Marocaine</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input etat_clear radiobox-ui result" type="radio" name="origine" id="origine2" value="1" @if(session()->get('origine')=='1') checked @endif>
                                    <label class="form-check-label" for="origine2">Importé</label>
                                </div>
                            </div>
                        </figure>                        
                        @endif
                        <figure class="SearchGroup">
                            <div class="col-md-12">
                                <h4 class="Sg-title float-left">Equipements indispensables à avoir sur sa voiture</h4>
                                <div class="Sg-del float-right"><label id="clearEquipements" class="btn btn-light-blue clearButton"><i class="zmdi zmdi-delete"></i> Effacer</label></div>
                            </div>
                            <div class="col-md-4 form-group form-group-check m-t-20">
                                <label class="block">Design intérieur</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="interiorDesign[]" type="checkbox" id="cuir" value="Cuir" @if(session()->has('interiorDesign') and in_array("Cuir",session()->get('interiorDesign'))) checked @endif >
                                    <label class="form-check-label" for="cuir">Cuir</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="interiorDesign[]" type="checkbox" id="tissu" value="Tissu" @if(session()->has('interiorDesign') and in_array("Tissu",session()->get('interiorDesign'))) checked @endif>
                                    <label class="form-check-label" for="tissu">Tissu</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="interiorDesign[]" type="checkbox" id="velours" value="Velours" @if(session()->has('interiorDesign') and in_array("Velours",session()->get('interiorDesign'))) checked @endif>
                                    <label class="form-check-label" for="velours">Velours</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" type="checkbox" name="interiorDesign[]" id="int-des-autre" value="0" @if(session()->has('interiorDesign') and in_array("0",session()->get('interiorDesign'))) checked @endif>
                                    <label class="form-check-label" for="int-des-autre">Autre</label>
                                </div>
                            </div>
                            <div class=" col-md-4 form-group interiorColor form-group-check m-t-20">
                                <label class="block">Couleur intérieure</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="interiorColor[]" type="checkbox" id="int-noir" value="Noir" @if(session()->has('interiorColor') and in_array("Noir",session()->get('interiorColor'))) checked @endif>
                                    <label class="form-check-label" for="int-noir">Noir</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="interiorColor[]" type="checkbox" id="int-gris" value="Gris" @if(session()->has('interiorColor') and in_array("Gris",session()->get('interiorColor'))) checked @endif>
                                    <label class="form-check-label" for="int-gris">Gris</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="interiorColor[]" type="checkbox" id="int-beige" value="Beige" @if(session()->has('interiorColor') and in_array("Beige",session()->get('interiorColor'))) checked @endif>
                                    <label class="form-check-label" for="int-beige">Beige</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" type="checkbox" name="interiorColor[]" id="int-autre" value="0" @if(session()->has('interiorColor') and in_array("0",session()->get('interiorColor'))) checked @endif>
                                    <label class="form-check-label" for="int-autre">Autre</label>
                                </div>
                            </div>
                            <div class=" col-md-4 form-group exteriorColor form-group-check m-t-20">
                                <label class="block">Couleur extérieure</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="exteriorColor[]" type="checkbox" id="ext-noir" value="Noir" @if(session()->has('exteriorColor') and in_array("Noir",session()->get('exteriorColor'))) checked @endif>
                                    <label class="form-check-label" for="ext-noir">Noir</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="exteriorColor[]" type="checkbox" id="ext-gris" value="Gris" @if(session()->has('exteriorColor') and in_array("Gris",session()->get('exteriorColor'))) checked @endif>
                                    <label class="form-check-label" for="ext-gris">Gris</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" name="exteriorColor[]" type="checkbox" id="ext-bleu" value="Bleu" @if(session()->has('exteriorColor') and in_array("Bleu",session()->get('exteriorColor'))) checked @endif>
                                    <label class="form-check-label" for="ext-bleu">Bleu</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input equi_clear checkbox-ui result" type="checkbox" name="exteriorColor[]" id="ext-autre" value="0" @if(session()->has('exteriorColor') and in_array("0",session()->get('exteriorColor'))) checked @endif>
                                    <label class="form-check-label" for="ext-autre">Autre</label>
                                </div>
                            </div>
                        </figure>
                        <div class="form-group col-sm-4 offset-sm-4 m-t-20 m-b-20">
                            <input type="submit" class="btn btn-primary btn-yellow btn-block sticky submit-button col-md-2" value="0 résultats"  id="afficher2" name="">
                        </div>    
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
<script src="{{asset('/js/detailed.js')}}"></script>
<script>
$(window).scroll(function () {
    var bn = $('#afficher2');
    if ($(window).scrollTop() > 800) {
        bn.addClass('reSticky');
        bn.removeClass('sticky');
    }
    else {
        bn.addClass('sticky');
        bn.removeClass('reSticky');
    }
});
function checkOffset() {
    if ($('#afficher2').offset().top + $('#afficher2').height() >= $('.footer').offset().top - 10)
        $('#afficher2').css('position', 'absolute');

    if ($(document).scrollTop() + window.innerHeight < $('.footer').offset().top)
        $('#afficher2').css('position', 'fixed'); // restore when you scroll up
    //$('#afficher2').text($(document).scrollTop() + window.innerHeight);
}
$(document).scroll(function () {
    checkOffset();
});
</script>
@endsection