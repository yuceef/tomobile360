@extends('layouts.app')
@section('title',$brand->name.'- Modèles')

@section('content')
<section class="PreBd m-t-30">	
	<div class="container">
        <div class="section-block section-block-white HasPadding">            
            <div class="row">
                <div class="Breadcrumps col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/neuf')}}">Voitures neuves</a></li>
                        <li><a href="{{url('/neuf/')}}{{$brand->slug}}">{{$brand->name}}</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-9">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="AutoBrandWrapper m-t-20 m-b-10">
                                <div class="float-left"><img src="{{$brand->logo}}" alt="{{$brand->name}}"> </div>
                                <span>{{$brand->name}}</span>
                            </div>

                            <p>{{$brand->description}}</p>
                        </div>

                    </div>

                    <h3 class="m-t-20 Title-Lis-bn">{{$brand->name}} - Modèles</h3>

                    <div class="AutoRM-container m-t-15">

                        <div class="row m-0">
                        @if(sizeof($modeles)!=0)
                            @foreach ($modeles as $model)
                                @if($model->vehicles_count > 0)
                                    <div class="col-md-4 ">
                                        <div class="AutoRM-DIV">
                                            @if(Session::has('brand'))
                                            <a class="mdResult" href="{{url('/search/neuf')}}/{{$brand->slug}}/{{$model->slug}}">
                                            @else
                                            <a class="mdResult" href="{{url('/neuf')}}/{{$brand->slug}}/{{$model->slug}}">
                                            @endif
                                            <article class="AutoRM-Box">
                                                <div class="AutoRM-Thumb">
                                                    <div style=" position: relative; overflow: hidden;"><img src="{{ $model->image }}" alt="" class="zoom"></div>
                                                </div>
                                                <div class="AutoRM-Body">
                                                    <h4>{{ $model->name}}</h4>
                                                    <span class="AutoRM-Version">{{ $model->vehicles_count }}  @if($model->vehicles_count > 1) versions disponibles @else version disponible @endif </span>
                                                    @if($model->minPrix->first())
                                                    <span class="AutoRM-Price">&Agrave; partir de {{$model->minPrix->first()['price'] }} DH</span>
                                                    @endif
                                                </div>
                                            </article>
                                            </a>
                                        </div>
                                    </div>
                                @endif	
                            @endforeach	
                        @endif	

                        </div>

                    </div>

                </div>

                <div class="col-md-3">
                <aside class="Sidebar">

                    @if(sizeof($promotions))
                        <div class="Widget WidgetWhite">
                            <div class="WidgetBanner">Offres spéciales</div>
                            <div class="Offre-spe owl-carousel owl-theme">
                        
                                @foreach ($promotions as $promotion)
                                    <div class="CP-Box text-center">
                                        <a href="{{url('/neuf')}}/{{$promotion->modele->brand->slug}}/{{$promotion->modele->slug}}/{{$promotion->slug}}">
                                            <div class="CP-Box-Thumb">
                                                <img src="{{$promotion->image}}" alt="{{$promotion->name}}">
                                            </div>
                                            <div class="CP-Box-Body">
                                                <h5 class="CP-Box-Title">{{$promotion->name}}</h5>
                                                <span class="CP-des">{{$promotion->promotionLast[0]->description}}</span>
                                                <span class="CD-Prox-span">Jusqu'à <strong>-{{$promotion->promotionLast[0]->percentage}}</strong></span>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach  
                                
                        </div>
                    @endif
                    @if(sizeof($articles))
                        <div class="Widget WidgetWhite">
                            <div class="WidgetBanner">Dernières ACTUS {{$brand->name}}</div>
                            <div class="ActuListing">
                                <ul>
                                    @foreach($articles as $article)
                                        <li><a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">{{substr($article->title,0,50)}}...</a></li>
                                    @endforeach
                                </ul>
                                <a href="{{url('/articles')}}" class="text-right actu-more">Voir toutes les actus</a>
                            </div>
                        </div>
                    @endif

                    <div class="Widget WidgetWhite">
                        <img src="{{ asset('/img/corbeille/ads-1.jpg') }}">
                    </div>

                </aside>
                </div>

            </div>
            
		</div>
	</div>	
</section>
@endsection