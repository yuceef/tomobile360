@extends('layouts.app')
@section('content')
<section class="PreBd m-t-30">	
	<div class="container">
        <div class="section-block section-block-white HasPadding">    

            <div class="row">
                <div class="Breadcrumps col-md-12 m-t-10">
                    <ul>
                        <li><a href="{{url('/')}}">Accueil</a></li>
                        <li><a href="{{url('/neuf')}}">Voitures neuves</a></li>
                        <li><a href="{{url('/neuf')}}/{{$brand->slug}}">{{$brand->name}}</a></li>
                        <li><a href="{{url('/neuf')}}/{{$brand->slug}}/{{$model->slug}}">{{$model->name}}</a></li>
                        <li class="active"><a href="{{url('/neuf')}}/{{$brand->slug}}/{{$model->slug}}/{{$version->slug}}">{{$version->name}}</a></li>
                    </ul>
                </div>
            </div>    

            <div class="Sr-results-Vn">

                <h6 class="Title-Lis-bn text-uppercase">Mes critères</h6>
                <div class="Sr-banner-sp">

                    <div class="row">
                        <div class="col-md-12" id="criteria">
                            <div class="Sr-Banner-child">
                                <span>{{ $brand->name }}</span>
                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                            </div>

                            <div class="Sr-Banner-child">
                                <span>{{ $model->name }}</span>
                                <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                            </div>
                            
                            @if(Session::has('GmaxPrice')  and session()->get('GmaxPrice') != 9000000)
                                <div class="Sr-Banner-child">
                                    <span>max: {{ session()->get('GmaxPrice') }} DH </span>
                                    <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('GminPrice')  and session()->get('GminPrice') != 0)
                                <div class="Sr-Banner-child">
                                    <span>min: {{ session()->get('GminPrice') }} DH </span>
                                    <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('maxPuissance')  and session()->get('maxPuissance') != 20)
                                <div class="Sr-Banner-child">
                                    <span>max: {{ session()->get('maxPuissance') }} CH </span>
                                    <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('minPuissance')  and session()->get('minPuissance') != 0)
                                <div class="Sr-Banner-child">
                                    <span>min: {{ session()->get('minPuissance') }} CH </span>
                                    <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('category')  and session()->get('category') != 0)
                                <div class="Sr-Banner-child">
                                    <span>{{ session()->get('categoryName') }}</span>
                                    <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                </div>
                            @endif
                            @if(Session::has('energy') and session()->get('energy') != 0)
                                @if(is_array(session()->get('energieName')))
                                    @foreach(session()->get('energieName') as $energie)
                                    <div class="Sr-Banner-child energie_jq">
                                        <span>{{ $energie }} </span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="Sr-Banner-child energie_jq">
                                        <span>{{ session()->get('energieName') }} </span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endif
                            @endif
                            @if(Session::has('trans') and session()->get('trans') != 0)
                                @if(is_array(session()->get('trans')))
                                    @foreach(session()->get('trans') as $trans)
                                    <div class="Sr-Banner-child trans_jq">
                                        <span>{{ $trans }} </span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="Sr-Banner-child trans_jq">
                                        <span>{{ session()->get('trans') }} </span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endif
                            @endif
                            @if(Session::has('interiorDesign'))
                                @foreach(session()->get('interiorDesign') as $interiorDesign)
                                    <div class="Sr-Banner-child">
                                        <span>Design intérieur: @if($interiorDesign != '0'){{ $interiorDesign }} @else Autres @endif</span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endforeach
                            @endif
                            @if(Session::has('interiorColor'))
                                @foreach(session()->get('interiorColor') as $interiorColor)
                                    <div class="Sr-Banner-child">
                                        <span>Couleur intérieure: @if($interiorColor != '0'){{ $interiorColor }} @else Autres @endif</span>
                                        <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                    </div>
                                @endforeach
                            @endif
                            @if(Session::has('exteriorColor'))
                                @foreach(session()->get('exteriorColor') as $exteriorColor)
                                    
                                        <div class="Sr-Banner-child">
                                            <span>Couleur extérieure: @if($exteriorColor != '0'){{ $exteriorColor }} @else Autres @endif</span>
                                            <a href="{{ url('/')}}" class="close"><i class="zmdi zmdi-close"></i></a>
                                        </div>
                                    
                                @endforeach
                            @endif
                            <a href="{{ url('/neuf')}}" class="Sr-clear" id="clearAll">(tout effacer)</a>

                            <div class="col-md-12 float-right text-right">
                                <a href="{{ url('/search/searchDetailed')}}" class="btn btn-blue float-right"><i class="zmdi zmdi-edit"></i> Modifier ma recherche</a>
                                <a href="{{ url('/login') }}" class="btn btn-blue float-right"><i class="zmdi zmdi-save"></i> Sauvegarder ma recherche</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="row m-t-20">
                <div class="col-md-8">
                    
                    <div class="VN-Results-wrapper">

                        <h1 class="VN-AutoTitle">
                            <span>{{ $brand->name }} {{ $model->name }} </span> - {{ $version->name }} > 2 portes / 3 places
                        </h1>

                        <div class="VN-AutoFooter">
                            <div class="rating float-left">
                                <ul>
                                    <li><i class="zmdi @if($version->rating >= 5) zmdi-star @else zmdi-star-outline @endif"></i></li>
                                    <li><i class="zmdi @if($version->rating >= 10) zmdi-star @else zmdi-star-outline @endif"></i></li>
                                    <li><i class="zmdi @if($version->rating >= 15) zmdi-star @else zmdi-star-outline @endif"></i></li>
                                    <li><i class="zmdi @if($version->rating >= 20) zmdi-star @else zmdi-star-outline @endif"></i></li>
                                    <li><i class="zmdi @if($version->rating >= 25) zmdi-star @else zmdi-star-outline @endif"></i></li>
                                </ul>
                            </div>

                            <div class="VN-avis-action float-left"><a href="">3 avis sur ce véhicule</a></div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="Heroslider HerosliderVN owl-carousel owl-theme overflow m-t-20">
                            @foreach($version->mediasList() as $media)
                                <div class="HeroImg cover item" style="background-image: url('{{ $media->path }}')"></div>
                            @endforeach	
                        </div>

                    </div>

                </div>

                <div class="col-md-4">
                <aside class="Sidebar">

                    <div class="VN-Results-Widget">
                        <div class="Vn-r-w-back">
                        <strong>{{ $version->price}} DH</strong>
                        </div>
                        <ul class="pr-sha">
                            <li>
                                <a href="javascript:window.print()">
                                    <i class="zmdi zmdi-print"></i>
                                    <span>Imprimer</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#shareModal" >
                                    <i class="zmdi zmdi-share"></i>
                                    <span>Partager</span>
                                </a>
                            </li>
                        </ul>

                        <div class="Vn-r-w-action offset-md-1 col-md-10">
                            <a href="#" class="btn btn-blue" data-toggle="modal" data-target="#formulaires" id="formulairesEtre">Etre contacté</a>
                            <a href="#" class="btn btn-blue" data-toggle="modal" data-target="#formulaires" id="formulairesAlert">Créer une alerte</a>
                            <a href="#" class="btn btn-blue" data-toggle="modal" data-target="#formulaires" id="formulairesReserver">Réservez un test-drive</a>
                            <a href="#" class="btn btn-blue" data-toggle="modal" data-target="" >Comparez ce véhicule </a>
                        </div>

                    </div>

                </aside>
                </div>

            </div>

            <div class="Vn-car-feat-wrapper m-t-30">
                <div class="col-md-12">

                    <div class="row">
                    
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Motorisation</h4>
                            <table class="table table-sm">
                                <tbody>
                                    @if(isset($version->engine))
                                    <tr>
                                        <td>Moteur</td>
                                        <td>{{ $version->engine }} cylindres en ligne</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->energie->name))<tr>
                                        <td>Enérgie</td>
                                        <td>{{ $version->energie->name }}</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->cylinder))
                                    <tr>
                                        <td>Cylindrée</td>
                                        <td>{{ $version->cylinder }} cm3</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>Boîte</td>
                                        <td>@if(isset($version->transmission)){{ $version->transmission }} @endif @if(isset($version->reports)), {{ $version->reports }} rapports @endif </td>
                                    </tr>
                                    @if(isset($version->fiscalPower))
                                    <tr>
                                        <td>Puissance fiscale</td>
                                        <td>{{$version->fiscalPower}} cv</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->realPower))
                                    <tr>
                                        <td>Puissance moteur</td>
                                        <td>{{$version->realPower }} ch</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->engineCouple))
                                    <tr>
                                        <td>Couple max</td>
                                        <td>{{$version->engineCouple }} Nm</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </figure>
                        
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Poids et mesure</h4>
                            <table class="table table-sm">
                                <tbody>
                                    @if(isset($version->length) and isset($version->width) and isset($version->height))
                                    <tr>
                                        <td>Dimensions ( L / l / h )</td>
                                        <td>{{$version->length }} / {{$version->width }} / {{$version->height }} m</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->reservoirCapacity))
                                    <tr>
                                        <td>Réservoir</td>
                                        <td>{{$version->reservoirCapacity }} L</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->unloadedWeight) and isset($version->loadedWeight))
                                    <tr>
                                        <td>Poids à vide/en charge</td>
                                        <td>{{$version->unloadedWeight }} kg / {{$version->loadedWeight }} kg</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->trunkVolume))
                                    <tr>
                                        <td>Volume coffre</td>
                                        <td>{{$version->trunkVolume }} L</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </figure>
                        
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Performances</h4>
                            <table class="table table-sm">
                                <tbody>
                                    @if(isset($version->maximumSpeed))
                                    <tr>
                                        <td>Vitesse max</td>
                                        <td>{{$version->maximumSpeed }} Km/h </td>
                                    </tr>
                                    @endif
                                    @if(isset($version->acceleration))
                                    <tr>
                                        <td>0 à 100 Km/h</td>
                                        <td>{{$version->acceleration }} s</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </figure>
                        
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Consommation</h4>
                            <table class="table table-sm">
                                <tbody>
                                    @if(isset($version->consumptionCity))
                                    <tr>
                                        <td>Urbaine</td>
                                        <td>{{$version->consumptionCity }} L/100km</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->consumptionRoad))
                                    <tr>
                                        <td>Extra urbaine</td>
                                        <td>{{$version->consumptionRoad}} L/100km</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->mixedConsumption))
                                    <tr>
                                        <td>Mixte</td>
                                        <td>{{$version->mixedConsumption }} L/100km</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->emissionCarbonDioxide))
                                    <tr>
                                        <td>Émission de CO2 (g/km)</td>
                                        <td>{{$version->emissionCarbonDioxide }} g/km</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </figure>
                        
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Châssis</h4>
                            <table class="table table-sm">
                                <tbody>
                                    @if(isset($version->turningDiameter))
                                    <tr>
                                        <td>Diamètre de braquage</td>
                                        <td>{{$version->turningDiameter }} m</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->frontTire))
                                    <tr>
                                        <td>Pneus AV</td>
                                        <td>/R{{$version->frontTire }}</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->rearTire))
                                    <tr>
                                        <td>Pneus AR</td>
                                        <td>/R{{$version->rearTire}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </figure>
                        
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Infos complémentaires</h4>
                            <table class="table table-sm tableHalfMiddle">
                                <tbody>
                                    @if(isset($version->numberOfDoors))
                                    <tr>
                                        <td scope="row">Nombre de portes</td>
                                        <td>{{$version->numberOfDoors }}</td>
                                    </tr>
                                    @endif
                                    @if(isset($version->numberOfPlaces))
                                    <tr>
                                        <td scope="row">Nombre de places</td>
                                        <td>{{$version->numberOfPlaces }}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </figure>
                        
                        <figure class="Vn-car-feat-table col-md-6">
                            <h4 class="Vn-car-feat-title">Infos complémentaires</h4>
                            <a href="" class="Vn-car-feat-download"><i class="zmdi zmdi-download"></i> Télécharger la brochure</a>
                            <a href="" class="Vn-car-feat-download"><i class="zmdi zmdi-download"></i> Télécharger le tarif</a>
                        </figure>

                    </div>

                </div>
            </div>
	
		</div>
	</div>	
</section> 

<section class="Vn-footer-action">
	<div class="container">
		<div class="row">
			<div class="col"><a href="{{url('/neuf')}}/{{$brand->slug}}/{{$model->slug}}" class="btn btn-blue">Voir les autres versions</a></div>
			<div class="col"><a href="" class="btn btn-blue">Avis des automobilistes</a></div>
			<div class="col"><a href="" class="btn btn-blue">Tester ce véhicule</a></div>
			<div class="col"><a href="" class="btn btn-blue">Assurer ce véhicule</a></div>
		</div>
	</div>
</section>

<section class="Ofse">
	
	<div class="container">
        <div class="section-block Ofse-yellow ">
            
            <div class="row">
                <div class="SectionTitle text-center col-md-12">
                    <h3 class="TheChild TextBlack">Offres & Services</h3>
                </div>
            </div>

            <div class="Of-ico-container">
            <div class="row">
                <div class="Of-ico-items owl-carousel owl-theme col-md-10 offset-md-1">
                    
                    <div class="Of-Child item">
                        <h5>Crédit Auto</h5>
                        <div><i class="tmb-15"></i></div>
                        <span>Faites votre <br />simulation</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Assurance Auto</h5>
                        <div><i class="tmb-7"></i></div>
                        <span>Demandez un devis <br />meilleures formules</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Test-drive</h5>
                        <div><i class="tmb-14"></i></div>
                                    <span>Réservez <br/>le véhicule de votre choix</span>
                    </div>

                    <div class="Of-Child item">
                                    <h5>Voiture neuve
                                        <small>Au meilleur prix</small>
                                    </h5>
                        <div><i class="tmb-3"></i></div>
                        <span>Demandez un devis</span>
                    </div>

                    <div class="Of-Child item">
                                    <h5>Voiture neuve
                                        <small>Au meilleur prix</small>
                                    </h5>
                        <div><i class="tmb-1"></i></div>
                        <span>Réserves votre voiture avec chauffeur</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Vignette</small></h5>
                        <div><i class="tmb-credit-card"></i></div>
                        <span>Payez votre vignette en ligne</span>
                    </div>	

                    <div class="Of-Child item">
                        <h5>Expertise auto</small></h5>
                        <div><i class="tmb-4"></i></div>
                        <span>Réservez votre expertise en ligne</span>
                    </div>	

                    <div class="Of-Child item">
                        <h5>Car sharing</small></h5>
                        <div><i class="tmb-ico2"></i></div>
                        <span>Trouvez un partenaire covoiturage</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Dédouanement</small></h5>
                        <div><i class="tmb-8"></i></div>
                        <span>Dédouanez votre voiture</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Ma route</small></h5>
                        <div><i class="tmb-9"></i></div>
                        <span></span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Pérmis de conduire</small></h5>
                        <div><i class="tmb-ico4"></i></div>
                        <span>Tout savoir sur le pérmis de conduite marocain</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Services premium</small></h5>
                        <div><i class="tmb-ico1"></i></div>
                        <span>Découvrez toutes nos prestations premium</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>CNPAC</small></h5>
                        <div><i class="tmb-ico3"></i></div>
                        <span>Tout savoir sur la sécurité routière au Maroc</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Infractions</small></h5>
                        <div><i class="tmb-12"></i></div>
                        <span>Consultez vos infractions en ligne</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Adm</small></h5>
                        <div><i class="tmb-13"></i></div>
                        <span>Infos utiles autoroute du maroc</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Pneus auto/moto</small></h5>
                        <div><i class="tmb-wheel"></i></div>
                        <span>Demandez un devis profitez des meilleurs prix</span>
                    </div>

                    <div class="Of-Child item">
                        <h5>Contrôle technique</small></h5>
                        <div><i class="tmb-11"></i></div>
                        <span>Infos utiles</span>
                    </div>


                    <div class="Of-Child item">
                        <h5>Taxi</small></h5>
                        <div><i class="tmb-frontal-taxi-cab"></i></div>
                        <span>Réservez en ligne</span>
                    </div>

                </div>
            </div>
            </div>

        </div>
	</div>
	
</section>

<section class="Ads">
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href=""><img src="{{ asset('/img/corbeille/ads-3.jpg') }}"></a>
            </div>
        </div>
	</div>
</section>

<section class="Vn-Cr-bottom-section m-t-10">
	<div class="container">		
		<div class="row m-b-20">
			<div class="col-md-6 BlockCr-Parent">
				<div class="WhiteSection">
				<div class="BlockCr-Box">
					<h5 class="BlockCr-title">les bonnes affaires</h5>
					<div class="Offre-spe col-md-10 offset-md-1 owl-carousel owl-theme m-t-20">
                        @foreach ($promotions as $promotion)
                            <div class="CP-Box text-center">
                                <a href="{{url('/neuf')}}/{{$promotion->modele->brand->slug}}/{{$promotion->modele->slug}}/{{$promotion->slug}}">
                                    <div class="CP-Box-Thumb">
                                        <img src="{{$promotion->image}}" alt="{{$promotion->name}}">
                                    </div>
                                    <div class="CP-Box-Body">
                                        <h5 class="CP-Box-Title">{{$promotion->name}}</h5>
                                        <span class="CP-des">{{$promotion->promotionLast[0]->description}}</span>
                                        <span class="CD-Prox-span">Jusqu'à <strong>-{{$promotion->promotionLast[0]->percentage}}</strong></span>
                                    </div>
                                </a>
                            </div>
                        @endforeach  
					</div>
				</div>
				</div>
			</div>

			<div class="col-md-6 BlockCr-Parent">
				<div class="WhiteSection">
				<div class="BlockCr-Box">
					<h5 class="BlockCr-title">Nos recommandations</h5>
					<div class="Offre-spe col-md-10 offset-md-1 owl-carousel owl-theme m-t-10">
                        @foreach ($recommendations as $recommendation)
						<div class="CP-Box text-center">
							<div class="CP-Box-Thumb">
								<a href="{{url('/neuf')}}/{{$brand->slug}}/{{$model->slug}}/{{$recommendation->slug}}"><img src="{{ $recommendation->image }}" alt="{{ $recommendation->name }}"></a>
							</div>
							<div class="CP-Box-Body">
								<a href=""><h5 class="CP-Box-Title">{{ $recommendation->modele->brand->name }}</h5></a>
								<span class="CP-Sub-Title">{{ $recommendation->modele->name }}</span>
								<span class="CD-Price">à partir  de {{ $recommendation->price }} DH</span>
							</div>
						</div>
                        @endforeach
					</div>
				</div>
				</div>
			</div>

			<div class="col-md-6 BlockCr-Parent">
				<div class="WhiteSection">
                    <div class="BlockCr-Box overflow">
                        <h5 class="BlockCr-title">Actus & Essais <small>toutes les infos sur {{ $brand->name }}</small></h5>
                        <div class="Offre-spe Vn-Actu-Carousel owl-carousel owl-theme m-t-20">
                            @foreach($articles as $article)
                            <article class="PostBox item">
                                <div class="PostThumbnail">
                                    <a href="{{url('/articles')}}/{{$article->slug}}-{{$article->id}}">
                                        <div class="cover"
                                            style="background-image: url('{{$article->cover }}')"></div>
                                    </a>
                                    <div class="m-t-20">
                                        <h1 class="VN-AutoTitle"><span>{{ $article->title}}</span></h1>
                                    </div>
                                </div>
                            </article>
                            @endforeach
                        </div>    
                        <a href="" class="Pn-more TextBlack float-right">Consulter toutes les Actus <i class="zmdi zmdi-caret-right-circle"></i></a>
                    </div>
				</div>
			</div>

			<div class="col-md-6 BlockCr-Parent">
				<div class="WhiteSection">
                    <div class="BlockCr-Box overflow">
                        <h5 class="BlockCr-title">Video <small>toutes les vidéos sur {{ $brand->name }}</small></h5>
                        <div class="Offre-spe Vn-Actu-Carousel owl-carousel owl-theme m-t-20">

                            @foreach ($videos as $video)
                            <div class="PostBox item">
                                <div class="PostThumbnail Ads-video">
                                    <a href="" class="Ads-video-action transform-both"><i class="zmdi zmdi-play-circle-outline"></i></a>
                                    <img src="{{ $video->image }}">
                                </div>
                                <div class="m-t-20">
                                <h1 class="VN-AutoTitle"><span>{{ $video->title}}</span></h1> 
                                </div>
                            </div>  
                            @endforeach
                        </div>
                        <a href="{{url('/videos')}}" class="Pn-more TextBlack float-right">Consulter toutes les vidéos <i class="zmdi zmdi-caret-right-circle"></i></a>
                    </div>
				</div>
			</div>

			<div class="col-md-6 BlockCr-Parent">
				<div class="WhiteSection">
                    <div class="BlockCr-Box overflow">
                        <h5 class="BlockCr-title">Video</h5>
                        <p class="BlockCr-title-small m-t-10"><strong>Vue d'ensemble des tests de voitures et des crash-tests</strong><br />
                            Les tests de voiture contiennent une foule d'indications utiles sur la consommation de carburant, les émissions de gaz d'échappement,</p>

                        <div class="Cr-small-form m-t-10">
                            <label>Thème</label>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <select class="form-control">
                                        <option>Testes voitures</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <select class="form-control">
                                        <option>Crash-tests</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                        <option>--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-10">
                            <div class="Ads-video">
                                <img src="{{ asset('/img/video-thumb/ads-video.jpg') }}">
                            </div>
                        </div>
                    </div>
				</div>
            </div>
            
			<div class="col-md-6 BlockCr-Parent">
				<div class="WhiteSection relative">
                    <div class="BlockCr-Box overflow">
                        <h6 class="text-uppercase transform-both title-none">Pub</h6>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>


<div id="confirmationContact" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="zmdi zmdi-check"></i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4>Félicitations!</h4>	
				<p>Votre demande a bien été envoyée. Vous serez contacté très prochainement!</p>
				<button class="btn btn-blue" style="text-align:unset;" data-dismiss="modal"><span>Fermer</span> </button>
			</div>
		</div>
	</div>
</div>  

<div id="confirmationAlerte" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="zmdi zmdi-check"></i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4>Félicitations!</h4>	
				<p>Vous avez souhaité être alerté par TOMOBILE360.COM concernant le véhicule : {{ $version->name }}</p>
				<button class="btn btn-blue" style="text-align:unset;" data-dismiss="modal"><span>Fermer</span> </button>
			</div>
		</div>
	</div>
</div> 

<div id="confirmationTestDrive" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="zmdi zmdi-check"></i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4>Félicitations!</h4>	
				<p>Votre demande a bien été envoyée. Vous serez contacté très prochainement!</p>
				<button class="btn btn-blue" style="text-align:unset;" data-dismiss="modal"><span>Fermer</span> </button>
			</div>
		</div>
	</div>
</div>  

<div class="modal fade" id="formulaires" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4  id="exampleModalLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formulairesform">
                    @csrf   
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" required name="nom" placeholder="Nom">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" required name="prenom" placeholder="Prénom">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" required name="email" placeholder="Email">
                        </div>
                        <input type="hidden" name="vehicleId" value="{{ $version->id }}"/>
                        <input type="hidden" id="formType" name="type" value=""/>
                    </div>
                    </br>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-blue" data-toggle="modal" id="envoyer" disabled="">Envoyer</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
$('#formulairesEtre').click(function(){
    $('#formType').val('etreconnecte')
    $('#exampleModalLabel').text('Etre contacté')
})
$('#formulairesAlert').click(function(){
    $('#formType').val('alerte')
    $('#exampleModalLabel').text('Alerte')
})
$('#formulairesReserver').click(function(){
    $('#formType').val('test')
    $('#exampleModalLabel').text('Réservez un test-drive')
})
$('#formulairesform input').change(function () {
    var requiredInputs = $('#formulairesform').find('input[required]');
    isDis = false;
    $.each(requiredInputs, function(){
        if( !$(this).val()) {
            isDis = true;
        }
    }); 
    if(isDis)$('#envoyer').attr('disabled','disabled');
    else $('#envoyer').removeAttr('disabled')

   
});
$('#envoyer').click(function(){
    $('#formulaires').hide()
    $.ajax({
        type: "POST",
        url: '/api/vehicles/formulaires',
        data: $('#formulairesform').serialize(),
        success: function (result) {
            console.log(result);
            
        }
    });
})
</script>
@endsection